// Chipper KiCAD symbol/footprint/3Dmodel generator
// Formula Engine for Chipper
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdetail.h"
#include "chippackage.h"
#include "chipfile.h"
#include "chipvariables.h"
#include "chipper.h"
#include "chipvariant.h"
#include "formula.h"
#include "templatepool.h"
#include "templates.h"

#include <ELAM/IntEngine>
#include <ELAM/FloatEngine>
#include <ELAM/StringEngine>
#include <ELAM/ArrayEngine>
#include <ELAM/BoolEngine>

#include <QUuid>
#include <QDateTime>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QCryptographicHash>

using namespace ELAM;

class Formula::Private
{
public:
    ChipData*chip=nullptr;
    ChipVariant variant;
    ChipPackage package;
    ChipFileTarget target;
    TemplateData tempdata;
    TemplateSymbol tsymbol;
    TemplateFootprint tfprint;
    Template3DModel tmodel;

    TemplateFile& tfile();
};

DEFINE_DPTR(Formula);

Formula::Formula(QObject*parent)
:Engine(parent)
{
    Engine::configureReflection(*this);
    IntEngine::configureIntEngine(*this);
    FloatEngine::configureFloatEngine(*this);
    StringEngine::configureStringEngine(*this);
    ArrayEngine::configureArrayEngine(*this);
    BoolEngine::configureBoolEngine(*this);
    BoolEngine::configureLogicEngine(*this);
    configure();
}

QVariant Formula::uhashFunc(const QList<QVariant>&par,Engine&en)
{
    auto*fen=qobject_cast<Formula*>(&en);
    if(fen==nullptr)
        return ELAM::Exception("Wrong formula engine type.");
    QStringList pars;
    for(const auto&p:par)pars.append(p.toString());
    return fen->uhash(pars);
}

QString Formula::uhash(const QStringList&par) const
{
    QCryptographicHash hash(QCryptographicHash::Sha1);
    //pre-init
    if(d->target.isValid())hash.addData(d->target.uid().toLatin1());
    if(d->package.isValid())hash.addData(d->package.uid().toLatin1());
    if(d->variant.isValid())hash.addData(d->variant.uid().toLatin1());
    //process parameters
    for(const auto&p:par)hash.addData(p.toLatin1());
    //run something like RFC4122
    auto res=hash.result();
    res.truncate(16);
    res[6]=(res[6]&0x0f)|0x50; //version 5: SHA-1 Name based
    res[8]=(res[8]&0x3f)|0x80; //fixed bits
    //convert to UUID string
    return QUuid::fromRfc4122(res).toString(QUuid::WithoutBraces);
}

static QVariant stringEscape(const QList<QVariant>&arg,Engine&)
{
        if(arg.size()<1 || arg.size()>3)return ELAM::Exception("Expecting 1-3 arguments: string, quote-char, escape-char");
        if(!arg[0].canConvert<QString>())return ELAM::Exception("Expecting string argument");
        QString str=arg[0].toString();
        QString quote =arg.size()>1 ? arg[1].toString() : "\"";
        QString escape=arg.size()>1 ? arg[1].toString() : "\\";
        return quote+str.replace(escape,escape+escape).replace(quote,escape+quote)+quote;
}


#define FINFO \
    if(p.size()!=1)return QVariant::fromValue(ELAM::Exception("Expecting one file name argument!"));\
    if(!p[0].canConvert<QString>())return QVariant::fromValue(ELAM::Exception("Expecting one file name (string) argument!"));\
    return QFileInfo(p[0].toString())

void Formula::configure()
{
    //NOTE: if you change anything here - don't forget to update FormulaExtraFunctions.html and FormulaVariables.html!!
    //basic string generators
    setFunction("uuid",[](const QList<QVariant>&,Engine&)->QVariant{return QUuid::createUuid().toString(QUuid::WithoutBraces);});
    setFunction("uhash",&uhashFunc);
    setConstant("brace","{");
    setConstant("dbrace","{{");
    setFunction("decNow",[](const QList<QVariant>&,Engine&)->QVariant{return QString::number(QDateTime::currentDateTime().toSecsSinceEpoch());});
    setFunction("hexNow",[](const QList<QVariant>&,Engine&)->QVariant{return QString::number(QDateTime::currentDateTime().toSecsSinceEpoch(),16).toUpper();});
    setFunction("now",[](const QList<QVariant>&,Engine&)->QVariant{return QDateTime::currentDateTime().toString(Qt::ISODate);});
    setFunction("metric",[](const QList<QVariant>&arg,Engine&)->QVariant{
        if(arg.size()!=1)return ELAM::Exception("Expecting one argument");
        if(arg[0].canConvert<qreal>())return QString::number(arg[0].toReal(),'f',2);
        else return ELAM::Exception("Expecting float argument");
    });
    setFunction("strEscape",&stringEscape);
    //file name operations
    setFunction("fileBaseName",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.baseName();});
    setFunction("fileCompleteBaseName",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.completeBaseName();});
    setFunction("fileSuffix",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.suffix();});
    setFunction("fileCompleteSuffix",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.completeSuffix();});
    setFunction("fileName",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.fileName();});
    setFunction("fileDirName",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.dir().path();});
    setFunction("fileAbsolutePath",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.absoluteFilePath();});
    setFunction("fileAbsoluteDir",[](const QList<QVariant>&p,Engine&)->QVariant{FINFO.absoluteDir().path();});
    //constants
    setConstant("inch",(qreal)25.4);
    setConstant("mil",(qreal)0.0254);
    setConstant("mm",(qreal)1);
    setConstant("cm",(qreal)10);
    setConstant("brace","{");
    setConstant("dbrace","{{");
    setConstant("symbolDir",absoluteDefaultPath(ChipperPath::Symbol));
    setConstant("symbolVarName",envNameForPath(ChipperPath::Symbol));
    setConstant("footprintDir",absoluteDefaultPath(ChipperPath::Footprint));
    setConstant("footprintVarName",envNameForPath(ChipperPath::Footprint));
    setConstant("modelDir",absoluteDefaultPath(ChipperPath::Model3D));
    setConstant("modelVarName",envNameForPath(ChipperPath::Model3D));
    setConstant("rootDir",absoluteDefaultPath(ChipperPath::Root));
    setConstant("rootVarName",envNameForPath(ChipperPath::Root));
}

void Formula::setChip(Chip::ChipData*chip)
{
    d->chip=chip;
    setVariable("chip",QVariant::fromValue(chip));
    setConstant("manufacturer",d->chip->metaData().manufacturer());
    setConstant("manufacturerFileName",name2path(d->chip->metaData().manufacturer()));
    setConstant("chipType",d->chip->metaData().chipType());
    setConstant("chipTypeFileName",name2path(d->chip->metaData().chipType()));
    setConstant("author",d->chip->metaData().author());
}

void Formula::setVariant(const Chip::ChipVariant&var)
{
    d->variant=var;
    setChip(const_cast<ChipData*>(var.chip()));
    setVariable("variant",QVariant::fromValue(var));
    setConstant("variantName",d->variant.name());
    setConstant("variantFileName",name2path(d->variant.name()));
}

void Formula::setPackage(const Chip::ChipPackage&pck)
{
    d->package=pck;
    d->tempdata=pck.templateRef();
    setVariant(pck.variant());
    setVariable("package",QVariant::fromValue(pck));
    setConstant("packageName",d->package.displayName());
    setConstant("packageFileName",name2path(d->package.displayName()));
    setConstant("templateName",d->package.templateRef().title());
    setConstant("templateFileName",name2path(d->package.templateRef().title()));
    setConstant("templateDir",d->tempdata.absolutePathName());
}

void Formula::setTarget(const Chip::ChipFileTarget&tgt)
{
    d->target=tgt;
    setPackage(tgt.package());
    //chip hierarchy objects
    setVariable("target",QVariant::fromValue(tgt));
    if(tgt.isSymbol()){
        setVariable("symbol",QVariant::fromValue(tgt.toSymbol()));
        setVariable("tsymbol",QVariant::fromValue(d->tsymbol=d->tempdata.symbol(tgt.templateId())));
    }
    if(tgt.isFootprint()){
        setVariable("footprint",QVariant::fromValue(tgt.toFootprint()));
        setVariable("tfootprint",QVariant::fromValue(d->tfprint=d->tempdata.footprint(tgt.templateId())));
    }
    if(tgt.is3DModel()){
        setVariable("model",QVariant::fromValue(tgt.toModel()));
        setVariable("tmodel",QVariant::fromValue(d->tmodel=d->tempdata.model(tgt.templateId())));
    }
    setConstant("targetName",d->target.name());
    setConstant("targetFileName",name2path(d->target.name()));
    setConstant("generatorId",d->tfile().generator());
    setConstant("inputFilePath",d->tempdata.absolutePathName()+"/"+d->tfile().fileName());
    setConstant("inputFileRoot",QFileInfo(d->tfile().fileName()).fileName());
}

void Formula::reset()
{
    //reset chip data
    if(d->target.isValid())setTarget(d->target);
    else if(d->package.isValid())setPackage(d->package);
    else if(d->variant.isValid())setVariant(d->variant);
    else if(d->chip)setChip(d->chip);
    //reset calculated values
    reCalculate();
}

void Formula::reCalculate()
{
    //calculate on package level
    if(d->package.isValid())calculate(d->package.variables());
    //calculate on target file level
    if(d->target.isValid())calculate(d->target.variables());
}

void Formula::calculate(const ChipVariables&cv)
{
    auto tvar=cv.templateVariables();
    for(int nv=0;nv<tvar.numVariables();nv++){
        auto tv=tvar.variable(nv);
        //get configured value
        if(cv.hasVariable(tv.id())){
            QString val=cv.variableValue(tv.id()).trimmed();
            if(!val.isEmpty()){
                switch(tv.variableType()){
                    case TemplateVariables::VarType::Int:
                        setVariable(tv.id(),val.toLongLong());
                        continue;
                    case TemplateVariables::VarType::String:
                        setVariable(tv.id(),val);
                        continue;
                    default:
                        setVariable(tv.id(),val.toDouble());
                        continue;
                }
            }
        }
        //get default value
        setVariable(tv.id(),evaluate(tv.defaultValue()));
    }
}

TemplateFile & Formula::Private::tfile()
{
    if(tsymbol.isValid())return tsymbol;
    if(tfprint.isValid())return tfprint;
    return tmodel;
}

ChipData * Formula::chip()
{
    return d->chip;
}

ChipVariant Formula::variant()
{
    return d->variant;
}

ChipPackage Formula::package()
{
    return d->package;
}

ChipFileTarget Formula::target()
{
    return d->target;
}

TemplateData Formula::templateData()
{
    return d->tempdata;
}

TemplateSymbol Formula::templateSymbol()
{
    return d->tsymbol;
}

TemplateFootprint Formula::templateFootprint()
{
    return d->tfprint;
}

Template3DModel Formula::template3DModel()
{
    return d->tmodel;
}

#include "moc_formula.cpp"
