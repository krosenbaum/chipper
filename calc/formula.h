// Chipper KiCAD symbol/footprint/3Dmodel generator
// Formula Engine for Chipper
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <ELAM/Engine>
#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip {
class ChipData;
class ChipVariant;
class ChipPackage;
class ChipFileTarget;
class ChipVariables;
}

namespace Template {
class TemplateData;
class Template3DModel;
class TemplateFootprint;
class TemplateSymbol;
}

namespace Generate {

///helper class for Chipper internal use of ELAM formula engine;
///auto-configures the engine to work with chipper internal values
class CHIPPER_EXPORT Formula:public ELAM::Engine
{
    Q_OBJECT
    DECLARE_DPTRX(d,CHIPPER_EXPORT);
public:
    ///instantiates the engine with basic functions needed for Chipper
    explicit Formula(QObject*parent=nullptr);

    ///sets the chip this represents
    void setChip(Chip::ChipData*);
    ///sets the variant this represents (implicitly sets the chip)
    void setVariant(const Chip::ChipVariant&);
    ///sets the package this represents (implicitly sets the variant)
    void setPackage(const Chip::ChipPackage&);
    ///sets the target file (symbol/footprint/model) this represents (implicitly sets the package)
    void setTarget(const Chip::ChipFileTarget&);

    ///reloads all variables from chip/variant/package/target and recalculates variables
    void reset();
    ///recalculates variables without reloading top variables
    void reCalculate();

    ///returns the chip represented by this engine
    Chip::ChipData*chip();
    ///returns the variant represented by this engine
    Chip::ChipVariant variant();
    ///returns the package represented by this engine
    Chip::ChipPackage package();
    ///returns the target file represented by this engine
    Chip::ChipFileTarget target();

    ///returns the template used by the package of this engine
    Template::TemplateData templateData();
    ///if this represents a symbol: returns the template symbol
    Template::TemplateSymbol templateSymbol();
    ///if this represents a footprint: returns the template footprint
    Template::TemplateFootprint templateFootprint();
    ///if this represents a model: returns the template model
    Template::Template3DModel template3DModel();

private:
    //helper for constructor
    void configure();
    //helper for reCalculate()
    void calculate(const Chip::ChipVariables&);
    //helper for uhash function
    QString uhash(const QStringList&)const;
    //engine side of uhash
    static QVariant uhashFunc(const QList<QVariant>&par,Engine&en);
};


//end of namespace
}}
using namespace Chipper::Generate;
