// Chipper KiCAD symbol/footprint/3Dmodel generator
// Environment Variable Scanner
//
// !! This file must compile with and without GUI classes !!
// !! This file may only rely on other files in this directory !!
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "kicadenv.h"
#include "chipper.h"

#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QProcessEnvironment>
#include <QSettings>
#include <QStandardPaths>


KiCadEnvironment::KiCadEnvironment()
{
    scanEnv();
    scanSelf();
    //get search directories
    // see also https://forum.kicad.info/t/where-are-the-configuration-files-settings-library-tables/17701
    QStringList base=
        QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation)+
        QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    for(auto p:base)msearch.append(p+"/kicad");

    //scan search directories
    for(auto p:msearch){
        QFileInfo fi(p);
        if(!fi.exists()||!fi.isDir())continue;
        scanDirRec(p);
    }
}

KiCadEnvironment& KiCadEnvironment::instance()
{
    static KiCadEnvironment env;
    return env;
}

QStringList KiCadEnvironment::envPathes()const
{
    QStringList r;
    for(const auto&e:menv)
        r<<e.path;
    return r;
}

QString KiCadEnvironment::envName(QString path)const
{
    for(const auto&e:menv)
        if(e.path==path)
            return e.name;
    return QString();
}

QString KiCadEnvironment::envVersion(QString path)const
{
    for(const auto&e:menv)
        if(e.path==path)
            return e.version;
    return QString();
}

bool KiCadEnvironment::isKiCad6(QString path) const
{
    return path.endsWith(".json");
}

QMap<QString,QString> KiCadEnvironment::environment(QString path)const
{
    for(const auto&e:menv)
        if(e.path==path)
            return e.env;
    return QMap<QString,QString>();
}

void KiCadEnvironment::scanEnv()
{
    auto env=QProcessEnvironment::systemEnvironment();
    QMap<QString,QString>map;
    for(auto k:env.keys()){
        map.insert(k,env.value(k));
        if(k=="KICAD_CONFIG_HOME")
            msearch.append(env.value(k));
        else if(k.toLower()=="appdata")
            msearch.append(env.value(k)+"/kicad");
    }
    addMap(systemEnvPath(),QString(),QCoreApplication::translate("KiCadEnvironment","System"),map);
}

void KiCadEnvironment::scanSelf()
{
    QMap<QString,QString>map;
    map.insert("__prefix",defaultPath(ChipperPath::EnvPrefix));
    map.insert(envNameForPath(ChipperPath::Root),absoluteDefaultPath(ChipperPath::Root));
    map.insert(envNameForPath(ChipperPath::Symbol),absoluteDefaultPath(ChipperPath::Symbol));
    map.insert(envNameForPath(ChipperPath::Footprint),absoluteDefaultPath(ChipperPath::Footprint));
    map.insert(envNameForPath(ChipperPath::Model3D),absoluteDefaultPath(ChipperPath::Model3D));
    map.insert(envNameForPath(ChipperPath::Template),absoluteDefaultPath(ChipperPath::Template));
    addMap(chipperEnvPath(),QString(),QCoreApplication::translate("KiCadEnvironment","Chipper"),map);
}

void KiCadEnvironment::scanIni(QString fn,QString nm)
{
    QMap<QString,QString>map;
    QSettings ini(fn,QSettings::IniFormat);
    ini.beginGroup("EnvironmentVariables");
    for(auto k:ini.childKeys())
        map.insert(k,ini.value(k).toString());
    addMap(fn,nm,QCoreApplication::translate("KiCadEnvironment","KiCAD 5: %1").arg(nm),map);
}

bool KiCadEnvironment::changeIni(QString path, QMap<QString, QString>add, QStringList del)
{
    //backup old file
    QFile(path+".vt_bak").remove();
    QFile(path).copy(path+".vt_bak");
    //open and modify
    QSettings ini(path,QSettings::IniFormat);
    ini.beginGroup("EnvironmentVariables");
    for(auto k:add.keys())
        ini.setValue(k,add[k]);
    for(auto k:del)
        ini.remove(k);
    //done
    ini.sync();
    return true;
}

void KiCadEnvironment::scanJson(QString fn,QString nm)
{
    QFile fd(fn);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Unable to open KiCAD JSON config file"<<fn;
        return;
    }
    QJsonParseError err;
    QJsonDocument doc=QJsonDocument::fromJson(fd.readAll(),&err);
    fd.close();
    if(doc.isNull()){
        qDebug()<<"Unable to parse KiCAD JSON config file"<<fn<<":"<<err.errorString();
        return;
    }
    if(!doc.isObject()){
        qDebug()<<"Unexpected JSON format in"<<fn;
        return;
    }
    auto env=doc.object().value("environment").toObject().value("vars").toObject();
    QMap<QString,QString>map;
    for(auto k:env.keys())
        map.insert(k,env.value(k).toString());
    addMap(fn,nm,QCoreApplication::translate("KiCadEnvironment","KiCAD 6: %1").arg(nm),map);
}

bool KiCadEnvironment::changeJson(QString path, QMap<QString, QString>add, QStringList del)
{
    //backup old file
    QFile(path+".vt_bak").remove();
    QFile(path).copy(path+".vt_bak");
    //open and navigate
    QFile fd(path);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Unable to open KiCAD JSON config file"<<path;
        return false;
    }
    QJsonParseError err;
    QJsonDocument doc=QJsonDocument::fromJson(fd.readAll(),&err);
    fd.close();
    if(doc.isNull()){
        qDebug()<<"Unable to parse KiCAD JSON config file"<<path<<":"<<err.errorString();
        return false;
    }
    if(!doc.isObject()){
        qDebug()<<"Unexpected JSON format in"<<path;
        return false;
    }
    auto root=doc.object();
    auto envobj=root.value("environment").toObject();
    auto env=envobj.value("vars").toObject();
    //modify
    for(auto k:add.keys())
        env.insert(k,QJsonValue(add[k]));
    for(auto k:del)
        env.remove(k);
    //store
    envobj.insert("vars",env);
    root.insert("environment",envobj);
    doc.setObject(root);
    if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        qDebug()<<"Unable to open KiCAD JSON config file for writing"<<path;
        return false;
    }
    fd.write(doc.toJson());
    fd.close();
    return true;
}

void KiCadEnvironment::addMap(const QString&path,const QString&version,const QString&name, const QMap<QString, QString>&map)
{
    EnvData ed;
    ed.path=path;
    ed.name=name;
    ed.version=version;
    ed.env=map;
    menv<<ed;
}

void KiCadEnvironment::scanDirRec(QString dn)
{
    QDir d(dn);
    if(d.exists("kicad_common")){
        QString f=d.absoluteFilePath("kicad_common");
        scanIni(f,d.dirName());
    }else if(d.exists("kicad_common.json")){
        QString f=d.absoluteFilePath("kicad_common.json");
        scanJson(f,d.dirName());
    }
    //recurse
    for(auto sub:d.entryList(QDir::AllDirs|QDir::NoDotAndDotDot))
        scanDirRec(dn+"/"+sub);
}

bool KiCadEnvironment::setVariables(QString path, const QMap<QString, QString>& vars)
{
    //check path
    if(path.isEmpty() || path.startsWith(":/"))return false;
    bool found=false;
    for(const auto&ed:menv)
        if(ed.path==path){
            found=true;
            break;
        }
    if(!found)return false;
    //check version and delegate
    if(isKiCad6(path))
        return changeJson(path,vars,QStringList());
    else
        return changeIni(path,vars,QStringList());
}

bool KiCadEnvironment::deleteVariables(QString path, QStringList vars)
{
    //check path
    if(path.isEmpty() || path.startsWith(":/"))return false;
    bool found=false;
    for(const auto&ed:menv)
        if(ed.path==path){
            found=true;
            break;
        }
    if(!found)return false;
    //check version and delegate
    if(isKiCad6(path))
        return changeJson(path,QMap<QString,QString>(),vars);
    else
        return changeIni(path,QMap<QString,QString>(),vars);
}
