// Chipper KiCAD symbol/footprint/3Dmodel generator
// Environment Variable Scanner
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QMap>
#include <QStringList>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Util {

///Helper singleton class to scan KiCAD configuration files for environment settings.
class CHIPPER_EXPORT KiCadEnvironment
{
    KiCadEnvironment();
    ~KiCadEnvironment(){};

    QStringList msearch;
    struct EnvData{
        QString path,name,version;
        QMap<QString,QString>env;
    };
    QList<EnvData>menv;

    ///internal helper to scan for environment variables
    void scanEnv();
    ///put Chipper configuration into the table
    void scanSelf();
    ///scan a KiCAD 5.x ini config file
    void scanIni(QString,QString);
    ///scan a KiCAD 6.x JSON config file
    void scanJson(QString,QString);
    ///modify old Ini file
    bool changeIni(QString,QMap<QString,QString>,QStringList);
    ///modify new JSON file
    bool changeJson(QString,QMap<QString,QString>,QStringList);
    ///enter configuration values into the store
    void addMap(const QString&,const QString&,const QString&,const QMap<QString,QString>&);
    ///run through a configuration dir (recursively)
    void scanDirRec(QString);
public:
    ///returns the instance of this singleton
    static KiCadEnvironment& instance();

    ///returns all pathes in which configuration was found (includes some pseudo-pathes)
    QStringList envPathes()const;
    ///returns a human readable name for a configuration path
    QString envName(QString path)const;
    ///returns the version number or string of the configuration path
    QString envVersion(QString path)const;
    ///returns the configuration map for a specific path
    QMap<QString,QString> environment(QString path)const;

    ///returns true if the path is a KiCAD 6 (or newer) configuration
    bool isKiCad6(QString path)const;

    ///tries to modify a config file by setting variables (note: does not update the internal model)
    bool setVariables(QString path,const QMap<QString,QString>&vars);
    ///tries to delete variables from a config file (note: does not update the internal model)
    bool deleteVariables(QString path,QStringList vars);

    ///returns the pseudo-path-name for system environment variables
    QString systemEnvPath()const{return ":/system";}
    ///returns the pseudo-path-name for chipper internal settings
    QString chipperEnvPath()const{return ":/chipper";}

    ///returns the system environment variables
    QMap<QString,QString> systemEnv()const{return environment(systemEnvPath());}
    ///returns chipper internal variables
    QMap<QString,QString> chipperEnv()const{return environment(chipperEnvPath());}
};

//end of namespace
}}
using namespace Chipper::Util;
