// Chipper KiCAD symbol/footprint/3Dmodel generator
// main file
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QString>

#ifdef QT_WIDGETS_LIB
# include "../libgui/chipperguiinit.h"
#endif

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Util {

///Type of root path used by chipper for its input and output
enum class ChipperPath {
    ///invalid path request
    None,
    ///absolute root if other pathes are relative, the default is the parent path of the application directory
    Root,
    ///where to put symbol files while generating
    Symbol,
    ///where to put footprint files while generating
    Footprint,
    ///where to put 3D models while generating
    Model3D,
    ///not a path, prefix for environment variables
    EnvPrefix,
    ///where to find template files
    Template
};

///returns the path that is valid for this session, to be used by any code that needs a path root
CHIPPER_EXPORT QString defaultPath(ChipperPath);
///returns the path that is valid for this session, to be used by any code that needs a path root,
///returns an absolute path that does not need to be processed any further
CHIPPER_EXPORT QString absoluteDefaultPath(ChipperPath);
///overrides the path that is valid for this session (non-permanent)
///normally this should only be used inside chipper.cpp while initializing the program
CHIPPER_EXPORT void setDefaultPath(ChipperPath,QString);

///returns the corresponding variable name for a path
CHIPPER_EXPORT QString envNameForPath(ChipperPath);
///returns the corresponding path type for a variable name
CHIPPER_EXPORT ChipperPath chipperPathForEnvName(QString);

///returns the path that is stored in configuration, to be used by settings dialog
CHIPPER_EXPORT QString pathConfig(ChipperPath);
///changes the path that is stored in configuration (needs restart), to be used by settings dialog,
///if the value is empty then the setting is deleted and the programm will fall back to the default
CHIPPER_EXPORT void setPathConfig(ChipperPath,QString);

///\internal
#define _VSTR_(v) "" # v
///\internal
#define _VSTR2_(v) _VSTR_(v)
///version string of this program instance - version number only
#define VERSIONSTR _VSTR2_(VERSION)

///returns detailed version information
CHIPPER_EXPORT QString versionInfo();

///\private auto-kill QApplication when main exits
class CHIPPER_EXPORT __AppKiller
{
public:
    __AppKiller();
    ~__AppKiller();
};

///\private
typedef void (*__chipperGuiInitPtr)(int&,char**);

///\private initialize app object, see below
CHIPPER_EXPORT void __initChipperApp(__chipperGuiInitPtr,int&,char**,bool gui=true);

#ifdef DOXYGEN_RUN

/** Should be called from main() to initialize the application and path config.
 \param argc number of arguments given to main()
 \param argv array of arguments given to main()
 \param gui optional: whether to use a GUI application or a console application object (only exists if the application is compiled with GUI support)
*/
void initChipperApp(int argc,char** argv,bool gui=true);

#else

//helper macro for app initialization
# ifdef QT_WIDGETS_LIB
#  define initChipperApp(...) __AppKiller __ak;__initChipperApp(&__chipperGuiInit,__VA_ARGS__);
# else
#  define initChipperApp(...) __AppKiller __ak;__initChipperApp(nullptr,__VA_ARGS__);
# endif

#endif

///helper: checks the CHIPPERDEBUG environment variable and returns true if it is set to >0
CHIPPER_EXPORT bool isDebugMode();
///can be used to override the debug mode
CHIPPER_EXPORT void setDebugMode(bool);

///log handling
CHIPPER_EXPORT void activateLog(QString filename=QString());
CHIPPER_EXPORT QString logFileName();
CHIPPER_EXPORT bool isLogActive();
CHIPPER_EXPORT void deactivateLog();


///helper: sanitize a name for use as file name
static inline QString name2path(QString name)
{
    QString r;
    static const QString allowed="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+,.%@^_=";
    for(auto c:name)
        if(allowed.contains(c))r+=c;
        else r+='_';
    return r;
}


//end of namespace
}}
using namespace Chipper::Util;
