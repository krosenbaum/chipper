// Chipper KiCAD symbol/footprint/3Dmodel generator
// environment common to all sub-apps
// !! this file must compile with and without GUI classes !!
// !! it must not rely on any other Chipper source files !!
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QFileInfo>
#include <QLibraryInfo>
#include <QMap>
#include <QSettings>
#include <QStringList>
#include <QSysInfo>
#include <QTimer>
#include <QTranslator>
#include <QVariant>

#include "chipper.h"

#include <ELAM/Engine>

namespace Chipper{ namespace Util{

QString versionInfo()
{
    QString r;
    r+=QCoreApplication::translate("Chipper","Chipper version: %1\n").arg(VERSIONSTR);
    r+=QCoreApplication::translate("Chipper","\nCompiled for Qt %1 (%6)\nRunning with Qt %2\nRunning on: %3; kernel %4; CPU %5\n")
        .arg(QT_VERSION_STR)
        .arg(qVersion())
        .arg(QSysInfo::prettyProductName())
        .arg(QSysInfo::kernelVersion())
        .arg(QSysInfo::currentCpuArchitecture())
        .arg(QSysInfo::buildCpuArchitecture())
        ;
    r+=QCoreApplication::translate("Chipper","\nELAM version: %1\n").arg(ELAM::versionInfo());
    //TODO: add GIT info (see issue T109)
    return r;
}

///\internal stores data for defaultPath
static QMap<ChipperPath,QString>dpath;

QString defaultPath(ChipperPath p)
{
    if(dpath.contains(p))return dpath[p];
    else return QString();
}
void setDefaultPath(ChipperPath p,QString s)
{
    dpath.insert(p,s);
}

QString absoluteDefaultPath(ChipperPath p)
{
    const QString rp=QFileInfo(defaultPath(ChipperPath::Root)).absoluteFilePath();
    switch(p){
        case ChipperPath::Root:return rp;
        case ChipperPath::Symbol:
        case ChipperPath::Footprint:
        case ChipperPath::Template:
        case ChipperPath::Model3D:{
            QFileInfo fi(defaultPath(p));
            if(fi.isAbsolute())return fi.absoluteFilePath();
            return QFileInfo(rp+"/"+fi.filePath()).absoluteFilePath();
        }
        default:
            qDebug()<<"Warning: some code called absoluteDefaultPath() with a non-path question. Making an empty threat now.";
            return QString();
    }
}

#define ENVPREFIXKEY "envPrefix"
#define ROOTDIRKEY "rootDir"
#define SYMBOLDIRKEY "symbolDir"
#define FOOTPRINTDIRKEY "footprintDir"
#define MODELDIRKEY "3dmodelDir"
#define TEMPLATEDIRKEY "templateDir"

QString pathConfig(ChipperPath p)
{
    switch(p){
        case ChipperPath::EnvPrefix:return QSettings().value(ENVPREFIXKEY).toString();
        case ChipperPath::Root:return QSettings().value(ROOTDIRKEY).toString();
        case ChipperPath::Symbol:return QSettings().value(SYMBOLDIRKEY).toString();
        case ChipperPath::Footprint:return QSettings().value(FOOTPRINTDIRKEY).toString();
        case ChipperPath::Model3D:return QSettings().value(MODELDIRKEY).toString();
        case ChipperPath::Template:return QSettings().value(TEMPLATEDIRKEY).toString();
        default:
            qDebug()<<"Ooops. Unknown path type config requested.";
            return QString();
    }
}
void setPathConfig(ChipperPath p,QString s)
{
    switch(p){
        case ChipperPath::EnvPrefix:
            if(s.isEmpty())QSettings().remove(ENVPREFIXKEY);
            else QSettings().setValue(ENVPREFIXKEY,QVariant(s));
            break;
        case ChipperPath::Root:
            if(s.isEmpty())QSettings().remove(ROOTDIRKEY);
            else QSettings().setValue(ROOTDIRKEY,QVariant(s));
            break;
        case ChipperPath::Symbol:
            if(s.isEmpty())QSettings().remove(SYMBOLDIRKEY);
            else QSettings().setValue(SYMBOLDIRKEY,QVariant(s));
            break;
        case ChipperPath::Footprint:
            if(s.isEmpty())QSettings().remove(FOOTPRINTDIRKEY);
            else QSettings().setValue(FOOTPRINTDIRKEY,QVariant(s));
            break;
        case ChipperPath::Model3D:
            if(s.isEmpty())QSettings().remove(MODELDIRKEY);
            else QSettings().setValue(MODELDIRKEY,QVariant(s));
            break;
        case ChipperPath::Template:
            if(s.isEmpty())QSettings().remove(TEMPLATEDIRKEY);
            else QSettings().setValue(TEMPLATEDIRKEY,QVariant(s));
            break;
        default:
            qDebug()<<"Ooops. Trying to configure unknown path type.";
            break;
    }
}

#define ENVNAME_ROOT "_ROOT"
#define ENVNAME_SYMB "_SYMBOL_DIR"
#define ENVNAME_FOOT "_FOOTPRINT_DIR"
#define ENVNAME_3DMO "_3DMODEL_DIR"
#define ENVNAME_TEMP "_TEMPLATE_DIR"

QString envNameForPath(ChipperPath p)
{
    switch(p){
        case ChipperPath::Root:return defaultPath(ChipperPath::EnvPrefix)+ENVNAME_ROOT;
        case ChipperPath::Symbol:return defaultPath(ChipperPath::EnvPrefix)+ENVNAME_SYMB;
        case ChipperPath::Footprint:return defaultPath(ChipperPath::EnvPrefix)+ENVNAME_FOOT;
        case ChipperPath::Model3D:return defaultPath(ChipperPath::EnvPrefix)+ENVNAME_3DMO;
        case ChipperPath::Template:return defaultPath(ChipperPath::EnvPrefix)+ENVNAME_TEMP;
        default:return QString();
    }
}

ChipperPath chipperPathForEnvName(QString ename)
{
    const QString epre=defaultPath(ChipperPath::EnvPrefix);
    if(!ename.startsWith(epre))return ChipperPath::None;
    ename=ename.mid(epre.size());
    if(ename==ENVNAME_ROOT)return ChipperPath::Root;
    if(ename==ENVNAME_SYMB)return ChipperPath::Symbol;
    if(ename==ENVNAME_FOOT)return ChipperPath::Footprint;
    if(ename==ENVNAME_3DMO)return ChipperPath::Model3D;
    if(ename==ENVNAME_TEMP)return ChipperPath::Template;
    return ChipperPath::None;
}

static void initDefaultPath()
{
    QString d,ename;
    QSettings s;
    //prefix
    if(!dpath.contains(ChipperPath::EnvPrefix)){
        dpath.insert(ChipperPath::EnvPrefix,s.value(ENVPREFIXKEY,"WIZZARD").toString());
    }
    const QString prefix=dpath[ChipperPath::EnvPrefix];
    //root
    d=QFileInfo(QCoreApplication::applicationDirPath()+"/..").canonicalFilePath();
    d=s.value(ROOTDIRKEY,QVariant(d)).toString();
    ename=prefix+ENVNAME_ROOT;
    if(qEnvironmentVariableIsSet(ename.toUtf8().data()))d=qEnvironmentVariable(ename.toUtf8().data());
    setDefaultPath(ChipperPath::Root,d);
    //symbol
    d=s.value(SYMBOLDIRKEY,"Symbols").toString();
    ename=prefix+ENVNAME_SYMB;
    if(qEnvironmentVariableIsSet(ename.toUtf8().data()))d=qEnvironmentVariable(ename.toUtf8().data());
    setDefaultPath(ChipperPath::Symbol,d);
    //footprint
    d=s.value(FOOTPRINTDIRKEY,"Footprints").toString();
    ename=prefix+ENVNAME_FOOT;
    if(qEnvironmentVariableIsSet(ename.toUtf8().data()))d=qEnvironmentVariable(ename.toUtf8().data());
    setDefaultPath(ChipperPath::Footprint,d);
    //3D models
    d=s.value(MODELDIRKEY,"3dmodels").toString();
    ename=prefix+ENVNAME_3DMO;
    if(qEnvironmentVariableIsSet(ename.toUtf8().data()))d=qEnvironmentVariable(ename.toUtf8().data());
    setDefaultPath(ChipperPath::Model3D,d);
    //templates
    d=QFileInfo(QCoreApplication::applicationDirPath()+"/../Templates").canonicalFilePath();
    d=s.value(TEMPLATEDIRKEY,QVariant(d)).toString();
    ename=prefix+ENVNAME_TEMP;
    if(qEnvironmentVariableIsSet(ename.toUtf8().data()))d=qEnvironmentVariable(ename.toUtf8().data());
    setDefaultPath(ChipperPath::Template,d);
}

static void initLogger();

static void initTranslation(bool gui)
{
    QLocale dloc;
    if(isDebugMode())qDebug()<<"Loading locale"<<dloc.language();
    //helper:
    auto loader=[&](const QString&fn,const QString&app,const QString&dir=QCoreApplication::applicationDirPath()){
        QTranslator *trans;
        //Qt
        trans=new QTranslator(/*qApp*/);
        if(trans->load(dloc,fn,QString(),dir)){
            if(qApp->installTranslator(trans)){
                if(isDebugMode())qDebug().noquote()<<" ...loaded translations for"<<app;
                return;
            }
        }
        if(isDebugMode())qDebug().noquote()<<" ...unable to load translation for"<<app;
        delete trans;
    };
    //Qt:
    const QString qtloc=
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
        QLibraryInfo::path(QLibraryInfo::TranslationsPath);
#else
        QLibraryInfo::location(QLibraryInfo::TranslationsPath);
#endif
    loader("qtbase_","Qt libs",qtloc);
    //Chipper:
    loader("chipper_lib_","Chipper Main");
    if(gui)loader("chipper_gui_","Chipper GUI");
}

void __initChipperApp(__chipperGuiInitPtr ginit,int &ac,char**av,bool gui)
{
    //initialize logger
    initLogger();
    //debug-details?
    if(isDebugMode()){
        qDebug()<<"initializing...";
        qDebug()<<"  args: num="<<ac<<"  parg="<<Qt::hex<<(qptrdiff)av;
        for(int i=0;i<ac;i++)
            qDebug()<<"  arg"<<i<<":"<<av[i];
    }
    //init settings
    QCoreApplication::setApplicationName("Chipper");
    QCoreApplication::setApplicationVersion( VERSIONSTR );
    QCoreApplication::setOrganizationDomain("chipper.wizzard.industries");
    QCoreApplication::setOrganizationName("Wizzard.Industries");
    //instantiate appropriate version of application object
    if(gui && ginit!=nullptr){
        ginit(ac,av);
    }else{
        if(isDebugMode())qDebug()<<"Initialize Chipper Command Line...";
        new QCoreApplication(ac,av);
    }
    if(isDebugMode())
        qDebug()<<"Instantiated qApp of type"<<qApp->metaObject()->className();
    //init i18n
    initTranslation(gui);
    //get default pathes
    initDefaultPath();
}

__AppKiller::__AppKiller()
{
    if(isDebugMode()){
        qDebug()<<"Scheduling auto-destruct of qApp";
        if(qApp==nullptr)
            qDebug()<<"  qApp is NULL";
        else
            qDebug()<<"  qApp exists as"<<qApp->metaObject()->className();
    }
}

__AppKiller::~__AppKiller()
{
    if(qApp)delete qApp;
    if(isDebugMode())
        qDebug()<<"Killing qApp";
}

static bool isdebug=false;
bool isDebugMode()
{
    static bool scanned=false;
    if(!scanned){
        scanned=true;
        auto v=qgetenv("CHIPPERDEBUG");
        if(v.size()>0){
            bool ok;
            isdebug = v.toInt(&ok)>0;
            if(!ok)isdebug=false;
        }
    }
    return isdebug;
}

void setDebugMode(bool d)
{
    isdebug=d;
}

static QString slogFileName=QString("chipper-%1.log").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm"));
static QFile slogFile;
static QtMessageHandler prevhandler=nullptr;

void activateLog(QString filename)
{
    initLogger();
    if(slogFile.isOpen())slogFile.close();
    if(!filename.isEmpty())slogFileName=filename;
    slogFile.setFileName(slogFileName);
    if(!slogFile.open(QIODevice::WriteOnly|QIODevice::Append))
        qWarning()<<"Unable to open log file"<<slogFileName<<"for writing.";
}

QString logFileName(){return slogFileName;}

bool isLogActive(){return slogFile.isOpen();}

void deactivateLog()
{
    if(slogFile.isOpen())slogFile.close();
}

static void chipperLogger(QtMsgType mtype, const QMessageLogContext&ctx, const QString&msg)
{
    if(prevhandler)prevhandler(mtype,ctx,msg);
    if(slogFile.isOpen()){
        QString out=QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz")+": ";
        if(isdebug){
        }
        out+=msg;
        out+="\n";
        slogFile.write(out.toUtf8());
        slogFile.flush();
    }
}

static void initLogger()
{
    static bool isInited=false;
    if(!isInited){
        prevhandler=qInstallMessageHandler(&chipperLogger);
        isInited=true;
    }
}

//end of namespace
}}
