// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for chip files
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Util {

///Automatically disconnects a signal-slot connection when it goes out of scope.
///This variant can not be copied, but can be re-assigned.
class CHIPPER_EXPORT AutoDisconnect
{
    QMetaObject::Connection mcon;
    AutoDisconnect(const AutoDisconnect&)=delete;
    AutoDisconnect& operator=(const AutoDisconnect&)=delete;

public:
    ///creates an auto-disconnector that does not track an actual connection, assign one to use it!
    AutoDisconnect()=default;
    ///creates a new auto-disconnector
    AutoDisconnect(QMetaObject::Connection c):mcon(c){}
    ///disconnects as the object goes out of scope
    ~AutoDisconnect(){if(mcon)QObject::disconnect(mcon);}

    ///forgets the current connection and remembers a new one
    AutoDisconnect& operator=(const QMetaObject::Connection&c){mcon=c;return *this;}

    ///returns the connection
    QMetaObject::Connection connection()const{return mcon;}

    ///forget about the connection, it will not be auto-disconnected
    void forget(){mcon=QMetaObject::Connection();}
};

///Automatically disconnects a signal-slot connection when the last copy of it goes out of scope.
class CHIPPER_EXPORT SharedAutoDisconnect
{
    struct Private
    {
        QMetaObject::Connection mcon;
        QAtomicInt mcnt;
        Private():mcnt(1){}
        Private(const QMetaObject::Connection&c):mcon(c),mcnt(1){}
        Private(const Private&)=delete;
        Private& operator=(const Private&)=delete;
    };
    Private *d=nullptr;
public:
    ///creates an invalid auto-disconnector
    SharedAutoDisconnect()=default;
    ///creates the first copy of the auto-disconnector referencing the connection "c"
    SharedAutoDisconnect(const QMetaObject::Connection&c):d(new Private(c)){}
    ///creates a new copy of the auto-disconnector
    SharedAutoDisconnect(const SharedAutoDisconnect&o)
    :d(o.d)
    {
        d->mcnt.ref();
    }
    ///moves the observation of the connection to a new instance
    SharedAutoDisconnect(SharedAutoDisconnect&&o):d(o.d){o.d=nullptr;}

    ///creates a new copy of the auto-disconnector;
    ///if this is the last instance observing the current connection then the connection is disconnected,
    ///use forget if you want to prevent this
    SharedAutoDisconnect& operator=(const SharedAutoDisconnect&o)
    {
        if(this==&o)return *this;
        if(d==o.d)return *this;
        deref();
        o.d->mcnt.ref();
        d=o.d;
        return *this;
    }
    ///moves the observation of the connection to a new instance;
    ///if this is the last instance observing the current connection then the connection is disconnected,
    ///use forget if you want to prevent this
    SharedAutoDisconnect& operator=(SharedAutoDisconnect&&o)
    {
        if(this==&o)return *this;
        deref();
        d=o.d;
        o.d=nullptr;
        return *this;
    }

    ///decreases the reference count of the observed connection and disconnects it if this is the last observer
    ~SharedAutoDisconnect(){deref();}

    ///decreases the reference count of the observed connection and disconnects it if this is the last observer;
    ///returns true if the connection has been disconnected, false otherwise
    bool deref()
    {
        if(!d)return false;
        if(!d->mcnt.deref()){
            QObject::disconnect(d->mcon);
            delete d;
            d=nullptr;
            return true;
        }
        d=nullptr;
        return false;
    }

    ///forgets about the current connection, if this is the last observer then the connection will not be auto-disconnected
    void forget()
    {
        if(!d)return;
        if(!d->mcnt.deref())
            delete d;
        d=nullptr;
    }
};


//end of namespace
}}
using namespace Chipper::Util;
