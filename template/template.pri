SOURCES += \
    $$PWD/templates.cpp \
    $$PWD/templatefile.cpp \
    $$PWD/templatepins.cpp \
    $$PWD/templatevariables.cpp \
    $$PWD/templatepool.cpp

HEADERS += \
    $$PWD/templates.h \
    $$PWD/templatefile.h \
    $$PWD/templatepins.h \
    $$PWD/templatevariables.h \
    $$PWD/templatepool.h

INCLUDEPATH += $$PWD
