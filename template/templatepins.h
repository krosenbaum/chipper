// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFileInfo;
class QDir;

namespace Chipper { namespace Template {


///represents a pin layout definition in a template
class CHIPPER_EXPORT TemplatePins
{
public:
    ///the layout mode of the pins
    enum class Mode {
        ///pins are laid out in distinct named rows, no repetition (e.g. QFP/QFN, DIP, SOP)
        Rows,
        ///pins are laid out in a repeating grid, rows form a single part of the pattern that repeats until all pins are generated (e.g. BGA)
        Grid
    };

    class FillAlgo;
    ///represents a single step in a Fill-Algorithm (\see FillAlgo)
    class FillStep{
    public:
        FillStep()=default;
        FillStep(const FillStep&)=default;
        FillStep(FillStep&&)=default;
        FillStep& operator=(const FillStep&)=default;
        FillStep& operator=(FillStep&&)=default;

        ///The type of step this represents
        enum class StepType {
            ///pre-sort pins by specific criteria
            Sort,
            ///pre-filter pins by specific criteria
            Filter,
            ///actually assign pins to a row, the formula decides which ones
            Take
        };
        ///Whether to automatically hide pins once they are assigned
        enum class AutoHide{
            ///no automatic hiding, all pins are visible
            None,
            ///hide pins with identical names
            Name,
            ///hide pins with identical function
            Type,
            ///hide all pins
            All
        };
        ///sub-category of StepType::Filter - type of filter to be applied
        enum class FilterBy{
            ///filters by pin type (connected/missing/stub/nc)
            Type,
            ///filters by pin function (power, ground, ...)
            Function,
            ///filters by pin name with simple wildcards
            Name,
            ///filters by pin name with regex
            NameRegex
        };
        ///sub-category of StepType::Sort - sort criteria
        enum class SortBy{
            ///brings pins into the same order they are in the variant pin table
            Table,
            ///sorts them by pin id / pin number (currently not very smart)
            Id,
            ///sorts by name (also not smart)
            Name
        };

        ///returns the step type
        StepType stepType()const{return mtype;}
        ///returns whether the step wants to auto-hide pins
        AutoHide autoHide()const{return mautohide;}
        ///returns the filter criteria
        FilterBy filterBy()const{return mfilter;}
        ///returns the sort criteria
        SortBy sortBy()const{return msort;}

        ///returns the human readable type of the step
        QString stepTypeString()const;
        ///returns the human readable auto-hide setting
        QString autoHideString()const;
        ///returns the human readable filter criteria
        QString filterByString()const;
        ///returns the human readable sort criteria
        QString sortByString()const;

        ///returns the row into which the step wants to take the pins
        QString intoRow()const{return minto;}

        ///returns the name by which to filter
        QString nameFilter()const{return mtext;}
        ///returns the formula for a StepType::Take step
        QString takeFormula()const{return mtext;}
    private:
        friend class FillAlgo;
        FillStep(const QDomElement&);
        StepType mtype=StepType::Sort;
        AutoHide mautohide=AutoHide::None;
        FilterBy mfilter=FilterBy::Function;
        SortBy msort=SortBy::Table;
        QString minto,mtext;
    };
    ///represents an auto-fill algorithm - each algorithm consists of a list of steps that operate on a set of pins, it starts with the full set
    class FillAlgo:public QList<FillStep>{
        QString mid,mname;
        friend class TemplatePins;
        FillAlgo(const QDomElement&);
    public:
        FillAlgo()=default;
        FillAlgo(const FillAlgo&)=default;
        FillAlgo(FillAlgo&&)=default;
        FillAlgo& operator=(const FillAlgo&)=default;
        FillAlgo& operator=(FillAlgo&&)=default;

        ///returns the ID of the algorithm under which it can be retrieved from the template
        QString id()const{return mid;}
        ///returns the human readable name of the algorithm
        QString name()const{return mname;}
    };
    ///represents the definition of a row of pins in the template
    class Row{
        QString mid,mtext,mmap;
        friend class TemplatePins;
        Row(const QDomElement&);
    public:
        Row()=default;
        Row(const Row&)=default;
        Row(Row&&)=default;
        Row& operator=(const Row&)=default;
        Row& operator=(Row&&)=default;

        ///returns the configured ID of the row
        QString id()const{return mid;}
        ///returns the help text (human readable) for this row
        QString text()const{return mtext;}
        ///returns alternate names for the row, to enable easy copying between slightly different targets
        QStringList mapHints()const{return mmap.split(' ',Qt::SkipEmptyParts);}
    };
private:
    friend class TemplateData;

    QString mrepeat="1",mdid,mname;
    Mode mmode=Mode::Rows;
    QStringList mrownames;
    QMap<QString,Row>mrows;
    QMap<QString,FillAlgo>mfill;

    ///called from TemplateData to instantiate the layout
    TemplatePins(const QDomElement&);
public:
    ///instantiates an invalid layout
    TemplatePins()=default;
    ///copies the layout
    TemplatePins(const TemplatePins&)=default;
    ///copies the layout
    TemplatePins(TemplatePins&&)=default;
    ///copies the layout
    TemplatePins& operator=(const TemplatePins&)=default;
    ///copies the layout
    TemplatePins& operator=(TemplatePins&&)=default;

    ///returns the ID of the pin definition
    QString definitionId()const{return mdid;}
    ///returns the human readable name of the definition
    QString definitionName()const{return mname;}
    ///returns the layout generator mode
    Mode mode()const{return mmode;}
    ///returns the mode as a human readable string
    QString modeString()const;
    ///returns the formula to calculate how often the pattern of a Grid layout repeats (currently not functional)
    QString repeatFormula()const{return mrepeat;}

    ///returns the names of all rows
    QStringList rowNames()const{return mrownames;}
    ///returns the row definition by name
    Row rowDefinition(QString id)const{return mrows.value(id);}

    ///returns IDs of fill algorithms
    QStringList fillAlgoIds()const{return mfill.keys();}
    ///returns fill algorithm by ID
    FillAlgo fillAlgo(QString id)const{return mfill.value(id);}
};


//end of namespace
}}
using namespace Chipper::Template;

