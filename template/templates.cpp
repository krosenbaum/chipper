// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>
#include <QStringList>

#include <DomNodeIterator>

#include <functional>

#include "templates.h"
#include "chipper.h"
#include "chipdatapriv.h"


// ////////////////////////////////////
// Template Data

static const QString metafname="chip_meta.xml";

QString TemplateData::metaFileName()
{
    return metafname;
}


TemplateData::TemplateData(QString pname)
:mpath(QFileInfo(pname).absoluteFilePath())
{
    if(pname.isEmpty()){
        qDebug()<<"Creating NULL template.";
        return;
    }
    loadFile(mpath,true);
}

void Chipper::Template::TemplateData::loadFile(QString pname, bool isMain)
{
    QFileInfo fi(pname);
    if(fi.isDir()){
        pname+="/"+metafname;
        fi.setFile(pname);
    }
    QDomDocument doc;
    QFile fd(pname);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Error: unable to read template file"<<fd.fileName();
        return;
    }
    QString err;
    int line,col;
    if(!doc.setContent(&fd,false,&err,&line,&col)){
        qDebug()<<"Error parsing template file"<<fd.fileName()<<":"<<err<<"line"<<line<<"col"<<col;
        return;
    }
    if(isMain)qDebug()<<"Initializing template"<<fd.fileName();
    else qDebug()<<" ...loading sub-template"<<fd.fileName();
    fd.close();
    const QString ver=doc.documentElement().attribute("v");
    if(ver!="1")
        qDebug()<<"Warning: only version 1 of the template file format is understood. This file has version"<<ver;
    for(auto n:doc.documentElement().childNodes()){
        if(!n.isElement())continue;
        QDomElement e=n.toElement();
        const QString tn=e.tagName();
        if(tn=="Description"){
            if(isMain){
                mtitle=e.attribute("title");
                mauthor=e.attribute("author");
                mlicense=e.attribute("license");
                mcopy=e.attribute("copy");
                mreadme=e.attribute("readme");
                mdescr=e.text().trimmed();
            }
        }
        else if(tn=="Symbol")
            msymbols.append(TemplateSymbol(e,fi.dir()));
        else if(tn=="Footprint")
            mfootprints<<TemplateFootprint(e,fi.dir());
        else if(tn=="Model")
            m3dmodels<<Template3DModel(e,fi.dir());
        else if(tn=="Variables")
            mvars.loadTemplate(e);
        else if(tn=="Pins"){
            TemplatePins tp(e);
            if(mpins.contains(tp.definitionId()))
                qDebug()<<"Warning: template in"<<pname<<"contains duplicate pin definition"<<tp.definitionId();
            mpins.insert(tp.definitionId(),tp);
        }else if(tn=="Include"){
            QString rel=e.attribute("path");
            if(rel.trimmed().isEmpty()){
                qDebug()<<"Warning: empty include in"<<pname;
                continue;
            }
            loadFile(fi.dir().absoluteFilePath(rel));
        }else
            qDebug()<<"Template"<<pname<<"contains unknown tag"<<tn<<"on line"<<e.lineNumber()<<"column"<<e.columnNumber();
    }
}

TemplateData::~TemplateData()
{
}

QString TemplateData::absoluteReadmePath() const
{
    return QFileInfo(mpath+"/"+mreadme).absoluteFilePath();
}

QString TemplateData::absolutePathName() const
{
    return QFileInfo(mpath).absoluteFilePath();
}

QStringList TemplateData::symbolIds() const
{
    QStringList r;
    for(const auto&s:msymbols)
        r<<s.id();
    return r;
}

TemplateSymbol TemplateData::symbol(QString id) const
{
    for(const auto&s:msymbols)
        if(s.id()==id)
            return s;
    return TemplateSymbol();
}

QStringList TemplateData::footprintIds() const
{
    QStringList r;
    for(const auto&f:mfootprints)
        r<<f.id();
    return r;
}

TemplateFootprint TemplateData::footprint(QString id) const
{
    for(const auto&f:mfootprints)
        if(f.id()==id)
            return f;
    return TemplateFootprint();
}

QStringList TemplateData::modelIds() const
{
    QStringList r;
    for(const auto&m:m3dmodels)
        r<<m.id();
    return r;
}

Template3DModel TemplateData::model(QString id) const
{
    for(const auto&m:m3dmodels)
        if(m.id()==id)
            return m;
    return Template3DModel();
}
