// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QMetaType>
#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFileInfo;
class QDir;

#include "templatevariables.h"

namespace Chipper { namespace Template {



///abstract base class of all template files
class CHIPPER_EXPORT TemplateFile
{
    QString mname,mfile,mid,mgen,mpath,mpinid,mprefill;
    TemplateVariables mvars;
protected:
    TemplateFile()=default;
    TemplateFile(const TemplateFile&)=default;
    TemplateFile(TemplateFile&&)=default;

    TemplateFile& operator=(const TemplateFile&)=default;
    TemplateFile& operator=(TemplateFile&&)=default;

    TemplateFile(const QDomElement&,const QDir&);

public:
    virtual ~TemplateFile()=0;

    ///returns the unique ID of the file (can be any string)
    QString id()const{return mid;}
    ///returns the file name (relative to the template dir in which it was referenced, may not be relative to the main template)
    QString fileName()const{return mfile;}
    ///returns the file name as an absolute path
    QString absoluteFileName()const{return mpath;}
    ///returns the human readable name of the file
    QString name()const{return mname;}
    ///returns the generator name
    QString generator()const{return mgen;}

    ///returns true if this is a valid template file definition
    bool isValid()const{return !mid.isEmpty();}

    ///kind of target file represented by this object
    enum class FileType {
        ///represents a symbol file
        Symbol,
        ///represents a footprint file
        Footprint,
        ///represents a 3D model file
        Model3D
    };
    ///returns the file type represented by this object
    virtual FileType fileType()const=0;
    ///returns the file type in human readable form
    QString fileTypeStr()const;

    ///returns the variables associated with this file
    TemplateVariables variables()const{return mvars;}

    ///returns the ID of the pin specification of the file
    QString pinSpecId()const{return mpinid;}
    ///returns the ID of the prefill algorithm for its pins
    QString pinPrefillId()const{return mprefill;}
};

///Represents a symbol template file.
class CHIPPER_EXPORT TemplateSymbol:public TemplateFile
{
    friend class TemplateData;
    TemplateSymbol(const QDomElement&e,const QDir&i):TemplateFile(e,i){}
public:
    TemplateSymbol()=default;
    TemplateSymbol(const TemplateSymbol&)=default;
    TemplateSymbol(TemplateSymbol&&)=default;

    TemplateSymbol& operator=(const TemplateSymbol&)=default;
    TemplateSymbol& operator=(TemplateSymbol&&)=default;

    virtual FileType fileType()const override{return FileType::Symbol;}
};

///Represents a footprint template file.
class CHIPPER_EXPORT TemplateFootprint:public TemplateFile
{
    friend class TemplateData;
    TemplateFootprint(const QDomElement&e,const QDir&i):TemplateFile(e,i){}
public:
    TemplateFootprint()=default;
    TemplateFootprint(const TemplateFootprint&)=default;
    TemplateFootprint(TemplateFootprint&&)=default;

    TemplateFootprint& operator=(const TemplateFootprint&)=default;
    TemplateFootprint& operator=(TemplateFootprint&&)=default;

    virtual FileType fileType()const override{return FileType::Footprint;}
};

///Represents a 3D model (OpenSCAD) template file.
class CHIPPER_EXPORT Template3DModel:public TemplateFile
{
    friend class TemplateData;
    Template3DModel(const QDomElement&e,const QDir&i):TemplateFile(e,i){}
public:
    Template3DModel()=default;
    Template3DModel(const Template3DModel&)=default;
    Template3DModel(Template3DModel&&)=default;

    Template3DModel& operator=(const Template3DModel&)=default;
    Template3DModel& operator=(Template3DModel&&)=default;

    virtual FileType fileType()const override{return FileType::Model3D;}
};


//end of namespace
}}
using namespace Chipper::Template;

Q_DECLARE_METATYPE(Chipper::Template::TemplateSymbol);
Q_DECLARE_METATYPE(Chipper::Template::TemplateFootprint);
Q_DECLARE_METATYPE(Chipper::Template::Template3DModel);
