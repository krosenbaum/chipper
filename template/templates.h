// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QMetaType>
#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFileInfo;
class QDir;

#include "templatefile.h"
#include "templatevariables.h"
#include "templatepins.h"

namespace Chipper { namespace Template {


///Main template object, contains/references all meta data for the template.
///Instantiated by the template pool, these objects are references to the actual template data
///and can be handed around as shared references.
///All template objects are read-only.
class CHIPPER_EXPORT TemplateData
{
    friend class TemplatePool;
    QString mpath,mtitle,mauthor,mlicense,mcopy,mreadme,mdescr;
    QList<TemplateSymbol>msymbols;
    QList<TemplateFootprint>mfootprints;
    QList<Template3DModel>m3dmodels;
    TemplateVariables mvars;
    QMap<QString,TemplatePins> mpins;

    ///called from template pool with directory name to load the template
    TemplateData(QString pname);
    ///helper for constructor: load a specific file
    void loadFile(QString pname,bool isMain=false);
public:
    ///instantiates an invalid template reference
    TemplateData()=default;
    ///instantiates a reference to the same template
    TemplateData(const TemplateData&)=default;
    ///instantiates a reference to the same template
    TemplateData(TemplateData&&)=default;
    ///drops a template reference
    ~TemplateData();

    ///makes this a reference to the same template
    TemplateData& operator=(const TemplateData&)=default;
    ///makes this a reference to the same template
    TemplateData& operator=(TemplateData&&)=default;

    ///returns true if this is a valid template
    bool isValid()const{return !mpath.isEmpty();}

    ///\internal returns the name ofthe meta data file for templates
    static QString metaFileName();

    ///returns the path to the template's directory
    QString pathName()const{return mpath;}
    ///returns the title of the template - this is referenced from chip files
    QString title()const{return mtitle;}
    ///returns the author of the template
    QString author()const{return mauthor;}
    ///returns the license of the template (usually not inherited by generated files!)
    QString license()const{return mlicense;}
    ///returns the copyright string of the template (usually not inherited by generated files!)
    QString copyright()const{return mcopy;}
    ///returns the relative path of the README file that can be displayed for help
    QString readmeFile()const{return mreadme;}
    ///returns a short description of the template
    QString description()const{return mdescr;}
    ///returns the full path of the README file that can be displayed for help
    QString absoluteReadmePath()const;
    ///returns the absolute path to the template directory
    QString absolutePathName()const;

    ///returns the IDs of all symbols defined in this template (can be any string)
    QStringList symbolIds()const;
    ///returns a symbol template by ID
    TemplateSymbol symbol(QString id)const;

    ///returns the IDs of all footprints defined in this template (can be any string)
    QStringList footprintIds()const;
    ///returns a footprint template by ID
    TemplateFootprint footprint(QString id)const;

    ///returns the IDs of all 3D models defined in this template (can be any string)
    QStringList modelIds()const;
    ///returns a 3D model template by ID
    Template3DModel model(QString id)const;

    ///returns all global template variables
    TemplateVariables variables()const{return mvars;}

    ///returns the IDs of all known pin definitions
    QStringList pinDefinitionIds()const{return mpins.keys();}
    ///returns a pin layout definition of the template by ID
    TemplatePins pins(QString id)const{return mpins.value(id);}
};

//end of namespace
}}
using namespace Chipper::Template;

Q_DECLARE_METATYPE(Chipper::Template::TemplateData);
