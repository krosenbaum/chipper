// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>
#include <QStringList>

#include <DomNodeIterator>

#include <functional>

#include "templates.h"
#include "templatepool.h"
#include "chipper.h"

// ////////////////////////////////////
// Template Pool

static QList<TemplateData>tdata;


TemplatePool::TemplatePool()
{
    if(tdata.size()>0)return;
    qDebug()<<"Creating Template Pool...";
    reload();
}

void TemplatePool::reload()
{
    if(!tdata.isEmpty()){
        qDebug()<<"Resetting Template Pool. Getting new Koi...";
        tdata.clear();
    }
    //search for templates
    QString tdir(defaultPath(ChipperPath::Template));
    std::function<void(QString)> scan=[&scan](QString dname){
//         qDebug()<<"Checking"<<dname<<"for templates...";
        QDir dir(dname);
        if(dir.exists(TemplateData::metaFileName()))
            tdata.append(TemplateData(dname));
        for(auto dn:dir.entryList(QDir::Dirs|QDir::AllDirs|QDir::NoDotAndDotDot,QDir::Name))
            scan(dname+"/"+dn);
    };
    scan(tdir);
    qDebug()<<"...Template Pool filled:"<<tdata.size()<<"fish inside.";
    emit reloaded();
}

TemplatePool::~TemplatePool()
{
    qDebug()<<"Emptying Template Pool.";
    tdata.clear();
    qDebug()<<"Template Pool gone. No Koi left.";
}

TemplatePool & TemplatePool::instance()
{
    static TemplatePool pool;
    return pool;
}

QStringList TemplatePool::pathList()
{
    QStringList r;
    for(const auto&d:tdata)
        r.append(d.pathName());
    return r;
}

QStringList TemplatePool::titleList()
{
    QStringList r;
    for(const auto&d:tdata)
        r.append(d.title());
    return r;
}

bool TemplatePool::hasTitle(QString t)
{
    for(auto&d:tdata)
        if(d.title()==t)return true;
    return false;
}

bool TemplatePool::hasPath(QString p)
{
    for(auto&d:tdata)
        if(d.pathName()==p)return true;
    return false;
}

TemplateData TemplatePool::byPath(QString p)
{
    for(auto&d:tdata)
        if(d.pathName()==p)return d;
    return TemplateData();
}

TemplateData TemplatePool::byTitle(QString t)
{
    for(auto&d:tdata)
        if(d.title()==t)return d;
    return TemplateData();
}

QString TemplatePool::titleForPath(QString pn)
{
    for(const auto&d:tdata)
        if(d.pathName()==pn)
            return d.title();
    return QString();
}

QString TemplatePool::pathForTitle(QString tt)
{
    for(const auto&d:tdata)
        if(d.title()==tt)
            return d.pathName();
    return QString();
}


#include "moc_templatepool.cpp"
