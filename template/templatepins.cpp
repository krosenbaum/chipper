// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>

#include <DomNodeIterator>

#include "templatepins.h"





// ////////////////////////////////////
// Template Pins

TemplatePins::TemplatePins(const QDomElement&e)
{
    mrepeat=e.attribute("repeat").trimmed();
    mdid=e.attribute("id").trimmed();
    mname=e.attribute("name",mdid).trimmed();
    if(e.hasAttribute("mode")){
        const QString m=e.attribute("mode").toLower().trimmed();
        if(m=="rows")mmode=Mode::Rows;
        else if(m=="grid")mmode=Mode::Grid;
        else qDebug()<<"Pin definition with unknown mode"<<m;
    }
    for(const auto&n:e.childNodes()){
        if(!n.isElement())continue;
        const QDomElement re=n.toElement();
        const QString tn=re.tagName();
        if(tn=="Row"){
            Row row(re);
            if(row.id().isEmpty()){
                qDebug()<<"Pin Row without a name! On line"<<re.lineNumber()<<"column"<<re.columnNumber();
                continue;
            }
            mrows.insert(row.id(),row);
            mrownames.append(row.id());
        }else if(tn=="Fill"){
            FillAlgo fa(re);
            if(fa.id().isEmpty()){
                qDebug()<<"Warning: Fill Algorithm with no name. Ignoring it. On line"<<re.lineNumber()<<"column"<<re.columnNumber();
                continue;
            }
            mfill.insert(fa.id(),fa);
        }else
            qDebug()<<"Unknown tag in pin definition:"<<tn;
    }
    if(mrows.isEmpty())
        qDebug()<<"Pin definition has no rows!";
}

QString TemplatePins::modeString() const
{
    switch(mmode){
        case Mode::Rows:return QCoreApplication::translate("TemplatePins","Row Mode");
        case Mode::Grid:return QCoreApplication::translate("TemplatePins","Grid Mode");
    }
    //should be unreachable
    return "??Internal Error??";
}

TemplatePins::FillAlgo::FillAlgo(const QDomElement&e)
{
    mid=e.attribute("id").trimmed();
    mname=e.attribute("name");
    for(const auto&n:e.childNodes()){
        if(!n.isElement())continue;
        const QDomElement re=n.toElement();
        append(FillStep(re));
    }
}

TemplatePins::FillStep::FillStep(const QDomElement&e)
{
    //basics
    mtext=e.text().trimmed();
    minto=e.attribute("into").trimmed();
    //type specifics
    const QString tn=e.tagName();
    const QString by=e.attribute("by","(missing)").trimmed().toLower();
    if(tn=="Sort"){
        mtype=StepType::Sort;
        if(by=="num")msort=SortBy::Table;
        else if(by=="id")msort=SortBy::Id;
        else if(by=="name")msort=SortBy::Name;
        else
            qDebug()<<"Warning: unknown pin sort criteria.";
    }else if(tn=="Filter"){
        mtype=StepType::Filter;
        if(by=="type")mfilter=FilterBy::Type;
        else if(by=="func")mfilter=FilterBy::Function;
        else if(by=="name")mfilter=FilterBy::Name;
        else if(by=="namere")mfilter=FilterBy::NameRegex;
        else
            qDebug()<<"Warning: unknown pin filter type"<<by;
    }else if(tn=="Take"){
        mtype=StepType::Take;
    }else
        qDebug()<<"Warning: unknown fill algorithm step type"<<tn<<"On line"<<e.lineNumber()<<"column"<<e.columnNumber();
    //auto-hide
    const QString ah=e.attribute("autohide","none");
    if(ah=="none")mautohide=AutoHide::None;
    else if(ah=="name")mautohide=AutoHide::Name;
    else if(ah=="type")mautohide=AutoHide::Type;
    else if(ah=="all")mautohide=AutoHide::All;
    else
        qDebug()<<"Warning: unknown pin auto-hide directive"<<ah;
}

QString Chipper::Template::TemplatePins::FillStep::stepTypeString() const
{
    switch(mtype){
        case StepType::Filter:return QCoreApplication::translate("TemplatePins::FillStep::StepType","Filter");
        case StepType::Sort:  return QCoreApplication::translate("TemplatePins::FillStep::StepType","Sort");
        case StepType::Take:  return QCoreApplication::translate("TemplatePins::FillStep::StepType","Take");
    }
    //unreachable?
    return "??";
}

QString Chipper::Template::TemplatePins::FillStep::autoHideString() const
{
    switch(mautohide){
        case AutoHide::All: return QCoreApplication::translate("TemplatePins::FillStep::AutoHide","All Pins");
        case AutoHide::Name:return QCoreApplication::translate("TemplatePins::FillStep::AutoHide","Same Name");
        case AutoHide::Type:return QCoreApplication::translate("TemplatePins::FillStep::AutoHide","Same Electrical Type");
        case AutoHide::None:return QCoreApplication::translate("TemplatePins::FillStep::AutoHide","No Hiding");
    }
    //unreachable?
    return "??";
}

QString Chipper::Template::TemplatePins::FillStep::filterByString() const
{
    switch(mfilter){
        case FilterBy::Function: return QCoreApplication::translate("TemplatePins::FillStep::FilterBy","Electrical Function");
        case FilterBy::Name:     return QCoreApplication::translate("TemplatePins::FillStep::FilterBy","Name, Wildcards");
        case FilterBy::NameRegex:return QCoreApplication::translate("TemplatePins::FillStep::FilterBy","Name, RegExp");
        case FilterBy::Type:     return QCoreApplication::translate("TemplatePins::FillStep::FilterBy","Pin Type");
    }
    //unreachable?
    return "??";
}

QString Chipper::Template::TemplatePins::FillStep::sortByString() const
{
    switch(msort){
        case SortBy::Table: return QCoreApplication::translate("TemplatePins::FillStep::SortBy","Variant Table Order");
        case SortBy::Name:  return QCoreApplication::translate("TemplatePins::FillStep::SortBy","Pin Name");
        case SortBy::Id:    return QCoreApplication::translate("TemplatePins::FillStep::SortBy","Pin 'Number'");
    }
    //unreachable?
    return "??";
}

TemplatePins::Row::Row(const QDomElement&e)
{
    mtext=e.text().trimmed();
    mid=e.attribute("id").trimmed();
    mmap=e.attribute("maphint").trimmed();
}
