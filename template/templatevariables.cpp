// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QDebug>
#include <QDomDocument>
#include <QDomElement>

#include <DomNodeIterator>

#include "templatevariables.h"




// ////////////////////////////////////
// Template Variables

TemplateVariables::TemplateVariables(const QDomElement&ve)
{
    loadTemplate(ve);
}

void TemplateVariables::loadTemplate(const QDomElement&ve)
{
    //parse element
    for(auto n:ve.childNodes()){
        if(!n.isElement())continue;
        QDomElement e=n.toElement();
        const QString tn=e.tagName();
        if(tn=="enum"){
            addEnum(e);
        }else if(tn=="v"){
            addFullVar(e);
        }else if(tn=="h"){
            addHiddenVar(e);
        }else
            qDebug()<<"Unknown variables tag"<<tn<<"line"<<e.lineNumber()<<"column"<<e.columnNumber()<<"- I'll just ignore it - okay?";
    }
    //internal consistency check
}

void TemplateVariables::addEnum(const QDomElement&e)
{
    QString id=e.attribute("id").trimmed();
    if(id.isEmpty()){
        qDebug()<<"Found an enum without a name on line"<<e.lineNumber()<<"column"<<e.columnNumber()<<"- skipping it.";
        return;
    }
    if(menums.contains(id)){
        qDebug()<<"   Warning: duplicate enum"<<id<<"- skipping it.";
    }
    QList<Enum> vals;
    for(QString v:e.text().trimmed().split(';')){
        v=v.trimmed();
        if(v.isEmpty())continue;
        int s=v.indexOf(' ');
        if(s<0)continue;
        double vv=v.left(s).toDouble();
        QString vd=v.mid(s).trimmed();
        vals.append(Enum(vv,vd));
    }
    if(vals.isEmpty()){
        qDebug()<<"Enum"<<id<<"has no data, skipping it.";
        return;
    }
    menums.insert(id,vals);
}

void TemplateVariables::addHiddenVar(const QDomElement&e)
{
    Variable var;
    var.mhidden=true;
    var.mid=e.attribute("id").trimmed();
    var.mdefaultVal=e.attribute("default").trimmed();
    if(var.mid.isEmpty()){
        qDebug()<<"Hidden variable without name on line"<<e.lineNumber()<<"column"<<e.columnNumber()<<"- black cats in dark rooms don't exist - skipping.";
        return;
    }
    if(hasVariable(var.mid)){
        Variable v2=variable(var.mid);
        if(v2.mhidden!=var.mhidden)
            qDebug()<<"  Warning: duplicate variable"<<var.mid<<"with different definition. Skipping it. World might end soon.";
        return;
    }
    if(var.mdefaultVal.isEmpty())
        qDebug()<<"Hidden variable without formula on line"<<e.lineNumber()<<"column"<<e.columnNumber()<<"- assuming dark sense of humor, going ahead anyway.";
    mvars.append(var);
}

void TemplateVariables::addFullVar(const QDomElement&e)
{
    Variable var;
    var.mid=e.attribute("id").trimmed();
    if(var.mid.isEmpty()){
        qDebug()<<"Visible variable without name on line"<<e.lineNumber()<<"column"<<e.columnNumber()<<"- riding a horse with no name - skipping.";
        return;
    }
    var.mdescription=e.text().trimmed();
    var.mtype=stringToVarType(e.attribute("type").trimmed());
    var.mdefaultVal=e.attribute("default").trimmed();
    var.mminVal=e.attribute("min").trimmed();
    var.mmaxVal=e.attribute("max").trimmed();
    var.mstepVal=e.attribute("step").trimmed();
    var.menumRef=e.attribute("enum").trimmed();
    if(hasVariable(var.mid)){
        Variable v2=variable(var.mid);
        if(v2.mhidden!=var.mhidden || v2.mtype!=var.mtype)
            qDebug()<<"  Warning: duplicate variable"<<var.mid<<"with different definition. Skipping it. Wizzard might get angry.";
        return;
    }
    mvars.append(var);
}

TemplateVariables::Variable TemplateVariables::variable(QString id) const
{
    for(int i=0;i<mvars.size();i++)
        if(mvars[i].id()==id)
            return mvars[i];
    return Variable();
}

bool TemplateVariables::hasVariable(QString id) const
{
    for(int i=0;i<mvars.size();i++)
        if(mvars[i].id()==id)
            return true;
    return false;
}

TemplateVariables::VarType TemplateVariables::stringToVarType(QString s)
{
    s=s.toLower();
    if(s=="int"||s=="integer")return VarType::Int;
    if(s=="enum")return VarType::Enum;
    if(s=="str"||s=="string")return VarType::String;
    return VarType::Float;
}

QString TemplateVariables::varTypeToString(TemplateVariables::VarType t)
{
    switch(t){
        case VarType::Enum:return "Enum";
        case VarType::Int:return "Int";
        case VarType::String:return "String";
        default:return "Float";
    }
}

TemplateVariables::Enum TemplateVariables::enumByValue(QString ename, double value, double epsilon) const
{
    auto elist=enumsByName(ename);
    if(elist.size()==0)return Enum();
    for(int i=0;i<elist.size();i++){
        const auto &en=elist[i];
        if(en.mvalue>=(value-epsilon) && en.mvalue<=(value+epsilon))
            return en;
    }
    return Enum();
}

bool Chipper::Template::TemplateVariables::hasVisibleVariables() const
{
    for(const auto&v:mvars)
        if(!v.isHidden())
            return true;
    return false;
}

