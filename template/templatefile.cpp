// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>

#include <DomNodeIterator>

#include <functional>

#include "templatefile.h"
#include "chipdatapriv.h"




// ////////////////////////////////////
// Template Symbol & Footprint

TemplateFile::TemplateFile(const QDomElement&e,const QDir&dir)
:mvars(findElement(e,"Variables"))
{
    mname=e.attribute("name");
    mfile=e.attribute("file");
    mpath=dir.absoluteFilePath(mfile);
    mid=e.attribute("id").trimmed();
    mgen=e.attribute("generator","default").trimmed();
    mpinid=e.attribute("rows").trimmed();
    mprefill=e.attribute("prefill").trimmed();
}

TemplateFile::~TemplateFile(){}

QString TemplateFile::fileTypeStr() const
{
    switch(fileType()){
        case FileType::Symbol:return QCoreApplication::translate("TemplateFile::FileType","Symbol");
        case FileType::Footprint:return QCoreApplication::translate("TemplateFile::FileType","Footprint");
        case FileType::Model3D:return QCoreApplication::translate("TemplateFile::FileType","3D Model");
        default:return QString();
    }
}
