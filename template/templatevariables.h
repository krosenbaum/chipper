// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QMetaType>
#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFileInfo;
class QDir;

namespace Chipper { namespace Template {

class TemplateData;
class TemplateFile;

///Holds variable definitions of a template.
class CHIPPER_EXPORT TemplateVariables
{
public:
    ///Variable data type.
    enum class VarType{
        ///(default) floating point value
        Float,
        ///integer value
        Int,
        ///enumeration (numeric, but can only hold specific named values)
        Enum,
        ///string data
        String
    };
    ///represents the definition of an enumeration value
    class Enum{
        double mvalue;
        QString mdisplay;
        friend class TemplateVariables;
        Enum(double v,QString d):mvalue(v),mdisplay(d){}
    public:
        ///empty enumeration
        Enum()=default;
        ///copies the enumeration
        Enum(const Enum&)=default;
        ///copies the enumeration
        Enum(Enum&&)=default;
        ///copies the enumeration
        Enum& operator=(const Enum&)=default;
        ///copies the enumeration
        Enum& operator=(Enum&&)=default;

        ///returns the numeric value
        double value()const{return mvalue;}
        ///returns the human readable name for the value
        QString displayString()const{return mdisplay;}
    };
    ///represents a full variable definition
    class Variable{
        QString mid,mdefaultVal,mminVal,mmaxVal,mstepVal,menumRef,mdescription;
        VarType mtype=VarType::Float;
        bool mhidden=false;
        friend class TemplateVariables;
    public:
        ///invalid variable definition
        Variable()=default;
        ///copies the variable definition
        Variable(const Variable&)=default;
        ///copies the variable definition
        Variable(Variable&&)=default;
        ///copies the variable definition
        Variable& operator=(const Variable&)=default;
        ///copies the variable definition
        Variable& operator=(Variable&&)=default;

        ///returns the ID/name of the variable
        QString id()const{return mid;}
        ///returns the default value in case the user does not set the variable (can be a formula)
        QString defaultValue()const{return mdefaultVal;}
        ///returns the minimum value (can be formula)
        QString minimumValue()const{return mminVal;}
        ///returns the maximum value (can be formula)
        QString maximumValue()const{return mmaxVal;}
        ///returns the single step value (can be formula)
        QString stepValue()const{return mstepVal;}
        ///returns the reference to an enum definition (optional)
        ///for enums this is mandatory, for float/int variables this suggests selectable default values
        QString enumReference()const{return menumRef;}
        ///human readable description of the variable
        QString description()const{return mdescription;}
        ///data type of the variable
        VarType variableType()const{return mtype;}
        ///true if this is a hidden variable (defaultValue must be set)
        bool isHidden()const{return mhidden;}
    };
private:
    QMap<QString,QList<Enum>> menums;
    QList<Variable> mvars;

    friend class TemplateData;
    friend class TemplateFile;
    TemplateVariables(const QDomElement&);
    void loadTemplate(const QDomElement&);
    void addEnum(const QDomElement&);
    void addFullVar(const QDomElement&);
    void addHiddenVar(const QDomElement&);
public:
    ///instantiates an invalid variable table
    TemplateVariables()=default;
    ///copies the variable table
    TemplateVariables(const TemplateVariables&)=default;
    ///copies the variable table
    TemplateVariables(TemplateVariables&&)=default;

    ///copies the variable table
    TemplateVariables& operator=(const TemplateVariables&)=default;
    ///copies the variable table
    TemplateVariables& operator=(TemplateVariables&&)=default;

    ///parses the string (from the XML file) and converts it to a data type
    static VarType stringToVarType(QString);
    ///returns a readable string for the variable type
    static QString varTypeToString(VarType);

    ///returns the number of enum definitions that exist in this table
    int numEnums()const{return menums.size();}
    ///returns all names of enum definitions
    QStringList enumNames()const{return menums.keys();}
    ///returns true if this enum exists in this table
    bool hasEnum(QString ename)const{return menums.contains(ename);}
    ///returns an enum definition by name (as referenced from a variable)
    QList<Enum> enumsByName(QString ename)const{return menums.value(ename);}
    ///returns a specific enum definition by enum name and value
    Enum enumByValue(QString ename,double value,double epsilon=0.001)const;

    ///returns how many variables definitions exist in the table
    int numVariables()const{return mvars.size();}
    ///returns a specific variable by index
    Variable variable(int i)const{return mvars[i];}
    ///returns a specific variable by name
    Variable variable(QString id)const;
    ///true if a specific variable exists
    bool hasVariable(QString id)const;
    ///true if this template has any visible variables
    bool hasVisibleVariables()const;
};


//end of namespace
}}
using namespace Chipper::Template;

Q_DECLARE_METATYPE(Chipper::Template::TemplateVariables);
