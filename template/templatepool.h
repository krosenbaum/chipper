// Chipper KiCAD symbol/footprint/3Dmodel generator
// Template Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Template {

class TemplateData;


///Pool that holds template definitions.
///This pool automatically loads all templates that it can find for later use.
///Templates can be retrieved by path or by title. In theory multiple pathes can have the same title configured,
///but this should be avoided in practice, since chip files reference the template title.
class CHIPPER_EXPORT TemplatePool:public QObject
{
    Q_OBJECT
    //singleton
    TemplatePool();
    ~TemplatePool();
public:
    ///reference to the pool
    static TemplatePool&instance();

    ///forces the pool to reload all templates
    void reload();

    ///returns the pathes of all templates
    QStringList pathList();
    ///returns a template by path (should be one of those returned by pathList())
    TemplateData byPath(QString);

    ///returns all template titles known to the pool
    QStringList titleList();
    ///returns a template by title (this is the reference from a chip file)
    TemplateData byTitle(QString);

    ///returns the title of a specific template path
    QString titleForPath(QString);

    ///returns the path of a specific template title
    QString pathForTitle(QString);

    ///returns true if a specific path exists
    bool hasPath(QString);

    ///returns true if a specific title exists
    bool hasTitle(QString);

signals:
    ///emitted if the entire pool has been reloaded
    void reloaded();
};

//end of namespace
}}
using namespace Chipper::Template;
