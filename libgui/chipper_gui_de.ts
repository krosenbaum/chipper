<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Chipper::GUI::AddPackageDialog</name>
    <message>
        <location filename="chipvariantedit.cpp" line="525"/>
        <source>Add Package to &apos;%1&apos;</source>
        <translation>Neues Gehäuse zu &apos;%1&apos; hinzufügen</translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="531"/>
        <source>UID:</source>
        <translation>UID:</translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="532"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="535"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="543"/>
        <source>Create Package</source>
        <translation>Gehäuse erzeugen</translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="545"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbruch</translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipAddTargetFileDialog</name>
    <message>
        <location filename="chippackageedit.cpp" line="432"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="433"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="439"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="441"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbruch</translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipAddVariantDialog</name>
    <message>
        <location filename="chipedit.cpp" line="740"/>
        <source>Create New Variant for Chip %1</source>
        <translation>Neue Variante für Chip %1 erzeugen</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="746"/>
        <source>UID:</source>
        <translation>UID:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="747"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="748"/>
        <source>Number of Pins:</source>
        <translation>Anzahl Pins:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="758"/>
        <source>Create &amp;Variant</source>
        <translation>&amp;Variante erzeugen</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="760"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbruch</translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipEditor</name>
    <message>
        <location filename="chipedit.cpp" line="50"/>
        <source>Basics</source>
        <translation>Grunddaten</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="51"/>
        <source>Target</source>
        <translation>Zieldaten</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="52"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="53"/>
        <source>Pins</source>
        <translation>Pins</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="67"/>
        <source>&lt;html&gt;&lt;h1&gt;Basic Chip Meta Data&lt;/h1&gt;</source>
        <translation>&lt;html&gt;&lt;h1&gt;Chip Metadaten&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="68"/>
        <source>Chip File:</source>
        <translation>Chipdatei:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="71"/>
        <source>&lt;html&gt;&lt;b&gt;Hardware Data:&lt;/b&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Hardwaredaten:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="72"/>
        <source>Manufacturer:</source>
        <translation>Hersteller:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="74"/>
        <source>Chip Type:</source>
        <translation>Chiptyp:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="76"/>
        <source>Datasheet URL:</source>
        <translation>Datenblatt URL:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="78"/>
        <source>Keywords:</source>
        <translation>Stichworte:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="80"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="82"/>
        <source>Reference Prefix:</source>
        <translation>Referenz-Präfix:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="87"/>
        <source>&lt;html&gt;&lt;b&gt;Soft-Model Data:&lt;/b&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Modelldaten:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="88"/>
        <source>File Author:</source>
        <translation>Dateiautor:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="90"/>
        <source>File Copyright:</source>
        <translation>Urheberrecht der Datei:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="92"/>
        <source>File License:</source>
        <translation>Lizenz der Datei:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="104"/>
        <source>&lt;html&gt;&lt;h1&gt;Target Files&lt;/h1&gt;</source>
        <translation>&lt;html&gt;&lt;h1&gt;Zieldateien&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="105"/>
        <source>Symbol File:</source>
        <translation>Schaltzeichen-Datei:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="107"/>
        <source>Symbol Root Dir:</source>
        <translation>Schaltzeichen-Ursprungsverzeichnis:</translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="108"/>
        <source>Footprint Dir:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="110"/>
        <source>Footprint Root Dir:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="111"/>
        <source>3D Model Dir:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="113"/>
        <source>3D Model Root Dir:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="115"/>
        <source>Notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="158"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="172"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="284"/>
        <source>&lt;html&gt;&lt;h1&gt;Pin Definitions&lt;/h1&gt;
&lt;p&gt;This page defines all logical pins that the chip can offer.&lt;/p&gt;
&lt;p&gt;Each distinct pin should appear exactly once (e.g. only one &quot;GND&quot;, but e.g. &quot;Vcc&quot; vs. &quot;AVcc&quot; are different) and every pin that exists in any variant of the chip must appear.&lt;br/&gt;
Variants can only reference pins that exist here, but they can reference each existing pin never, once or even multiple times.&lt;br/&gt;
Pins that have no function (NC) should not appear here.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="294"/>
        <source>Pins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="306"/>
        <source>&amp;Add Pin...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="308"/>
        <source>&amp;Add Pin Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="309"/>
        <source>Add Alt &amp;Function...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="311"/>
        <source>&amp;Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="314"/>
        <source>&amp;Delete Pin or Alt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="317"/>
        <source>Move &amp;Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="318"/>
        <source>&amp;Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="320"/>
        <source>&amp;Sort Pins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="330"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="330"/>
        <source>Electrical Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="330"/>
        <source>Line Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="475"/>
        <location filename="chipedit.cpp" line="497"/>
        <source>Really Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="475"/>
        <source>Really delete alternate function &apos;%1&apos; of pin &apos;%2&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="484"/>
        <location filename="chipedit.cpp" line="501"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="484"/>
        <source>Unable to delete this alternate function!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="497"/>
        <source>Really delete pin &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="501"/>
        <source>Unable to delete this pin!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="530"/>
        <source>Unknown Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="532"/>
        <source>Chip: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="567"/>
        <source>Really delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="567"/>
        <source>Do you really want to delete the variant &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipPackageEditor</name>
    <message>
        <location filename="chippackageedit.cpp" line="46"/>
        <source>Package: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="54"/>
        <source>&lt;html&gt;&lt;h1&gt;Package&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="59"/>
        <source>UID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="60"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="65"/>
        <source>Show...</source>
        <comment>show template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="68"/>
        <source>&lt;html&gt;&lt;font color=&apos;red&apos;&gt;Warning: Template not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="69"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="73"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="77"/>
        <source>Keywords:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="99"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="99"/>
        <source>Please select a symbol!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="100"/>
        <source>Footprints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="100"/>
        <source>Please select a footprint!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="101"/>
        <source>3D Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="101"/>
        <source>Please select a 3D model!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="151"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="152"/>
        <source>Footprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="153"/>
        <source>3D Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="166"/>
        <source>Add Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="167"/>
        <source>Please select a symbol template and a name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="178"/>
        <source>Add Footprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="179"/>
        <source>Please select a footprint template and a name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="190"/>
        <source>Add 3D Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="191"/>
        <source>Please select a 3D model template and a name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="227"/>
        <source>Really delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chippackageedit.cpp" line="227"/>
        <source>Really delete &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipPinDialog</name>
    <message>
        <location filename="chipedit.cpp" line="635"/>
        <source>%1 %2 %3</source>
        <comment>Verb(Edit/New) ObjType(Pin/Alt) Name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="636"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="636"/>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="637"/>
        <source>Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="637"/>
        <source>Alternate Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="646"/>
        <source>Pin Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="646"/>
        <source>Alternate Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="647"/>
        <source>Electrical Pin Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="655"/>
        <source>Line Style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="668"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="670"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="681"/>
        <location filename="chipedit.cpp" line="700"/>
        <location filename="chipedit.cpp" line="706"/>
        <location filename="chipedit.cpp" line="718"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="681"/>
        <source>Pin names must not be empty, use ~ if you want it to appear empty in KiCad.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="686"/>
        <source>Pin with Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="686"/>
        <source>Pin names must not contain spaces. Replace with &apos;_&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="694"/>
        <source>Invalid Pin Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="694"/>
        <source>The pin name &apos;%1&apos; is not valid - only letters, digits, and the special characters &apos;~&apos;, &apos;_&apos;, &apos;{&apos; and &apos;}&apos; should be used.

Store it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="700"/>
        <source>Unable to change pin name. Please make sure there is no other pin/alternate function with the same name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="706"/>
        <source>Unable to create new pin named &apos;%1&apos; - make sure there is no pin with this name yet!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="711"/>
        <source>Ooops!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="711"/>
        <source>Hmm, parent pin for this alternate function is invalid - cannot create one.
Repent! The end is nigh! Or something.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipedit.cpp" line="718"/>
        <source>Unable to create alternate function. Make sure there is no pin or alternate function with the same name!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ChipVariantEditor</name>
    <message>
        <location filename="chipvariantedit.cpp" line="60"/>
        <source>Variant Basics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="74"/>
        <source>Variant: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="84"/>
        <source>&lt;html&gt;&lt;h1&gt;Main Variant Data&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="88"/>
        <source>Variant ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="89"/>
        <source>Variant Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="91"/>
        <source>Number of Pins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="102"/>
        <source>Pins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="114"/>
        <source>&amp;Remove Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="117"/>
        <source>Move &amp;Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="119"/>
        <source>Move &amp;Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="122"/>
        <source>Re&amp;fresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="167"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="chipvariantedit.cpp" line="168"/>
        <source>The variant %1 had references to pin name %2 on pin number: %3</source>
        <comment>may need plural for &apos;number&apos;</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="171"/>
        <source>, </source>
        <comment>list of numbers</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="183"/>
        <source>Number</source>
        <comment>pin number header</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="184"/>
        <source>Type</source>
        <comment>pin type header</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="185"/>
        <source>Name</source>
        <comment>pin name header</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="186"/>
        <source>Status</source>
        <comment>pin status header</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="231"/>
        <source>too many pins!</source>
        <comment>pin state</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="236"/>
        <source>no number!</source>
        <comment>pin state</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="241"/>
        <source>duplicate number!</source>
        <comment>pin state</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="248"/>
        <source>select pin!</source>
        <comment>pin state</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="251"/>
        <location filename="chipvariantedit.cpp" line="257"/>
        <source>okay</source>
        <comment>pin state</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="412"/>
        <source>Delete Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="412"/>
        <source>Really delete pin %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="507"/>
        <source>Really delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="507"/>
        <source>Do you really want to delete the package &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::EnvironmentInspector</name>
    <message>
        <location filename="configdlg.cpp" line="155"/>
        <source>Environment Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="176"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="194"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="194"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::FileEditPage</name>
    <message>
        <location filename="chipfileedit.cpp" line="48"/>
        <source>UID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="50"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="73"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="80"/>
        <source>&lt;html&gt;&lt;b&gt;%1: %2&lt;/b&gt;</source>
        <comment>Symbol/Footprint/Model: Name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::FilePinEditPage</name>
    <message>
        <location filename="chipfileedit.cpp" line="188"/>
        <source>Row</source>
        <comment>symbol pins</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="188"/>
        <source>Pins</source>
        <comment>symbol pins</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="218"/>
        <source>Pins</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::FootprintEditPage</name>
    <message>
        <location filename="chipfileedit.cpp" line="146"/>
        <source>Default 3D Model:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::HelpWindow</name>
    <message>
        <location filename="helpwin.cpp" line="33"/>
        <source>Chipper Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="helpwin.cpp" line="50"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::LogTab</name>
    <message>
        <location filename="logtab.cpp" line="62"/>
        <source>Max Lines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="logtab.cpp" line="77"/>
        <source>Log to File:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::MainWindow</name>
    <message>
        <location filename="mainwin.cpp" line="44"/>
        <source>Chipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="48"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="49"/>
        <source>&amp;New Chip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="50"/>
        <source>&amp;Open Chip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="52"/>
        <source>&amp;Chip Pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="54"/>
        <source>&amp;Recent Chips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="60"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="62"/>
        <source>&amp;Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="63"/>
        <source>&amp;Save Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="65"/>
        <source>C&amp;lone Current Chip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="67"/>
        <source>Add &amp;Variant...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="68"/>
        <source>Add &amp;Package...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="69"/>
        <source>Add Symbol...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="70"/>
        <source>Add Footprint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="71"/>
        <source>Add 3D Model...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="73"/>
        <source>Delete Current Variant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="74"/>
        <source>Delete Current Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="75"/>
        <source>Delete Current File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="77"/>
        <source>&amp;Unload Current Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="79"/>
        <source>&amp;Generate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="80"/>
        <source>Generate &amp;Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="81"/>
        <source>Generate &amp;All in Pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="83"/>
        <source>&amp;Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="86"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="87"/>
        <source>P&amp;ath Configuration...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="88"/>
        <source>&amp;Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="90"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="91"/>
        <source>Inspect &amp;Environment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="92"/>
        <source>&amp;Log Viewer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="94"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="97"/>
        <source>Chipper &amp;Version...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="97"/>
        <source>Chipper Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="98"/>
        <source>About &amp;Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="115"/>
        <source>Chips: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="122"/>
        <source>Templates: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="134"/>
        <location filename="mainwin.cpp" line="272"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="134"/>
        <source>Error while loading chip file %1:
%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="137"/>
        <source>Generating %1 ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="138"/>
        <source>Successfully generated: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="139"/>
        <source>Error in %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="165"/>
        <source>View %1...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="169"/>
        <source>&amp;Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="265"/>
        <source>Open Chip File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="272"/>
        <source>Unable to open chip file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="285"/>
        <source>%1: %2</source>
        <comment>manufacturer: chip</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="422"/>
        <source>Delete Current %1...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::NewChipWizard</name>
    <message>
        <location filename="newchipwiz.cpp" line="27"/>
        <source>Create New Chip File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="41"/>
        <source>&lt;html&gt;&lt;h1&gt;Create Chip File from:&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="42"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="42"/>
        <source>Create a new empty chip file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="44"/>
        <source>Minimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="44"/>
        <source>Create a new chip file with minimal example data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="46"/>
        <source>Clone Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="46"/>
        <source>Create a new chip file as a clone of a currently loaded chip.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="48"/>
        <source>Clone External</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="48"/>
        <source>Create a new chip file as a clone of an unloaded chip file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="53"/>
        <location filename="newchipwiz.cpp" line="75"/>
        <location filename="newchipwiz.cpp" line="96"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="58"/>
        <source>&lt;html&gt;&lt;h1&gt;Create Chip File into:&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="63"/>
        <source>Enter File Name: *.chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="71"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="80"/>
        <source>&lt;html&gt;&lt;h1&gt;Create Chip File:&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="84"/>
        <source>Source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="85"/>
        <source>New File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="88"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="88"/>
        <source>Create the new chip file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="94"/>
        <source>Create Chip File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="111"/>
        <source>%1) Chip: %2; File: %3</source>
        <comment>translated format must start with %1 followed by a non-numeric char</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="118"/>
        <source>Clone Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="118"/>
        <source>Select the chip that should be cloned:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="137"/>
        <source>Select Source Chip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="141"/>
        <location filename="newchipwiz.cpp" line="176"/>
        <source>Chip Files (*.chip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="141"/>
        <location filename="newchipwiz.cpp" line="176"/>
        <source>Any Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="174"/>
        <source>Create new Chip File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="204"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="newchipwiz.cpp" line="204"/>
        <source>Unable to create new file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::PathConfigDialog</name>
    <message>
        <location filename="configdlg.cpp" line="72"/>
        <source>Chipper Path Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="79"/>
        <source>Environment Prefix:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="80"/>
        <source>Session Env. Prefix:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="81"/>
        <source>Root Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="82"/>
        <source>Session Root Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="83"/>
        <source>Symbol Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="84"/>
        <source>Session Symbol Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="85"/>
        <source>Footprint Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="86"/>
        <source>Session Footprint Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="87"/>
        <source>3D Model Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="88"/>
        <source>Session 3D Model Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="89"/>
        <source>Template Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="90"/>
        <source>Session Template Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="95"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="98"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::PreferenceDialog</name>
    <message>
        <location filename="configdlg.cpp" line="119"/>
        <source>Chipper Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="128"/>
        <source>Unit of Measure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="129"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="129"/>
        <source>Millimeters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="129"/>
        <source>Millimetres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="129"/>
        <source>Metric Millimeters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="129"/>
        <source>1/25.4 inch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="130"/>
        <source>Autosync Interval (s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="136"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdlg.cpp" line="139"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::ReadmePage</name>
    <message>
        <location filename="chippackageedit.cpp" line="417"/>
        <source>%1 ReadMe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::SymbolEditPage</name>
    <message>
        <location filename="chipfileedit.cpp" line="123"/>
        <source>Default Footprint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipfileedit.cpp" line="125"/>
        <source>Default Value:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::TemplateViewer</name>
    <message>
        <location filename="templateviewer.cpp" line="27"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="42"/>
        <source>Footprints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="56"/>
        <source>3D Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <location filename="templateviewer.cpp" line="97"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <source>Input Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <source>Script Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <source>Pin Definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="70"/>
        <source>Pin Prefill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="76"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="76"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="97"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="97"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="97"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="98"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="98"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="98"/>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="98"/>
        <source>Enum Ref.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="99"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="104"/>
        <source>Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="104"/>
        <source>Visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="120"/>
        <source>Def. ID
 ⇒ Row ID
 ⇒ Fill Algo ID
 ⇒⇒ Step Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="121"/>
        <source>Mode
 
 
By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="122"/>
        <source>Formula
Map Hint
 
Into</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="123"/>
        <source>
 
 
Auto-Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="124"/>
        <source>Definition Name
Row Hint
Algo. Name
Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="142"/>
        <source>Row: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="152"/>
        <source>Fill Algo: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="175"/>
        <source>Template: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="179"/>
        <source>Main Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="182"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="183"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="184"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="185"/>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="186"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="187"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="195"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="199"/>
        <source>Enums:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="211"/>
        <source>Variables:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="219"/>
        <source>Pins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="221"/>
        <source>Pin Row Definitions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="233"/>
        <source>Generators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="templateviewer.cpp" line="244"/>
        <source>ReadMe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::VariableEditPage</name>
    <message>
        <location filename="variableedit.cpp" line="54"/>
        <source>Name</source>
        <comment>variable</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="variableedit.cpp" line="54"/>
        <source>Value</source>
        <comment>variable</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="variableedit.cpp" line="54"/>
        <source>Description</source>
        <comment>variable</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="variableedit.cpp" line="106"/>
        <source>Variables</source>
        <comment>title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::GUI::VariantPinDelegate</name>
    <message>
        <location filename="chipvariantedit.cpp" line="325"/>
        <location filename="chipvariantedit.cpp" line="353"/>
        <location filename="chipvariantedit.cpp" line="364"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="325"/>
        <source>Pin number must not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="330"/>
        <location filename="chipvariantedit.cpp" line="335"/>
        <source>Invalid Pin Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="330"/>
        <source>Pin number must not start with a dash or contain spaces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="335"/>
        <source>Pin numbers should only consist of letters and digits. Store anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="340"/>
        <source>Duplicate Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="340"/>
        <source>A pin with the number &apos;%1&apos; already exists. Cannot store this one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="353"/>
        <source>Unable to add pin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="chipvariantedit.cpp" line="364"/>
        <source>Oops. Unable to renumber pin.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
