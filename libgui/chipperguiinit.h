// Chipper KiCAD symbol/footprint/3Dmodel generator
// GUI init
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QtGlobal>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace GUI {
CHIPPERGUI_EXPORT void __chipperGuiInit(int&,char**);
}}
