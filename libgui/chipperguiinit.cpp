// Chipper KiCAD symbol/footprint/3Dmodel generator
// GUI init
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipperguiinit.h"
#include "chipper.h"
#include "logtab.h"

#include <QApplication>
#include <QIcon>
#include <QDebug>

void Chipper::GUI::__chipperGuiInit(int& ac,char**av)
{
    if(isDebugMode())qDebug()<<"Initialize Chipper GUI...";
    QApplication::setApplicationDisplayName("Chipper");
    new QApplication(ac,av);
    QApplication::setWindowIcon(QIcon(":/chipper.png"));
    LogTab::init();
}
