#main defs
TEMPLATE = lib
TARGET = chippergui

include(../basics.pri)
DEFINES += CHIPPERGUI_EXPORT=Q_DECL_EXPORT
LIBS+=-lchipper

#Qt basics
QT += gui widgets xml

#Taurus Libs: ELAM, DOMext, ...
include(../Taurus/chester.pri)
include(../Taurus/elam.pri)
#include(../Taurus/domext.pri)

include(../chipgui/chipgui.pri)
include(../tempgui/tempgui.pri)
include(../widgets/widgets.pri)
include(../windows/windows.pri)

#sources
SOURCES += \
    $$PWD/chipperguiinit.cpp

HEADERS += \
    $$PWD/chipperguiinit.h

INCLUDEPATH += $$PWD $$PWD/../chip $$PWD/../generate $$PWD/../template $$PWD/../src $$PWD/../calc

#make sure we re-compile dependend sources
DEPENDPATH += $$INCLUDEPATH

#global resources
RESOURCES += ../icons/chipper.qrc $$OUT_PWD/../doc.qrc

#translations
CONFIG += lrelease
LRELEASE_DIR = $$DESTDIR
TRANSLATIONS += \
    chipper_gui_de.ts
