TARGET = chipper
QT+= gui widgets
LIBS += -lchippergui
INCLUDEPATH += $$PWD/../libgui $$PWD/../windows
include (main.pri)
DEFINES += CHIPPER_GUIBUILD=1
OBJECTS_DIR = guiobj
win32:CONFIG+=windows
