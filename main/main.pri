#main defs
TEMPLATE = app
QT+= xml

include(../basics.pri)

LIBS += -lchipper

SOURCES += $$PWD/main.cpp
INCLUDEPATH += $$PWD $$PWD/../src $$PWD/../gui $$PWD/../generate $$PWD/../chip $$PWD/../template

#make sure we re-compile dependend sources
DEPENDPATH += $$INCLUDEPATH
