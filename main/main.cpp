// Chipper KiCAD symbol/footprint/3Dmodel generator
// main file
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDebug>
#include <QFileInfo>
#include <QMap>
#include <QSettings>
#include <QStringList>
#include <QTimer>
#include <QVariant>

#include "chipper.h"
#include "generator.h"
#include "chippool.h"
#include "templatepool.h"

#ifdef CHIPPER_GUIBUILD
#include <QIcon>
#include <QProgressDialog>

#include "mainwin.h"

#define GUIHELP \
" -gui             - start in GUI mode\n" \
" -nogui           - start in command line mode\n"

#define GUISTART true

#else
#define GUIHELP
#define GUISTART false
#endif

namespace Chipper {
///output for help on command line
static const QString HELP="Chipper arguments:\n" \
GUIHELP
" -generate        - run in generator mode\n" \
" -envprefix=TOKEN - use TOKEN as environment variable prefix (default: WIZZARD)\n" \
" -root=path       - root path for all other pathes (if those are relative; default is current working dir)\n" \
" -symbol=path     - root of symbol libraries\n" \
" -footprint=path  - root of footprint libary files\n" \
" -3dmodel=path    - root of 3D model files\n" \
" -template=path   - root of template directories\n" \
" -help            - output this help\n" \
" -version         - show version information\n" \
" -log=file        - copy all output to the named log file\n"\
" -log             - copy all output to default log file\n"\
" -keep            - keep temporary directories around\n"\
" *.chip           - pre-load Chip file in GUI or process in generator mode\n" \
"\n" \
"Environment variables:\n" \
" $WIZZARD_ROOT          - root path for all other pathes\n" \
" $WIZZARD_SYMBOL_DIR    - root of symbol libraries\n" \
" $WIZZARD_FOOTPRINT_DIR - root of footprint libary files\n" \
" $WIZZARD_3DMODEL_DIR   - root of 3D model files\n" \
" $WIZZARD_TEMPLATE_DIR  - root of template directories\n" \
" $CHIPPERDEBUG=1        - enable debug output\n";




enum class RunMode {Gui,Generator,Help,Version};

///parse command line, initialize application, start GUI or generator
int main(int ac,char**av)
{
    //check for gui/nogui arguments; bootstrap default pathes prefix
    bool gui=GUISTART;
    QStringList args;
#ifdef CHIPPER_GUIBUILD
    args=QStringList(av+1,av+ac);
    for(const auto&a:args){
        if(a=="-nogui" || a=="--nogui")gui=false;
        else if(a=="-gui" || a=="--gui")gui=true;
        else if(a.startsWith("-env=") || a.startsWith("--env="))
            setDefaultPath(ChipperPath::EnvPrefix,a.split('=').value(1));
        else if(a=="-log")activateLog();
        else if(a.startsWith("-log="))activateLog(a.mid(5));
    }
#endif
    //create app
    initChipperApp(ac,av,gui);
    //process arguments
    args=qApp->arguments();//re-filter
    QStringList files;
    RunMode rm=gui ? RunMode::Gui : RunMode::Generator;
    for(const auto&a:args.mid(1)){
        if(a.endsWith(".chip"))
            files.append(a);
        else if(a=="-generate" || a=="--generate" || a=="generate")
            rm=RunMode::Generator;
#ifdef CHIPPER_GUIBUILD
        else if(a=="-gui" || a=="--gui" || a=="gui")
            rm=RunMode::Gui;
        else if(a=="-nogui" || a=="--nogui" || a=="nogui")
            {/*ignore*/}
#endif
        else if(a=="-log" || a.startsWith("-log="))
            {/*already handled*/}
        else if(a=="-keep" || a=="--keep")
            Generator::setKeepTempDir(true);
        else if(a.startsWith("-root=") || a.startsWith("--root="))
            setDefaultPath(ChipperPath::Root,a.split('=').value(1));
        else if(a.startsWith("-symbol=") || a.startsWith("--symbol="))
            setDefaultPath(ChipperPath::Symbol,a.split('=').value(1));
        else if(a.startsWith("-footprint=") || a.startsWith("--footprint="))
            setDefaultPath(ChipperPath::Footprint,a.split('=').value(1));
        else if(a.startsWith("-3dmodel=") || a.startsWith("--3dmodel="))
            setDefaultPath(ChipperPath::Model3D,a.split('=').value(1));
        else if(a.startsWith("-template=") || a.startsWith("--template="))
            setDefaultPath(ChipperPath::Template,a.split('=').value(1));
        else if(a.startsWith("-envprefix=") || a.startsWith("--envprefix="))
            setDefaultPath(ChipperPath::EnvPrefix,a.split('=').value(1));
        else if(a=="-help" || a=="--help" || a=="help" || a=="-h" || a=="-?")
            rm=RunMode::Help;
        else if(a=="-version" || a=="--version" || a=="version" || a=="-v")
            rm=RunMode::Version;
        else{
            qDebug()<<"Error: unrecognized command line argument"<<a;
            qDebug()<<"Hint: try -help";
            return 1;
        }
    }
    //initialize app
    switch(rm){
#ifdef CHIPPER_GUIBUILD
        case RunMode::Gui:{
            if(gui==false){
                qDebug()<<"Error: schizophrenic action requested - cannot start GUI in non-GUI mode.";
                return 1;
            }
            qDebug()<<"Activating GUI mode...";
            ChipPool::instance().loadChips(files);
            auto *mw=new MainWindow;
            if(!mw->isVisible())
                mw->show();
            break;
        }
#endif
        case RunMode::Generator:{
            qDebug()<<"Running generator...";
            TemplatePool::instance();//forces template load
            Generator &g=Generator::instance();
            QObject::connect(&g,&Generator::poolFinished,qApp,&QCoreApplication::quit,Qt::QueuedConnection);
            ChipPool::instance().loadChips(files);
#ifdef CHIPPER_GUIBUILD
            if(gui){
                auto*pd=new QProgressDialog(QCoreApplication::translate("main","Generating Chips..."),QString(),0,100);
                QObject::connect(&g,&Generator::startGenerating,pd,std::bind(&QProgressDialog::setRange,pd,0,std::placeholders::_1));
                QObject::connect(&g,&Generator::progress,pd,&QProgressDialog::setValue);
                QObject::connect(&g,&Generator::progress,qApp,[]{QCoreApplication::processEvents();});
                QObject::connect(&g,&Generator::poolFinished,pd,&QWidget::close);
                pd->setAttribute(Qt::WA_DeleteOnClose);
                pd->show();
                QCoreApplication::processEvents();
            }
#endif
            QTimer::singleShot(0,&g,&Generator::createPool);
            break;
        }
        case RunMode::Help:{
            qDebug().noquote()<<HELP;
            return 0;
        }
        case RunMode::Version:{
            qDebug().noquote()<<Chipper::Util::versionInfo();
            return 0;
        }
        default:{
            qDebug()<<"Error: unknown run mode. Giving up.";
            return 2;
        }
    }
    //event loop
    return qApp->exec();
    //cleanup is automatic: AppKiller will delete qApp
}

//End of namespace
}

///\private real main
int main(int ac,char**av){return Chipper::main(ac,av);}
