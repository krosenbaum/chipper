// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for chip files
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipgen.h"
#include "filegen.h"

#include "chipdata.h"
#include "chipvariant.h"
#include "chippackage.h"
#include "chipfile.h"
#include "tracer.h"

#include <QDebug>


// ////////////////////////////////////
// Chip File Generator


ChipGen::ChipGen(ChipData*cd, QObject* parent)
:QObject(parent),mchip(cd)
{
    //TODO: calculate pathes
}

ChipData * ChipGen::chip() const
{
    return mchip;
}

void ChipGen::convert()
{
    qDebug()<<"...converting chip"<<mchip->metaData().manufacturer()<<mchip->metaData().chipType();
    emit progress(0);
    auto&trc=Tracer::instance();
    auto r=trc.generateChipCall(this);
    if(r.isContinue()){
        const auto allv=mchip->allVariants();
        int num=1;
        const int max=allv.size();
        for(const auto&v:allv){
            VariantGen(this,v).convert();
            emit progress((num++)*100/max);
        }
    }
    emit progress(100);
}







// ////////////////////////////////////
// Chip Variant Generator

class VariantGen::Private
{
    friend class VariantGen;
    ChipGen*parent=nullptr;
    ChipVariant variant;
};

DEFINE_DPTR(VariantGen);

VariantGen::VariantGen(ChipGen*cg, const ChipVariant&cv)
:QObject(cg)
{
    d->parent=cg;
    d->variant=cv;
    connect(this,&VariantGen::error,cg,&ChipGen::error);
}

ChipGen * VariantGen::chipGenerator() const
{
    return d->parent;
}

ChipVariant VariantGen::variant() const
{
    return d->variant;
}

ChipData * VariantGen::chip() const
{
    return d->parent->chip();
}

void VariantGen::convert()
{
    qDebug()<<"   ...converting variant"<<d->variant.name()<<d->variant.uid();
    auto &trc=Tracer::instance();
    auto r=trc.generateVariantCall(this);
    if(r.isContinue())
        for(auto id:d->variant.packageIds())
            PackageGen(this,d->variant.packageById(id)).convert();
}








// ////////////////////////////////////
// Chip Package Generator

class PackageGen::Private
{
    friend class PackageGen;
    VariantGen*parent=nullptr;
    ChipPackage package;
};

DEFINE_DPTR(PackageGen);

PackageGen::PackageGen(VariantGen*vg, const ChipPackage&cp)
:QObject(vg)
{
    d->parent=vg;
    d->package=cp;
    connect(this,&PackageGen::error,vg,&VariantGen::error);
}

ChipPackage PackageGen::package() const
{
    return d->package;
}

ChipVariant PackageGen::variant() const
{
    return d->parent->variant();
}

ChipData * PackageGen::chip() const
{
    return d->parent->chip();
}

ChipGen * PackageGen::chipGenerator() const
{
    return d->parent->chipGenerator();
}

VariantGen * PackageGen::variantGenerator() const
{
    return d->parent;
}

void PackageGen::convert()
{
    qDebug()<<"      ...converting package"<<d->package.displayName()<<d->package.uid();
    auto &trc=Tracer::instance();
    auto r=trc.generatePackageCall(this);
    if(r.isContinue()){
        for(auto id:d->package.symbolIds())
            FileGen(this,d->package.symbol(id)).convert();
        for(auto id:d->package.footprintIds())
            FileGen(this,d->package.footprint(id)).convert();
        for(auto id:d->package.modelIds())
            FileGen(this,d->package.model(id)).convert();
    }
}


#include "moc_chipgen.cpp"
