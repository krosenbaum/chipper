// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "gtempfile.h"
#include "stages.h"
#include "formula.h"
#include "chipper.h"

#include <QDebug>
#include <QDir>
#include <QDomAttr>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>
#include <QList>

#include "DomNodeIterator"
#include "DomNodeList"

#include "ELAM/ArrayEngine"

// #include <functional>

#define xDebug if(isDebugMode())qDebug

namespace Chipper { namespace Generate {

// //////////////////////////////////////
// constants

///standard sections
static const QString
    sectionAreaPrefix="AREA:",
    sectionProlog="PROLOG",
    sectionPreCalc="PRE",
    sectionPostRun="POST",
    sectionTemplate="TEMPLATE",
    sectionEpilog="EPILOG",
    sectionComment="COMMENT",
    sectionSkip="SKIP",
    sectionEnd="END";

///special commands
static const QString
    commandIf = "IF",
    commandElse = "ELSE",
    commandEnd = "END",
    commandFor = "FOR",
    commandForEach = "FOREACH",
    commandWhile = "WHILE",
    commandEval = "EVAL",
    commandOutput = "OUTPUT";

// //////////////////////////////////////
// Template File: Line

GenTemplateFile::Line::Line(QString t, int n, QString c)
:mtext(t),mnum(n)
{
    if(!c.isEmpty() && t.trimmed().startsWith(c)){
        mcmd=t.indexOf(c)+c.size();
    }
}

bool GenTemplateFile::Line::isArea() const
{
    return commandLine().startsWith(sectionAreaPrefix);
}

bool GenTemplateFile::Line::isInlineComment() const
{
    return commandLine().trimmed().startsWith('!');
}

bool GenTemplateFile::Line::isStartCommand() const
{
    if(!isCommand())return false;
    const QString cn=commandName();
    return cn==commandFor || cn==commandForEach || cn == commandWhile || cn==commandIf;
}

bool GenTemplateFile::Line::isEndCommand() const
{
    if(!isCommand())return false;
    return commandName()==commandEnd;
}

bool GenTemplateFile::Line::isIfCommand() const
{
    if(!isCommand())return false;
    return commandName()==commandIf;
}

bool GenTemplateFile::Line::isLoopCommand() const
{
    if(!isCommand())return false;
    const QString cn=commandName();
    return cn==commandFor || cn==commandForEach || cn == commandWhile;
}

bool GenTemplateFile::Line::isEvalCommand() const
{
    if(!isCommand())return false;
    return commandName()==commandEval;
}

bool GenTemplateFile::Line::isOutputCommand() const
{
    if(!isCommand())return false;
    return commandName()==commandOutput;
}

QString GenTemplateFile::Line::commandName() const
{
    if(mcmd<0)return QString();
    int p=0,e;
    const QString c=mtext.mid(mcmd);
    if(c.isEmpty())return QString();
    //skip spaces
    while(p<c.size() && c[p].isSpace())p++;
    //find end of command
    for(e=p;e<c.size();e++)
        if(c[e].isSpace())break;
    //split
    xDebug()<<"line"<<mtext<<"yields command"<<c.mid(p,e-p);
    return c.mid(p,e-p);
}

GenTemplateFile::LineList GenTemplateFile::Line::commandParsed(const GenStage&stage) const
{
    LineList ret;
    int p=mcmd;
    //find command
    while(p<mtext.size() && mtext[p].isSpace())p++;
    //skip command
    while(p<mtext.size() && !mtext[p].isSpace())p++;

    //find parameters
    const QString otag=stage.openTag();
    const QString ctag=stage.closeTag();
    while(p<mtext.size()){
        //reduce whitespace on left
        while(p<mtext.size() && mtext[p].isSpace())p++;
        if(p>=mtext.size())break;
        //what kind of argument is it?
        if(mtext[p]=='!')//comment, skip the rest
            break;
        if(mtext.mid(p,otag.size())==otag){
            //it's a formula: find closing tag
            int e=mtext.indexOf(ctag,p);
            if(e<0){
                qDebug()<<"Warning: line"<<mnum<<"opening tag on column"<<p<<"has no closing tag, adding it to the end.";
                ret<<Line(mtext.mid(p)+ctag,p);
                break;
            }else{
                ret<<Line(mtext.mid(p,e-p+ctag.size()),p);
                p=e+ctag.size();
                continue;
            }
        }else{
            //it's a normal token: find space or end
            int e=p;
            while(e<mtext.size() && !mtext[e].isSpace())e++;
            ret<<Line(mtext.mid(p,e-p),p);
            p=e;
            continue;
        }
    }

    return ret;
}

bool GenTemplateFile::Line::isFormula(const GenStage&stage) const
{
    return mtext.startsWith(stage.openTag());
}

QString GenTemplateFile::Line::formulaText(const GenStage&stage) const
{
    return mtext.mid(stage.openTag().size(),mtext.size()-stage.openTag().size()-stage.closeTag().size());
}

GenTemplateFile::LineList GenTemplateFile::Line::lineParsed(const GenStage&stage) const
{
    LineList ret;

    const QString otag=stage.openTag();
    const QString ctag=stage.closeTag();

    int p=0,e=0;
    while(p<mtext.size()){
        //find next open tag
        e=mtext.indexOf(otag,p);
        if(e<0){
            //no more open tags, consume line and stop
            ret<<Line(mtext.mid(p),p);
            break;
        }
        if(e>p){
            //normal text before open tag, add it
            ret<<Line(mtext.mid(p,e-p),p);
        }
        p=e;
        //find close tag
        e=mtext.indexOf(ctag,p);
        if(e<0){
            //oops! no close tag
            qDebug()<<"Warning: on line"<<mnum<<"column"<<p<<"missing close tag, adding it to the end.";
            ret<<Line(mtext.mid(p)+ctag,p);
            break;
        }else{
            e+=ctag.size();
            ret<<Line(mtext.mid(p,e-p),p);
            p=e;
            continue;
        }
    }

    return ret;
}


QDebug operator<<(QDebug debug, const GenTemplateFile::Line& line)
{
    QDebugStateSaver sav(debug);
    debug.nospace()<<"Line("<<line.text()<<",line="<<line.lineNumber()<<",isCmd="<<line.isCommand()<<")";
    return debug;
}


// //////////////////////////////////////
// Template File: LineList

QString GenTemplateFile::LineList::joinText(QString s) const
{
    QString ret;
    bool first=true;
    for(const auto & sub:*this){
        if(first)first=false;
        else ret+=s;
        ret+=sub.text();
    }
    return ret;
}


// //////////////////////////////////////
// Template File

GenTemplateFile::GenTemplateFile(QString fname, QString prefix_)
:mfname(fname),mprefix(prefix_)
{
    QFile fd(fname);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Warning: unable to open template file"<<fname<<":"<<fd.errorString();
        merror=true;
        return;
    }
    qDebug()<<"Parsing template file"<<fname;
    //find sections
    QString csec=sectionSkip;
    int lnum=0;
    for(const QString&line:QString::fromUtf8(fd.readAll()).split('\n')){
        lnum++;
        const QString l=line.trimmed();
        if(l.startsWith(mprefix+sectionAreaPrefix)){
            csec=l.mid(mprefix.size()+sectionAreaPrefix.size()).toUpper();
            continue;
        }
        if(!msections.contains(csec))msections.insert(csec,LineList());
        msections[csec].append(Line(line,lnum,mprefix));
    }
    fd.close();
    //compress: remove heading and trailing empty lines
    for(const QString&k:msections.keys()){
        LineList&l=msections[k];
        while(l.size()>0 && l[0].trimmed().isEmpty())l.takeFirst();
        while(l.size()>0 && l[l.size()-1].trimmed().isEmpty())l.takeLast();
    }
    //remove comment and skip sections
    if(msections.contains(sectionComment))msections.remove(sectionComment);
    if(msections.contains(sectionSkip))msections.remove(sectionSkip);
    //rule checks
    if(!msections.contains(sectionTemplate))
        qDebug()<<"Warning: missing TEMPLATE section in"<<fname;
    if(msections.contains(sectionEnd)){
        if(msections[sectionEnd].size()>0)
            qDebug()<<"Warning: content after END in"<<fname;
        msections.remove(sectionEnd);
    }
    if(msections.contains(sectionProlog)){
        if(msections[sectionProlog].size()==0)msections.remove(sectionProlog);
    }
    if(msections.contains(sectionEpilog)){
        if(msections[sectionEpilog].size()==0)msections.remove(sectionEpilog);
    }
    //check that only known sections exist
    static const QStringList knownSections{sectionEpilog,sectionPreCalc,sectionProlog,sectionTemplate,sectionPostRun};
    for(const QString &s:msections.keys())
        if(!knownSections.contains(s))
            qDebug()<<"Warning: template file"<<fname<<"contains unknown section"<<s<<"- it will be ignored.";
}

bool GenTemplateFile::processScript(QFile&outfd, Formula&form,const GenStage&stage)
{
    if(!hasSection(sectionTemplate)){
        qDebug()<<"Error: oops. Template file"<<mfname<<"has no Template section. Giving up.";
        return false;
    }
    //go through pre-section of script, output directly
    if(hasSection(sectionPreCalc)){
        QStringList sec;
        moutgen=true;moutempty=true;mouttrim=false;
        if(processSection(sec,msections[sectionPreCalc],form,stage)){
            if(sec.size()>0)
                qDebug().noquote().nospace()<<"Pre-generator output:\n"<<sec.join('\n');
        }else{
            qDebug()<<"Error while running pre-generator section. Giving up on this file.";
            return false;
        }
    }
    //process prolog and remember for later
    QString prolog;
    if(hasSection(sectionProlog)){
        moutgen=true;moutempty=true;mouttrim=false;
        QStringList sec;
        if(!processSection(sec,msections[sectionProlog],form,stage)){
            qDebug()<<"Error while running PROLOG section. Giving up.";
            return false;
        }
        prolog=sec.join('\n')+'\n';
    }
    //go through main section of script, generate as string
    QStringList mainsec;
    moutgen=true;moutempty=true;mouttrim=false;
    if(!processSection(mainsec,msections[sectionTemplate],form,stage)){
        qDebug()<<"Error while generating output, not writing anything.";
        return false;
    }
    //process epilog and remember for later
    QString epilog;
    if(hasSection(sectionEpilog)){
        //generate
        moutgen=true;moutempty=false;mouttrim=false;
        QStringList sec;
        if(!processSection(sec,msections[sectionEpilog],form,stage)){
            qDebug()<<"Error while running EPILOG section. Giving up.";
            return false;
        }
        //reduce
        QStringList red;
        for(const QString&l:sec)
            if(!l.trimmed().isEmpty())red<<l;
        //check
        if(red.size()>1){
            qDebug()<<"Warning: EPILOG produces more than one line! Ignoring all but one.";
        }
        if(red.size()>0)
            epilog=red[0];
    }
    //check for prolog vs. epilog
    if(outfd.size()==0){
        //empty file: add prolog
        if(!prolog.isEmpty())
            outfd.write(prolog.toUtf8());
    }else{
        //non-empty, search for epilog and remove it
        if(!epilog.isEmpty()){
            //read epilog size+256 (should be enough)
            const int chunksize=epilog.toUtf8().size()+256;
            int pos=outfd.size()-chunksize;
            if(pos<0)pos=0;
            outfd.seek(pos);
            const auto chunk=outfd.read(chunksize);
            //find lines, eliminate empty ones, check for epilog
            int epos=chunk.size();
            bool found=false;
            do{
                int spos=chunk.lastIndexOf('\n',epos-1);
                if(spos<0)spos=0;
                auto line=chunk.mid(spos,epos-spos).trimmed();
                epos=spos;
                if(line.isEmpty())continue;
                //non-empty
                if(QString::fromUtf8(line.trimmed()) == epilog.trimmed()){
                    found=true;
                    break;
                }else{
                    break;
                }
            }while(epos>0);
            //eliminate
            if(found){
                int pos=outfd.size()-(chunk.size()-epos)+1;
                outfd.resize(pos);
                outfd.seek(pos);
            }else //not found
                outfd.seek(outfd.size());
        }else //no epilog configured
            outfd.seek(outfd.size());
    }
    //add main section
    outfd.write(QString(mainsec.join('\n')+"\n").toUtf8());
    //add epilog
    if(!epilog.isEmpty()){
        outfd.write(QString(epilog+"\n").toUtf8());
    }
    //go through post-section of script, output directly
    if(hasSection(sectionPostRun)){
        QStringList sec;
        moutgen=true;moutempty=true;mouttrim=false;
        if(processSection(sec,msections[sectionPostRun],form,stage)){
            if(sec.size()>0)
                qDebug().noquote().nospace()<<"Post-generator output:\n"<<sec.join('\n');
        }else{
            qDebug()<<"Warning: Error while running post-generator section. Shrugging shoulders and moving on.";
        }
    }
    //all done
    return true;
}

bool GenTemplateFile::processSection(
    QStringList& out, const LineList& in,
    Chipper::Generate::Formula&form, const Chipper::Generate::GenStage&stage)
{
    enum class PMode {
        //ordinary text
        Normal,
        //for,foreach,while
        Loop,
        //if - main branch
        If,
        //if - else branch
        IfElse
    };
    PMode mode=PMode::Normal;
    int depth=0;
    LineList sublist,subelselist;
    Line command;
    //go through lines
    for(int i=0;i<in.size();i++){
        //get my bearings...
        const Line &line=in[i];
        //main evaluation level...
        if(depth==0){
            if(line.isCommand()){
                //special line: look into it
                if(line.isArea()||line.isInlineComment())continue;
                if(line.isEndCommand()){
                    qDebug()<<"Spurious ##END command in"<<mfname<<"line"<<line.lineNumber()<<"- aborting.";
                    return false;
                }
                if(line.commandName()==commandElse){
                    qDebug()<<"Spurious ##ELSE command in"<<mfname<<"line"<<line.lineNumber()<<"- aborting.";
                    return false;
                }
                if(line.isLoopCommand()){
                    mode=PMode::Loop;
                    depth=1;
                    command=line;
                    sublist.clear();
                    continue;
                }
                if(line.isIfCommand()){
                    mode=PMode::If;
                    depth=1;
                    command=line;
                    sublist.clear();
                    subelselist.clear();
                    continue;
                }
                if(line.isEvalCommand()){
                    //process (like a normal line) and discard (we are interested in the side effect of calculating)
                    LineList tokens=line.lineParsed(stage);
                    for(const Line &tok:tokens)
                        if(tok.isFormula(stage)){
                            auto r=form.evaluate(tok.formulaText(stage));
                            if(r.canConvert<ELAM::Exception>()){
                                qDebug()<<"Error: exception while evaluating"<<tok.text()<<"file"<<mfname<<"line"<<line.lineNumber()<<"column"<<tok.lineNumber();
                                qDebug()<<"   details:"<<r.value<ELAM::Exception>();
                                return false;
                            }
                        }
                    continue;
                }
                if(line.isOutputCommand()){
                    //process command tokens as plain text
                    LineList tokens=line.commandParsed(stage);
                    for(const Line &tok:tokens){
                        const auto txt=tok.text().trimmed().toLower();
                        if(txt.isEmpty())continue;
                        if(txt=="on"||txt=="yes"||txt=="true")moutgen=true;
                        else if(txt=="off"||txt=="no"||txt=="false")moutgen=false;
                        else if(txt=="empty")moutempty=true;
                        else if(txt=="noempty")moutempty=false;
                        else if(txt=="trim")mouttrim=true;
                        else if(txt=="notrim")mouttrim=false;
                        else{
                            qDebug()<<"Unknown ##OUTPUT option"<<txt<<"on line"<<line.lineNumber()<<"column"<<tok.lineNumber();
                            return false;
                        }
                    }
                    continue;
                }
                //unknown command
                qDebug()<<"Unknown command line"<<line.trimmed()<<"in file"<<mfname<<"line"<<line.lineNumber()<<"- ignoring it.";
                continue;
            }else{
                //normal line, process it
                LineList tokens=line.lineParsed(stage);
                QString ln;
                for(const Line &tok:tokens)
                    if(tok.isFormula(stage)){
                        auto r=form.evaluate(tok.formulaText(stage));
                        if(r.canConvert<ELAM::Exception>()){
                            qDebug()<<"Error: exception while evaluating"<<tok.text()<<"file"<<mfname<<"line"<<line.lineNumber()<<"column"<<tok.lineNumber();
                            qDebug()<<"   details:"<<r.value<ELAM::Exception>();
                            return false;
                        }
                        ln+=r.toString();
                    }else
                        ln+=tok.text();
                //output handling
                if(moutgen){
                    if(!moutempty && ln.trimmed().isEmpty())continue;
                    if(mouttrim)ln=ln.trimmed();
                    out<<ln;
                }
                continue;
            }
        }
        //inside special construct
        if(depth==1){
            //look for end
            if(line.isEndCommand()){
                depth--;
                //what command is this?
                if(command.isIfCommand()){
                    if(!processIfSection(command,out,sublist,subelselist,form,stage))return false;
                    //done with IF
                    continue;
                }
                if(command.isLoopCommand()){
                    if(!processLoopSection(command,out,sublist,form,stage))return false;
                    continue;
                }
                //OOOPS! This should not happen.
                qDebug()<<"Error: template processor is high! Sending it home to count pink elephants!";
                qDebug()<<"  State: mode="<<(int)mode<<"; depth="<<depth<<"; file="<<mfname<<"; sublist.size="<<sublist.size()<<";subelselist.size="<<subelselist.size();
                qDebug()<<"  Current Command:"<<command;
                qDebug()<<"  Current line:"<<line;
                return false;
            }
            //look for else
            if(line.commandName()==commandElse){
                //is this correct?
                if(mode==PMode::If){
                    mode=PMode::IfElse;
                    continue;
                }else{
                    qDebug()<<"Error: spurious ##ELSE in file"<<mfname<<"line"<<line.lineNumber()<<"- aborting.";
                    return false;
                }
            }
            //store
            if(mode==PMode::IfElse)subelselist.append(line);
            else sublist.append(line);
            //check for deeper levels
            if(line.isStartCommand())depth++;
            continue;
        }
        //inside deep level
        if(depth>1){
            //store the line
            if(mode==PMode::IfElse)subelselist.append(line);
            else sublist.append(line);
            //count levels...
            if(line.isCommand()){
                if(line.isStartCommand())depth++;
                else if(line.isEndCommand())depth--;
            }
            continue;
        }
        //oops! This should not happen.
        qDebug()<<"Error: template processor is drunk! Sending it home to sober up!";
        qDebug()<<"  State: mode="<<(int)mode<<"; depth="<<depth<<"; file="<<mfname<<"; sublist.size="<<sublist.size()<<";subelselist.size="<<subelselist.size();
        qDebug()<<"  Current Command:"<<command;
        qDebug()<<"  Current line:"<<line;
        return false;
    }
    //sanity check
    if(depth>0){
        qDebug()<<"Error: template file"<<mfname<<"is not properly terminated - missing an ##END. Command:"<<command<<"Line:"<<in.value(in.size()-1).lineNumber();
        return false;
    }
    //done
    return true;
}

bool GenTemplateFile::processIfSection(const Line& command, QStringList& out, const LineList& in, const LineList& inelse, Formula&form, const GenStage&stage)
{
    auto par=command.commandParsed(stage);
    if(par.size()!=1){
        qDebug()<<"Error: ##IF expects exactly one argument in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
        return false;
    }
    if(!par[0].isFormula(stage)){
        qDebug()<<"Error: expecting argument to ##IF to be a formula - did you forget the tags? In"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
        return false;
    }
    auto r=form.evaluate(par[0].formulaText(stage));
    if(r.canConvert<ELAM::Exception>()){
        qDebug()<<"Error: exception while evaluating condition in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
        qDebug()<<"  Details:"<<r.value<ELAM::Exception>();
        return false;
    }
    if(!r.canConvert<bool>()){
        qDebug()<<"Error: expecting condition to evaluate to bool in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
        qDebug()<<"   Result:"<<r;
        return false;
    }
    if(r.toBool())
        return processSection(out,in,form,stage);
    else
        return processSection(out,inelse,form,stage);
}

bool GenTemplateFile::processLoopSection(const Line& command, QStringList& out, const LineList& in, Formula&form, const GenStage&stage)
{
    const QString cn=command.commandName();
    auto par=command.commandParsed(stage);
    //FOR
    if(cn==commandFor){
        if(par.size()!=3){
            qDebug()<<"Error: expecting 3 parameters to ##FOR loop, got"<<par.size()<<"in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        if(!par[0].isFormula(stage) || !par[1].isFormula(stage) || !par[2].isFormula(stage)){
            qDebug()<<"Error: parameters to ##FOR must be expressions - did you forget tags? In"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        //init loop
        auto r=form.evaluate(par[0].formulaText(stage));
        if(r.canConvert<ELAM::Exception>()){
            qDebug()<<"Error: ##FOR loop initialization"<<par[0].text()<<"yields exception in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            qDebug()<<"   Details:"<<r.value<ELAM::Exception>();
            return false;
        }
        //go through iterations
        const QString check=par[1].formulaText(stage);
        const QString post=par[2].formulaText(stage);
        int count=-1;
        while(true){
            count++;
            //eval iteration check
            r=form.evaluate(check);
            if(r.canConvert<ELAM::Exception>()){
                qDebug()<<"Error: ##FOR loop iteration check"<<par[0].text()<<"yields exception in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
                qDebug()<<"   Iteration #:"<<count;
                qDebug()<<"   Details:"<<r.value<ELAM::Exception>();
                return false;
            }
            if(!r.canConvert<bool>()){
                qDebug()<<"Error: expecting ##FOR loop iteration check"<<par[0].text()<<"to be of bool type in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
                qDebug()<<"   Iteration #:"<<count;
                qDebug()<<"   Actual Value:"<<r;
                return false;
            }
            if(!r.toBool())return true;
            //run body
            if(!processSection(out,in,form,stage))return false;
            //post-iteration eval
            r=form.evaluate(post);
            if(r.canConvert<ELAM::Exception>()){
                qDebug()<<"Error: ##FOR loop iteration check"<<par[0].text()<<"yields exception in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
                qDebug()<<"   Iteration #:"<<count;
                qDebug()<<"   Details:"<<r.value<ELAM::Exception>();
                return false;
            }
        }
        //unreachable
        qDebug()<<"Ooooops. For Loop implementation has an internal error!";
        return false;
    }
    //FOREACH
    if(cn==commandForEach){
        if(par.size()!=2){
            qDebug()<<"Error: expecting 2 parameters to ##FOREACH loop, got"<<par.size()<<"in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        if(par[0].isFormula(stage) || !par[1].isFormula(stage)){
            qDebug()<<"Error: parameters to ##FOREACH must be of the form [key,]value {{expression}}. In"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        //parse key-value
        const QStringList kv=par[0].text().split(',',Qt::SkipEmptyParts);
        if(kv.size()<1 || kv.size()>2){
            qDebug()<<"Error: first parameter to ##FOREACH should be 'variablename' or 'keyVarName,valueVarName' in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        const QString keyvar=kv.size()>1?kv[0]:QString();
        const QString valvar=kv[kv.size()-1];
        //evaluate formula
        auto r=form.evaluate(par[1].formulaText(stage));
        if(r.canConvert<ELAM::Exception>()){
            qDebug()<<"Error: ##FOREACH loop array expression"<<par[1].text()<<"yields exception in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            qDebug()<<"   Details:"<<r.value<ELAM::Exception>();
            return false;
        }
        //go through
        auto loopiter=[&](const QVariant&key,const QVariant&value)->bool{
            if(!keyvar.isEmpty())form.setVariable(keyvar,key);
            form.setVariable(valvar,value);
            return processSection(out,in,form,stage);
        };
        return ELAM::ArrayEngine::iterate(r,loopiter);
    }
    //WHILE
    if(cn==commandWhile){
        if(par.size()!=1){
            qDebug()<<"Error: expecting 2 parameters to ##FOREACH loop, got"<<par.size()<<"in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        if(!par[0].isFormula(stage)){
            qDebug()<<"Error: parameter to ##WHILE must be an {{expression}}. In"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
            return false;
        }
        //get formula
        const QString cond=par[0].formulaText(stage);
        //loopy
        int count=-1;
        while(true){
            count++;
            //condition check
            auto r=form.evaluate(cond);
            if(r.canConvert<ELAM::Exception>()){
                qDebug()<<"Error: ##WHILE loop iteration check"<<cond<<"yields exception in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
                qDebug()<<"   Iteration #:"<<count;
                qDebug()<<"   Details:"<<r.value<ELAM::Exception>();
                return false;
            }
            if(!r.canConvert<bool>()){
                qDebug()<<"Error: expecting ##FOR loop iteration check"<<par[0].text()<<"to be of bool type in"<<mfname<<"line"<<command.lineNumber()<<"- aborting.";
                qDebug()<<"   Iteration #:"<<count;
                qDebug()<<"   Actual Value:"<<r;
                return false;
            }
            if(!r.toBool())return true;
            //run body
            if(!processSection(out,in,form,stage))return false;
        }
        //unreachable
        qDebug()<<"Ooooops. While Loop implementation has an internal error!";
        return false;
    }
    //OOOPS - should be unreachable
    qDebug()<<"Error: Ooops. Loop command"<<command.commandName()<<"apparently not implemented. Am I sane? Aborting.";
    return false;
}

//end of namespace
}}
