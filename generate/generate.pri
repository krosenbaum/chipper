SOURCES += \
    $$PWD/chipgen.cpp \
    $$PWD/filegen.cpp \
    $$PWD/generator.cpp \
    $$PWD/gtempfile.cpp \
    $$PWD/stages.cpp \
    $$PWD/tracer.cpp

HEADERS += \
    $$PWD/chipgen.h \
    $$PWD/filegen.h \
    $$PWD/generator.h \
    $$PWD/gtempfile.h \
    $$PWD/stages.h \
    $$PWD/tracer.h

INCLUDEPATH += $$PWD
