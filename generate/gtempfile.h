// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFile;

namespace Chipper { namespace Generate {

class Formula;
class GenStage;

///template file referenced from generator script
class CHIPPER_EXPORT GenTemplateFile
{
public:
    ///create a copy of the file
    GenTemplateFile(const GenTemplateFile&)=default;
    ///instantiate from file
    /// \param fname file name
    /// \param prefix the prefix characters for special commands
    GenTemplateFile(QString fname,QString prefix);

    ///copy the file
    GenTemplateFile& operator=(const GenTemplateFile&)=default;

    class LineList;
    ///helper class representing a single line in the template
    class Line {
        friend class GenTemplateFile;
        ///instantiate from parent
        Line(QString t,int n,QString c=QString());
        QString mtext;
        int mnum=-1,mcmd=-1;
    public:
        ///instantiate invalid line
        Line()=default;
        ///copy line
        Line(const Line&)=default;
        ///move line
        Line(Line&&)=default;
        ///copy line
        Line& operator=(const Line&)=default;
        ///move line
        Line& operator=(Line&&)=default;

        ///true if this is a valid line from the template file
        bool isValid()const{return !mtext.isEmpty() || mnum>0;}
        ///returns true if this is a special command line
        bool isCommand()const{return mcmd>=0;}
        ///returns true if this is an inline comment or empty command
        bool isInlineComment()const;
        ///returns true if this is an area line
        bool isArea()const;
        ///returns true if this is the start of if/for/while...
        bool isStartCommand()const;
        ///returns true if this is an ##END command
        bool isEndCommand()const;
        ///true if this is an ##IF
        bool isIfCommand()const;
        ///true if this is a loop
        bool isLoopCommand()const;
        ///true if this is an eval command
        bool isEvalCommand()const;
        ///true if this is an output control command
        bool isOutputCommand()const;

        ///returns the complete text of the line
        QString text()const{return mtext;}
        ///returns the trimmed text of the line
        QString trimmed()const{return mtext.trimmed();}

        ///for special command lines returns the part starting at the command (excluding the prefix), otherwise empty string
        QString commandLine()const{if(mcmd>=0)return mtext.mid(mcmd).trimmed();else return QString();}
        ///for special command lines: returns the command name only (for areas: including "AREA:")
        QString commandName()const;
        ///For special command lines: returns the parsed command line.
        ///Each sub-token or formula gets its own token. The lineNumber() function points to the column instead.
        ///The command name and inline comments get discarded.
        LineList commandParsed(const GenStage&)const;

        ///For normal non-command lines: return it parsed into sections alternating between ordinary text and formulae.
        ///In the returned values the lineNumber property holds the column instead.
        LineList lineParsed(const GenStage&)const;
        ///true if this is a formula according to the stage definition
        bool isFormula(const GenStage&)const;
        ///returns the pure formula text, stripped of open/close tags
        QString formulaText(const GenStage&)const;

        ///returns the line number this instance represents
        int lineNumber()const{return mnum;}
    };
    ///type alias for list of lines, used for representing entire sections
    class LineList : public QList<Line> {
    public:
        ///create empty list
        LineList()=default;
        ///copy list
        LineList(const LineList&)=default;
        ///move list
        LineList(LineList&&)=default;
        ///copy list
        LineList& operator=(const LineList&)=default;
        ///move list
        LineList& operator=(LineList&&)=default;
        ///copy list
        LineList(const QList<Line>&l):QList<Line>(l){}
        ///move list
        LineList(QList<Line>&&l):QList<Line>(l){}

        ///join the text of all lines in this list
        QString joinText(QChar c)const{return joinText(QString(c));}
        ///join the text of all lines in this list
        QString joinText(QString)const;
    };

    ///returns true if a specific section exists
    bool hasSection(QString name)const{return msections.contains(name);}
    ///returns the lines of the section
    LineList section(QString name)const{if(msections.contains(name))return msections[name];else return LineList();}

    ///returns the file name of this template file
    QString fileName()const{return mfname;}
    ///returns the command prefix
    QString prefix()const{return mprefix;}

    ///true if there was an error while processing the file
    bool error()const{return merror;}

    ///process a file
    bool processScript(QFile&,Formula&,const GenStage&);

private:
    QString mfname,mprefix;
    QMap<QString,LineList> msections; // sections of the template file: maps name -> lines
    bool merror=false;
    bool moutgen=true,moutempty=true,mouttrim=false;

    ///process a script section and add to stringlist
    bool processSection(QStringList&out,const LineList&in,Formula&,const GenStage&);
    ///process a loop inside a section
    bool processLoopSection(const Line &command,QStringList&out,const LineList&in,Formula&,const GenStage&);
    ///process an if inside a section
    bool processIfSection(const Line &command,QStringList&out,const LineList&in,const LineList&inelse,Formula&,const GenStage&);
};

//end of namespace
}}
