// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for chip files
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QPointer>

#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip {
class ChipData;
class ChipVariant;
class ChipPackage;
}
using namespace Chip;

namespace Generate {

///Generates a complete chip definition.
class CHIPPER_EXPORT ChipGen:public QObject
{
    Q_OBJECT
    QPointer<ChipData>mchip;
public:
    ///create the generator object for a specific chip file
    explicit ChipGen(ChipData*,QObject*parent=nullptr);

    ///returns the chip file this generator is working on
    ChipData*chip()const;

public slots:
    ///converts the entire chip file into KiCAD files
    void convert();

signals:
    ///emitted if an error occurs
    void error(QString msg);
    ///emitted to report progress
    ///\param value - progress on a scale 0..100
    void progress(int);
};

///Generates a variant of a chip.
class CHIPPER_EXPORT VariantGen:public QObject
{
    Q_OBJECT
    DECLARE_DPTR(d);
    VariantGen(ChipGen*,const ChipVariant&);
    friend class ChipGen;
public:
    ///returns the parent chip generator
    ChipGen* chipGenerator()const;
    ///returns the chip file this generator is working on
    ChipData* chip()const;
    ///returns the variant handled by this instance
    ChipVariant variant()const;

public slots:
    ///converts the variant into KiCAD files
    void convert();

signals:
    ///emitted if an error occurs
    void error(QString);
};


///Generates a package of a variant of a chip...
class CHIPPER_EXPORT PackageGen:public QObject
{
    Q_OBJECT
    DECLARE_DPTR(d);
    friend class VariantGen;
    PackageGen(VariantGen*,const ChipPackage&);
public:
    ///returns the top chip generator
    ChipGen* chipGenerator()const;
    ///returns the parent variant generator
    VariantGen* variantGenerator()const;
    ///returns a pointer to the chip this is a part of...
    ChipData* chip()const;
    ///returns a reference to the variant this is a part of...
    ChipVariant variant()const;
    ///returns the package this instance is generating
    ChipPackage package()const;

public slots:
    ///converts the package into KiCAD files
    void convert();

signals:
    ///emitted if an error occurs
    void error(QString);
};

//end of namespace
}}
using namespace Chipper::Generate;
