// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator: trace facility
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "tracer.h"
#include "generator.h"
#include "filegen.h"
#include "chipgen.h"
#include "chipdata.h"
#include "chipvariant.h"
#include "chippackage.h"
#include "chipfile.h"

#include <QDebug>

Tracer & Tracer::instance()
{
    return Generator::tracer();
}

Tracer::CReturn Chipper::Generate::Tracer::generatorCall()
{
    auto r=emit generator(State::PreCall);
    CReturn cr(r,CReturn::Type::Gen,nullptr);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}

Tracer::CReturn Chipper::Generate::Tracer::generateChipCall(Chipper::Generate::ChipGen*g)
{
    auto r=emit generateChip(g,g->chip(),State::PreCall);
    CReturn cr(r,CReturn::Type::Chip,g);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}

Tracer::CReturn Chipper::Generate::Tracer::generateVariantCall(Chipper::Generate::VariantGen*g)
{
    auto r=emit generateVariant(g,g->variant(),State::PreCall);
    CReturn cr(r,CReturn::Type::Variant,g);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}

Tracer::CReturn Chipper::Generate::Tracer::generatePackageCall(Chipper::Generate::PackageGen*g)
{
    auto r=emit generatePackage(g,g->package(),State::PreCall);
    CReturn cr(r,CReturn::Type::Package,g);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}

Tracer::CReturn Chipper::Generate::Tracer::generateFileCall(Chipper::Generate::FileGen*g)
{
    Return r;
    auto tf=g->targetFile();
    switch(tf.fileType()){
        case ChipFileTarget::FileType::Symbol:
            r=emit generateSymbol(g,tf.toSymbol(),State::PreCall);
            break;
        case ChipFileTarget::FileType::Footprint:
            r=emit generateFootprint(g,tf.toFootprint(),State::PreCall);
            break;
        case ChipFileTarget::FileType::Model3D:
            r=emit generate3DModel(g,tf.toModel(),State::PreCall);
            break;
        default:
            qDebug()<<"Ooops! Tracer does not understand new Target File Type value"<<(int)tf.fileType()<<"in"<<__FILE__<<"line"<<__LINE__;
            return CReturn();
    }
    CReturn cr(r,CReturn::Type::File,g);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}

Tracer::CReturn Chipper::Generate::Tracer::generateFileStageCall(Chipper::Generate::FileGen*g, const Chipper::Generate::GenStage&s)
{
    auto r=emit generateFileStage(g,g->targetFile(),s,State::PreCall);
    CReturn cr(r,g,s);
    if(r==Return::Abort)
        throw AbortException();
    return cr;
}




void Chipper::Generate::Tracer::CReturn::detach()
{
    if(!d)return;
    d->ctr--;
    if(d->ctr>0){
        d=nullptr;
        return;
    }

    //move from PreCall to Success
    if(d->mret<1)d->mret=1;

    auto &trc=Tracer::instance();
    switch(d->mtyp){
        case Type::Gen:
            emit trc.generator(Tracer::State(d->mret));
            break;
        case Type::Chip:
            emit trc.generateChip((ChipGen*)d->mgen, ((ChipGen*)d->mgen)->chip(), Tracer::State(d->mret));
            break;
        case Type::Variant:
            emit trc.generateVariant((VariantGen*)d->mgen, ((VariantGen*)d->mgen)->variant(),Tracer::State(d->mret));
            break;
        case Type::Package:
            emit trc.generatePackage((PackageGen*)d->mgen, ((PackageGen*)d->mgen)->package(),Tracer::State(d->mret));
            break;
        case Type::File:{
            FileGen*g=(FileGen*)d->mgen;
            auto tf=g->targetFile();
            switch(tf.fileType()){
                case ChipFileTarget::FileType::Symbol:
                    emit trc.generateSymbol(g,tf.toSymbol(),State(d->mret));
                    break;
                case ChipFileTarget::FileType::Footprint:
                    emit trc.generateFootprint(g,tf.toFootprint(),State(d->mret));
                    break;
                case ChipFileTarget::FileType::Model3D:
                    emit trc.generate3DModel(g,tf.toModel(),State(d->mret));
                    break;
                case ChipFileTarget::FileType::Invalid:
                    qDebug()<<"Ouch! Invalid file being generated! Aborting tracer before something bad happens.";
                    return;
            }
            break;
        }
        case Type::Stage:{
            FileGen*g=(FileGen*)d->mgen;
            auto tf=g->targetFile();
            emit trc.generateFileStage(g,tf,*(d->mstage),State(d->mret));
        }
    }

    delete d;
    d=nullptr;
}


#include "moc_tracer.cpp"
