// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QDebug>
#include <QDir>
#include <QDomAttr>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>
#include <QList>

#include "DomNodeIterator"
#include "DomNodeList"

#include "stages.h"
#include "formula.h"
#include "chipper.h"

//verbose debugging
#define xDebug if(isDebugMode())qDebug

namespace Chipper { namespace Generate {

const GenStage::DefStageC GenStage::DefaultStage;

// //////////////////////////////////////
// Script Stages

//part of FileGen::parseGeneratorScript
GenStage::GenStage(const QDomElement &tag,const QString&fname)
:cmd(tag.attribute("cmd")), in(tag.attribute("in")), out(tag.attribute("out")),
 wd(tag.attribute("wd")), pfx(tag.attribute("prefix","##")),filename(fname),
 linenum(tag.lineNumber()),tmout(tag.attribute("timeout","30").toInt())
{
    //parameters
    const QString sep_s=tag.attribute("sep"," ");
    QChar sep=' ';
    if(!sep_s.isEmpty())sep=sep_s[0];
    if(sep_s.size()>1)
        qDebug()<<"Warning: cannot have more than one separator char, using only the first one of"<<sep_s
                <<"on line"<<tag.lineNumber()<<"column"<<tag.columnNumber();
    params=tag.text().trimmed().split(sep,sep==' '?Qt::SkipEmptyParts:Qt::KeepEmptyParts);
    //mode
    const QString mode=tag.attribute("mode","cmd").trimmed().toLower();
    if(mode.startsWith("parse"))md=Mode::Parser;
    else if(mode.startsWith("skip"))md=Mode::Skip;
    else if(mode=="cmd" || mode=="command")md=Mode::Command;
    else if(mode=="cp" || mode=="copy" || mode=="mv" || mode=="move" || mode=="rm" || mode=="del" || mode=="delete" || mode=="remove"){
        md=Mode::FileOp;
        cmd=mode.left(1);
    }
    else if(mode.startsWith("stage")){
        if(tag.tagName()=="Stage"){
            md=Mode::Skip;
            qDebug()<<"Recursive Stage in"<<fname<<"line"<<tag.lineNumber()<<"char"<<tag.columnNumber()<<"- skipping this stage.";
        }else md=Mode::Stages;
    }else {
        qDebug()<<"Unknown stage type"<<mode<<"in"<<fname<<"line"<<tag.lineNumber()<<"char"<<tag.columnNumber()<<"- skipping this stage.";
        md=Mode::Skip;
    }
    //timeout check
    if(tmout<=0)tmout=30;
}

QString GenStage::toString() const
{
    return
    QString("XML file %1 line %2:\n")
        .arg(filename).arg(linenum)
        +
    QString("\tmode=%1 command='%2' in='%3' out='%4' timeout=%5\n")
        .arg(modeToString(md)).arg(cmd).arg(in).arg(out).arg(wd).arg(tmout)
        +
    QString("\tworkDir='%1' prefix='%2'\n")
        .arg(wd).arg(pfx)
        +
    QString("\tparameters: %1")
        .arg(params.join(", "));
}

QString GenStage::modeToString(Chipper::Generate::GenStage::Mode md)
{
    switch(md){
        case Mode::Skip:    return "skip-me";
        case Mode::Command: return "command-line";
        case Mode::Parser:  return "parse-file";
        case Mode::Stages:  return "recursive-stages";
        case Mode::FileOp:  return "file-operation";
    }
    return "unknown?";
}



//end of namespace
}}
