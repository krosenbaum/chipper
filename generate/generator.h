// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip {
class ChipData;
}

namespace Generate {

class Tracer;

///Singleton: Generator interface that converts all loaded chips to KiCAD files.
class CHIPPER_EXPORT Generator:public QObject
{
    Q_OBJECT
    Generator();
    ~Generator();

    ///internal: generate a single chip, called from the create* methods
    void runChip(Chip::ChipData*,int);
    ///internal: output current environment for debugging
    void dumpEnv();
public:
    ///returns a reference to the singleton
    static Generator& instance();

    ///returns a reference to the tracer instance, this can be used to debug into the generator while it is working
    static Tracer& tracer();

    ///set that temporary dirs are kept around
    static void setKeepTempDir(bool keep);
    ///returns whether temporary dirs are kept around (default: false)
    static bool keepTempDir();

public slots:
    ///creates every chip currently in the pool
    void createPool();
    ///create a single chip
    void createChip(Chip::ChipData*);

signals:
    ///the entire pool is done
    void poolFinished();
    ///starting to generate a file
    void generating(QString filename);
    ///file had an error that stopped the generator
    void error(QString filename,QString errormsg);
    ///finished generating a chip
    void chipFinished(QString filename);

    ///emitted when it starts to generate the pool or a chip
    ///\param maxrange the maximum number that the progress bar can reach
    void startGenerating(int maxrange);
    ///advance the progress bar
    void progress(int);
};

//end of namespace
}}
using namespace Chipper::Generate;
