// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include "filegen.h"
#include "chipgen.h"
#include "generator.h"
#include "chipper.h"

#include "chipdata.h"
#include "chipdetail.h"
#include "chipvariant.h"
#include "chippackage.h"
#include "chipfile.h"
#include "chipper.h"
#include "templates.h"
#include "templatepool.h"
#include "formula.h"
#include "stages.h"
#include "gtempfile.h"
#include "tracer.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QDomAttr>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QProcess>
#include <QStringList>
#include <QTemporaryDir>
#include <QUuid>

#include "DomNodeIterator"
#include "DomNodeList"

#include <functional>

//extremely verbose debug output
#define xDebug if(isDebugMode())qDebug

namespace Chipper { namespace Generate {


///internal data of FileGen class, d-pointer
class FileGen::Private
{
    friend class FileGen;
    PackageGen*parent; //main generator
    ChipFileTarget target; //what we are generating
    TemplateData tdata;//main template
    TemplateSymbol tsymbol;//if a symbol: symbol description
    TemplateFootprint tfprint;//if a footprint: fp description
    Template3DModel tmodel;//if a model: 3D description
    TemplateFile *tfile=nullptr;//pointer to above sym/fp/3D instance for convenience; nullptr in case of load error
    QString gendir,generatorName;//where to find the generator script
    bool genCreateTempDir=false;//true if the script wants a temporary directory (currently we create it anyway)
    GenStageList genstages,genpreview;//stages for normal creation and preview mode
    QMap<QString,QString>genenviron;//environment variables for generator
    PreviewMode previewMode=PreviewMode::Default;
    QTemporaryDir tempDir;//temporary dir
    QString targetFileName;

    //pseudo methods to make the DPTR macro happy
    Private()=default;
    Private(const Private&){}
    Private& operator=(const Private&){return *this;}
};

DEFINE_DPTR(FileGen);

FileGen::FileGen(PackageGen*pg,const ChipFileTarget&tg)
:QObject(pg)
{
    //remember direct chip data
    d->parent=pg;
    d->target=tg;
    xDebug()<<"initializing FileGen for"<<tg.fileTypeName()<<tg.name();
    //connect signals
    connect(this,&FileGen::error,d->parent,&PackageGen::error);
    //get template data
    auto&tp=TemplatePool::instance();
    const QString tname=package().templateName();
    xDebug()<<"  ...using template"<<tname;
    if(tp.hasTitle(tname)&&d->target.isValid()){
        d->tdata=tp.byTitle(tname);
        const QString tid=d->target.templateId();
        xDebug()<<"  ...template ID"<<tid;
        switch(d->target.fileType()){
            case ChipFileTarget::FileType::Symbol:
                d->tsymbol=d->tdata.symbol(tid);
                d->tfile=&d->tsymbol;
                break;
            case ChipFileTarget::FileType::Footprint:
                d->tfprint=d->tdata.footprint(tid);
                d->tfile=&d->tfprint;
                break;
            case ChipFileTarget::FileType::Model3D:
                d->tmodel=d->tdata.model(tid);
                d->tfile=&d->tmodel;
                break;
            case ChipFileTarget::FileType::Invalid:
                qDebug()<<"Ooops! Attempting to generate invalid target file. This will not end well!";
                return;
        }
        if(d->tfile){
            xDebug()<<"    ...output file type"<<d->tfile->fileTypeStr()<<d->tfile->fileName()<<"generator"<<d->tfile->generator();
            if(d->tfile->fileName().isEmpty())
                xDebug()<<"      ...was looking for"<<tid<<"got symbols="<<d->tdata.symbolIds()<<"; fprints="<<d->tdata.footprintIds()<<"; models="<<d->tdata.modelIds();
        }else
            xDebug()<<"    ...invalid output file type"<<(int)d->target.fileType();
    }else{
        qDebug()<<"Error: invalid template referenced, unable to load anything.";
        emit error(tr("Invalid template reference '%1' generator will fail.").arg(tname));
    }
    //parse generator script into something we can process
    if(d->tfile)d->generatorName=d->tfile->generator();
    parseGeneratorScript();
    if(d->genstages.size()==0){
        qDebug()<<"    Warning: no stages could be loaded. Generating a default.";
        emit error(tr("File generator chip '%1', file '%2' unable to load stages, using default.")
                    .arg(chip()->fileName())
                    .arg(d->target.name())
                  );
        d->genstages.append(GenStage::DefaultStage);
    }
    //initialize basics
    switch(d->target.fileType()){
        case ChipFileTarget::FileType::Footprint: d->targetFileName=resolvedFileName(chip()->targets().footprintFile());break;
        case ChipFileTarget::FileType::Symbol: d->targetFileName=resolvedFileName(chip()->targets().symbolFile());break;
        case ChipFileTarget::FileType::Model3D: d->targetFileName=resolvedFileName(chip()->targets().modelFile());break;
        case ChipFileTarget::FileType::Invalid: /*oops!*/ break;
    }
    //keep tempdir?
    if(Generator::keepTempDir()){
        d->tempDir.setAutoRemove(false);
        qDebug()<<"Keeping Temp-Dir around:"<<d->tempDir.path();
    }
}

FileGen::~FileGen()
{
    //check whether directory contains something
    if(Generator::keepTempDir()){
        QDir dir(d->tempDir.path());
        if(dir.isEmpty()){
            qDebug()<<"Discarding empty temporary dir"<<d->tempDir.path();
            d->tempDir.setAutoRemove(true);
        }
    }
}

ChipGen * FileGen::chipGenerator() const
{
    return d->parent->chipGenerator();
}

VariantGen* FileGen::variantGenerator()const
{
    return d->parent->variantGenerator();
}

PackageGen * FileGen::packageGenerator() const
{
    return d->parent;
}

ChipData * FileGen::chip() const
{
    return d->parent->chip();
}

ChipVariant FileGen::variant() const
{
    return d->parent->variant();
}

ChipPackage FileGen::package() const
{
    return d->parent->package();
}

ChipFileTarget FileGen::targetFile() const
{
    return d->target;
}

QString FileGen::absoluteScriptDirName() const
{
    return absoluteDefaultPath(ChipperPath::Template)+"/generator/";
}

QString FileGen::absoluteScriptFileName() const
{
    return absoluteScriptDirName()+d->generatorName+".xml";
}

void FileGen::parseGeneratorScript()
{
    //reset and check for default
    d->genstages.clear();
    d->genpreview.clear();
    d->genenviron.clear();
    if(d->generatorName.isEmpty())d->generatorName="default";
    //get script file
    const QString fname=absoluteScriptFileName();
    xDebug()<<"Loading generator script"<<fname;
    QFile fd(fname);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Error while trying to open generator script"<<fname<<":"<<fd.errorString();
        qDebug()<<" ...using default instead.";
        emit error(tr("File generator chip '%1', file '%2' unable to open script '%3': %4")
                    .arg(chip()->fileName())
                    .arg(d->target.name())
                    .arg(fname)
                    .arg(fd.errorString())
                  );
        return;
    }
    QDomDocument doc;
    QString err;
    int line,col;
    if(!doc.setContent(&fd,&err,&line,&col)){
        qDebug()<<"Error while parsing generator script"<<fname<<"on line"<<line<<"column"<<col<<":"<<err;
        qDebug()<<" ...using default instead.";
        emit error(tr("File generator chip '%1', file '%2' XML error in script '%3'.")
                    .arg(chip()->fileName())
                    .arg(d->target.name())
                    .arg(fname)
                  );
        return;
    }
    fd.close();
    //go through it...
    QDomElement root=doc.documentElement();
    if(root.tagName()!="FileGen"){
        qDebug()<<"Error while parsing"<<fname<<": not a generator script! Falling back to default.";
        emit error(tr("File generator chip '%1', file '%2' - file '%3' is not a generator script.")
                    .arg(chip()->fileName())
                    .arg(d->target.name())
                    .arg(fname)
                  );
        return;
    }
    //root attributes
    d->genCreateTempDir=root.attribute("tempDir","0").toInt()!=0;
    const QString pm=root.attribute("preview","default").toLower().trimmed();
    if(pm=="none")d->previewMode=PreviewMode::None;else
    if(pm=="image")d->previewMode=PreviewMode::Image;else
    if(pm=="call")d->previewMode=PreviewMode::Call;
    else d->previewMode=PreviewMode::Default;
    //sub-tags
    for(QDomElement sub:DomElementList(root.childNodes())){
        if(sub.tagName()=="Env")d->genenviron.insert(sub.attribute("name"),sub.text());else
        if(sub.tagName()=="Stage")d->genstages.append(GenStage(sub,fname));else
        if(sub.tagName()=="Preview")d->genpreview.append(GenStage(sub,fname));
        else{
            qDebug()<<"Warning: unknown tag"<<sub.tagName()<<"in generator script"<<fname;
            emit error(tr("Warning: unknown tag %1 in generator script %2").arg(sub.tagName()).arg(fname));
        }
    }
    //adjust preview mode
    if(d->previewMode==PreviewMode::Default)
        d->previewMode = d->genpreview.size()>0 ? PreviewMode::Image : PreviewMode::None;
}

void FileGen::convert(Conversion conv)
{
    qDebug()<<"         ...creating"<<d->target.fileTypeName()<<":"<<targetFileName();
    auto &trc=Tracer::instance();
    auto r=trc.generateFileCall(this);
    if(r.isContinue()){
        //run through template
        GenStageList stages;
        if(conv==Conversion::TargetFile)stages=d->genstages;
        else stages=d->genpreview;

        std::function<bool(const GenStageList&)> runStages;
        runStages=[&](const GenStageList&stages)->bool{
            for(const auto&stage:stages){
                switch(stage.mode()){
                    case GenStage::Mode::Command: if(!processCommandStage(stage))return false;break;
                    case GenStage::Mode::Parser:  if(!processParserStage(stage))return false;break;
                    case GenStage::Mode::FileOp:  if(!processFileStage(stage))return false;break;
                    case GenStage::Mode::Stages:  if(!runStages(d->genstages))return false;break;
                    default: /*skip*/ break;
                }
            }
            return true;
        };
        //run...!
        if(!runStages(stages))
            qDebug()<<"         Warning: error while trying to generate"<<targetFileName();
    }
}

bool FileGen::processParserStage(const GenStage&stage)
{
    xDebug()<<"Parser Stage input="<<stage.sourceFileName()<<"output="<<stage.targetFileName()<<"prefix="<<stage.prefix();
    const QString infile=resolvedFileName(stage.sourceFileName());
    const QString outfile=resolvedFileName(stage.targetFileName());
    xDebug()<<"   ...input="<<infile;
    xDebug()<<"   ...output="<<outfile;
    //pre-call
    auto r=Generator::tracer().generateFileStageCall(this,stage);
    if(!r.isContinue())return false;
    //load template
    GenTemplateFile gtf(infile,stage.prefix());
    if(gtf.error()){
        qDebug()<<"Error while loading template file:"<<infile;
        emit error(tr("Error while loading template file '%1'.").arg(infile));
        qDebug().noquote()<<"   ...stage data: "<<stage.toString();
        return false;
    }
    //open output file
    QFileInfo fi(outfile);
    fi.makeAbsolute();
    fi.dir().mkpath(".");
    QFile fd(outfile);
    if(!fd.open(QIODevice::ReadWrite)){
        qDebug()<<"Unable to open output file"<<outfile<<"error:"<<fd.errorString();
        emit error(tr("Unable to open output file '%1': %2").arg(outfile).arg(fd.errorString()));
        qDebug().noquote()<<"   ...stage data: "<<stage.toString();
        return false;
    }
    //init formula system
    Formula form;
    initFormula(form,stage);
    //process template
    bool b = gtf.processScript(fd,form,stage);
    if(!b)r.setState(Tracer::State::Failure);
    return b;
}

bool FileGen::processCommandStage(const GenStage&stage)
{
    xDebug()<<"Command Stage"<<stage.commandName()<<"args"<<stage.parameters()<<"; in"<<stage.stdinName()<<"; out"<<stage.stdoutName()<<"; wd"<<stage.workingDir();
    //pre-call
    auto r=Generator::tracer().generateFileStageCall(this,stage);
    if(!r.isContinue())return false;
    //resolve executable
    const QString exe=executablePath(stage.commandName());
    xDebug()<<"  ...full cmd"<<exe;
    QProcess proc;
    proc.setProgram(exe);
    //resolve parameters
    QStringList args;
    for(const auto&a:stage.parameters())
        args.append(resolvedFileName(a));
    proc.setArguments(args);
    xDebug()<<"  ...full args"<<args;
    //prepare environment
    auto env=QProcessEnvironment::systemEnvironment();
    for(auto en:d->genenviron.keys()){
        auto val=resolvedFileName(d->genenviron[en],env.value(en));
        env.insert(en,val);
        xDebug()<<"  ...env"<<en<<"="<<val;
    }
    proc.setProcessEnvironment(env);
    //redirect working dir and stdin/out
    if(!stage.workingDir().isEmpty()){
        auto dir=resolvedFileName(stage.workingDir());
        proc.setWorkingDirectory(dir);
        xDebug()<<"  ...wdir"<<dir;
    }
    if(!stage.stdinName().isEmpty()){
        auto fn=resolvedFileName(stage.stdinName());
        proc.setStandardInputFile(fn);
        xDebug()<<"  ...stdin"<<fn;
    }
    if(!stage.stdoutName().isEmpty()){
        auto fn=resolvedFileName(stage.stdoutName());
        proc.setStandardOutputFile(fn);
        xDebug()<<"  ...stdout"<<fn;
    }
    //run; TODO: configurable timeout? more detailed error tracking? (issue T108)
    proc.start();
    if(proc.waitForFinished(stage.timeout()*1000)){
        if(proc.exitStatus()==QProcess::NormalExit && proc.exitCode()==0)
            return true;
        else{
            qDebug()<<"Error: process exited with non-ideal state:"<<(proc.exitStatus()==QProcess::NormalExit?"normal":"crashed")<<" code="<<proc.exitCode();
            emit error(tr("Process exited with non-ideal state: %1 (%2)").arg(proc.exitStatus()==QProcess::NormalExit?"normal":"crashed").arg(proc.exitCode()));
            qDebug().noquote()<<"   ...stage data: "<<stage.toString();
        }
    }else{
        qDebug()<<"Error: process still running after timeout. PID:"<<proc.processId();
        emit error(tr("Process %1 still running after timeout.").arg(proc.processId()));
        qDebug().noquote()<<"   ...stage data: "<<stage.toString();
        qDebug()<<"...killing process.";
        proc.kill();
    }
    r.setState(Tracer::State::Failure);
    return false;
}

bool FileGen::processFileStage(const GenStage&stage)
{
    const QString in=resolvedFileName(stage.sourceFileName());
    const QString out=resolvedFileName(stage.targetFileName());
    QFile file(in);
    if(stage.isCopy()){
        QFileInfo(out).dir().mkpath(".");
        if(QFile::exists(out))QFile::remove(out);
        if(file.copy(out))
            return true;
        else{
            qDebug()<<"Error copying file"<<in<<"to"<<out<<"error:"<<file.errorString();
            emit error(tr("Error copying file %1 to %2").arg(in).arg(out));
            qDebug().noquote()<<"   ...stage data: "<<stage.toString();
            return false;
        }
    }
    if(stage.isMove()){
        QFileInfo(out).dir().mkpath(".");
        if(QFile::exists(out))QFile::remove(out);
        if(file.rename(out))
            return true;
        else{
            qDebug()<<"Error moving file"<<in<<"to"<<out<<"error:"<<file.errorString();
            emit error(tr("Error moving file %1 to %2").arg(in).arg(out));
            qDebug().noquote()<<"   ...stage data: "<<stage.toString();
            return false;
        }
    }
    if(stage.isDelete()){
        if(file.remove())
            return true;
        else{
            qDebug()<<"Error removing file"<<in<<"error:"<<file.errorString();
            emit error(tr("Error removing file %1").arg(in));
            qDebug().noquote()<<"   ...stage data: "<<stage.toString();
            return false;
        }
    }
    //oops!
    qDebug()<<"Error: unknown file operation!";
    emit error(tr("Unknown file operation."));
    qDebug().noquote()<<"   ...stage data: "<<stage.toString();
    return false;
}

void FileGen::initFormula(Formula&form,const GenStage&stage)
{
    form.setTarget(targetFile());
    //pathes
    form.setConstant("tempDir",kicadFileName("%Z"));
    form.setConstant("previewDir",kicadFileName("%z"));
    form.setConstant("generatorDir",kicadFileName("%G"));
    form.setConstant("generatorFilePath",kicadFileName("%s"));
    form.setConstant("targetFilePath",kicadFileName("%Y"));
    form.setConstant("targetFileRoot",kicadFileName("%y"));
    form.setConstant("targetDir",kicadFileName("%d"));
    //basics
    form.setConstant("openTag",stage.openTag());
    form.setConstant("closeTag",stage.closeTag());
    //additional functions
    form.setFunction("resolve",[this](const QList<QVariant>&args,ELAM::Engine&)->QVariant{
        if(args.size()!=1 || !args.value(0).canConvert<QString>())
            return QVariant::fromValue(ELAM::Exception("Expecting exactly one string argument!"));
        return resolvedFileName(args[0].toString());
    });
    //defined variables
    form.reCalculate();
}

QString FileGen::targetFileName() const
{
    return targetFile().name();
}

//translate %-sequences to path components
//use %% for a single %
QString FileGen::kicadFileName(QString pattern,const QString&oldVal) const
{
    QString r;
    bool isPrc=false;
    for(auto c:pattern){
        if(isPrc){
            isPrc=false;
            switch(c.unicode()){
                case '%':r+='%';break;
                case 'M':r+=chip()->metaData().manufacturer();break;
                case 'm':r+=name2path(chip()->metaData().manufacturer());break;
                case 'C':r+=chip()->metaData().chipType();break;
                case 'c':r+=name2path(chip()->metaData().chipType());break;
                case 'A':r+=chip()->metaData().author();break;
                case 'V':r+=variant().name();break;
                case 'v':r+=name2path(variant().name());break;
                case 'P':r+=package().displayName();break;
                case 'p':r+=name2path(package().displayName());break;
                case 'T':r+=package().templateRef().title();break;
                case 't':r+=name2path(package().templateRef().title());break;
                case 'N':r+=d->target.name();break;
                case 'n':r+=name2path(d->target.name());break;
                case 'S':r+=absoluteDefaultPath(ChipperPath::Symbol);break;
                case 'F':r+=absoluteDefaultPath(ChipperPath::Footprint);break;
                case '3':r+=absoluteDefaultPath(ChipperPath::Model3D);break;
                case 'R':r+=absoluteDefaultPath(ChipperPath::Root);break;
                case 'I':if(d->tfile)r+=d->tfile->absoluteFileName();break;
                case 'i':if(d->tfile)r+=QFileInfo(d->tfile->fileName()).fileName();break;
                case 'z':Q_FALLTHROUGH();//FIXME: for the moment preview dir and temp dir are the same...
                case 'Z':r+=d->tempDir.path();break;
                case 'Y':r+=d->targetFileName;break;
                case 'y':r+=QFileInfo(d->targetFileName).fileName();break;
                case 'D':r+=d->tdata.absolutePathName();break;
                case 'd':r+=QFileInfo(d->targetFileName).absoluteDir().absolutePath();break;
                case 'G':r+=absoluteScriptDirName();break;
                case 'g':r+=d->generatorName;break;
                case 's':r+=absoluteScriptFileName();break;
                case 'o':r+=oldVal;break;
                default:
                    qDebug().noquote()<<QString("Unknown '%1' sequence in file name pattern '%2'.").arg(QString("%")+c).arg(pattern);
                    r+='%';r+=c;
                    break;
            }
        }else{
            if(c=='%')isPrc=true;
            else r+=c;
        }
    }
    return r;
}

//uses kicadFileName to help with % sequences
//find sequences of ${varname} and then tries to find the variable:
// 1) as a configured chipper value
// 2) as an environment variable
// 3) if not found: replace by empty value
//as exception $$ is replaced with $
//{***} on its own is not replaced, must be preceded by $
QString FileGen::resolvedFileName(QString pattern,const QString&oldVal) const
{
    QString r,v;
    enum Mode {
        Verb,   //verbatim mode, no replacement, looking for $
        Dollar, //found a $ sign, checking for ${ or $$
        Var     //inside ${ ... }, accumulating variable name, looking for }
    };
    Mode mode=Verb;
    for(auto c:kicadFileName(pattern,oldVal)){
        if(mode==Verb){ //verbatim mode, looking for $
            if(c=='$')mode=Dollar;//found $, switch mode
            else r+=c; //accumulate verbatim chars
        }else if(mode==Dollar){//found $, looking for $$ or ${
            if(c=='$'){r+='$';mode=Verb;continue;}// $$ -> $
            if(c=='{'){v.clear();mode=Var;continue;}//${ -> variable name
            //invalid $* sequence, complain
            qDebug().noquote()<<QString("invalid sequence $%1 in path '%2'").arg(c).arg(pattern);
            mode=Verb;
        }else{//Var mode, inside variable name
            if(c=='}'){//end of variable name
                mode=Verb;
                //find internal variable
                const auto p=chipperPathForEnvName(v);
                if(p!=ChipperPath::None)
                    r+=absoluteDefaultPath(p);
                //find external variable
                else
                    r+=qEnvironmentVariable(v.toUtf8().data());
                //done with variable, return to normal
                v.clear();
            }else v+=c;//accumulate variable name
        }
    }
    //done replacing
    return r;
}

QString FileGen::executablePath(const QString& execName) const
{
    //TODO: find config
    return execName;
}


//end of namespace
}}


#include "moc_filegen.cpp"
