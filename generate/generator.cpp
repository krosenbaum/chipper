// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QTimer>
#include <QDebug>

#include "generator.h"
#include "chipdata.h"
#include "chippool.h"
#include "chipgen.h"
#include "chipper.h"
#include "autodisconnect.h"
#include "tracer.h"

using namespace Chipper::Chip;

Generator::Generator()
{
    qDebug()<<"Generator active: On Stand-By.";
    connect(this,&Generator::error,this,[](QString fn,QString msg){qDebug()<<"  >>Generator Error in"<<fn<<":"<<msg;});
}

Generator::~Generator()
{
    qDebug()<<"Generator shut down. No more chip power.";
}

Generator & Generator::instance()
{
    static Generator inst;
    return inst;
}

Tracer & Generator::tracer()
{
    static Tracer trc;
    return trc;
}


void Generator::dumpEnv()
{
    qDebug()<<"Environment:";
    qDebug()<<"  Variable Prefix:"<<defaultPath(ChipperPath::EnvPrefix);
    qDebug()<<"     Template Dir:"<<absoluteDefaultPath(ChipperPath::Template);
    qDebug()<<"         Root Dir:"<<absoluteDefaultPath(ChipperPath::Root);
    qDebug()<<"       Symbol Dir:"<<absoluteDefaultPath(ChipperPath::Symbol);
    qDebug()<<"    Footprint Dir:"<<absoluteDefaultPath(ChipperPath::Footprint);
    qDebug()<<"     3D Model Dir:"<<absoluteDefaultPath(ChipperPath::Model3D);
}

void Generator::createPool()
{
    qDebug()<<"Generating Pool...";
    int errnum=0;
    AutoDisconnect ad(connect(this,&Generator::error,this,[&errnum]{errnum++;}));
    auto&cp=ChipPool::instance();
    const int psize=cp.numChips();
    qDebug()<<"  There are"<<psize<<"chips in the pool.";
    emit startGenerating(psize*100);
    dumpEnv();
    try{
        auto r=emit tracer().generatorCall();
        if(r.isContinue())
            for(int i=0;i<psize;i++)
                runChip(cp.chip(i),i);
    }catch(AbortException){
        qDebug()<<"Tracer aborted the Generator. Target files probably incomplete.";
    }
    qDebug()<<"Done generating pool."<<errnum<<"errors detected.";
    emit poolFinished();
}

void Generator::createChip(ChipData*cd)
{
    emit startGenerating(100);
    dumpEnv();
    try{
        auto r=emit tracer().generatorCall();
        if(r.isContinue())
            runChip(cd,-1);
    }catch(AbortException){
        qDebug()<<"Tracer aborted the Generator. Target files probably incomplete.";
    }
}

void Generator::runChip(ChipData*cd, int num)
{
    if(cd==nullptr){
        emit error(QString(),tr("Unable to generate NULL chip."));
        return;
    }
    emit generating(cd->fileName());
    qDebug()<<"Generating files from"<<cd->fileName();

    if(num<0)emit progress(0);
    else emit progress(num*100);

    ChipGen cg(cd,this);
    connect(&cg,&ChipGen::error,this,std::bind(&Generator::error,this,cd->fileName(),std::placeholders::_1));
    if(num<0)connect(&cg,&ChipGen::progress,this,&Generator::progress);
    else     connect(&cg,&ChipGen::progress,this,[this,num](int p){emit progress(num*100+p);});
    cg.convert();

    if(num<0)emit progress(100);
    else emit progress((num+1)*100);

    emit chipFinished(cd->fileName());
}


static bool s_keepTempDir=false;

bool Generator::keepTempDir()
{
    return s_keepTempDir;
}

void Generator::setKeepTempDir(bool keep)
{
    s_keepTempDir=keep;
}


#include "moc_generator.cpp"
