// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QMap>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

class QDomElement;
class QFile;

namespace Chipper { namespace Generate {

class Formula;

///represents a single stage in the generation of a file - either parsing a template file or executing an external command
class CHIPPER_EXPORT GenStage
{
public:
    class DefStageC {};
    static const DefStageC DefaultStage;
    ///create an invalid stage
    GenStage()=default;
    ///create the default stage (parse template input file into chip output file)
    GenStage(const DefStageC&):in("%I"),out("%Y"),md(Mode::Parser){}
    ///copy a stage
    GenStage(const GenStage&)=default;
    ///create stage from XML (element in generator XML file)
    GenStage(const QDomElement&,const QString&);

    ///copy stage
    GenStage& operator=(const GenStage&)=default;

    ///represents the stage type
    enum class Mode {
        ///do not execute anything
        Skip,
        ///execute external command
        Command,
        ///execute a parser stage
        Parser,
        ///recursively execute main stages (used in preview)
        Stages,
        ///copy, delete or move a file
        FileOp
    };

    ///returns the stage mode
    Mode mode()const{return md;}

    ///true if this is a simple parser stage using the internal syntax
    inline bool isInternal()const{return md==Mode::Parser || md==Mode::FileOp;}
    ///true if this stage executes an externa, command
    inline bool isCommand()const{return md==Mode::Command;}
    ///true if this stage is skipped
    inline bool isSkipped()const{return md==Mode::Skip;}
    ///true if this is a recursive stage
    inline bool isRunStages()const{return md==Mode::Stages;}
    ///true if this is a copy/move stage
    inline bool isFile()const{return md==Mode::FileOp;}

    ///true if this is a copy operation
    inline bool isCopy()const{return isFile() && cmd=="c";}
    ///true if this is a move operation
    inline bool isMove()const{return isFile() && cmd=="m";}
    ///true if this is a delete operation
    inline bool isDelete()const{return isFile() && (cmd=="d" || cmd=="r");}

    ///external command stage: returns the command
    QString commandName()const{if(isCommand())return cmd;else return QString();}

    ///command stage: returns the file name to which stdin should be redirected
    QString stdinName()const{if(isCommand())return in;else return QString();}
    ///command stage: returns the file name to which stdout should be redirected
    QString stdoutName()const{if(isCommand())return out;else return QString();}

    ///internal parser stage: returns the source file (template) for the parser
    QString sourceFileName()const{if(isInternal())return in;else return QString();}
    ///internal parser stage: returns the target file (immediate output) for the parser
    QString targetFileName()const{if(isInternal())return out;else return QString();}

    ///command stage: returns the working directory for the stage (if non-empty)
    QString workingDir()const{return wd;}

    ///command stage: returns the command parameters
    QStringList parameters()const{return params;}

    ///parser stage: returns the prefix for special commands in the template file
    QString prefix()const{return pfx;}

    ///timeout for the stage
    int timeout()const{return tmout;}

    ///returns the open tag for formulae
    //TODO: make it configurable
    QString openTag()const{return "{{";}
    ///returns the close tag for formulae
    //TODO: make it configurable
    QString closeTag()const{return "}}";}

    ///dump stage content
    QString toString()const;

    ///string version of the mode for debugging
    static QString modeToString(Mode);

private:
    QString cmd,in,out,wd,pfx="##",filename;
    QStringList params;
    Mode md=Mode::Command;
    int linenum=-1,tmout=30;
};

///List of generator stages.
typedef QList<GenStage> GenStageList;

///the kind of preview configured in a generator script
enum class PreviewMode{
    ///default mode - None if there are no stages, Image is there are stages
    Default,
    ///no preview possible
    None,
    ///the preview stages generate images that can be shown internally
    Image,
    ///the preview is done by an external tool
    Call
};

//end of namespace
}}
