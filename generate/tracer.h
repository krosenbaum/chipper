// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator: trace facility
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip {
class ChipData;
class ChipVariant;
class ChipPackage;
class ChipFileTarget;
class ChipSymbol;
class ChipFootprint;
class ChipModel;
}
using namespace Chip;

namespace Generate {

class Generator;
class ChipGen;
class VariantGen;
class PackageGen;
class FileGen;
class GenStage;
class GenTemplateFile;
class Tracer;

///Singleton: Generator trace interface.
///
///Subscribe to the signals of this instance to be able to trace the file generator and to influence it while running.
///The return value of the signals (slots connected to them) influences how the generator continues (or aborts).
///
///Generator classes are friends of this class and call the ***Call methods, which then relay to the signals.
class CHIPPER_EXPORT Tracer:public QObject
{
    Q_OBJECT
    friend class Generator;
    friend class ChipGen;
    friend class VariantGen;
    friend class PackageGen;
    friend class FileGen;
    Tracer()=default;
    Tracer(const Tracer&)=delete;
    Tracer(Tracer&&)=delete;
public:
    ///returns a reference to the singleton instance of the Tracer
    static Tracer& instance();

    //NOTE: the Return and State enums should be synchronized - using the same int value for the same action - they get cast into each other
    ///What the Generator is supposed to do after a trace call.
    enum class Return{
        ///continue to run normally
        Continue = 0,
        ///default action, used when no trace method is connected
        DefaultAction = Continue,
        ///can be returned by pre-tracers, skips the actual call and continues with the post-tracer, acts as if the call was successful
        Skip=2,
        ///fail the step and continue with the next one
        Fail=3,
        ///abort the entire generator run
        Abort=4
    };
    ///Represents the current state of the traced call
    enum class State {
        ///Pre-Trace-Call, no decision yet
        PreCall=0,
        ///the generator call was a success
        Success=1,
        ///the generator call was a failure
        Failure = (int)Return::Fail,
        ///the generator call was skipped
        Skipped = (int)Return::Skip,
        ///the generator was aborted
        Aborted = (int)Return::Abort
    };
signals:
    ///emitted by the main generator
    Tracer::Return generator(State);
    ///emitted for every Chip that is being generated
    Tracer::Return generateChip(ChipGen*,ChipData*,State);
    ///emitted for every variant that is being generated
    Tracer::Return generateVariant(VariantGen*,const ChipVariant&,State);
    ///emitted for every package that is being generated
    Tracer::Return generatePackage(PackageGen*,const ChipPackage&,State);
    ///emitted for every symbol that is being generated
    Tracer::Return generateSymbol(FileGen*,const ChipSymbol&,State);
    ///emitted for every footprint that is being generated
    Tracer::Return generateFootprint(FileGen*,const ChipFootprint&,State);
    ///emitted for every 3D Model that is being generated
    Tracer::Return generate3DModel(FileGen*,const ChipModel&,State);
    ///emitted for files for every generator stage
    Tracer::Return generateFileStage(FileGen*,const ChipFileTarget&,const GenStage&,State);
private:
    ///A more intelligent return class - also makes the second call implicitly.
    ///Used by the generator methods to automate the second call after returning.
    ///This class is explicitly shared, the call is made when the last copy goes out of scope.
    ///It is not thread safe.
    class CReturn{
    public:
        ///The type of call represented by this return value.
        enum class Type{
            ///Trace call made by the Generator.
            Gen,
            ///Trace call made by the ChipGen.
            Chip,
            ///Trace call made by the VariantGen.
            Variant,
            ///Trace call made by the PackageGen.
            Package,
            ///Trace call made by the FileGen for the entire process.
            File,
            ///Trace call made by the FileGen for each stage.
            Stage
        };
        ///creates an empty instance with no function
        CReturn()=default;
        ///creates a shared copy of the instance
        CReturn(const CReturn&o):d(o.d){if(d)d->ctr++;}
        ///creates a new copy of the other instance, detaches this one first
        CReturn& operator=(const CReturn&o){detach();d=o.d;if(d)d->ctr++;return *this;}

        ///detaches and makes the second call if this was the last instance
        ~CReturn(){detach();}

        ///true if this is a valid instance, false if it is empty
        bool isValid()const{return d!=nullptr;}
        ///true if the state of this instance asks to continue with processing
        bool isContinue()const{return d && d->mret==(int)Return::Continue;}
        ///changes the state of this instance, the state is handed to the second call
        void setState(State s){if(d)d->mret=(int)s;}

        ///removes this instance from the shared group, if it was the last instance it makes the second Tracer call
        void detach();
    private:
        friend class ::Chipper::Generate::Tracer;
        ///used by the ***Call methods to instantiate the first in a shared group - most calls use this constructor
        CReturn(Return r,Type t,void*g):d(new D((int)r,t,g)){}
        ///used by generateFileStageCall to instantiate the first in a shared group
        CReturn(Return r,FileGen*g,const GenStage&s):d(new D((int)r,g,&s)){}
        ///actual data: shared among instances that point to the same result
        struct D {
            D()=delete;
            D(const D&)=delete;
            D& operator=(const D&)=delete;
            D(int r,Type t,void*g):mret(r),mtyp(t),mgen(g){}
            D(int r,FileGen*g,const GenStage*s):mret(r),mtyp(Type::Stage),mgen(g),mstage(s){}
            int mret=0;
            Type mtyp=Type::Gen;
            void*mgen=nullptr;
            const GenStage*mstage=nullptr;
            int ctr=1;
        };
        ///shared data
        D*d=nullptr;
    };
    ///make sure callbacks can be done by CReturn::detach()
    friend class CReturn;
    ///emitted by the main generator)
    Tracer::CReturn generatorCall();
    ///emitted for every Chip that is being generated
    Tracer::CReturn generateChipCall(ChipGen*);
    ///emitted for every variant that is being generated
    Tracer::CReturn generateVariantCall(VariantGen*);
    ///emitted for every package that is being generated
    Tracer::CReturn generatePackageCall(PackageGen*);
    ///emitted for every symbol that is being generated
    Tracer::CReturn generateFileCall(FileGen*);
    ///emitted for every symbol that is being generated
    Tracer::CReturn generateFileStageCall(FileGen*,const GenStage&);
};

class CHIPPER_EXPORT AbortException {};

}}
using namespace Chipper::Generate;
