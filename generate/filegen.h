// Chipper KiCAD symbol/footprint/3Dmodel generator
// automated generator for KiCAD files
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QPointer>

#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip {
class ChipData;
class ChipVariant;
class ChipPackage;
class ChipFileTarget;
}
using namespace Chip;

namespace Generate {

class PackageGen;
class VariantGen;
class ChipGen;

class GenStage;

class Formula;

///Generator for symbol/footprint/3D output files.
///Each instance generates exactly one file (or in the case of 3D models: one
///set of files) and is configurable on how to generate the file(s).
class CHIPPER_EXPORT FileGen:public QObject
{
    Q_OBJECT
    friend class PackageGen;
    DECLARE_DPTR(d);
    FileGen()=delete;
    FileGen(const FileGen&)=delete;
    FileGen& operator=(const FileGen&)=delete;
public:
    ~FileGen();

    ChipGen* chipGenerator()const;
    VariantGen* variantGenerator()const;
    PackageGen* packageGenerator()const;
    ChipData* chip()const;
    ChipVariant variant()const;
    ChipPackage package()const;
    ChipFileTarget targetFile()const;

    enum class Conversion {
        Preview,
        TargetFile,
        ///\internal recursive call for executing stages during preview
        PreviewStage
    };

    ///returns the file name pattern of the target file, override it to write to a different file name
    virtual QString targetFileName()const;

    ///convert a file name pattern to KiCAD format (%-sequences replaced, ${variables} remain)
    QString kicadFileName(QString pattern,const QString&oldVal=QString())const;
    ///convert a file name pattern to fully resolved file name (%-sequences and ${variables} replaced)
    QString resolvedFileName(QString pattern,const QString&oldVal=QString())const;

    ///returns the full path + file name of the generator script
    QString absoluteScriptFileName()const;
    ///returns the full path leading to the generator script (without actual script file name)
    QString absoluteScriptDirName()const;

protected:
    ///overridden by child classes
    explicit FileGen(PackageGen*,const ChipFileTarget&);

    ///Internal Parser Stage Processing
    bool processParserStage(const GenStage&);
    ///External COmmand Stage Processing
    bool processCommandStage(const GenStage&);
    ///Internal Copy/Move/Del Stage Processing
    bool processFileStage(const GenStage&);

private:
    ///helper to parse the generator script file
    void parseGeneratorScript();

    ///initializes the formula environment for a process stage
    void initFormula(Formula&,const GenStage&);

    ///returns the configured path for an executable called by a stage, returns the executable name unchanged if there is not configuration
    QString executablePath(const QString&execName)const;

public slots:
    ///called by parent generator to actually generate the file(s)
    void convert(Conversion c=Conversion::TargetFile);

signals:
    ///file had an error that stopped the generator
    void error(QString errormsg);
};

//end of namespace
}}
using namespace Chipper::Generate;
