#!/bin/bash

Q="doc.qrc"
echo '<!DOCTYPE RCC><RCC version="1.0"><qresource>' >$Q

for i in $(find doc/ -type f) ; do
    echo '<file>'$i'</file>' >>$Q
done

echo '</qresource></RCC>' >>$Q

