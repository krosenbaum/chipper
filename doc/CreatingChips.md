Creating New Chips with Chipper
================================

Chipper allows the user to leverage templates for common physical chip (device) packages to create KiCAD files that can then be used to create new circuits and PCBs in KiCAD. It is thus geared towards creating symbols, footprints and 3D models for chips that use common chip packages, but have new pinouts. It is not meant to recreate very simple or common devices (like resistors or diodes) that already exist in KiCAD.

This document explains how to create a new Chip description File using one of the existing templates.

See [Generating Files](GeneratingFiles.md) for details in how to convert a Chip File into KiCAD files.

Concepts
----------

### KiCAD Concepts

**Symbol** - is a circuit symbol that can be used in a circuit diagram. A single symbol file can contain multiple symbols. It describes the connection interface of a device.

![Example Symbol](example-symbol.png)

**Footprint** - is a description of the features on a PCB that are used to connect a device physically, like solder pads, alignment information, solder mask, silk screen, etc. Although footprint files may contain multiple footprints usually it is one footprint per file.

![Example Footprint](example-footprint.png)

**3D Model** - are 3D vector files describing device outlines for the 3D view of PCBs. The files are in STEP (\*.stp, \*.step) or VRML (\*.wrl) format, one model per file.

![Example 3D Model](example-3dmodel.png)

### Chipper Concepts

**Chip** - in Chipper terminology a "chip" is a device family with one or multiple variants that share the same function and pins - with each variant exposing a subset of those pins or possibly a different pinout or different package type.

**Chip File** - is a full description of a "chip" in all its variants. There is common meta data (like manufacturer, chip family name, etc.) and some basic target data (directories where Chipper will put generated files).

**Variant** - is a specific pinout of the chip with a common subset of functional pins assigned to the same pin number. A variant can have any number of packages (e.g. with slightly different dimensions or a QFP and a QFN package with identical pinout).

**Package** - is the most specific description of a chip - it is a specific packaging of a variant. Packages define specific symbols, footprints and 3D models for the chip - the user can create zero, one or multiple of each of those (although it is usually not useful to create more than one 3D model). A package defines physical dimensions and references a template that helps generating the specific files from those dimensions. The package inherits its specific pinout from the variant that it is a part of.

**Pins** - are defined on the chip level and the variant level. The chip level defines pins as distinct functions, with each function existing exactly once. The variant selects a sub-set of those functions and assigns them to specific physical pins (leads) of the chip - a function on the chip level can be assigned to zero, one or multiple physical pins on the variant level.

For example a micro controller Xctrl might exist in a 24 pin SOP variant, and a 32 pin variant with 2 TQFP packages with different pin pitches and a QFN package - with all 32 pin packages sharing a pinout. In this case the user would define the following hierarchy:

 * Chip File with:
    * meta data and target directories
    * up to 32 pin functions in the main pin table
    * 1 variant with 24 physical pins:
        * assigning functions to those 24 physical pins
        * containing 1 SOP package with physical dimensions
            * defining a symbol, footprint and 3D model
    * 1 variant with 32 physical pins:
        * assigning functions to those 32 physical pins
        * a package for QFP with a set of dimensions
            * defining a symbol, footprint and 3D model
        * a package for QFP with a different set of dimensions
            * defining a symbol, footprint and 3D model
        * a package for QFN with a third set of dimensions
            * defining a symbol, footprint and 3D model

Strictly speaking it is not necessary to create a symbol in all of the packages of a variant. Having only one symbol for the entire variant is potentially less confusing, but requires the user to explicitly assign a footprint when using the symbol in a circuit. Having a symbol (or multiple symbols) per package allows to set a default footprint, so that the user does not need to select one.

Template Types
---------------

The program is equipped with a handful of basic templates for the most common device types that are worth generating with a program.

**DIP** - *Dual Inline Package* (also known as *DIL*) - a basic THT (Through Hole Technology) template.

![Example DIP](example-3dmodel.png)

**SOP** - *Small Outline Package* - SMD equivalent of DIP with two lines of pins on the chip. The template is useable for variants of *SOP* (SSOP, TSSOP, ...) and *SOIC* chips.

![Example DIP](example-3d-sop.png)

**QFP** - *Quad Flat-Pack* - SMD template for variants of QFP, like TQFP, VQFP, etc.

![Example DIP](example-3d-qfp.png)

**QFN** - *Quad Flatpack No-leads* - SMD template for variants of QFN (VQFN, ...) chips. The bottom pad is configurable.

![Example DIP](example-3d-qfn.png)

(TODO: SIP, irregular DIP variants, BGA variants)

Basic Data
-----------

The "Basics" page contains meta data about the chip (family) being described by this file and the file itself.

The hardware data section describes the chip (hardware) itself - manufacturer, name of the chip or family of chips, the
URL of the data sheet. This data is reflected in a visible way in the generated files. It may also influence the target
file names (if the corresponding patterns are used).

As a rule of thumb: if it has its own name and its own data sheet then it should have a separate chip file.
Conversely: if it is described in the same data sheet than it probably should appear in the same chip file.

The "Soft-Model" section describes meta data of the chip file itself. The author line should contain the name of whoever
created the chip file, not the person or company who created the chip. The copyright and license lines will also usually
contain the choices of the author, but contractual obligations may influence this decision (e.g. if there is an NDA
with the manufacturer or if the file is created for someone else).

Soft-Model data will only be reflected in comments in the generated files.


Target Files
-------------

TODO

Pin Definitions
----------------



Variants
---------



Packages
---------
