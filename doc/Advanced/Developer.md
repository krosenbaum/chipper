Developer Notes for Chipper
============================

These notes exist for developers who wish to change the Chipper program itself.

Coding Style
-------------

...just a few notes and requests:

 * as a basis generic Qt/KDE style is fine
 * I'm using 4 spaces (no tabs) for indentations - keep it consistent.
 * make it readable, use internal comments in complex methods
 * don't forget documentation comments
    * /** normal Doxygen style */
    * ...remind me if I forgot comments...
 * use new-style signal-slot connections using functors
    * this way the compiler already detects most problems
    * use qOverload if you need to distinguish overloads
 * any standard C++17 feature can be used
    * for the moment avoid C++20 (or newer) features, until compiler support is widespread
    * avoid GCC specific features to keep it compatible with other systems

Binaries and Directories
------------------------

Non-Source Directories:
 * doc -> documentation (gets built into the binary)
 * Templates -> chip template files (part of runtime distribution)

Source Directories:
 * main -> main executables (GUI and command line version) - these are thin wrappers around libchipper
 * libchipper -> main non-GUI library that contains the bulk of Chipper logic
   * src -> common sources
   * chip -> Chip file XML access classes
   * template -> Template XML parser/access classes
   * generate -> output generator
   * calc -> formula engine with Chipper specific functions
 * libgui -> GUI library
   * chipgui -> chip editor
   * tempgui -> template viewer
   * widgets -> additional widgets and layouts that are used by the GUI
   * windows -> generic windows and dialogs
   * icons -> contains additional icons for the GUI
 * fpview -> footprint viewer app
 * symview -> symbol viewer app
 * vtool -> variable installer app
 * tests -> unit tests

Binaries:
 * chipper -> main application, can be started in GUI or command line mode (Unix/MacOS)
 * chipper-cmd -> main application, command line mode only (for Windows)
 * chipper-vtool -> helper application: variable manipulation tool for symbol library management
 * chipper-*view -> preview apps (not functional at the moment)
 * ctest-* , libctest-* -> unit tests, only built for debug builds
 * libchipper -> main library that contains the bulk of the functionality
 * libchippergui -> GUI library that contains all editors, dialogs, windows...
 * libelam -> ELAM library containing the formula engine
 * libdomext -> helper library for XML handling

Localization:
 * libchipper/chipper_lib_*.ts -> main library
 * gui/chipper_gui_*.ts -> GUI


GUI vs. non-GUI Mode
---------------------

main/main.cpp: int main(int,char**)
src/chipper.cpp: initChipperApp

The application has a GUI mode for normal editor operation and a non-GUI mode
for generating KiCAD files on the command line (e.g. script or Makefile). Data
and generator classes are the same, the GUI classes are not activated in non-GUI
mode.

The main function instantiates a QApplication object in GUI mode and a
QCoreApplication in non-GUI mode.

Due to this it is important that template, chip data and generator classes work
without referring to any GUI classes.

The main function first scans the command line parameters and its environment,
then it decides in what mode (GUI, non-GUI) and what function to run (window or
generator).


Path Configuration
-------------------

src/chipper.*

The generation of files very much depends on environment variables. Functions to
access those path variables are defined in chipper.* . Values for those variables
are taken from the actual environment, the command line or configuration.

Order of precendence:
 1) Value set inside the Session (should normally not happen)
 2) Command Line Parameters
 3) Real Environment Variables
 4) Configuration Values

Functions:
 * pathConfig - returns the configured path value
 * defaultPath - returns the value according to precendence
 * absoluteDefaultPath - returns the value according to precendence as an absolute path name
 * setDefaultPath - overrides the value in this session without storing it
 * setPathConfig - saves a new configuration value
 * envNameForPath - returns the corresponding environment variable name for a specific path type
 * chipperPathForEnvName - returns the path type for a environment variable name

Most parts of the program should use absoluteDefaultPath to query the correct path.


src/kicadenv.*

The KiCadEnvironment class scans environment variables and inspects the KiCAD
configurations found in the user's Home Directory.

Currently this is only used for inspecting the environment through the GUI, actual
variable values for the generator are retrieved through the interface above.

Display Classes
----------------

gui/*

Most of the classes defined here are part of the chip editor. This editor is very
hierarchical, reflecting the chip file hierarchy - mostly using tabs. All parts of
the chip editor are supposed to synchronize live with the underlying data objects (see
below), so that no explicit save/apply action is necessary. When the main editor
object is closed it triggers are complete save.

The template viewer merely displays the main data of the templates, it currently
does not allow for editing templates.

In most cases in which tabs are used the tab title is retrieved from the windowTitle
property of the widget in this tab. The windowTitleChanged signal should be used to
adjust the tab title when the underlying name property changes, which should reflect
the current human readable name of the object it represents.

### I18N

All displayed strings should be surrounded by "tr(...)", even though the program is
not fully translated yet. This will make it easier once there is time for translation.


Template Pool
--------------

template/templatepool.*

The TemplatePool singleton automatically loads all templates found in the template
directory (one of the configurable standard pathes, see above). Whenever a reference
to a template needs to be resolved the program must go through the pool.

Template Data
--------------

template/*

The TemplateData class represents an instance of a template, with most of the other
classes representing parts of the data hierarchy of the template. All of those classes are
implemented as data objects that can be freely copied around.

Currently templates are not editable. If this ever changes they should be modelled the same
way as the chip data classes (see below).

Chip Pool
-----------

chip/chippool.*

The ChipPool singleton holds all ChipData object - each one representing a chip
file. Loading and resolving chip files needs to go through the pool, since it
is not possible to instantiate ChipData outside of the pool context.

The chip pool served two purposes: it makes sure that chip files are not loaded
multiple times and it is the central repository for the generator to access.

Chip Data
----------

chip/*

The ChipData class is the main object representing the top of the chip file
hierarchy, it is also the only QObject in that hierarchy and cannot be
copied - only referenced.

All other data classes represent parts of the chip file hierarchy and are
implemented as shared references to those parts. Internally each object
holds a reference to the XML (DOM) element that encodes its content and
retrieves data directly from that DOM element when needed and writes into
that DOM element when data changes. The main ChipData object then synchronizes
the DOM hierarchy to disk from time to time or when requested.

The base behavior of all chip data objects is implemented in ChipDataPartBase.
Sub-classes should implement the following methods:
 * Constructors:
    * functional constructor - classname(ChipData*parentChip,const QDomElement&partElement) -> calling the same constructor in the base class to instantiate objects that reference parts of the hierarchy
    * default constructor - classname()=default -> to instantiate a null instance that does not reference an actual part of the hierarchy
    * copy constructor - classname(const classname&)=default -> to create a new reference to the same data
 * copy operator - classname& operator=(const classname&)=default -> to create a new reference to the same data
 * reload logic functions that are called if the XML is loaded fresh to update the references
    * void preloadSync() -> this is called just before the XML is reloaded and should be used to remember identifying information about this object - document() and element() are valid
    * void resetElement() -> called just after reloading the XML and should use the information remebered in preloadSync to find the defining element again - document() is valid, element() is not valid and needs to be overridden with setElement(...)

Sub-classes of ChipDataPartBase can rely on those methods implemented in the base class:
 * altered() -> must be called every time data is altered
 * chip() -> returns a pointer to the ChipData object that this object belongs to
 * document() -> returns a reference to the document node that this object belongs to
 * element() -> returns a reference to the element represented by this object
 * setElement(...) -> overrides the element represented by this object - should only be called from resetElement() and should be an existing sub-node of document()
 * isValid() -> returns whether this is a valid object by checking the element() node for validity, can be overridden for more sophisticated tests
 * constructors and copy operator (see above for signatures): to be called by sub-classes to implement their own (defaulted) versions

Note: the reload logic is currently not completely implemented. While most data
classes do contain the necessary methods the base class and GUI are incomplete.
The functionality is meant to allow the user to load backup or write-ahead copies
if the main chip file is corrupted or incomplete.

The Generator
--------------

generator/*

Like the two pools the Generator is a singleton. This Generator object exists to coordinate the conversion of
chip files and templates into KiCAD files. The actual work is done by ChipGen and its helpers.

TODO


Documentation and Help
-----------------------

doc/*

The built-in help browser is able to display MarkDown files and HTML.

Any file found inside the doc folder of the source tree is included in the
binary and is available for display at runtime. Files ending in .md are
interpreted as MarkDown, files ending in .html are interpreted as HTML.

The file name is also used as a subject for the help topic - the file extension
is stripped off and a space character is inserted before every upper case letter.

Whenever you add, delete or rename a file in the doc folder call the mkdoc.sh
script to regenerate the resource file.

TODO: currently there is no way to localize documentation or templates.

External Libraries
-------------------

Taurus/*

Chipper uses some libraries from the Taurus repositories to implement certain
functions:

 * DomExt - XML utilities, used for parsing Chip and Template files
 * ELAM - Formula parser and executor
