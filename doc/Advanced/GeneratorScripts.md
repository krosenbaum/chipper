Generator Scripts
==================

Directory: Templates/generator
Files: *.xml

Reference: basic file name without directory or xml extension

Default: output of the primary generator goes directly into the target file.

Generator scripts define how files are generated from templates.

XML Hierarchy
--------------

Example:

    <FileGen tempDir="1">
        <Env name="CAD_DIR">%G/../../cadscripts</Env>
        <Stage mode="parse" in="%I" out="%T/%f.tmp" />
        <Stage cmd="@script.template">%T/%f_script.py</Stage>
        <Stage cmd="pseudocad" wd="%T">-in=%T/%f.tmp -out=%F.wrl -script=%f_script.py</Stage>
        <Stage cmd="convertfile" in="%F.wrl" out="%F.step" sep=";">from WRL;to STEP</Stage>
        <Preview cmd="pseudocad">-in=%T/%f.tmp -out=%P/1.png</Preview>
    </FileGen>

XML Hierarchy (XPath syntax):
 * /FileGen -> document tag, defines some basic properties
    * @tempDir -> boolean (0/1): if set to 1 a temporary directory is generated and %T is set, the directory is removed after finishing, default: 0
    * @preview -> preview mode:
        * none -> no preview is generated (default if there are no Preview tags)
        * image -> images are generated through the Preview tags (default if Preview tags exist)
        * run -> the Preview tags open an external program that shows the preview
    * Stage -> defines one stage of the generator script, one stage/command per Stage tag, stages are executed in the order they are defined
        * @mode -> the generation mode of the stage:
          * cmd -> call a command (this is the default if the attribute is missing)
          * parse -> use the internal parser
          * stages -> preview mode only: execute all normal mode stages, equivalent to "skip" in normal mode
          * cp or copy -> copy a file, use @in and @out attributes to define which file goes where
          * mv or move -> move/rename a file, use @in and @out attributes to define which file goes where
          * rm, del or delete -> delete a file, use the @in attribute to define which file is deleted
          * skip* -> any stage can be made to be skipped by prefixing it with "skip" (this is the default if the attribute value is unknown)
        * @cmd -> the command to be executed
        * @wd -> optional working directory for the command
        * @in -> command mode: redirect stdin to the specified file name, optional; parser/file mode: input file for the parser/operation
        * @out -> command mode: redirect stdout to the specified file name, optional; parser/file mode: output file for the parser/operation
        * @sep -> separator character for parameters, this can be used if parameters can contain spaces, default: space
        * @prefix -> the prefix before special commands, per default "##"
        * text() -> parameters separated by spaces (or the @sep character); if the command starts with "@" this is the output file name
        * Note: when the separator is the default (space) then spaces are collapsed, so that empty parameters are avoided; if it is non-space then empty parameters are not ignored
        * Note: space at the beginning and end of the entire list of parameters is always trimmed
    * Preview -> defines one stage to generate preview pictures, same attributes and content as Stage
        * if the first Preview tag is just <Preview mode="stages"/> then all normal mode stages are executed first with %F as a temporary file
    * Env -> environment variable definition
        * @name -> name of the variable
        * text() -> value of the variable

Parameter Tags
---------------

The same % sequences that can be used in the chip file can be used in the generator script.

Logic
------

The file generator executes the following actions in order:

 1) if tempDir="1" -> create a temporary directory and set %T
 2) generate the first stage file from the template file, use the @first attribute as target file name or the configured target file name if the attribute is missing
 3) set all environment variables
 4) execute all Stage definitions in order:
    1) check location of the command, if a path is configured, use it
    2) redirect input and output streams if configured
    3) replace all % tags in parameters
    4) execute command
 5) when all stages are done: delete the temporary directory

For Previews:
 * same as above, except:
    * %F is set to a temporary file name
    * %P is set to an isolated directory
    * if the first Preview tag is the command "#stages" then all Stage definitions are run first
    * Preview tags are used instead of (or in addition to) Stage tags

Existing Generators
--------------------

3d-freePyScad -> uses FreeCAD and the FreePySCAD module to generate 3D Models

3d-openscad -> uses FreeCAD to convert OpenSCAD files to 3D Models (and OpenSCAD to generate previews)
