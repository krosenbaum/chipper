Building Chipper
=================

Requirements:
 * Qt 5.15 or 6.x
 * Linux w/ X11 (Windows, MacOS and any other OSes may work, but are untested)
 * compatible C++ 17 compiler (tested with GCC 10, modern Clang will probably work, GCC 8/9 will probably work)

Retrieve dependencies:
 bash$ git submodule init
 bash$ git submodule update
Build it:
 bash$ mkdir builddir
 bash$ cd builddir
 bash$ ../build.sh

Only Out-of-tree builds are supported - call the build script from the target directory.
Call build.sh with "-help" to see possible options.

Other OS'es
------------

If you want to compile for Windows or MacOS you will need to adjust the recipe
above. The application has never been tested on those OS's.

On Windows you will probably need to use the MinGW version of Qt, since the build
script requires a Unix shell.

Distributing
-------------

You can move the "bin" directory to any place you like, but the libraries need to stay
inside to be found by the executables.

The "Templates" directory must be available in the parent directory of the executable.

Any other directory or file in the target directory contains only temporary files (including
"lib", which contains temporary copies of some libraries).

If you want to ship it with your own Qt you will have to copy the Qt DLLs to the bin directory
and relevant plugins to the correct places below it. You will at least need QtCore, QtGui, QtWidgets,
QtXml and related plugins (e.g. generic, iconengines, imageformats, platforms - you need to test which
ones are required).

Good Luck.
