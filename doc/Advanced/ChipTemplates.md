Understanding/Writing Chip Templates
======================================

Each chip template is in one directory and consists of multiple files that are
described below.

Chip Meta File
---------------

These files are the computer readable description of all meta data in chip
templates. There should be exactly one such file per template directory.

File name: chip_meta.xml

Format: XML (UTF8)

XML hierarchy:

* /ChipMeta tag -> document tag, attributes:
    * @v="1" -> file format version, currently only version "1" is valid

    * /ChipMeta/Description -> human readable description and copyright
        * text() -> full description
        * @title -> short title, shown in tables/lists
        * @author -> optional name of the author
        * @license -> optional license this chip file is under
        * @copy -> optional copyright string
        * @readme -> documentation file name (can be simple UTF8 text or HTML if the
                file name ends in .html)

    * /ChipMeta/Pins -> pin definitions
        * @id -> ID of this pin definition (referenced from target files below)
        * @mode -> geometry calculation mode:
            * @mode="rows" -> composed of discrete rows (SIP, DIP, SOP, QFx), this is the default
            * @mode="grid" -> composed of a grid (BGA)
        * @repeat -> grid mode: formula that determines how often the row pattern repeats
        * /ChipMeta/Pins/Row -> definition of a single row in rows mode or grid pattern row in grid mode
            * @id:
                * rows mode: any unique alphanumeric value
                * grid mode: formula to calculate the row number, !TODO!
            * @maphint: space separated list of equivalent names for the copy-algorithm
            * text(): human readable description for the row
        * /ChipMeta/Pins/Fill -> auto-fill ruleset that can be selected from menu
            * @id -> reference ID for the pre-selection upon generating the target file
            * @name -> human readable name for the menu
            * sub-elements are an ordered list of instructions for the algorithm:
            * /ChipMeta/Pins/Fill/Sort -> sort the current pool of pins
                * @by="num" -> sort by the order in which it appears in the variant table
                * @by="id" -> sort by the "pin number" value, case-insensitive ASCII order
                * @by="name" -> sort by the main pin name, case-insensitive ASCII order
            * /ChipMeta/Pins/Fill/Take -> take a group of pins and assign them to a row
                * @into -> the row into which it is taken, use the special row "" to completely remove pins from the pool
                * @autohide -> automatically hides some pins (default: none)
                    * "none" -> no auto hiding
                    * "name" -> consecutive pins with the same name get the same position
                    * "all" -> all pins in this set get the same position
                    * "type" -> all pins with the same electrical type get the same position
                * text() -> the formula to arrive at the list
            * /ChipMeta/Pins/Fill/Filter -> filters pins from the pool by specified criteria into a specified row; matching pins are removed from the pool
                * @by -> filter criteria:
                    * "type" -> filter by basic type, see [Chip File Format](ChipFileFormat.md) section /Chip/Variant/Pin/@type
                    * "func" -> filter by electrical function, see [Chip File Format](ChipFileFormat.md) section /Chip/Pins/Pin/@func
                    * "name" -> filter by pin name, you can use ? and * as wildcard like with file names
                    * "namere" -> filter by pin name with a regular expression
                * @into -> row that matching pins are moved into, use the special row "" to completely remove pins from the pool
                * @autohide -> automatically hides some pins (see above)
                * text() -> depends on filter type:
                    * by="type": list of pin types that match the filter (separated by spaces)
                    * by="func": list of pin electrical functions that match
                    * by="name"/by="namere": wildcard or regex pattern that matches the name
            * TODO: specify how grid style rows are handled


    * /ChipMeta/Variables -> collection of variables that are used in templates, contains:
        * /ChipMeta/Variables/v -> single visible variable
            * text() -> human readable description of the variable
            * @id -> variable name
            * @default -> default value, this can be a formula
            * @min -> minimum value - the input validator complains below this value
            * @max -> maximum value - the input validator complains above this value
            * @step -> value of a step in the spin box used to edit the value (not fully enforced, just a convenience)
            * @type -> Data type of the variable
                * @type="float" -> the default, any floating point value allowed
                * @type="int" -> validate as integer value
                * @type="enum" -> use a drop down box instead, @enum is mandatory
                * @type="string" -> free form string data
            * @enum -> references an enum definition;
                    for floating point or int: offers the values as defaults in an optional drop-down;
                    for enum: defines selection of drop-down box and restricts values to those
        * /ChipMeta/Variables/h -> single hidden variable
            * @default -> formula to calculate this variable (see below)
            * @id -> variable name
        * /ChipMeta/Variables/enum -> defines an enum
            * @id -> name of the enum that can be referenced via @enum in a variable
            * text() -> enum definition, values are semicolon-separated, each value
                    consists of a floating point value and optional text separated
                    by a space, e.g.:
                    "0.5 small pitch; 1.0 medium pitch; 2.54 large pitch"
                    defines 3 values with numeric values 0.5, 1.0 and 2.54


    * /ChipMeta/Symbol -> symbol template
        * @id -> unique symbol id used as a reference within the template/program
        * @name -> human readable name of the symbol template
        * @file -> template file in kicad_sym format plus special syntax (see below)
        * @left/@right/@top/@bottom -> pin row definitions, see below
        * @generator -> name of the generator script to be used, see [file generator script](GeneratorScripts.md)
        * @rows -> ID of the pin row specification for this target, see /ChipMeta/Pins/@id above
        * @prefill -> ID of the fill algorithm for the pin rows, see /ChipMeta/Pins/Fill/@id above

    * /ChipMeta/Footprint -> footprint template
        * @id -> unique footprint id used as a reference within the template/program
        * @name -> human readable name of the footprint template
        * @file -> template file in kicad_mod format plus special syntax (see below)
        * @generator -> name of the generator script to be used, see [file generator script](GeneratorScripts.md)
        * @rows -> ID of the pin row specification for this target, see /ChipMeta/Pins/@id above
        * @prefill -> ID of the fill algorithm for the pin rows, see /ChipMeta/Pins/Fill/@id above

    * /ChipMeta/Model -> 3D model template
        * @id -> unique 3D model id used as reference
        * @name -> human readable name of the 3D model template
        * @file -> OpenSCAD file (see chapter below)
        * @generator -> name of the generator script to be used, see [file generator script](GeneratorScripts.md)
        * @rows -> ID of the pin row specification for this target, see /ChipMeta/Pins/@id above
        * @prefill -> ID of the fill algorithm for the pin rows, see /ChipMeta/Pins/Fill/@id above

    * /ChipMeta/Include -> include another chip meta file
        * @path -> either path to the directory containing a chip_meta.xml file or complete path to the XML file itself (can have a different name than chip_meta.xml)
        * pin definitions, symbols, footprints, and models are added to the current template
        * variables are added to the template if another variable with the same name does not yet exist, outputs a warning if a variable with the same name but different type exists
        * description and all other meta data are ignored


Hidden variables and default values can use very simple formulas:
see [Formula Syntax](../Formula/Syntax.html) for basic syntax. See [Formula Variables](../Formula/Variables.html) for available variables.


Predefined Variables
---------------------

See [Formula Variables](Formula/Variables.html) for details.

All variables only exist in the context of the chip target that is being generated.

Rules for Symbol and Footprint Templates
-----------------------------------------

Comments are lines that start with "#". Special commands start with "##" on their own line.
Spaces at the beginning of comment or command lines are permitted for formatting.

Values can be replaced with formulas at any place by using {{formula}} syntax.
All defined variables are available and the syntax is the same as described
above. More variables can be defined (see below).

Special array variables contain multiple complex values that can only be
accessed from inside a loop. For example the pin array should be accessed by
a ##LOOP:p:pin - during each iteration there are "p.num" and "p.name"
variables with the respective values for that pin while the variable "p"
contains the array index.

File Structure / Area Commands:

    ##AREA:PROLOG
    ##AREA:PRE
    ##AREA:TEMPLATE
    ##AREA:EPILOG
    ##AREA:COMMENT
    ##AREA:SKIP
    ##AREA:POST
    ##AREA:END
 - The area between PROLOG and the next area command contains lines that are copied to the start of the file if the file
   does not yet exist or is empty
 - The area between EPILOG and the next area contains text that is copied to the end of the file:
    - if the file does already exist the parser tries to find the epilog text and removes it before inserting the new content
    - the generated epilog is expected to be a single line if it exists at all (empty lines are eliminated)
 - The "Pre" area is run before any actual output is run, it can be used to do calculations or to output some basic
   information to the user, it is not written into the target file, if it fails the target file is not touched and the generator run fails
 - The "Post" area is run after the file has been generated, it can be used to output some information for the user, it
   is not written into the target file; if it fails the generator does not care and moves on
 - The area between TEMPLATE and the next area command contains the actual template - the use of variables,
   loops and other commands is possible
 - the special comment and skip sections may appear multiple times, their contents are ignored - anything in front of the first
   section or behind END counts as comment
 - currently no more content is allowed after the END tag, if it is missing it is implied by the end of the file
 - the order of areas in the file can vary, however they are always executed in this order when generating:
   1) run the PRE area and display its output directly
   2) run the PROLOG section and remember it
   3) run the TEMPLATE section and remember it
   4) run the EPILOG section and remember it
   5) check whether the file is empty, if yes: insert PROLOG (if it exists) otherwise discard it
   6) check whether the file ends in EPILOG, if yes: remove it
   7) append generated TEMPLATE text
   8) append EPILOG text
   9) run the POST area and display its output directly
   10) done
 - the TEMPLATE section must exist, all other sections are optional and presumed empty if they do not exist
 - if a section appears multiple times then the content of all occurances is concatenated and handled as one continuous section
 - the order in which sections appear in the template file does not influence the order of execution


Inline Comments:

    ##AREA:COMMENT
    ##! inline comment
 - the comment AREA should be used for larger pieces of texts, like design explanations and copyright notices
 - inline comments inside an area can be put on a single line starting with the "!" special command
 - inline comments should be used if an area would disrupt the flow of the template
 - inline comments and comment areas can also be used if the underlying target language does not have a comment
   syntax of its own, since these comments do not appear in the target file

Control Structures:

    ##FOR {{init}} {{condition}} {{incr}}
    ##FOREACH var {{array-expression}}
    ##WHILE {{condition}}
    ##END
    ##END ! comment text
 - various kinds of loops, each one begins with a FOR, FOREACH, or WHILE line and ends with an END line
 - loops can be nested
 - FOR: simple C-like iteration
    - {{init}} - executed once before starting the loop, e.g. {{i=0}}
    - {{condition}} - evaluated after each iteration, must yield boolean, loop continues if true, e.g. {{i<42}}
    - {{incr}} - executed after each iteration before the next iteration, e.g. {{i=i+1}}
 - FOREACH: iteration over an array
    - var is the name of the variable to which the current iteration value is assigned, must be a valid variable name, alternatively:
    - idx,var (with comma, no spaces or other special chars) - assigns the current array index to "idx" and the value to "var"
    - {{array-expression}} - an expression that yields an array or structure, the loop will iterate over its members
 - WHILE: iterates while the {{condition}} yields boolean true
 - END may be followed by a !comment

[]()

    ##IF {{condition}}
    ##ELSE
    ##END
     or
    ##IF {{condition}}
    ##ELSE ! comment one
    ##END ! comment two
 - decision, casts the result of formula to bool and then decides
 - ELSE is optional
 - both ELSE and END may be followed by a comment

Calculations:

    ##EVAL {{formula}}
 - executes the formula, it may contain any functions or assignments, the
   result is discarded without an explicit assignment
 - executed when encountered, IF and loops are evaluated before CALC, so a
    variable assignment inside a loop is executed for each iteration, an
    assignment inside IF is only executed if this branch is used

Output control:

    ##OUTPUT [on/off] [(no)empty] [(no)trim]
 - changes output settings of the current section:
   - on - activates output - generated lines are written to the target (file or stdout depending on section)
   - off - de-activates output - generated lines are discarded
   - empty - empty (whitespace only) lines are written if output is on
   - noempty - whitespace only lines are discarded
   - trim - lines are trimmed of leading and trailing whitespace before they are written
   - notrim - lines are uses as-is without trimming spaces
 - the default for each new section is "##OUTPUT on empty notrim"
 - the command has an effect on all lines following it until the next occurance of ##OUTPUT
 - it is not possible to use a {{formula}} to calculate sub-commands, but the command can be
   made conditional by putting it into an ##IF or ##ELSE section

Script Variables and functions for Templates
-------------------------------

String values as helpers:
 * brace -> brace "{" string
 * dbrace -> double brace "{{" string
 * openTag -> the configured open tag (usually "{{")
 * closeTag -> the configured close tag (usually "}}")

Basic functions:
 * uuid() -> generates a new random UUID
 * now() -> current date/time in ISO format
 * decNow() -> current Unix timestamp (seconds since Epoch) as decimal number in a string
 * hexNow() -> current Unix timestamp (seconds since Epoch) as hexa-decimal number (needed for footprints, "tedit" fields)

Pathnames and other names:
 * manufacturer -> (%M) manufacturer name
 * manufacturerFileName -> (%m) manufacturer name, sanitized for use as file name
 * chipType -> (%C) chip type name
 * chipTypeFileName -> (%c)     chip type name, sanitized for use as file name
 * author -> (%A) file author name
 * variantName -> (%V) variant name that generates the file
 * variantFileName -> (%v) variant name that generates the file, sanitized for use as file name
 * packageName -> (%P) package name (default: template title)
 * packageFileName -> (%p) package name (default: template title), sanitized for use as file name
 * templateName -> (%T) template title
 * templateFileName -> (%t) template title, sanitized for use as file name
 * targetName -> (%N) target name configured in Symbol/Footprint/Model
 * targetFileName -> (%n) sanitized target name for use as a file name
 * generatorId -> (%g) generator file ID from template
 * generatorDir -> (%G) generator script directory name (including path)
 * generatorFilePath -> (%s) full generator script file name (including path)
 * symbolDir -> (%S) symbol directory (normally you would use ${*_SYMBOL_DIR}
 * symbolVarName" -> variable name for symbols
 * footprintDir -> (%F) footprint directory (normally you would use ${*_FOOTPRINT_DIR}
 * footprintVarName -> variable name for footprints
 * modelDir -> (%3) 3D model directory (normally you would use ${*_3DMODEL_DIR}
 * modelVarName -> variable name for 3D models
 * rootDir -> (%R) root target directory
 * rootVarName -> variable name for root directory
 * targetFilePath -> (%Y) target file name, the one configured in the Chip file's Target section (a temporary file in preview mode), fully resolved
 * targetFileRoot -> (%y) root of the target file name (if %Y is "path/to/target-file" then %y is "target-file"), fully resolved
 * inputFilePath -> (%I) input file name, the one configured in the Chip file's Symbol/Footprint/Model output section
 * inputFileRoot -> (%i) root of the input file name (if %I is "path/to/input-file" then %i is "input-file")
 * templateDir -> (%D) template directory
 * targetDir -> (%d) target directory
 * tempDir -> (%Z) temporary directory if it is generated (always exists in preview mode)
 * previewDir -> (%z) preview directory (undefined for non-preview mode)

Objects:
 * chip -> the complete chip object
 * variant -> the variant that is being transformed
 * package -> the package that is being transformed
 * target -> the target file definition, also available as:
   * symbol -> (if this is a symbol file) the symbol being transformed
   * footprint -> (if this is a footprint file) the footprint being transformed
   * model -> (if this is a 3D model file) the model being transformed

3D Models and OpenSCAD
-----------------------

3D Model definitions only reference an OpenSCAD script file. All rendering is
done according to the rules below.

The pipeline for 3D models:
1) create OpenSCAD script from chip configuration and template
2) Generate output:
    * run it through OpenSCAD for preview pictures
    * run it through FreeCAD to convert to STEP format for KiCAD

Put defaults for the variables into a COMMENT section, so that you
can test the OpenSCAD script in OpenSCAD.

In order to work in FreeCAD some restrictions should be followed:
 * certain complex functions (like minkowsky) don't work
 * the entire rendered chip must be a single entity - i.e. it needs to be
   wrapped in union(){ ... }
 * color() instructions must be placed immediately before the body they influence
   (like cube() or cylinder()), otherwise they get lost - placing color() before
   rotate() or before union() and similar instructions does not work.
