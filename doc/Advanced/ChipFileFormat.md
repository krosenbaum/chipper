Chip Definition Files
========================

Actual chips are stored in Chip Definition files. In conjunction with a
template these definition files can be used to regenerate the symbols,
footprints and 3D models of the chip.

File extension: *.chip

Format: XML (UTF8)

This document describes how those files are structured internally. If you do not plan
to edit chip files manually or to extend/change the program itself then you can skip this
document.

XML Hierarchy
---------------

* /Chip -> document tag
   * @v="1" -> currently only version 1 is supported

    * /Chip/Meta -> Meta data about the chip file
        * @manufacturer -> name of manufacturer
        * @type -> name of chip type
        * @author -> name of author
        * @copy -> copyright string of this chip definition (not the chip itself)
        * @license -> license of this chip definition (not the chip itself)
        * @datasheet -> URL for the datasheet
        * @refprefix -> symbol reference prefix (default: "U")
        * @keywords -> keywords that describe this chip
        * @description -> description of this chip

    * /Chip/Targets -> definition of target libraries
        * Note: file name substitution as described below is possible
        * @symbols -> kicad_sym file that this definition is appended to (relative to configured symbol directory)
        * @footprints -> *.pretty directory that gets the kicad_mod files (relative to configured footprint directory)
        * @models -> 3D model directory (relative to configured 3d models directory)

    * /Chip/Pins -> pin definitions
        * /Chip/Pins/Pin -> definition of a single pin, only functional pins are declared
            * @name -> unique name of the pin
            * @func -> electrical function of the pin:
                * "signal" - a bidirectional logic signal pin (default; KiCad: bidirectional)
                  * "signalin" - an input signal pin (KiCad: input)
                  * "signalout" - an output signal pin (KiCad: output)
                * "tri" - a tri-stated output pin (KiCad: tri_state)
                * "opcol" - open collector output (KiCad: open_collector)
                * "opem" - open emitter output (KiCad: open_emitter)
                * "passive" - passive pin that can be anything (KiCad: passive)
                * "powerin" - a power input pin (e.g. Vcc, Vdd; KiCad: power_in)
                  * "powerout" - a power output pin (KiCad: power_out)
                  * "power" - a bidirectional or passive power pin (KiCad: passive)
                  * note that hidden/stacked power pins are generated as "passive" regardless of type, since otherwise they would cause ERC problems
                * "groundin" - a ground input connection pin (e.g. GND; KiCad: power_in)
                  * "groundout" - a ground output connection (KiCad: power_out)
                  * "ground" - a passive/bidi ground pin (KiCad: passive)
                  * note that hidden/stacked ground pins are generated as "passive" regardless of type, since otherwise they would cause ERC problems
                * "nc" - not connected (KiCad: no_connect)
                * "free" - generates a "free" pin that does not generate any ERC issues
                * "unspec" - generates an "unspecified" pin to force end users to modify the symbol themselves - creates an error no matter what it is connected to
                * note: other than KiCad Chipper makes a difference between Power and Ground to be able to sort pins more easily
            * @line -> line type of the pin (how the pin is drawn)
                * line - simple line (default)
                * input_low - used for low active inputs
                * inverted - used for inverted I/O
                * non_logic - used for non-logic pins (usually not connected pins that are visible)
                * output_low - used for low active outputs
                * clock - used for clock I/O
                * clock_low - used for clock I/O that advance at low
                * edge_clock_high - used for clock I/O that advance at signal edges
                * inverted_clock - used for inverted clock I/O
            * /Chip/Pins/Pin/Alt -> definition of alternative functions
                * alternative functions (Alt elements) can have the same attributes, but must not have child elements
                * the @name attribute must be unique among the parent pin name and all alternative names

    * /Chip/Variant -> definition of a pin-layout for this chip, all package types
            within a variant have the same pin to function assignments;
            e.g. 32pin variants of an MCU
        * @uid -> a UUID that uniquely identifies the variant
        * @name -> name of the variant
        * @pins -> number of pins for this variant
        * /Chip/Variant/Pin -> definition of a single pin
            * @num -> pin number
            * @type -> pin type:
                * "con" - connected pin (default, visible in symbol and footprint)
                * "missing" - pin is missing from footprint (no name!), does not appear in symbol or footprint, not even as hidden pin
                * "stub" - like a missing pin, has no function or contact, but is 3D rendered as a small metal stub
                * "nc" - pin is internally not connected (no name), but visible as "NC" so it can be e.g. grounded; visible in symbol and footprint
            * @name -> pin name (must be empty or exist in /Chip/Pins/Pin/@name), multiple physical
                    pins can correspond to one chip pin, not all chip pins have to be
                    connected to a physical pin
            * Note: connected and hidden pins must have a name so they can appear in the symbol file with that name;
                    not connected pins should not have a name - they appear as "NC" in the symbol file;
                    missing pins must not have a name - they do not appear in any target files and are even
                    rendered as missing pins in the 3D model;
                    stub pins are similar to missing pins - they have no name and do not appear in symbol or footprint files,
                    in the 3D model they are rendered as shortened pins that do not touch the PCB
            * Note: the file may contain fewer or more pins than configured in the @pins attribute above, the
                    internal interface must only use pins with numbers between 1..@pins and pins that are not
                    defined in the file are assumed to be connected with no name; the program may eliminate pins
                    with invalid numbers (non-integer, 0 or negative)

        * /Chip/Variant/Package -> concrete package for a variant; e.g. 32pin TQFP vs. 32pin TQFN of the same chip
            * @uid -> a UUID that uniquely identifies the package
            * @template -> template files to be used (this is the directory name of the template, e.g. "QFP")
            * @name -> optional name/alias for the package definition (can be used to distinguish e.g. TQFP-32 0.8mm vs. TQFP-32 0.5mm); default: template name
            * @keywords -> keywords that describe this package (added to Meta data)
            * @description -> description of this chip (default: whatever is found in Meta above)

            * /Chip/Variant/Package/Variables -> variable values for the template
                * /Chip/Variant/Package/Variables/v -> value for a visible variable (hidden variables are calculated on the spot)
                    * @id -> variable name
                    * @value -> variable value

            * /Chip/Variant/Package/Symbol -> symbol variation to be generated
                * @uid -> a UUID that uniquely identifies the symbol
                * @id -> selected template ID of the symbol (corresponds to id in template)
                * @name -> name of the symbol
                * @footprint -> default footprint
                * @value -> default value (normally symbol name)
                * ...TBD
                * /Chip/Variant/Package/Symbol/Pins -> definition of a pin row
                    * @id -> pin row name (see template)
                    * text() -> space separated ordered list of pin numbers:
                        * positive integer or text (digits and ASCII letters allowed) -> the pin with the given "number" is visible
                        * "+" -> a pin-sized space without pin
                        * prefixed with a minus "-1", "-Vcc" -> the pin is hidden and occupies the same space as the previous one;
                          this means that this hidden pin is physically connected to the previous pin on the PCB

            * /Chip/Variant/Package/Footprint -> footprint variation to be generated
                * @uid -> a UUID that uniquely identifies the footprint
                * @id -> selected id of the footprint (corresponds to template)
                * @name -> name of the footprint (file) - per default constructed from variant name and template footprint name
                * @model -> 3D model associated with this footprint

            * /Chip/Variant/Package/Model -> 3D model to be generated
                * @uid -> a UUID that uniquely identifies the model
                * @id -> selected id of the model (corresponds to template)
                * @name -> name of the model (file) -> per default constructed from variant name and template model name

File Name Substitution
-----------------------

Wherever target files or directories are specified in a chip file the substitution described
here can be utilized.

Environment variables can be used in the KiCAD syntax ${varname}. The sequence $$ can be used to generate a single $ sign.
The { and } characters are not interpreted on their own.

%-substitution can be used to auto-generate some path name components:
    * %M - manufacturer name
    * %m - manufacturer name, sanitized for use as file name
    * %C - chip type name
    * %c - chip type name, sanitized for use as file name
    * %A - file author name
    * %V - variant name that generates the file
    * %v - variant name that generates the file, sanitized for use as file name
    * %P - package name (default: template title)
    * %p - package name (default: template title), sanitized for use as file name
    * %T - template title
    * %t - template title, sanitized for use as file name
    * %N - target name configured in Symbol/Footprint/Model
    * %n - sanitized target name for use as a file name
    * %g - generator file ID from template
    * %G - generator script directory name (including path)
    * %s - full generator script file name (including path)
    * %S - symbol directory (normally you would use ${*_SYMBOL_DIR}
    * %F - footprint directory (normally you would use ${*_FOOTPRINT_DIR}
    * %3 - 3D model directory (normally you would use ${*_3DMODEL_DIR}
    * %R - root target directory
    * %Y - target file name, the one configured in the Chip file's Target section (a temporary file in preview mode), fully resolved
    * %y - root of the target file name (if %Y is "path/to/target-file" then %y is "target-file"), fully resolved
    * %I - input file name, the one configured in the Chip file's Symbol/Footprint/Model output section
    * %i - root of the input file name (if %I is "path/to/input-file" then %i is "input-file")
    * %D - template directory
    * %d - target directory
    * %Z - temporary directory if it is generated (always exists in preview mode)
    * %z - preview directory (undefined for non-preview mode)
    * %o - in Env tags of generator scripts only: old value of the variable or empty string if it does not exist yet
    * %% - single % character

When creating files Chipper substitutes %-sequences as well as environment variables to generate the name of the file to write into.

When exporting file names into variables to be stored in generated files, Chipper does replace %-sequences, but not variable names - those are left for KiCAD to interprete.
