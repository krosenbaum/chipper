Chipper Generator
==================

<img alt="Logo" src="qrc:/chipper.png" align="right" />

Chipper is a Symbol/Footprint/3Dmodel generator for KiCAD. It uses templates to
make the generation of many similar models and variations easier.

(c) Konrad Rosenbaum, 2021-22
protected under the [GNU GPL v.3](License.html) or at your option any newer

Clarification/Exception: although this program uses substantial portions of template
files to generate symbols, footprints and 3D models, those output files are
not considered derivatives of the program or its templates per section 2 of the
GPL. They are considered derivatives of their input chip description files (*.chip)
only and as such are considered the property of the copyright owner of those
chip files.

Target Audience
----------------

Chipper allows the user to leverage templates for common physical chip (device) packages to create KiCAD files that can then be used to create new circuits and PCBs in KiCAD. It is thus geared towards creating symbols, footprints and 3D models for chips that use common chip packages, but have new pinouts. It is not meant to recreate very simple or common devices (like resistors or diodes) that already exist in KiCAD.

For example:

 * new transistor type in a typical transistor package: use one of the existing transistors in KiCAD and alter its "value"
 * common resistor: use one of the many resistor symbols and packages in KiCAD
 * new device with an unusual custom package: sorry, you will have to create the symbol/footprint/3D model manually in KiCAD (and OpenSCAD or FreeCAD for 3D)
 * common chip that exists in KiCAD: use the KiCAD symbol and footprint, unless it is lacking in a way that is important to you then Chipper might help you...
 * new variant of a common chip, uses a standard package: whether it has a new pinout or new dimensions, Chipper will help you generate KiCAD files
 * new chip type with standard package: Chipper will help you generate KiCAD files
 * new chip with unsupported standard package: new templates are welcome!
 * new chip with non-standard package: it is probably easier to create the files manually than to create a template first
