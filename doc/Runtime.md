Running Chipper
================

Simply start the executable without any parameters for the GUI mode.

Runtime requirements:
 * the templates directory must be either available directly from the
   executable's parent directory or must be configured to an alternative
   location
 * OpenSCAD (2021.xx or newer) and FreeCAD (0.19 or newer) are needed to
   generate 3D models

Per default Chipper starts in the interactive GUI mode.
You can also start it in generator mode, in which case it simply processes any
*.chip files you specify on the command line and creates all configured symbols,
footprints and 3D models.


Configuration Values
----------------------

The following pathes are configurable:

 * Root Path: if any of the other pathes are relative this is prefixed to them, the default is the parent of the application directory
 * Symbol Path: the directory to which symbol files are written
 * Footprint Path: the directory to which footprint files are written
 * 3D Model Path: the directory to which 3D model files are written
 * Template Path: the directory where the program looks for templates

In addition the Environment Variable Prefix is configurable (see below).

Most of those values can be configured via:
 1) Command Line Parameters take precedence over any other configuration.
 2) Environment Variables take precedence over:
 3) Values configured in the GUI configuration dialog.

The environment variable prefix can not be configured via environment variable.

Command Line Parameters:
-------------------------

GUI Mode options:
 * -gui  -> start with GUI (default)
 * -nogui -> suppress GUI (for batch execution)

What to run:
 * -generate -> start in generator mode
 * -help -> only show some help and exit
 * -version -> show the Chipper version number and exit
 * no parameter means the normal interactive GUI is loaded

Configuration Options:
 * -root=... -> sets the root path (prefixed to all other pathes if they are relative)
 * -symbol=... -> sets the symbol directory
 * -footprint=... -> sets the footprint directory
 * -3dmodel=... -> sets the 3D model directory
 * -template=... -> sets the template directory
 * -envprefix=... -> sets the environment variable prefix (see below)

Open files:
 * *.chip -> open the Chip file (either in GUI or to generate)


Environment Variables
----------------------

Environment variables can be used to override path configurations. Variable
names have the format $PREFIX_PATH - with PREFIX defaulting to WIZZARD and PATH
being one of the following:

 * $***_ROOT -> root directory
 * $***_SYMBOL_DIR -> symbol directory
 * $***_FOOTPRINT_DIR -> footprint directory
 * $***_3DMODEL_DIR -> 3D model directory
 * $***_TEMPLATE_DIR -> template directory

For example this will override the root directory on command line and the 3D model directory from environment variables:<br/>

    MYPREFIX_3DMODEL_DIR=relative/path/3dmodel chipper -envprefix=MYPREFIX -root=/opt/chipper/my/root



Running as Generator
---------------------

Starting the command with the "generate" or "-generate" parameter will run it in generator mode. In this mode
all chip files mentioned on the command line are converted to KiCAD files and then the program exits.

If the "-nogui" option is also given the program runs in full command line mode without trying to connect to
the GUI (X11, wayland, ...). Per default or if the "-gui" option is given the program tries to connect to the
GUI and to display a progress dialog while running.

For example this could be part of a Makefile to auto-generate chips:

    chipper -nogui -root=`pwd`/../output -symbol=mychip_symbols -footprint=mychip.pretty -3dmodel=3dmodels *.chip
