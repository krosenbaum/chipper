#!/bin/bash

cd `dirname $0`

#clean up dir
rm -rf source-doc
mkdir -p source-doc

#create Taurus doc
(
 cd Taurus
 ./build-src-doc.sh
)
ln -s `pwd`/Taurus/doc/chester source-doc/chester
ln -s `pwd`/Taurus/doc/elam/source source-doc/elam
ln -s  `pwd`/Taurus/doc/domext source-doc/domext

#create chipper doc
doxygen

#create index
cat >source-doc/index.html <<EOF
<html>
<ul>
<li><a href="chipper/index.html" target="_blank">Chipper</a> Application
<li>Libraries<ul>
 <li><a href="elam/namespaceELAM.html" target="_blank">ELAM</a> Formula Library
 <li><a href="domext/index.html" target="_blank">DOM extension</a> Library
 <li><a href="chester/index.html" target="_blank">Chester</a> d-pointer Library
</ul></ul>
</html>
EOF
