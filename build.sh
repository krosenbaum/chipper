#!/bin/bash

#set to abort on error, output details
set -e
#set -x

#locate source and build dirs
TDIR=`pwd`
cd `dirname $0`
SDIR=`pwd`
SCRIPT="$0"
NOBUILD=0

#check that this is an out-of-tree build
test "$SDIR" = "$TDIR" && {
 echo "Only out-of-tree builds are supported."
 exit 2
}

#default settings
BINDIR=bin
ELAM=elam
DOMX=domx
QMAKE=qmake
MAKE=make

QM_ARGS=""
QM_CONF=""

#help output
help(){
cat <<EOF
Usage: $SCRIPT [-options ...]
    -help - output help
    -debug - build in debug mode, including tests
    -release - build in release mode, excludes tests
    -tests - build tests
    -notests - do not build tests
    -nobuild - do not actually build, just output what would be done
    -qt=* - Qt version selection relayed to QMake (assuming QtChoser)
    -qmake /path/to/qmake - set the QMake binary
    -qconf "arguments" - add arguments to QMake
    -make "/path/to/make [arguments ...]" - set the Make binary and options
EOF
 exit 0
}

nobuild(){
 echo "Not building. This is what I would do:"
 echo "QMake: $QMAKE"
 echo "  Arguments: $QM_ARGS"
 echo "  Config: $QM_CONF"
 echo "  Qt Version:" $($QMAKE $QM_ARGS -query QT_VERSION)
 echo "Make: $MAKE"
 echo "Source Dir: $SDIR"
 echo "Target Dir: $TDIR"
 echo "Script File: $SCRIPT"
 exit 0
}

#get cmd line args
while test $# -gt 0 ; do
 case "$1" in
    -help)
        help
        ;;
    -debug)
        QM_CONF="$QM_CONF CONFIG+=debug CONFIG+=tests"
        ;;
    -release)
        QM_CONF="$QM_CONF CONFIG+=release CONFIG+=notests"
        ;;
    -tests)
        QM_CONF="$QM_CONF CONFIG+=tests"
        ;;
    -notests)
        QM_CONF="$QM_CONF CONFIG+=notests"
        ;;
    -nobuild)
        NOBUILD=1
        ;;
    -qt=*)
        QM_ARGS="$1"
        ;;
    -qmake)
        QMAKE="$2"
        shift
        ;;
    -qconf)
        QM_CONF="$QM_CONF $2"
        shift
        ;;
    -make)
        MAKE="$2"
        shift
        ;;
    *)
        echo "Unknown argument: $1"
        help
        exit
        ;;
 esac
 shift
done

test $NOBUILD -gt 0 && nobuild
echo "Building Chipper..."

#create sub-dirs
cd $TDIR
mkdir -p $ELAM $DOMX $BINDIR
ln -sf $SDIR/Templates . || true

#build external libs
cd $ELAM
$QMAKE $QM_ARGS $QM_CONF $SDIR/Taurus/elam
$MAKE
cd ../$DOMX
$QMAKE $QM_ARGS $QM_CONF $SDIR/Taurus/domext
$MAKE

cd $TDIR
cp -ra lib/lib* $BINDIR

#copy documentation and create qrc
cp -ra $SDIR/doc .
cp -a $SDIR/Taurus/doc/elam/syntax.html doc/Formula/Syntax.html
cp -a $SDIR/Taurus/doc/elam/predef.html doc/Formula/BasicFunctions.html
$SDIR/mkdoc.sh

#actually build chipper
$QMAKE $QM_ARGS $QM_CONF -recursive $SDIR
$MAKE

echo
echo "Done Building Chipper."

exit 0
