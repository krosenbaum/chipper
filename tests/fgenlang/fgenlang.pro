TARGET = ctest-fgenlang
TEMPLATE = app
QT += testlib xml
QT -= gui
CONFIG += testcase

include (../../basics.pri)
include (../../Taurus/chester.pri)

LIBS += -lchipper -lctest-fglib

HEADERS += fgl.h
SOURCES += fgl.cpp

INCLUDEPATH += ../../src ../../template ../../chip ../../generate ../fgenlib
