// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "fgl.h"

#include "autodisconnect.h"
#include "chipdata.h"
#include "chipdetail.h"
#include "chippackage.h"
#include "chipper.h"
#include "chippool.h"
#include "chipvariant.h"
#include "filegen.h"
#include "generator.h"
#include "templatepool.h"
#include "templates.h"
#include "tracer.h"

#include <QtTest>
#include <QDir>
#include <QByteArrayList>
#include <QStringList>

void FglTest::initTestCase()
{
    //init env
    FgTLib::initTestCase();
    //load chip
    QVERIFY(ChipPool::instance().findOrLoadChip(":/tlang.chip") != nullptr);
    Generator::instance().createPool();
}

void FglTest::languageFlat()
{
    //check contents
    QFile fd("Sym1");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    auto l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();

    QCOMPARE(l.size(),14);
    QCOMPARE(l[0],"for 0");
    QCOMPARE(l[1],"for 1");
    QCOMPARE(l[2],"for 2");
    QCOMPARE(l[3],"for 3");
    QCOMPARE(l[4],"for 4");
    QCOMPARE(l[5],"after for 5");
    QCOMPARE(l[6],"foreach 1");
    QCOMPARE(l[7],"foreach 2");
    QCOMPARE(l[8],"foreach 3");
    QCOMPARE(l[9],"foreach 4");
    QCOMPARE(l[10],"while 10");
    QCOMPARE(l[11],"while 11");
    QCOMPARE(l[12],"if:true");
    QCOMPARE(l[13],"if:true");
}

void FglTest::languageNested()
{
    //check contents
    QFile fd("Sym2");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QStringList l;
    for(auto s:QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts)){
        s=s.trimmed();
        if(s.isEmpty())continue;
        l<<s;
    }
    fd.close();

    QCOMPARE(l.size(),21);
    QCOMPARE(l[0],"for 0");
    QCOMPARE(l[1],"foreach a 0");
    QCOMPARE(l[2],"foreach b 0");
    QCOMPARE(l[3],"if: 0 <= 2");
    QCOMPARE(l[4],"for 1");
    QCOMPARE(l[5],"foreach a 1");
    QCOMPARE(l[6],"foreach b 1");
    QCOMPARE(l[7],"if: 1 <= 2");
    QCOMPARE(l[8],"for 2");
    QCOMPARE(l[9],"foreach a 2");
    QCOMPARE(l[10],"foreach b 2");
    QCOMPARE(l[11],"if: 2 <= 2");
    QCOMPARE(l[12],"for 3");
    QCOMPARE(l[13],"foreach a 3");
    QCOMPARE(l[14],"foreach b 3");
    QCOMPARE(l[15],"if: 3 > 2");
    QCOMPARE(l[16],"for 4");
    QCOMPARE(l[17],"foreach a 4");
    QCOMPARE(l[18],"foreach b 4");
    QCOMPARE(l[19],"if: 4 > 2");
    QCOMPARE(l[20],"after for 5 b");
}



QTEST_MAIN(FglTest)
