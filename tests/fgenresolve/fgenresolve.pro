TARGET = ctest-fgenresolve
TEMPLATE = app
QT += testlib xml
QT -= gui
CONFIG += testcase

include (../../basics.pri)
include (../../Taurus/chester.pri)

LIBS += -lchipper -lctest-fglib

HEADERS += fgr.h
SOURCES += fgr.cpp

INCLUDEPATH += ../../src ../../template ../../chip ../../generate ../fgenlib
