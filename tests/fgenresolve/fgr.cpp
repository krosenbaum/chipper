// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "fgr.h"

#include "autodisconnect.h"
#include "chipdata.h"
#include "chipdetail.h"
#include "chipfile.h"
#include "chippackage.h"
#include "chipper.h"
#include "chippool.h"
#include "chipvariant.h"
#include "filegen.h"
#include "generator.h"
#include "templatepool.h"
#include "templates.h"
#include "tracer.h"

#include <QtTest>
#include <QDir>
#include <QByteArrayList>
#include <QStringList>

void FgrTest::basicStrings()
{
    cleanupTestCase();
    QVERIFY(ChipPool::instance().findOrLoadChip(":/t1.chip") != nullptr);
    bool hasrun=false;
    auto test=[&](FileGen*gen,const ChipSymbol&,Tracer::State)->Tracer::Return
    {
        hasrun=true;
        basicStringsTest(gen);
        return Tracer::Return::Abort;
    };
    AutoDisconnect ad(connect(&Generator::tracer(),&Tracer::generateSymbol,this,test));
    Generator::instance().createPool();
    QVERIFY(hasrun);
}

void FgrTest::basicStringsTest(Chipper::Generate::FileGen*gen)
{
    QCOMPARE(gen->resolvedFileName("%M"),gen->chip()->metaData().manufacturer());
    QCOMPARE(gen->resolvedFileName("%m"),gen->chip()->metaData().manufacturer().replace(' ','_'));
    QCOMPARE(gen->resolvedFileName("%C"),gen->chip()->metaData().chipType());
    QCOMPARE(gen->resolvedFileName("%c"),gen->chip()->metaData().chipType());
    QCOMPARE(gen->resolvedFileName("%A"),gen->chip()->metaData().author());
    QCOMPARE(gen->resolvedFileName("%V"),gen->variant().name());
    QCOMPARE(gen->resolvedFileName("%v"),gen->variant().name());
    QCOMPARE(gen->resolvedFileName("%P"),gen->package().displayName());
    QCOMPARE(gen->resolvedFileName("%p"),gen->package().displayName());
    QCOMPARE(gen->resolvedFileName("%T"),gen->package().templateRef().title());
    QCOMPARE(gen->resolvedFileName("%t"),gen->package().templateRef().title());
    QCOMPARE(gen->resolvedFileName("%N"),gen->targetFile().name());
    QCOMPARE(gen->resolvedFileName("%n"),gen->targetFile().name());
    QCOMPARE(gen->resolvedFileName("%S"),absoluteDefaultPath(ChipperPath::Symbol));
    QCOMPARE(gen->resolvedFileName("%F"),absoluteDefaultPath(ChipperPath::Footprint));
    QCOMPARE(gen->resolvedFileName("%3"),absoluteDefaultPath(ChipperPath::Model3D));
    QCOMPARE(gen->resolvedFileName("%R"),absoluteDefaultPath(ChipperPath::Root));
//     QCOMPARE(gen->resolvedFileName("%I"),d->tdata.absolutePathName()+"/"+d->tfile->fileName());
//     QCOMPARE(gen->resolvedFileName("%i"),QFileInfo(d->tfile->fileName()).fileName());
//     case 'z':Q_FALLTHROUGH();//FIXME: for the moment preview dir and temp dir are the same...
//     QCOMPARE(gen->resolvedFileName("%Z"),d->tempDir.path());
    QCOMPARE(gen->resolvedFileName("%Y"),"sym1/file");
    QCOMPARE(gen->resolvedFileName("%y"),"file");
    QCOMPARE(gen->resolvedFileName("%G"),gen->absoluteScriptDirName());
    QCOMPARE(gen->resolvedFileName("%g"),"default");
    QCOMPARE(gen->resolvedFileName("%s"),gen->absoluteScriptFileName());
    QCOMPARE(gen->resolvedFileName("%d"),QDir::currentPath()+"/sym1");
    QCOMPARE(gen->resolvedFileName("%D"),":/Templates/TT");
}

void FgrTest::generateStrings()
{
    cleanupTestCase();
    QVERIFY(ChipPool::instance().findOrLoadChip(":/t2.chip") != nullptr);
    //run generator
    int errcount=0;
    auto&gen=Generator::instance();
    AutoDisconnect ad(connect(&gen,&Generator::error,this,[&](QString e){errcount++;qDebug()<<"received error"<<e;}));
    gen.createPool();
    //verify: no errors
    QCOMPARE(errcount,0);
    //prepare for comparison
    QFile fd(":/Templates/TT/s2");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    const QByteArray cloneFile=fd.readAll();
    fd.close();
    //verify Sym1: detailed content
    fd.setFileName("Sym1");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    const QByteArray symFile=fd.readAll();
    fd.close();
    //verify
    bool first=true;
    for(auto line:QString::fromLatin1(symFile).split('\n')){
        line=line.trimmed();
        if(line.isEmpty())continue;
        QStringList sp=line.split("//");
        QCOMPARE(sp.size(),2);
        if(first){
            QCOMPARE(sp[0].trimmed().size(),0);
            QCOMPARE(sp[1].trimmed().size(),0);
            first=false;
        }else{
            QCOMPARE(sp[0].trimmed(),sp[1].trimmed());
            QVERIFY2(sp[0].trimmed().size()>0, sp[0].toUtf8().data());
        }
    }
    //verify Sym2: clone
    fd.setFileName("Sym2");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QCOMPARE(fd.readAll(),cloneFile);
    fd.close();
    //verify Sym3: clone
    fd.setFileName("Sym3");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QCOMPARE(fd.readAll(),cloneFile);
    fd.close();
    //verify Sym4: should have about 2x as many lines as Sym1 (content slightly different)
    fd.setFileName("Sym4");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    const auto symFile4=fd.readAll();
    fd.close();
    const int sz4=QString::fromLatin1(symFile4).split('\n').size();
    const int sz1=QString::fromLatin1(symFile).split('\n').size();
    QVERIFY((sz1*2-2)<=sz4);
    QVERIFY((sz1*2+2)>=sz4);
}




QTEST_MAIN(FgrTest)
