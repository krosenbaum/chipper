// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include "fglib.h"

namespace Chipper{namespace Generate{
class FileGen;
}}

class FgrTest:public FgTLib
{
    Q_OBJECT
public:
    FgrTest():FgTLib({"TT"}){}
private slots:
    void basicStrings();
    void generateStrings();
private:
    void basicStringsTest(Chipper::Generate::FileGen*);
};
