// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "fga.h"

#include "autodisconnect.h"
#include "chipdata.h"
#include "chipdetail.h"
#include "chippackage.h"
#include "chipper.h"
#include "chippool.h"
#include "chipvariant.h"
#include "filegen.h"
#include "generator.h"
#include "templatepool.h"
#include "templates.h"
#include "tracer.h"

#include <QtTest>
#include <QDir>
#include <QByteArrayList>
#include <QStringList>

void FgaTest::initTestCase()
{
    FgTLib::initTestCase();
    //prepare Sym3
    QFile fd("Sym3");
    QVERIFY(fd.open(QIODevice::WriteOnly|QIODevice::Truncate));
    fd.write("content prep\n  epilog  \n\t\n");
    fd.close();
    //load and generate chip file
    QVERIFY(ChipPool::instance().findOrLoadChip(":/tapp.chip") != nullptr);
    int errcnt=0;
    AutoDisconnect ad(connect(&Generator::instance(),&Generator::error,this,[&]{errcnt++;}));
    Generator::instance().createPool();
}

void FgaTest::checkSimpleFile()
{
    //Sym1: one line with name
    QFile fd("Sym1");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QStringList l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();
    QCOMPARE(l.size(),1);
    QCOMPARE(l[0],"content Sym1");
    //Sym2: 3 lines with pro/epilog
    fd.setFileName("Sym2");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();
    QCOMPARE(l.size(),3);
    QCOMPARE(l[0],"prolog");
    QCOMPARE(l[1],"content Sym2");
    QCOMPARE(l[2],"epilog");
}

void FgaTest::checkEpilogSpaces()
{
    //Sym2: 3 lines with pro/epilog
    QFile fd("Sym3");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QStringList l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();
    QCOMPARE(l.size(),3);
    QCOMPARE(l[0],"content prep");
    QCOMPARE(l[1],"content Sym3");
    QCOMPARE(l[2],"epilog");
}

void FgaTest::checkMultiFileEasy()
{
    //FPrint: two content lines
    QFile fd("FPrint");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QStringList l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();
    QCOMPARE(l.size(),2);
    QCOMPARE(l[0],"content FPrint1");
    QCOMPARE(l[1],"content FPrint2");
}

void FgaTest::checkMultiFileProc()
{
    //Model: prolog, 2 content lines, epilog
    QFile fd("Model");
    QVERIFY(fd.open(QIODevice::ReadOnly));
    QStringList l=QString::fromLatin1(fd.readAll()).split('\n',Qt::SkipEmptyParts);
    fd.close();
    QCOMPARE(l.size(),4);
    QCOMPARE(l[0],"prolog");
    QCOMPARE(l[1],"content Mod1");
    QCOMPARE(l[2],"content Mod2");
    QCOMPARE(l[3],"epilog");
}



QTEST_MAIN(FgaTest)
