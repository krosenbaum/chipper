TARGET = ctest-fgenappend
TEMPLATE = app
QT += testlib xml
QT -= gui
CONFIG += testcase

include (../../basics.pri)
include (../../Taurus/chester.pri)

LIBS += -lchipper -lctest-fglib

HEADERS += fga.h
SOURCES += fga.cpp

INCLUDEPATH += ../../src ../../template ../../chip ../../generate ../fgenlib
