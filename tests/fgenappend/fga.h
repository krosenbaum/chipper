// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include "fglib.h"

namespace Chipper{namespace Generate{
class FileGen;
}}

class FgaTest:public FgTLib
{
    Q_OBJECT
public:
    FgaTest():FgTLib({"TA"}){}
private slots:
    void initTestCase();

    void checkSimpleFile();
    void checkEpilogSpaces();
    void checkMultiFileEasy();
    void checkMultiFileProc();
};
