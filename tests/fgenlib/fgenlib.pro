TEMPLATE = lib
TARGET = ctest-fglib
QT += testlib xml
QT -= gui
include (../../basics.pri)
include (../../Taurus/chester.pri)

LIBS += -lchipper

HEADERS += fglib.h
SOURCES += fglib.cpp
RESOURCES += ../files/files.qrc
DEFINES += FGTLIBEXPORT=Q_DECL_EXPORT

INCLUDEPATH += ../../src ../../template ../../chip ../../generate
