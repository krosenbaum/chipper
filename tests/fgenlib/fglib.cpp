// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "fglib.h"

#include "autodisconnect.h"
#include "chipdata.h"
#include "chipdetail.h"
#include "chippackage.h"
#include "chipper.h"
#include "chippool.h"
#include "chipvariant.h"
#include "filegen.h"
#include "generator.h"
#include "templatepool.h"
#include "templates.h"
#include "tracer.h"

#include <QtTest>
#include <QDir>
#include <QByteArrayList>
#include <QStringList>

static int zero=0;
void FgTLib::initTestCase()
{
    //init env
    __initChipperApp([](int&,char**){},zero,nullptr,true);
    setDefaultPath(ChipperPath::Template,":/Templates");
    //load chip file and templates
    for(auto t:mpretemp){
        QVERIFY(TemplatePool::instance().hasTitle(t));
        auto tmp=TemplatePool::instance().byTitle(t);
        QVERIFY(tmp.symbolIds().size()>0);
    }
    //cleanup
    QDir d(".");
    for(auto e:d.entryList(QStringList()<<"Sym*"<<"FPrint*"<<"Mod*",QDir::Files))
       QVERIFY(QFile::remove(e));
}

void FgTLib::cleanupTestCase()
{
    //cleanup chip pool
    auto&cp=ChipPool::instance();
    for(const QString&ch:cp.loadedChipNames())
        cp.unloadChip(cp.chipByName(ch));
    QCOMPARE(cp.numChips(),0);
    //cleanup files
    QDir d(".");
    if(!qEnvironmentVariableIsSet("CHIPPERTEST_NOCLEAN"))
    for(auto e:d.entryList(QStringList()<<"Sym*",QDir::Files))
       QVERIFY(QFile::remove(e));
}
