// Chipper KiCAD symbol/footprint/3Dmodel generator
// filegen tests
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QStringList>

#ifndef FGTLIBEXPORT
#define FGTLIBEXPORT Q_DECL_IMPORT
#endif

namespace Chipper{namespace Generate{
class FileGen;
}}

class FGTLIBEXPORT FgTLib:public QObject
{
    Q_OBJECT
    QStringList mpretemp;
protected:
    FgTLib(QStringList templates=QStringList()):mpretemp(templates){}
protected slots:
    virtual void initTestCase();
    virtual void cleanupTestCase();
};
