TEMPLATE = subdirs

SUBDIRS += fgenlib fgenresolve fgenappend fgenlang

fgenlang.depends += fgenlib
fgenresolve.depends += fgenlib
fgenappend.depends += fgenlib
