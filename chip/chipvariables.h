// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaObject>
#include <QObject>
#include <QPointer>
#include <QVariant>

#include "chipvariant.h"
#include "chipfile.h"

#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Template {
class TemplateData;
class TemplateVariables;
class TemplatePins;
}

namespace Chip {
class ChipSymbol;
class ChipFootprint;
class ChipModel;
class ChipVariables;
class ChipFileTarget;


///Contains variable values entered into the ChipPackage form.
class CHIPPER_EXPORT ChipVariables:public ChipDataPartBase
{
    Q_GADGET
    DECLARE_DPTR(d)
protected:
    friend class ChipPackage;
    friend class ChipFileTarget;
    ///used by package to instantiate variables
    ChipVariables(ChipData*c,const QDomElement&e,const Template::TemplateData&);
    ///called after reload to find the variant again and keep the object intact
    void resetElement()override;
    ///called before reload to remember UUID
    void preloadSync()override;
public:
    ///creates an empty instance
    ChipVariables()=default;
    ///copy the instance
    ChipVariables(const ChipVariables&)=default;
    ///discard the instance
    ~ChipVariables();

    ///copy the instance
    ChipVariables& operator=(const ChipVariables&)=default;

    ///returns the UID of the containing package
    QString packageUid()const;
    ///returns the UID of the containing variant
    QString variantUid()const;
    ///returns the UID of the containing target file or an empty string if it is attached to the package directly
    QString fileUid()const;

    Q_PROPERTY(QString packageUid READ packageUid);
    Q_PROPERTY(QString variantUid READ variantUid);
    Q_PROPERTY(QString fileUid READ fileUid);

    ChipVariant variant()const;
    ChipPackage package()const;
    ChipFileTarget fileTarget()const;

    ///true if this is a variable object attached to a target file (symbol/footprint/model)
    bool isOnTargetFile()const;
    ///true if this is a variable object attached to the package directly
    bool isOnPackage()const;

    Q_PROPERTY(bool isOnTargetFile READ isOnTargetFile);
    Q_PROPERTY(bool isOnPackage READ isOnPackage);

    ///returns the file type of the parent target file, Invalid if it is attached to the package or an invalid object
    ChipFileTarget::FileType parentFileType()const;

    ///returns a reference to the variable definition of the corresponding template
    Template::TemplateVariables templateVariables()const;

    Q_PROPERTY(TemplateVariables templateVariables READ templateVariables);

    ///returns the corresponding package level variables (returns itself if it is on package level)
    ChipVariables packageVariables()const;

    ///returns a list of all variable names that have been set in the variant
    QStringList definedVariables()const;
    ///returns the value for a specific variable name or empty string if it does not exist
    QString variableValue(QString)const;
    ///returns true if a value for this variable is known
    bool hasVariable(QString)const;

    ///sets a variable to the given value
    void setVariable(QString name,QString value);
    ///deletes a variable from this variant (effectively setting it to default or empty string)
    void removeVariable(QString);

    ///helper for scripts
    QMap<QString,QVariant>variableMap()const;
    Q_PROPERTY(QStringList definedVariables READ definedVariables);
    Q_PROPERTY(QMap<QString,QVariant> variables READ variableMap);
};

//end of namespace
}}
using namespace Chipper::Chip;

Q_DECLARE_METATYPE(ChipVariables);
