// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdatapriv.h"
#include "chipfile.h"
#include "chippackage.h"
#include "chipper.h"
#include "chipvariables.h"
#include "templatepool.h"
#include "templates.h"

#include <QCryptographicHash>
#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>
#include <QUuid>

#include <DomNodeIterator>







// ////////////////////////////////////
// Base for Target Files Definition

void ChipFileTarget::initTemplatePins()
{
    auto tref=package().templateRef();
    //find target spec -> pin spec
    QString pinid;
    if(isSymbol()){
        auto tgt=tref.symbol(templateId());
        pinid=tgt.pinSpecId();
        mprefill=tgt.pinPrefillId();
    }else if(isFootprint()){
        auto tgt=tref.footprint(templateId());
        pinid=tgt.pinSpecId();
        mprefill=tgt.pinPrefillId();
    }else if(is3DModel()){
        auto tgt=tref.model(templateId());
        pinid=tgt.pinSpecId();
        mprefill=tgt.pinPrefillId();
    }else{
        mtmppins=TemplatePins();
        mprefill.clear();
    }
    //find pins
    mtmppins=tref.pins(pinid);
}

void ChipFileTarget::preloadSync()
{
    muid=uid();
    mvarid=variantUid();
    mpackid=packageUid();
}

void ChipFileTarget::resetElement()
{
    //find variant
    QDomElement var;
    for(auto n:document().documentElement().childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==VARIANTELEMENTNAME && e.attribute("uid")==mvarid){
            var=e;
            break;
        }
    }
    if(var.isNull())return;
    //find package
    QDomElement pack;
    for(auto n:var.childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==PACKAGEELEMENTNAME && e.attribute("uid")==mpackid){
            pack=e;
            break;
        }
    }
    //find symbol
    if(!pack.isNull())
    for(auto n:pack.childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==tagName() && e.attribute("uid")==muid){
            setElement(e);
            break;
        }
    }
    //re-init pin cache
    initTemplatePins();
}

QString ChipFileTarget::name() const
{
    return element().attribute("name");
}

void ChipFileTarget::setName(QString n)
{
    element().setAttribute("name",n);
    altered();
}

QString ChipFileTarget::uid() const
{
    return element().attribute("uid");
}

QString ChipFileTarget::packageUid() const
{
    return element().parentNode().toElement().attribute("uid");
}

QString ChipFileTarget::variantUid() const
{
    return element().parentNode().toElement().parentNode().toElement().attribute("uid");
}

QString ChipFileTarget::templateId() const
{
    return element().attribute("id");
}

QString ChipFileTarget::tagName() const
{
    switch(mtype){
        case FileType::Symbol:return SYMBOLELEMENTNAME;
        case FileType::Footprint:return FOOTPRINTELEMENTNAME;
        case FileType::Model3D:return MODELELEMENTNAME;
        default:return QString();
    }
}

ChipFileTarget::FileType ChipFileTarget::tagNameToType(const QString &tn)
{
    if(tn==SYMBOLELEMENTNAME)return FileType::Symbol;
    if(tn==FOOTPRINTELEMENTNAME)return FileType::Footprint;
    if(tn==MODELELEMENTNAME)return FileType::Model3D;
    return FileType::Invalid;
}

QString ChipFileTarget::fileTypeName() const
{
    switch(fileType()){
        case FileType::Symbol:return tr("Symbol File");
        case FileType::Footprint:return tr("Footprint File");
        case FileType::Model3D:return tr("3D Model File");
        default:return QString();
    }
}

ChipSymbol ChipFileTarget::toSymbol()const
{
    if(mtype==FileType::Symbol)return ChipSymbol(*this);
    return ChipSymbol();
}

ChipFootprint ChipFileTarget::toFootprint() const
{
    if(mtype==FileType::Footprint)return ChipFootprint(*this);
    return ChipFootprint();
}

ChipModel ChipFileTarget::toModel() const
{
    if(mtype==FileType::Model3D)return ChipModel(*this);
    return ChipModel();
}

QStringList ChipFileTarget::pinRowNames() const
{
    //TODO: handle grid style with formula
    return mtmppins.rowNames();
}

QList<TargetPin> ChipFileTarget::pinRow(QString rid) const
{
    QString ps;
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==SYMBOLPINSELEMENTNAME && e.attribute("id")==rid){
            ps=e.text();
            break;
        }
    }
    if(ps.isEmpty())return QList<TargetPin>();
    QList<TargetPin> r;
    for(auto s:ps.split(' ',Qt::SkipEmptyParts)){
        r.append(s);
    }
    return r;
}

bool ChipFileTarget::setPinRow(QString rname, const QList<TargetPin>&pins)
{
    //sanity check: row name/id
    if(rname.isEmpty())return false;
    if(!pinRowNames().contains(rname))return false;
    //convert pins to string
    QStringList psl;
    for(const auto&p:pins){
        if(!p.isValid())return false;
        psl.append(p.mcode);
    }
    const QString ps=psl.join(' ');
    //find element and store
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==SYMBOLPINSELEMENTNAME && e.attribute("id")==rname){
            elementSetText(e,ps);
            altered();
            return true;
        }
    }
    //not found: create new
    auto e=document().createElement(SYMBOLPINSELEMENTNAME);
    elementSetText(e,ps);
    e.setAttribute("id",rname);
    element().appendChild(e);
    altered();
    return true;
}

QMap<QString, QVariant> ChipFileTarget::pinRowMap() const
{
    QMap<QString,QVariant> ret;
    for(const auto&k:pinRowNames()){
        QList<QVariant> row;
        for(auto n:pinRow(k))
            row.append(QVariant::fromValue(TargetPinExt(n,*this)));
        ret.insert(k,row);
    }
    return ret;
}

TargetPinExt ChipFileTarget::makePin(QString pid,bool isHidden)const
{
    //validate
    if(variant().pin(pid).isValid())
        return TargetPinExt(isHidden?("-"+pid):pid,*this);
    else
        return TargetPinExt();
}

TargetPinExt ChipFileTarget::makeGap()const
{
    return TargetPinExt("+",*this);
}

QList<TargetPinExt> ChipFileTarget::makePinPool() const
{
    QList<TargetPinExt> ret;
    for(auto p:variant().pinIds())
        ret.append(TargetPinExt(p,*this));
    return ret;
}

QVariantList ChipFileTarget::pinListToVariant(const QList<TargetPin>&pl)
{
    QVariantList vl;
    for(auto p:pl)
        vl.append(QVariant::fromValue(p));
    return vl;
}

QVariantList ChipFileTarget::pinListToVariant(const QList<TargetPinExt>&pl)
{
    QVariantList vl;
    for(auto p:pl)
        vl.append(QVariant::fromValue(p));
    return vl;
}

ChipPackage ChipFileTarget::package() const
{
    return ChipPackage(const_cast<ChipData*>(chip()),element().parentNode().toElement());
}

ChipVariant ChipFileTarget::variant() const
{
    return ChipVariant(const_cast<ChipData*>(chip()),element().parentNode().parentNode().toElement());
}

ChipVariables ChipFileTarget::variables() const
{
    return ChipVariables(const_cast<ChipData*>(chip()),findOrCreateElement(element(),VARIABLESELEMENTNAME),package().templateRef());
}




TargetPinExt::TargetPinExt(QString cd, const ChipFileTarget&tg)
:TargetPin(cd)
{
    mvpin=tg.variant().pin(id());
    mcpin=tg.chip()->pins().byName(mvpin.name());
    mtuid=tg.uid();
}

QString TargetPinExt::pinUuid()const
{
    //get hash
    QCryptographicHash hash(QCryptographicHash::Sha1);
    hash.addData(mtuid.toLatin1());
    hash.addData(id().toLatin1());
    auto res=hash.result();
    //run something like RFC4122
    res.truncate(16);
    res[6]=(res[6]&0x0f)|0x50; //version 5: SHA-1 Name based
    res[8]=(res[8]&0x3f)|0x80; //fixed bits
    //convert to UUID string
    return QUuid::fromRfc4122(res).toString(QUuid::WithoutBraces);
}

static int metaCFTid=qRegisterMetaType<ChipFileTarget>();

static int metaTargetPinId=qRegisterMetaType<TargetPin>();
static int metaTargetPinExtId=qRegisterMetaType<TargetPinExt>();









// ////////////////////////////////////
// Chip Symbol Definition

QString ChipSymbol::tagName()const
{
    return SYMBOLELEMENTNAME;
}

QString ChipSymbol::defaultValue() const
{
    return element().attribute("value");
}

void ChipSymbol::setDefaultValue(QString dv)
{
    element().setAttribute("value",dv);
    altered();
}

QString ChipSymbol::defaultFootprint() const
{
    return element().attribute("footprint");
}

void ChipSymbol::setDefaultFootprint(QString df)
{
    element().setAttribute("footprint",df);
    altered();
}



static int metaSymbolId=qRegisterMetaType<ChipSymbol>();







// ////////////////////////////////////
// Chip Footprint Definition

QString ChipFootprint::tagName() const
{
    return FOOTPRINTELEMENTNAME;
}

QString ChipFootprint::defaultModel() const
{
    return element().attribute("model");
}

void ChipFootprint::setDefaultModel(QString dm)
{
    element().setAttribute("model",dm);
    altered();
}

static int metaFootprintId=qRegisterMetaType<ChipFootprint>();









// ////////////////////////////////////
// Chip 3D Model Definition

QString ChipModel::tagName() const
{
    return MODELELEMENTNAME;
}

static int metaModelId=qRegisterMetaType<ChipModel>();








#include "moc_chipfile.cpp"
