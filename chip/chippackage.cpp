// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdatapriv.h"
#include "chipfile.h"
#include "chippackage.h"
#include "chipper.h"
#include "chipvariables.h"
#include "templatepool.h"
#include "templates.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>
#include <QUuid>

#include <DomNodeIterator>




// ////////////////////////////////////
// Chip Package Definition


void ChipPackage::preloadSync()
{
    mvuid=parentUid();
    mpuid=uid();
}

void ChipPackage::resetElement()
{
    //try to find parent element
    QDomElement variant;
    for(auto n:document().documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==mvuid){
                variant=e;
                break;
            }
        }
    if(!variant.isNull() && variant.isElement())
    for(auto n:variant.childNodes())
        if(n.isElement() && n.nodeName()==PACKAGEELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==mpuid){
                setElement(e);
                return;
            }
        }
    //fall back to a full reset
    ChipDataPartBase::resetElement();
}

QString ChipPackage::name() const
{
    return element().attribute("name");
}

void ChipPackage::setName(QString n)
{
    if(n==name())return;
    altered();
    element().setAttribute("name",n);
}

QString ChipPackage::templateName() const
{
    return element().attribute("template");
}

QString ChipPackage::displayName() const
{
    QString n=name().trimmed();
    if(n.isEmpty())return templateName();
    else return n;
}

QString ChipPackage::uid() const
{
    return element().attribute("uid");
}

QString ChipPackage::parentUid() const
{
    return element().parentNode().toElement().attribute("uid");
}

ChipVariant ChipPackage::variant() const
{
    return ChipVariant(const_cast<ChipData*>(chip()),element().parentNode().toElement());
}

QStringList ChipPackage::symbolIds() const
{
    QStringList r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==SYMBOLELEMENTNAME)
            r<<e.attribute("uid");
    }
    return r;
}

QStringList ChipPackage::footprintIds() const
{
    QStringList r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==FOOTPRINTELEMENTNAME)
            r<<e.attribute("uid");
    }
    return r;
}

QStringList ChipPackage::modelIds() const
{
    QStringList r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==MODELELEMENTNAME)
            r<<e.attribute("uid");
    }
    return r;
}

ChipSymbol ChipPackage::symbol(QString uid) const
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==SYMBOLELEMENTNAME && e.attribute("uid")==uid)
            return ChipSymbol(const_cast<ChipData*>(chip()),e);
    }
    return ChipSymbol();
}

QString ChipPackage::addSymbol(QString templateId, QString name)
{
    if(templateId.isEmpty() || !templateRef().symbolIds().contains(templateId)){
        qDebug()<<"Hmm. Trying to instantiate a symbol with invalid template ID"<<templateId;
        return QString();
    }
    if(name.isEmpty()){
        qDebug()<<"Unable to create symbol"<<name;
        return QString();
    }
    auto nsym=document().createElement(SYMBOLELEMENTNAME);
    nsym.setAttribute("id",templateId);
    nsym.setAttribute("name",name);
    QString uid=QUuid::createUuid().toString(QUuid::WithoutBraces);
    nsym.setAttribute("uid",uid);
    element().appendChild(nsym);
    altered();
    return uid;
}

bool ChipPackage::removeSymbol(QString uid)
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==SYMBOLELEMENTNAME && e.attribute("uid")==uid){
            element().removeChild(e);
            altered();
            return true;
        }
    }
    return false;
}

ChipFootprint ChipPackage::footprint(QString uid) const
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==FOOTPRINTELEMENTNAME && e.attribute("uid")==uid)
            return ChipFootprint(const_cast<ChipData*>(chip()),e);
    }
    return ChipFootprint();
}

QString ChipPackage::addFootprint(QString templateId, QString name)
{
    if(templateId.isEmpty() || !templateRef().footprintIds().contains(templateId)){
        qDebug()<<"Hmm. Trying to instantiate a footprint with invalid template ID"<<templateId;
        return QString();
    }
    if(name.isEmpty()){
        qDebug()<<"Unable to create footprint"<<name;
        return QString();
    }
    auto nsym=document().createElement(FOOTPRINTELEMENTNAME);
    nsym.setAttribute("id",templateId);
    nsym.setAttribute("name",name);
    QString uid=QUuid::createUuid().toString(QUuid::WithoutBraces);
    nsym.setAttribute("uid",uid);
    element().appendChild(nsym);
    altered();
    return uid;
}

bool ChipPackage::removeFootprint(QString uid)
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==FOOTPRINTELEMENTNAME && e.attribute("uid")==uid){
            element().removeChild(e);
            altered();
            return true;
        }
    }
    return false;
}

ChipModel ChipPackage::model(QString uid) const
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==MODELELEMENTNAME && e.attribute("uid")==uid)
            return ChipModel(const_cast<ChipData*>(chip()),e);
    }
    return ChipModel();
}

QString ChipPackage::addModel(QString templateId, QString name)
{
    if(templateId.isEmpty() || !templateRef().modelIds().contains(templateId)){
        qDebug()<<"Hmm. Trying to instantiate a 3D model with invalid template ID"<<templateId;
        return QString();
    }
    if(name.isEmpty()){
        qDebug()<<"Unable to create 3D model"<<name;
        return QString();
    }
    auto nsym=document().createElement(MODELELEMENTNAME);
    nsym.setAttribute("id",templateId);
    nsym.setAttribute("name",name);
    QString uid=QUuid::createUuid().toString(QUuid::WithoutBraces);
    nsym.setAttribute("uid",uid);
    element().appendChild(nsym);
    altered();
    return uid;
}

bool ChipPackage::removeModel(QString uid)
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()==MODELELEMENTNAME && e.attribute("uid")==uid){
            element().removeChild(e);
            altered();
            return true;
        }
    }
    return false;
}

TemplateData ChipPackage::templateRef() const
{
    return TemplatePool::instance().byTitle(templateName());
}

ChipVariables ChipPackage::variables() const
{
    return ChipVariables(const_cast<ChipData*>(chip()),findOrCreateElement(element(),VARIABLESELEMENTNAME),templateRef());
}

QString ChipPackage::keywords() const
{
    QString r=chipKeywords();
    if(!r.isEmpty())r+=" ";
    r+=packageKeywords();
    return r.trimmed();
}

QString ChipPackage::packageKeywords()const
{
    return element().attribute("keywords",QString());
}

void ChipPackage::setPackageKeywords(QString d)
{
    element().setAttribute("keywords",d);
    altered();
}

QString ChipPackage::chipKeywords()const
{
    return chip()->metaData().keywords();
}

QString ChipPackage::description()const
{
    const QString r=packageDescription();
    if(r.isEmpty())return chipDescription();
    else return r;
}

QString ChipPackage::packageDescription()const
{
    return element().attribute("description",QString());
}

void ChipPackage::setPackageDescription(QString d)
{
    if(d.isEmpty())element().removeAttribute("description");
    else element().setAttribute("description",d);
    altered();
}

QString ChipPackage::chipDescription()const
{
    return chip()->metaData().description();
}

static int metaPackageId=qRegisterMetaType<ChipPackage>();



#include "moc_chippackage.cpp"
