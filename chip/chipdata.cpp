// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chippool.h"
#include "chipper.h"
#include "chipdatapriv.h"
#include "chipdetail.h"
#include "chipvariant.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>
#include <QUuid>

#include <DomNodeIterator>


// ////////////////////////////////////
// Main Chip object

ChipData::ChipData(QString fn)
:mfname(fn)
{
    reloadFile();
    connect(&ChipPool::instance(),&ChipPool::syncChips,this,&ChipData::syncFile);
}

ChipData::~ChipData()
{
    emit aboutToUnload(mfname);
    qDebug()<<"Unloading Chip"<<mfname;
    saveFile(FileCopy::Ahead);
    mfname.clear();
    mlderror.clear();
    mxml.clear();
    msavestate=SaveState::None;
    emit unloaded(mfname);
}

static const QString aheadext="~atmp";
static const QString backupext="~bak";

bool ChipData::reloadFile(ChipData::FileCopy fc)
{
    QString fn=mfname;
    switch(fc){
        case FileCopy::Chip:break;
        case FileCopy::Ahead:fn+=aheadext;break;
        case FileCopy::Backup:fn+backupext;break;
        default:
            qDebug()<<"Unknown file copy ID"<<(int)fc<<"while loading"<<fn;
            mlderror=tr("Internal error, invalid copy ID %1 while loading %2").arg((int)fc).arg(fn);
            return false;
    }
    qDebug()<<"Loading Chip file"<<mfname<<"...";
    QFile fd(fn);
    if(!fd.open(QIODevice::ReadOnly)){
        qDebug()<<"Unable to open"<<fn;
        mlderror=tr("Unable to open %1 for reading.").arg(fn);
        return false;
    }
    //reset internal data TODO
    emit preloadSync();
    mlderror.clear();
    mxml.clear();
    msavestate=SaveState::None;
    //get XML
    QString err;
    int line,col;
    if(!mxml.setContent(&fd,false,&err,&line,&col)){
        qDebug()<<"XML parsing error, file"<<fn<<"error"<<err<<"line"<<line<<"col"<<col;
        mlderror=tr("XML parser error in %1: %2, line %3, column %4").arg(fn).arg(err).arg(line).arg(col);
        return false;
    }
    //verification
    if(mxml.documentElement().tagName()!="Chip"){
        qDebug()<<"Although fine XML, this is not a Chip file:"<<fn;
        mlderror=tr("Not a Chip file: %1").arg(fn);
        mxml.clear();
        return false;
    }
    switch(fc){
        case FileCopy::Chip:msavestate=SaveState::Saved;break;
        case FileCopy::Ahead:msavestate=SaveState::Synced;break;
        case FileCopy::Backup:msavestate=SaveState::Altered;break;
        default:break;
    }
    //re-create object structure
    emit internalReset();
    return true;
}

bool ChipData::saveFile(ChipData::FileCopy fc)
{
    if(msavestate==SaveState::None || mfname.isEmpty()){
        qDebug()<<"There is no data loaded. Cannot save it.";
        return false;
    }
    if(fc==FileCopy::Backup){
        qDebug()<<"Error: saving chip directly into backup is not allowed. This is not Sandpiper you know?";
        return false;
    }
    if(fc==FileCopy::None){
        qDebug()<<"Error: requesting to save to none. Okay, not saving. Is this Slippin' Jimmy again?";
        return false;
    }
    if(fc!=FileCopy::Chip && fc!=FileCopy::Ahead){
        qDebug()<<"Error: request to save to unknown chip copy"<<(int)fc<<"- you're in trouble? Better Call Saul!";
        return false;
    }
    //simple sync
    if(fc==FileCopy::Ahead){
        if((int)msavestate>(int)SaveState::Synced){
            if(dumpXml(mfname+aheadext)){
                msavestate=SaveState::Synced;
                return true;
            }else{
                qDebug()<<"Error saving to sync file"<<mfname+aheadext;
                return false;
            }
        }else
            return true;
    }
    //Save Data
    if(msavestate==SaveState::Saved)//nothing to do
        return true;
    //save a backup
    if(QFile::exists(mfname)){
        QFile::remove(mfname+backupext);
        if(!QFile::rename(mfname,mfname+backupext))
            qDebug()<<"Warning: unable to backup"<<mfname;
    }
    //if sync'd try to rename sync file
    if(msavestate==SaveState::Synced){
        if(QFile::rename(mfname+aheadext,mfname)){
            qDebug()<<"Synced -> Saved:"<<mfname;
            msavestate=SaveState::Saved;
            return true;
        }
    }
    //not sync'd: dump XML and delete sync file
    if(dumpXml(mfname)){
        msavestate=SaveState::Saved;
        QFile::remove(mfname+aheadext);
        return true;
    }
    //hmm, dang!
    qDebug()<<"Error: unable to save chip file.";
    return false;
}

bool ChipData::syncFile()
{
    return saveFile(FileCopy::Ahead);
}

bool ChipData::dumpXml(QString fn)
{
    QByteArray a=mxml.toByteArray();
    if(a.size()==0){
        qDebug()<<"Error: Ooops! Unable to convert XML data to bytes. Was Heisenberg here?";
        return false;
    }
    QFile fd(fn);
    if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        qDebug()<<"Error: Unable to open"<<fn<<"for writing.";
        return false;
    }
    if(fd.write(a)<a.size()){
        qDebug()<<"Error: fewer bytes written to"<<fn<<"than generated. I hope you got backups?";
        fd.remove();
        return false;
    }
    fd.close();
    qDebug()<<"Successfully wrote"<<fn;
    return true;
}

bool ChipData::hasFileCopy(ChipData::FileCopy fc) const
{
    QString fn=mfname;
    if(fn.isEmpty())return false;
    switch(fc){
        case FileCopy::Chip:break;
        case FileCopy::Ahead:fn+=aheadext;break;
        case FileCopy::Backup:fn+=backupext;break;
        default:return false;
    }
    QFileInfo fi(fn);
    return fi.exists()&&fi.isFile();
}

QString ChipData::fileNameOfState() const
{
    switch(msavestate){
        case SaveState::Synced:return mfname+aheadext;
        case SaveState::Altered:return mfname+(hasFileCopy(FileCopy::Ahead)?aheadext:QString());
        default:return mfname;
    }
}

ChipData::FileCopies ChipData::fileCopies() const
{
    FileCopies fc=hasFileCopy(FileCopy::Chip)?FileCopy::Chip:FileCopy::None;
    fc|=hasFileCopy(FileCopy::Ahead)?FileCopy::Ahead:FileCopy::None;
    fc|=hasFileCopy(FileCopy::Backup)?FileCopy::Backup:FileCopy::None;
    return fc;
}

void ChipData::altered()
{
    if(msavestate==SaveState::None)return;
    msavestate=SaveState::Altered;
}

ChipMetaData ChipData::metaData() const
{
    return ChipMetaData(const_cast<ChipData*>(this),findElement(mxml.documentElement(),METAELEMENTNAME));
}

ChipTargetFiles ChipData::targets() const
{
    return ChipTargetFiles(const_cast<ChipData*>(this),findElement(mxml.documentElement(),TARGETELEMENTNAME));
}

ChipPins ChipData::pins()const
{
    return ChipPins(const_cast<ChipData*>(this),findElement(mxml.documentElement(),PINSELEMENTNAME));
}

QString ChipData::notes() const
{
    return findElement(mxml.documentElement(),NOTESELEMENTNAME).text();
}

void ChipData::setNotes(QString n)
{
    altered();
    auto e=findElement(mxml.documentElement(),NOTESELEMENTNAME);
    if(e.isNull())
        e=mxml.documentElement().appendChild(mxml.createElement(NOTESELEMENTNAME)).toElement();
    elementSetText(e,n);
}

int ChipData::numVariants() const
{
    int c=0;
    for(auto n:mxml.documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME)
            c++;
    return c;
}

QStringList ChipData::variantIds() const
{
    QStringList r;
    for(auto n:mxml.documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME)
            r.append(n.toElement().attribute("uid"));
    return r;
}

QList<ChipVariant> ChipData::allVariants()
{
    QList<ChipVariant>r;
    for(auto n:mxml.documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME)
            r.append(ChipVariant(this,n.toElement()));
    return r;
}

ChipVariant ChipData::variantById(QString id) const
{
    for(auto n:mxml.documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==id)
                return ChipVariant(const_cast<ChipData*>(this),e);
        }
    return ChipVariant();
}

bool ChipData::hasVariantId(QString id) const
{
    for(auto n:mxml.documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==id)
                return true;
        }
    return false;
}

ChipVariant ChipData::newVariant(QString id)
{
    //validate ID
    QUuid uid(id);
    if(uid.isNull()){
        uid=QUuid::createUuid();
        id=uid.toString(QUuid::WithoutBraces);
    }else{
        id=uid.toString(QUuid::WithoutBraces);
        if(hasVariantId(id)){
            qDebug()<<"Warning: trying to create Variant with ID"<<id<<"which already exists.";
            return ChipVariant();
        }
    }
    //create
    QDomElement nv=mxml.createElement(VARIANTELEMENTNAME);
    nv.setAttribute("uid",id);
    nv=mxml.documentElement().appendChild(nv).toElement();
    altered();
    //return
    return ChipVariant(this,nv);
}

bool ChipData::deleteVariant(QString id)
{
    auto doc=mxml.documentElement();
    for(auto n:doc.childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==id){
                doc.removeChild(n);
                altered();
                return true;
            }
        }
    return false;
}



#include "moc_chipdata.cpp"
