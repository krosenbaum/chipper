// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Private Stuff
//
// (c) Konrad Rosenbaum, 2021-24
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <functional>

class QDomElement;
class QString;

namespace Chipper {
///special namespace for helper functions for Chip Data
namespace ChipDataPriv {
///pointer to initializer function when creating elements (\see findOrCreateElement below)
typedef std::function<void(QDomElement&)>ElementInit;

///\internal default initializer: does nothing
void noElementInit(QDomElement&);

///tries to find an element inside a parent, or creates it if not found
///\param parent parent element in which to search
///\param name tag name of the element we are looking for
///\param init if the element is not found and needs to be created: function to initialize it
QDomElement findOrCreateElement(QDomElement parent,QString name,ElementInit init=noElementInit);
///tries to find an element inside a parent, returns an invalid element if not found
///\param parent parent element in which to search
///\param name tag name of the element we are looking for
QDomElement findElement(QDomElement parent,QString name);

///attempts to replace all current text (and CDATA) with the new text
///\param element the element on which to operate
///\param text the text to insert into the element
///\param cdata if true: generates a CDATA node instead of a text node
///\note may malfunction if the element has sub-elements with text
QDomElement elementSetText(QDomElement element,QString text,bool cdata=false);


}};
using namespace Chipper::ChipDataPriv;

///name of Meta data element
#define METAELEMENTNAME "Meta"
///name of Target path spec element
#define TARGETELEMENTNAME "Targets"
///name of pin table element
#define PINSELEMENTNAME "Pins"
///name of notes element
#define NOTESELEMENTNAME "Notes"
///name of chip variant element
#define VARIANTELEMENTNAME "Variant"
///name of Chip variant pin definition element
#define VARIANTPINELEMENTNAME "Pin"
///name of chip package element
#define PACKAGEELEMENTNAME "Package"
///name of symbol definition element
#define SYMBOLELEMENTNAME "Symbol"
///name of symbol definition element
#define SYMBOLPINSELEMENTNAME "Pins"
///name of footprint definition element
#define FOOTPRINTELEMENTNAME "Footprint"
///name of model definition element
#define MODELELEMENTNAME "Model"
///name of package variables element
#define VARIABLESELEMENTNAME "Variables"
