// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaObject>
#include <QObject>
#include <QPointer>
#include <QVariant>

#include "chipvariant.h"

#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Template {
class TemplateData;
class TemplateVariables;
class TemplatePins;
}

namespace Chip {
class ChipSymbol;
class ChipFootprint;
class ChipModel;
class ChipVariables;
class ChipFileTarget;

///represents a specific package type within a variant of a chip
class CHIPPER_EXPORT ChipPackage:public ChipDataPartBase
{
    Q_GADGET
    ///\internal used during reload
    QString mvuid,mpuid;
protected:
    friend class ChipData;
    friend class ChipVariant;
    friend class ChipFileTarget;
    friend class ChipVariables;
    ChipPackage(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){}
    ///called after reload to find the variant again and keep the object intact
    void resetElement()override;
    ///called before reload to remember UUID
    void preloadSync()override;
public:
    ChipPackage()=default;
    ChipPackage(const ChipPackage&)=default;

    ChipPackage& operator=(const ChipPackage&)=default;

    ///returns the unique ID of the chip package (used internally only)
    QString uid()const;

    ///returns the uid of the parent variant
    QString parentUid()const;

    ///returns the parent variant
    ChipVariant variant()const;

    ///returns the human readable name of the package, if configured
    QString name()const;
    ///sets the name of the package
    void setName(QString);

    ///returns the template reference name of the package
    QString templateName()const;

    ///returns the template object for this package
    Template::TemplateData templateRef()const;

    ///returns the display name of the package
    QString displayName()const;

    ///returns (combined) keywords for this chip
    QString keywords()const;
    ///returns only package local keywords
    QString packageKeywords()const;
    ///returns chip global keywords
    QString chipKeywords()const;
    ///sets the package part of the keywords
    void setPackageKeywords(QString);

    ///returns description or falls back to the same function in the chip
    QString description()const;
    ///returns the locally configured description or empty if the chip one is used
    QString packageDescription()const;
    ///returns the chip global description
    QString chipDescription()const;
    ///sets the package description, set to empty string to fall back to chip description
    void setPackageDescription(QString);

    Q_PROPERTY(QString uid READ uid);
    Q_PROPERTY(QString parentUid READ parentUid);
    Q_PROPERTY(QString name READ name WRITE setName);
    Q_PROPERTY(QString templateName READ templateName);
    Q_PROPERTY(QString displayName READ displayName);
    Q_PROPERTY(TemplateData templateRef READ templateRef);
    Q_PROPERTY(QString keywords READ keywords);
    Q_PROPERTY(QString description READ description);

    ChipVariables variables()const;

    Q_PROPERTY(ChipVariables variables READ variables);

    QStringList symbolIds()const;
    QStringList footprintIds()const;
    QStringList modelIds()const;

    Q_PROPERTY(QStringList symbolIds READ symbolIds);
    Q_PROPERTY(QStringList footprintIds READ footprintIds);
    Q_PROPERTY(QStringList modelIds READ modelIds);

    ChipSymbol symbol(QString uid)const;
    ChipFootprint footprint(QString uid)const;
    ChipModel model(QString uid)const;

    bool removeSymbol(QString uid);
    bool removeFootprint(QString uid);
    bool removeModel(QString uid);

    QString addSymbol(QString templateId,QString name);
    QString addFootprint(QString templateId,QString name);
    QString addModel(QString templateId,QString name);

    //TODO: do we need to expose actual symbols/footprints/models as properties?
};

//end of namespace
}}
using namespace Chipper::Chip;

Q_DECLARE_METATYPE(ChipPackage);
