// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Private Stuff
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QString>
#include <QDomElement>
#include <QDomNodeList>
#include <DomNodeIterator>

#include "chipdatapriv.h"
namespace Chipper {

void ChipDataPriv::noElementInit(QDomElement&){}

QDomElement ChipDataPriv::findOrCreateElement(QDomElement parent,QString name,ElementInit init)
{
    QDomElement me;
    for(const auto&n:parent.childNodes()){
        if(!n.isElement())continue;
        if(n.nodeName()==name)me=n.toElement();
    }
    if(me.isNull()){
        me=parent.ownerDocument().createElement(name);
        init(me);
        parent.appendChild(me);
    }
    return me;
}

QDomElement ChipDataPriv::findElement(QDomElement parent,QString name)
{
    QDomElement me;
    for(const auto&n:parent.childNodes()){
        if(!n.isElement())continue;
        if(n.nodeName()==name)me=n.toElement();
    }
    return me;
}

QDomElement ChipDataPriv::elementSetText(QDomElement element, QString text, bool cdata)
{
    if(element.isNull())return QDomElement();
    //delete old text
    for(QDomNode n:element.childNodes()){
        if(n.isCDATASection() || n.isText())
            element.removeChild(n);
    }
    //sanity check
    if(text.isEmpty())return element;
    //create new text
    if(cdata)
        element.appendChild(element.ownerDocument().createCDATASection(text));
    else
        element.appendChild(element.ownerDocument().createTextNode(text));
    //done
    return element;
}


//end of namespace
}
