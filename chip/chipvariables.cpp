// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdatapriv.h"
#include "chippackage.h"
#include "chipfile.h"
#include "chipvariables.h"
#include "chipper.h"
#include "templates.h"
#include "templatepool.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>
#include <QUuid>

#include <DomNodeIterator>





// ////////////////////////////////////
// Chip Variable Definition

class ChipVariables::Private
{
public:
    QString mpackid,mvarid,mfileid,mptagname;
    TemplateData mtemp;
    TemplateVariables mtvar;
};

DEFINE_DPTR(ChipVariables);

ChipVariables::ChipVariables(ChipData* c, const QDomElement& e, const TemplateData &t)
:ChipDataPartBase(c,e)
{
    d->mtemp=t;
    switch(parentFileType()){
        case ChipFileTarget::FileType::Symbol:
            d->mtvar=t.symbol(e.parentNode().toElement().attribute("id")).variables();
            break;
        case ChipFileTarget::FileType::Footprint:
            d->mtvar=t.footprint(e.parentNode().toElement().attribute("id")).variables();
            break;
        case ChipFileTarget::FileType::Model3D:
            d->mtvar=t.model(e.parentNode().toElement().attribute("id")).variables();
            break;
        default:
            d->mtvar=t.variables();
            break;
    }
}

ChipVariables::~ChipVariables()
{
}

QString ChipVariables::packageUid() const
{
    auto pelem=element().parentNode().toElement();
    if(pelem.tagName()!=PACKAGEELEMENTNAME)pelem=pelem.parentNode().toElement();
    return pelem.attribute("uid");
}

QString ChipVariables::variantUid() const
{
    auto pelem=element().parentNode().toElement();
    if(pelem.tagName()!=PACKAGEELEMENTNAME)pelem=pelem.parentNode().toElement();
    return pelem.parentNode().toElement().attribute("uid");
}

QString ChipVariables::fileUid() const
{
    auto pelem=element().parentNode().toElement();
    if(pelem.tagName()==PACKAGEELEMENTNAME)return QString();
    else return pelem.attribute("uid");
}

bool ChipVariables::isOnTargetFile() const
{
    if(!isValid())return false;
    return !fileUid().isEmpty();
}

bool ChipVariables::isOnPackage() const
{
    if(!isValid())return false;
    return fileUid().isEmpty();
}

ChipFileTarget::FileType ChipVariables::parentFileType() const
{
    const QString tn=element().parentNode().toElement().tagName();
    return ChipFileTarget::tagNameToType(tn);
}

void ChipVariables::preloadSync()
{
    d->mvarid=variantUid();
    d->mpackid=packageUid();
    d->mfileid=fileUid();
    d->mptagname=element().parentNode().toElement().tagName();
}

void ChipVariables::resetElement()
{
    //find variant
    QDomElement var;
    for(auto n:document().documentElement().childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==VARIANTELEMENTNAME && e.attribute("uid")==d->mvarid){
            var=e;
            break;
        }
    }
    if(var.isNull())return;
    //find package
    QDomElement pack;
    for(auto n:var.childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==PACKAGEELEMENTNAME && e.attribute("uid")==d->mpackid){
            pack=e;
            break;
        }
    }
    if(pack.isNull())return;
    //find target file
    QDomElement parelem=pack;
    if(!d->mfileid.isEmpty()){
        for(auto n:pack.childNodes()){
            auto e=n.toElement();
            if(e.isNull())continue;
            if(e.tagName()==d->mptagname && e.attribute("uid")==d->mfileid){
                parelem=e;
                break;
            }
        }
    }
    //find variables
    for(auto n:parelem.childNodes()){
        auto e=n.toElement();
        if(e.isNull())continue;
        if(e.tagName()==VARIABLESELEMENTNAME){
            setElement(e);
            return;
        }
    }
}

QStringList ChipVariables::definedVariables() const
{
    QStringList ids;
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull() || e.tagName()!="v")continue;
        ids.append(e.attribute("id"));
    }
    return ids;
}

bool ChipVariables::hasVariable(QString id) const
{
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull() || e.tagName()!="v")continue;
        if(e.attribute("id")==id)return true;
    }
    return false;
}

QString ChipVariables::variableValue(QString id) const
{
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull() || e.tagName()!="v")continue;
        if(e.attribute("id")==id)return e.attribute("value");
    }
    return QString();
}

void ChipVariables::setVariable(QString id, QString val)
{
    if(id.isEmpty())return;
    if(val.isEmpty()){//TODO: is this correct?
        removeVariable(id);
        return;
    }
    //try to find and set it
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull() || e.tagName()!="v")continue;
        if(e.attribute("id")==id){
            e.setAttribute("value",val);
            altered();
            return;
        }
    }
    //append
    auto e=document().createElement("v");
    e.setAttribute("id",id);
    e.setAttribute("value",val);
    element().appendChild(e);
    altered();
}

void ChipVariables::removeVariable(QString id)
{
    for(auto n:element().childNodes()){
        auto e=n.toElement();
        if(e.isNull() || e.tagName()!="v")continue;
        if(e.attribute("id")==id){
            element().removeChild(e);
            altered();
            return;
        }
    }
}

TemplateVariables ChipVariables::templateVariables() const
{
    return d->mtvar;
}

ChipVariables ChipVariables::packageVariables() const
{
    if(isOnPackage())return *this;
    if(isOnTargetFile())return package().variables();
    return ChipVariables();
}

QMap<QString, QVariant> ChipVariables::variableMap() const
{
    QMap<QString, QVariant>ret;
    for(const auto&k:definedVariables())
        ret.insert(k,variableValue(k));
    return ret;
}

ChipFileTarget ChipVariables::fileTarget() const
{
    if(isOnTargetFile())
        return ChipFileTarget(const_cast<ChipData*>(chip()),element().parentNode().toElement());
    else
        return ChipFileTarget();
}

ChipPackage ChipVariables::package() const
{
    QDomElement pelem=element().parentNode().toElement();
    if(isOnTargetFile())pelem=pelem.parentNode().toElement();
    return ChipPackage(const_cast<ChipData*>(chip()),pelem);
}

ChipVariant ChipVariables::variant() const
{
    QDomElement pelem=element().parentNode().toElement().parentNode().toElement();
    if(isOnTargetFile())pelem=pelem.parentNode().toElement();
    return ChipVariant(const_cast<ChipData*>(chip()),pelem);
}


int metaVariablesId=qRegisterMetaType<ChipVariables>();


#include "moc_chipvariables.cpp"
