// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaObject>
#include <QObject>
#include <QPointer>
#include <QVariant>

#include "chipvariant.h"
#include "templatepins.h"

#include <DPtr>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Template {
class TemplateData;
class TemplateVariables;
}

namespace Chip {
class ChipSymbol;
class ChipFootprint;
class ChipModel;
class ChipVariables;
class ChipFileTarget;
class ChipPackage;
class TargetPinExt;

///represents a pin inside a symbol/footprint/model
class CHIPPER_EXPORT TargetPin
{
    Q_GADGET
protected:
    ///the direct code as stored in the XML file, contains additional syntax
    QString mcode;
    friend class ChipFileTarget;
    friend class TargetPinExt;
    TargetPin(QString cd):mcode(cd){}
public:
    TargetPin()=default;
    TargetPin(const TargetPin&)=default;
    TargetPin(TargetPin&&)=default;

    TargetPin& operator=(const TargetPin&)=default;
    TargetPin& operator=(TargetPin&&)=default;

    bool operator==(const TargetPin&o)const{return mcode==o.mcode;}

    ///returns true if this is a valid (existing) pin definition
    bool isValid()const{return !mcode.isEmpty();}
    ///returns true if this is a gap instead of a normal pin
    bool isGap()const{return mcode=="+";}
    ///returns true if this is an ordinary pin, hidden or visible
    bool isPin()const{return isValid() && !isGap();}
    ///returns true if this is a hidden pin
    bool isHidden()const{return mcode.startsWith("-");}

    ///hides the pin
    void hide(){if(isPin() && !mcode.startsWith("-"))mcode="-"+mcode;}
    ///makes the pin visible
    void unhide(){if(isHidden())mcode=mcode.mid(1);}

    ///returns the pure pin-ID (number) without additional qualifiers (empty for gaps)
    QString id()const{if(mcode.startsWith("-")||mcode.startsWith("+"))return mcode.mid(1);else return mcode;}

    Q_PROPERTY(QString id READ id);
    Q_PROPERTY(QString number READ id);
    Q_PROPERTY(bool isValid READ isValid);
    Q_PROPERTY(bool isGap READ isGap);
    Q_PROPERTY(bool isHidden READ isHidden);
    Q_PROPERTY(bool isPin READ isPin);
};

///extended pin which inherits data from variant and chip layers
class CHIPPER_EXPORT TargetPinExt:public TargetPin
{
    Q_GADGET
    ChipVariantPin mvpin;
    ChipPin mcpin;
    QString mtuid;
protected:
    friend class ChipFileTarget;
    TargetPinExt(QString cd,const ChipFileTarget&);
    TargetPinExt(const TargetPin&p,const ChipFileTarget&t):TargetPinExt(p.mcode,t){}
public:
    TargetPinExt()=default;
    TargetPinExt(const TargetPinExt&)=default;
    TargetPinExt(TargetPinExt&&)=default;

    TargetPinExt& operator=(const TargetPinExt&)=default;
    TargetPinExt& operator=(TargetPinExt&&)=default;

    bool operator==(const TargetPin&o)const{return TargetPin::operator==(o);}

    ///returns the pin type on variant level (connected, nc, ...)
    ChipVariantPin::PinType type()const{return mvpin.type();}
    ///returns the pin type on variant level (connected, nc, ...) as human readable string
    QString typeStr()const{return ChipVariant::pinTypeToString(mvpin.type());}
    ///returns the main name of the pin on variant level
    QString name()const{return mvpin.name();}

    ///returns the electrical function of the pin on chip level
    ChipPin::Function function()const{return mcpin.function();}
    ///returns the electrical function of the pin on chip level as human readable string
    QString functionString()const{return mcpin.functionString();}
    ///returns the electrical function of the pin on chip level as string appropriate for KiCad symbols
    QString functionKiCad()const{return mcpin.functionKiCad(isHidden());}

    ///returns the line type for symbols
    ChipPin::Line line()const{return mcpin.line();}
    ///returns the line type for symbols as human readable string
    QString lineString()const{return mcpin.lineString();}
    ///returns the line type for symbols as string for KiCad symbols
    QString lineKiCad()const{return mcpin.lineKiCad();}

    ///returns all alternate names for the pin
    QStringList alternateNames()const{return mcpin.alternateNames();}
    ///returns the alternate sub-structures for the formula and generator (exposed as property)
    QVariantList alt()const{QVariantList r;for(const auto&a:mcpin.allAlternates())r<<QVariant::fromValue(a);return r;}

    ///returns a generated UUID for this pin - it is dependent on the target file UID and pin number
    QString pinUuid()const;

    ///true if this a standard connected pin
    bool isConnected()const{return mvpin.isConnected();}
    ///true if this is a NC pin
    bool isNotConnected()const{return mvpin.isNotConnected();}
    ///true if this is a missing pin
    bool isMissing()const{return mvpin.isMissing();}
    ///true if this is a stubby pin
    bool isStub()const{return mvpin.isStub();}

    Q_PROPERTY(QString type READ typeStr);
    Q_PROPERTY(QString name READ name);
    Q_PROPERTY(ChipPin::Function function READ function);
    Q_PROPERTY(QString functionString READ functionString);
    Q_PROPERTY(QString functionKiCad READ functionKiCad);
    Q_PROPERTY(ChipPin::Line line READ line);
    Q_PROPERTY(QString lineString READ lineString);
    Q_PROPERTY(QString lineKiCad READ lineKiCad);
    Q_PROPERTY(QStringList alternateNames READ alternateNames);
    Q_PROPERTY(QVariantList alt READ alt);
    Q_PROPERTY(QString pinUuid READ pinUuid);
    Q_PROPERTY(bool isConnected READ isConnected);
    Q_PROPERTY(bool isNotConnected READ isNotConnected);
    Q_PROPERTY(bool isMissing READ isMissing);
    Q_PROPERTY(bool isStub READ isStub);
};

///base class for target files, can also be used as generic target
class CHIPPER_EXPORT ChipFileTarget:public ChipDataPartBase
{
    Q_GADGET
    Q_DECLARE_TR_FUNCTIONS(ChipFileTarget)
public:
    ///creates a null (invalid) target
    ChipFileTarget()=default;
    ///makes a new reference to the same target file
    ChipFileTarget(const ChipFileTarget&)=default;

    ///make this a new reference to the same target file
    ChipFileTarget& operator=(const ChipFileTarget&)=default;

    ///returns the parent package of the target file
    ChipPackage package()const;

    ///returns the parent variant of the target file
    ChipVariant variant()const;

    ///kind of target file represented by this object
    enum class FileType {
        ///this is an invalid target file
        Invalid,
        ///represents a symbol file
        Symbol,
        ///represents a footprint file
        Footprint,
        ///represents a 3D model file
        Model3D
    };
    ///returns the file type represented by this object
    virtual FileType fileType()const{return mtype;}
    ///returns the name of the file type represented by this object, for human readable output
    QString fileTypeName()const;

    Q_PROPERTY(FileType fileType READ fileType CONSTANT);
    Q_PROPERTY(QString fileTypeName READ fileTypeName CONSTANT);

    ///tries to convert this object to a symbol, returns an invalid symbol on failure
    ChipSymbol toSymbol()const;
    ///tries to convert this object to a footprint, returns an invalid footprint on failure
    ChipFootprint toFootprint()const;
    ///tries to convert this object to a 3D model, returns an invalid 3D model on failure
    ChipModel toModel()const;

    ///returns true if this object represents a symbol
    bool isSymbol()const{return isValid() && fileType()==FileType::Symbol;}
    ///returns true if this object represents a footprint
    bool isFootprint()const{return isValid() && fileType()==FileType::Footprint;}
    ///returns true if this object represents a 3D model
    bool is3DModel()const{return isValid() && fileType()==FileType::Model3D;}

    Q_PROPERTY(bool isSymbol READ isSymbol CONSTANT);
    Q_PROPERTY(bool isFootprint READ isFootprint CONSTANT);
    Q_PROPERTY(bool is3DModel READ is3DModel CONSTANT);

    ///returns the unique ID of the file target
    QString uid()const;
    ///returns the unique ID of the package that contains this file target
    QString packageUid()const;
    ///returns the unique ID of the variant that contains this file target
    QString variantUid()const;

    ///returns the file name of the target file
    QString name()const;
    ///sets a new name for the file
    void setName(QString);

    ///returns the name/id of the template file definition, check the package to check for the complete template name
    QString templateId()const;

    Q_PROPERTY(QString uid READ uid CONSTANT);
    Q_PROPERTY(QString packageUid READ packageUid CONSTANT);
    Q_PROPERTY(QString variantUid READ variantUid CONSTANT);
    Q_PROPERTY(QString name READ name);
    Q_PROPERTY(QString templateId READ templateId);

    ///returns names of pin rows configured in the symbol file template
    QStringList pinRowNames()const;
    ///returns values of the specified row
    ///\param rname name of the row to be returned
    QList<TargetPin> pinRow(QString rname)const;
    ///sets the pin row to specific pins
    ///\returns true on success (row exists, pins are valid)
    bool setPinRow(QString rname,const QList<TargetPin>&);
    ///helper to export pin rows into formulas
    QMap<QString,QVariant> pinRowMap()const;
    ///returns the template definition of a named pin row
    TemplatePins::Row templatePinRow(QString rname)const{return mtmppins.rowDefinition(rname);}
    ///returns the template definition
    TemplatePins templatePinDefinition()const{return mtmppins;}

    ///helper for the pin editor: create a valid pin reference, validates the pin first
    TargetPinExt makePin(QString pinid,bool isHidden=false)const;
    ///helper for the pin editor: create a gap pin reference
    TargetPinExt makeGap()const;
    ///helper for the pin editor: returns all variant level pins as target pin specs, so they can be stored in the formula pool for algorithms
    QList<TargetPinExt>makePinPool()const;
    ///helper for the pin editor: returns all variant level pins as target pin specs, so they can be stored in the formula pool for algorithms
    QVariantList makePinPoolVariant()const{return pinListToVariant(makePinPool());}
    ///helper for the pin editor: convert pin list to QVariant for formula
    static QVariantList pinListToVariant(const QList<TargetPin>&);
    ///helper for the pin editor: convert pin list to QVariant for formula
    static QVariantList pinListToVariant(const QList<TargetPinExt>&);
    ///helper for pin editor: make sure the pin list is usable for setting pin rows
    static QList<TargetPin> pinList(const QList<TargetPin>&p){return p;}
    ///helper for pin editor: make sure the pin list is usable for setting pin rows
    static QList<TargetPin> pinList(const QList<TargetPinExt>&p){QList<TargetPin> r;for(const auto&pp:p)r.append(pp);return r;}

    Q_PROPERTY(QStringList pinRowNames READ pinRowNames);
    Q_PROPERTY(QMap<QString,QVariant> pinRowMap READ pinRowMap);

    ///returns a reference to the variables definition in the target file
    ChipVariables variables()const;

    Q_PROPERTY(ChipVariables variables READ variables);

protected:
    ChipFileTarget(ChipData*c,const QDomElement&e,FileType ft):ChipDataPartBase(c,e),mtype(ft){initTemplatePins();}
    ChipFileTarget(ChipData*c,const QDomElement&e):ChipFileTarget(c,e,tagNameToType(e.tagName())){initTemplatePins();}
    ///called after reload to find the variant again and keep the object intact
    void resetElement()override;
    ///called before reload to remember UUID
    void preloadSync()override;
    //helper for resetElement: returns the tag name of the element
    virtual QString tagName()const;
    ///helper to convert tag name to file type
    static FileType tagNameToType(const QString&);
private:
    QString muid,mpackid,mvarid;
    FileType mtype=FileType::Invalid;
    TemplatePins mtmppins;
    QString mprefill;
    friend class ChipVariables;
    ///\internal helper to assign template pin cache
    void initTemplatePins();
};

///represents a target symbol file as part of a package
class CHIPPER_EXPORT ChipSymbol:public ChipFileTarget
{
    Q_GADGET
protected:
    friend class ChipData;
    friend class ChipPackage;
    friend class ChipFileTarget;
    ChipSymbol(ChipData*c,const QDomElement&e):ChipFileTarget(c,e,FileType::Symbol){}
    ChipSymbol(const ChipFileTarget&f):ChipFileTarget(f){}
    virtual QString tagName()const override;
public:
    ///creates an invalid symbol
    ChipSymbol()=default;
    ///creates a new reference to the same symbol
    ChipSymbol(const ChipSymbol&)=default;

    ///makes this a reference to the same symbol
    ChipSymbol& operator=(const ChipSymbol&)=default;

    virtual FileType fileType()const override{return FileType::Symbol;}

    ///returns a string for the default footprint in a symbol file
    QString defaultFootprint()const;
    ///set default footprint
    void setDefaultFootprint(QString);
    ///returns a string for the default device "value" to note in the symbol file
    QString defaultValue()const;
    ///set default value
    void setDefaultValue(QString);

    Q_PROPERTY(QString defaultFootprint READ defaultFootprint);
    Q_PROPERTY(QString defaultValue READ defaultValue);
};

///represents a target footprint file inside a package
class CHIPPER_EXPORT ChipFootprint:public ChipFileTarget
{
    Q_GADGET
protected:
    friend class ChipData;
    friend class ChipPackage;
    friend class ChipFileTarget;
    ChipFootprint(ChipData*c,const QDomElement&e):ChipFileTarget(c,e,FileType::Footprint){}
    ChipFootprint(const ChipFileTarget&f):ChipFileTarget(f){}
    virtual QString tagName()const override;
public:
    ///creates an invalid footprint
    ChipFootprint()=default;
    ///makes this a reference to the same footprint
    ChipFootprint(const ChipFootprint&)=default;

    ///makes this a reference to the same footprint
    ChipFootprint& operator=(const ChipFootprint&)=default;

    virtual FileType fileType()const override{return FileType::Footprint;}

    //TODO: get TemplateFootprint

    ///returns a string for the default 3D model file for the footprint
    QString defaultModel()const;
    ///sets the default model for the footprint
    void setDefaultModel(QString);

    Q_PROPERTY(QString defaultModel READ defaultModel);
};

///represents a target 3D model file inside a package
class CHIPPER_EXPORT ChipModel:public ChipFileTarget
{
    Q_GADGET
protected:
    friend class ChipData;
    friend class ChipPackage;
    friend class ChipFileTarget;
    ChipModel(ChipData*c,const QDomElement&e):ChipFileTarget(c,e,FileType::Model3D){}
    ChipModel(const ChipFileTarget&f):ChipFileTarget(f){}
    virtual QString tagName()const override;
public:
    ///creates an invalid 3D model file target
    ChipModel()=default;
    ///makes this a reference to the same model
    ChipModel(const ChipModel&)=default;

    ///makes this a reference to the same model
    ChipModel& operator=(const ChipModel&)=default;

    virtual FileType fileType()const override{return FileType::Model3D;}

    //TODO: get TemplateModel
};

//end of namespace
}}
using namespace Chipper::Chip;

Q_DECLARE_METATYPE(ChipFileTarget);
Q_DECLARE_METATYPE(ChipSymbol);
Q_DECLARE_METATYPE(TargetPin);
Q_DECLARE_METATYPE(TargetPinExt);
Q_DECLARE_METATYPE(ChipFootprint);
Q_DECLARE_METATYPE(ChipModel);
