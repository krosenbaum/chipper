// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdatapriv.h"
#include "chipvariant.h"
#include "chippackage.h"
#include "chipper.h"
#include "templates.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QSettings>
#include <QTimer>
#include <QUuid>

#include <DomNodeIterator>




// ////////////////////////////////////
// Chip Variant Definition

void ChipVariant::preloadSync()
{
    muid=uid();
}

void ChipVariant::resetElement()
{
    //try to find element
    for(auto n:document().documentElement().childNodes())
        if(n.isElement() && n.nodeName()==VARIANTELEMENTNAME){
            QDomElement e=n.toElement();
            if(e.attribute("uid")==muid){
                setElement(e);
                return;
            }
        }
    //fall back to a full reset
    ChipDataPartBase::resetElement();
}

QString ChipVariant::uid() const
{
    return element().attribute("uid");
}

QString ChipVariant::name() const
{
    return element().attribute("name");
}

void ChipVariant::setName(QString n)
{
    if(n!=name()){
        element().setAttribute("name",n);
        altered();
    }
}

QStringList ChipVariant::packageIds() const
{
    QStringList r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=PACKAGEELEMENTNAME)continue;
        r<<e.attribute("uid");
    }
    return r;
}

ChipPackage ChipVariant::packageById(QString id) const
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=PACKAGEELEMENTNAME)continue;
        if(e.attribute("uid")==id)
            return ChipPackage(const_cast<ChipData*>(chip()),e);
    }
    return ChipPackage();
}

bool ChipVariant::hasPackageId(QString id) const
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=PACKAGEELEMENTNAME)continue;
        if(e.attribute("uid")==id)
            return true;
    }
    return false;
}

ChipPackage ChipVariant::newPackage(const Template::TemplateData&tmp,QString id)
{
    if(!tmp.isValid())return ChipPackage();
    //validate ID
    QUuid uid(id);
    if(uid.isNull())
        uid=QUuid::createUuid();
    id=uid.toString(QUuid::WithoutBraces);
    if(hasPackageId(id))return ChipPackage();
    //create element
    auto np=document().createElement(PACKAGEELEMENTNAME);
    np.setAttribute("uid",id);
    np.setAttribute("template",tmp.title());
    element().appendChild(np);
    altered();
    return ChipPackage(chip(),np);
}

bool ChipVariant::deletePackage(QString id)
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=PACKAGEELEMENTNAME)continue;
        if(e.attribute("uid")==id){
            element().removeChild(n);
            altered();
            return true;
        }
    }
    return false;
}

int ChipVariant::numPins() const
{
    return element().attribute("pins","1").toInt();
}

void ChipVariant::setNumPins(int n)
{
    if(n!=numPins()){
        altered();
        element().setAttribute("pins",QString::number(n));
    }
}
//internal: XML to enum
static inline ChipVariantPin::PinType s2pintype(QString s)
{
    s=s.toLower().trimmed();
    if(s=="missing")return ChipVariantPin::PinType::Missing;
    if(s=="stub")return ChipVariantPin::PinType::Stub;
    if(s=="nc")return ChipVariantPin::PinType::NotConnected;
    return ChipVariantPin::PinType::Connected;
}

//internal: enum to XML
static inline QString pintype2s(ChipVariantPin::PinType p)
{
    switch(p)
    {
        case ChipVariantPin::PinType::NotConnected:return "nc";
        case ChipVariantPin::PinType::Connected:return "con";
        case ChipVariantPin::PinType::Missing:return "missing";
        case ChipVariantPin::PinType::Stub:return "stub";
    }
    //oops!
    qDebug()<<"Unhandled pin type"<<(int)p;
    return "?";
}

//external: enum to display
QString ChipVariant::pinTypeToString(ChipVariantPin::PinType p)
{
    switch(p)
    {
        case ChipVariantPin::PinType::Connected:return tr("Connected");
        case ChipVariantPin::PinType::NotConnected:return tr("Not Connected");
        case ChipVariantPin::PinType::Missing:return tr("Missing");
        case ChipVariantPin::PinType::Stub:return tr("Stub");
        default:return "?";
    }
}

QList<QPair<ChipVariantPin::PinType, QString>> ChipVariant::allPinTypes()
{
    return QList<QPair<ChipVariantPin::PinType, QString>>()
        <<QPair<ChipVariantPin::PinType, QString>(ChipVariantPin::PinType::Connected,   pinTypeToString(ChipVariantPin::PinType::Connected))
        <<QPair<ChipVariantPin::PinType, QString>(ChipVariantPin::PinType::Missing,     pinTypeToString(ChipVariantPin::PinType::Missing))
        <<QPair<ChipVariantPin::PinType, QString>(ChipVariantPin::PinType::NotConnected,pinTypeToString(ChipVariantPin::PinType::NotConnected))
        <<QPair<ChipVariantPin::PinType, QString>(ChipVariantPin::PinType::Stub,        pinTypeToString(ChipVariantPin::PinType::Stub));
}

QList<ChipVariantPin::PinType> ChipVariant::stringToPinTypeList(QString text)
{
    QList<ChipVariantPin::PinType> r;
    for(const auto&s:text.split(' ',Qt::SkipEmptyParts))
        r.append(s2pintype(s));
    return r;
}

ChipVariantPin ChipVariant::pin(QString num) const
{
    //try to find it
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        if(e.attribute("num")==num){
            const auto type=s2pintype(e.attribute("type"));
            auto name=e.attribute("name");
            if(type!=ChipVariantPin::PinType::Connected){
                if(type==ChipVariantPin::PinType::NotConnected)name="NC";
                else name="~";
            }
            return ChipVariantPin(num,type,name);
        }
    }
    return ChipVariantPin();
}

bool ChipVariant::setPin(QString id, ChipVariantPin::PinType tp, QString nm)
{
    return setPin(ChipVariantPin(id,tp,nm));
}

bool ChipVariant::setPin(const ChipVariantPin&pin)
{
    if(pin.id().isEmpty()){
        qDebug()<<"Ooops. Trying to set variant pin with no ID. Ignoring it.";
        return false;
    }
    //try to find it: replace
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        if(e.attribute("num")==pin.id()){
            e.setAttribute("type",pintype2s(pin.type()));
            if(pin.type()==ChipVariantPin::PinType::Connected)
                e.setAttribute("name",pin.name());
            else
                e.removeAttribute("name");
            altered();
            return true;
        }
    }
    //not found: append
    return false;
}

bool ChipVariant::addPin(const ChipVariantPin& pin, QString before)
{
    //check name
    if(pin.id().isEmpty())return false;
    if(pinIds().contains(pin.id()))return false;
    //create new element
    QDomElement npe=document().createElement(VARIANTPINELEMENTNAME);
    npe.setAttribute("num",pin.id());
    npe.setAttribute("type",pintype2s(pin.type()));
    if(pin.type()==ChipVariantPin::PinType::Connected)
        npe.setAttribute("name",pin.name());
    //find before
    QDomElement be;
    if(!before.isEmpty())
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        if(e.attribute("num")==before){
            be=e;
            break;
        }
    }
    //add element
    if(before.isEmpty() || be.isNull())
        element().appendChild(npe);
    else
        element().insertBefore(npe,be);
    altered();
    return true;
}

bool ChipVariant::swapPins(QString p1, QString p2)
{
    //check name
    if(p1==p2 || p1.isEmpty() || p2.isEmpty())return false;
    //find them
    QDomElement pe1,pe2;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        const auto num=e.attribute("num");
        if(num==p1)pe1=e;
        else if(num==p2)pe2=e;
        if(!pe1.isNull() && !pe2.isNull())break;
    }
    if(pe1.isNull() || pe2.isNull())return false;
    //get data
    const QString name1=pe1.attribute("name");
    const QString type1=pe1.attribute("type");
    const QString num_1=pe1.attribute("num");
    const QString name2=pe2.attribute("name");
    const QString type2=pe2.attribute("type");
    const QString num_2=pe2.attribute("num");
    //p1 <== p2
    if(name2.isEmpty())pe1.removeAttribute("name");
    else pe1.setAttribute("name",name2);

    if(type2.isEmpty())pe1.removeAttribute("type");
    else pe1.setAttribute("type",type2);

    pe1.setAttribute("num",num_2);
    //p2 <== p1
    if(name1.isEmpty())pe2.removeAttribute("name");
    else pe2.setAttribute("name",name1);

    if(type1.isEmpty())pe2.removeAttribute("type");
    else pe2.setAttribute("type",type1);

    pe2.setAttribute("num",num_1);
    //done
    altered();
    return true;
}

void ChipVariant::removePin(QString id)
{
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        if(e.attribute("num")==id){
            element().removeChild(n);
            altered();
            break;
        }
    }
}

QStringList ChipVariant::pinIds() const
{
    QStringList ret;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        ret.append(e.attribute("num"));
    }
    return ret;
}

bool ChipVariant::renumberPin(QString oldid, QString newid)
{
    if(oldid.isEmpty() || newid.isEmpty())return false;
    if(oldid==newid)return true;
    //check
    QDomElement pine;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        const QString num=e.attribute("num");
        if(num==oldid)pine=e;
        else if(num==newid)
            return false;
    }
    if(pine.isNull())return false;
    //set
    pine.setAttribute("num",newid);
    altered();
    return true;
}

QList<QVariant> ChipVariant::pinVarList() const
{
    QList<QVariant> ret;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.tagName()!=VARIANTPINELEMENTNAME)continue;
        ChipVariantPin cvp(e.attribute("num"),s2pintype(e.attribute("type")),e.attribute("name"));
        ret.append(QVariant::fromValue(cvp));
    }
    return ret;
}

QMap<QString, QVariant> ChipVariant::packageMap() const
{
    QMap<QString, QVariant>ret;
    for(const auto&k:packageIds())
        ret.insert(k,QVariant::fromValue(packageById(k)));
    return ret;
}



QString ChipVariantPin::typeStr() const
{
    return ChipVariant::pinTypeToString(mtype);
}



static int metaVariantId=qRegisterMetaType<ChipVariant>();
static int metaVariantPinId=qRegisterMetaType<ChipVariantPin>();

#include "moc_chipvariant.cpp"
