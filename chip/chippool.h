// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Pool
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QStringList>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Chip {

class ChipData;


// ////////////////////////////////////
// Pool

///Central pool if chip file data - chip file objects can only exist inside this pool.
class CHIPPER_EXPORT ChipPool final:public QObject
{
    Q_OBJECT
    ///\internal constructor
    ChipPool();
    ///\internal destructor
    ~ChipPool();

    //no funny business
    ChipPool(const ChipPool&)=delete;
    ChipPool(ChipPool&&)=delete;
    ChipPool& operator=(const ChipPool&)=delete;
    ChipPool& operator=(ChipPool&&)=delete;
public:
    ///reference to the pool singleton
    static ChipPool&instance();

    ///returns how many chips are currently in the pool.
    int numChips()const;
    ///returns chip by position in pool (or nullptr if there is none at that position)
    ChipData* chip(int)const;
    ///returns chip by file name (or nullptr if that file does not exist in the pool)
    ChipData* chipByName(QString)const;
    ///returns chip by file name and attempts to load the file if it is not already loaded (returns nullptr on failure to load)
    ChipData* findOrLoadChip(QString);

    ///attempts to load all file names in this list
    void loadChips(QStringList);
    ///returns the file names of all currently loaded chips (can be used with chipByName or findOrLoadChip)
    QStringList loadedChipNames()const;

    ///configures the synchronization timer for chip files (default: 30s; 0 turns autosync off)
    bool setSyncTimer(int timeInSeconds);

    ///returns the current auto-sync interval in seconds
    static int syncTimerInterval();

    ///attempts to unload the chip
    void unloadChip(ChipData*);

private slots:
    ///\internal used to remove null pointers from the chip pool
    void compress();
signals:
    ///emitted if a new chip is loaded
    void chipLoaded(QString filename,ChipData*chipobject);
    ///emitted after a chip is removed from the pool
    void chipUnloaded(QString filename);
    ///emitted if loading a chip file fails
    void chipLoadFailed(QString filename,QString errorStr);
    ///\internal signals to chip objects that they need to auto-sync now
    void syncChips();
};

//end of namespace
}}
using namespace Chipper::Chip;
