// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Structures
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QDomDocument>
#include <QDomElement>
#include <QPointer>
#include <QMetaObject>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Chip {

// ////////////////////////////////////
// Data Details

class ChipData;

class ChipDataPartBase;
class ChipMetaData;
class ChipTargetFiles;
class ChipPins;
class ChipVariant;

// ////////////////////////////////////
// Main Data Object

///Represents a chip file and all its internal data.
///ChipData objects can not be instantiated or deleted by most code, only the chip pool can do so.
class CHIPPER_EXPORT ChipData final:public QObject
{
    Q_OBJECT
    //usually instantiated from the pool
    friend class ChipPool;
    ///instantiate from file name
    ChipData(QString);
    ///delete the object
    ~ChipData();
    //no funny stuff!
    ChipData()=delete;
    ChipData(const ChipData&)=delete;
    ChipData(ChipData&&)=delete;

    //meta data for object state
    QString mfname,mlderror;
    //actual chip data in XML form
    QDomDocument mxml;
public:
    Q_PROPERTY(bool hasLoadError READ hasLoadError);
    Q_PROPERTY(QString loadError READ loadError);
    ///returns true if there was a problem loading the chip file
    bool hasLoadError()const{return !mlderror.isEmpty();}
    ///returns the error while loading the chip file if hasLoadError() returned true
    QString loadError()const{return mlderror;}

    ///descrihes what copy of the chip file to access
    enum class FileCopy{
        ///Flags: no copy exists
        None=0,
        ///Access the chip file directly (*.chip)
        Chip=1,
        ///Access the write-ahead copy (*.chip~atmp)
        Ahead=2,
        ///Access the backup copy (*.chip~bak)
        Backup=4
    };
    Q_DECLARE_FLAGS(FileCopies,FileCopy);
    Q_ENUM(FileCopy);
    Q_FLAG(FileCopies);
    ///Current synchronization state of data in memory vs. on disk
    enum class SaveState{
        ///no data is loaded into the memory or loading produced an error
        None=0,
        ///data is saved to the chip file, this is also the initial state after a successful load or after creating a new chip
        Saved=1,
        ///data is synced to the write-ahead file, but not the chip file; also the initial state after loading from the write-ahead file
        Synced=2,
        ///data in memory is altered, alterations are not on disk, also the initial state after loading from the backup file
        Altered=3
    };
    Q_ENUM(SaveState);

    ///returns true if a specific copy exists
    bool hasFileCopy(FileCopy)const;
    ///returns which chip file copies exist
    FileCopies fileCopies()const;

    ///returns the current state of the object in relation to on-disk data
    SaveState saveState()const{return msavestate;}

    Q_PROPERTY(QString fileName READ fileName);
    ///returns the (main) file name of the chip
    QString fileName()const{return mfname;}

    ///returns the file name of the most current copy that exists
    QString fileNameOfState()const;

    Q_PROPERTY(ChipMetaData metaData READ metaData);
    ///returns a reference to meta data
    ChipMetaData metaData()const;

    Q_PROPERTY(ChipTargetFiles targets READ targets);
    ///returns a reference to generator target pathes
    ChipTargetFiles targets()const;

    Q_PROPERTY(ChipPins pins READ pins);
    ///returns a reference to the generic pin table
    ChipPins pins()const;

    ///returns the number of chip variants that currently exist
    int numVariants()const;
    ///returns the UUIDs of all variants that exist in order of defintion
    QStringList variantIds()const;
    ///returns references to all variants of this chip
    QList<ChipVariant>allVariants();
    ///returns a variant by its ID (or an invalid variant if it does not exist)
    ChipVariant variantById(QString)const;
    ///returns true if the variant ID exists
    bool hasVariantId(QString)const;

    ///creates a new variant and returns a reference to it (non-valid on error)
    ///\param id the UID of the new variant, if not a valid UUID a new id is generated
    ChipVariant newVariant(QString id=QString());
    ///deletes a variant, returns true on success
    bool deleteVariant(QString id);

    ///returns notes text of the chip
    QString notes()const;
    ///sets the notes text of the chip
    void setNotes(QString);

    Q_PROPERTY(int numVariants READ numVariants);
    Q_PROPERTY(QStringList variantIds READ variantIds);
    Q_PROPERTY(QList<ChipVariant> allVariants READ allVariants);

public slots:
    ///reload from a specific copy
    bool reloadFile(FileCopy fc=FileCopy::Chip);
    ///save to a specific copy (main file per default), happens automatically on tab close
    bool saveFile(FileCopy fc=FileCopy::Chip);
    ///synchronize to the Ahead-file (same as saveFile(FileCopy::Ahead)), used by pool
    bool syncFile();

signals:
    ///emitted just before the chip object is closed, last chance to save it
    void aboutToUnload(QString);
    ///emitted after the chip object has been discarded
    void unloaded(QString);

    ///\internal reset all connected parts
    void internalReset();
    ///\internal remember part coordinates
    void preloadSync();

private:
    //current state of the object in relation to on-disk storage
    SaveState msavestate=SaveState::None;

    //helper to save XML data to file, no state check logic
    bool dumpXml(QString);

    //helper to mark the instance as altered, called from child objects
    void altered();
    friend class ChipDataPartBase;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(ChipData::FileCopies);

//end of namespace
}}
using namespace Chipper::Chip;
