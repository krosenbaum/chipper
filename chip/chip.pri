SOURCES += \
    $$PWD/chipdata.cpp \
    $$PWD/chipdatapriv.cpp \
    $$PWD/chipdetail.cpp \
    $$PWD/chipvariant.cpp \
    $$PWD/chippackage.cpp \
    $$PWD/chippool.cpp \
    $$PWD/chipfile.cpp \
    $$PWD/chipvariables.cpp

HEADERS += \
    $$PWD/chipdata.h \
    $$PWD/chipdatapriv.h \
    $$PWD/chipdetail.h \
    $$PWD/chipvariant.h \
    $$PWD/chippackage.h \
    $$PWD/chippool.h \
    $$PWD/chipfile.h \
    $$PWD/chipvariables.h

INCLUDEPATH += $$PWD

RESOURCES += $$PWD/chip.qrc
