// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Pool
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "chippool.h"
#include "chipdata.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>


// ////////////////////////////////////
// Chip Pool

ChipPool & ChipPool::instance()
{
    static ChipPool inst;
    return inst;
}

static QList<QPointer<ChipData>>chips;
static QPointer<QTimer>syncTimer;

#define SYNCTIMERKEY "autoSyncTimerInterval"


ChipPool::ChipPool()
{
    qDebug()<<"Initializing Chip Pool.";
    syncTimer=new QTimer(this);
    connect(syncTimer,&QTimer::timeout,this,&ChipPool::syncChips);
    int tm=syncTimerInterval();
    syncTimer->setSingleShot(false);
    if(tm>0){
        syncTimer->setInterval(tm*1000);
        syncTimer->start();
    }
}

ChipPool::~ChipPool()
{
    qDebug()<<"Unloading Chip Pool...";
    QList<QPointer<ChipData>>cc=chips;
    chips.clear();
    for(auto c:cc)
        if(!c.isNull()){
            c->disconnect(this);
            delete c.data();
        }
    qDebug()<<"Chip Pool Empty.";
}

int ChipPool::numChips() const
{
    return chips.size();
}

ChipData * ChipPool::chip(int i) const
{
    return chips.value(i);
}

ChipData * ChipPool::chipByName(QString f) const
{
    //normalize file name
    QString fn=QFileInfo(f).canonicalFilePath();
    if(fn.isEmpty())
        return nullptr;
    //try to find it
    for(auto chip:chips)
        if(!chip.isNull() && chip->fileName()==fn)
            return chip;
    //none found
    return nullptr;
}

ChipData * ChipPool::findOrLoadChip(QString f)
{
    //normalize file name
    QString fn=QFileInfo(f).canonicalFilePath();
    if(fn.isEmpty()){
        qDebug()<<"Chip file"<<f<<"does not exist. Cannot load.";
        emit chipLoadFailed(f,tr("Chip file %1 does not exist.").arg(f));
        return nullptr;
    }
    //attempt to find it
    auto*chip=chipByName(fn);
    if(chip)return chip;
    //attempt to load it
    chip=new ChipData(fn);
    if(chip->hasLoadError()){
        qDebug()<<"Error while loading chip file"<<fn<<":"<<chip->loadError();
        emit chipLoadFailed(fn,chip->loadError());
        delete chip;
        return nullptr;
    }
    chips.append(chip);
    connect(chip,&ChipData::unloaded,this,&ChipPool::chipUnloaded);
    connect(chip,&ChipData::destroyed,this,&ChipPool::compress,Qt::QueuedConnection);
    qDebug()<<"Chip file"<<fn<<"successfully loaded.";
    emit chipLoaded(fn,chip);
    return chip;
}

void ChipPool::loadChips(QStringList l)
{
    for(auto f:l)
        findOrLoadChip(f);
}

QStringList ChipPool::loadedChipNames() const
{
    QStringList r;
    for(auto chip:chips)
        r<<chip->fileName();
    return r;
}

void ChipPool::compress()
{
    QList<QPointer<ChipData>> np;
    for(auto c:chips)if(!c.isNull())np.append(c);
    chips=np;
}

int ChipPool::syncTimerInterval()
{
    return QSettings().value(SYNCTIMERKEY,30).toInt();
}

bool ChipPool::setSyncTimer(int tm)
{
    if(tm<0 || tm>3600)return false;
    syncTimer->stop();
    if(tm>0){
        syncTimer->setInterval(tm*1000);
        syncTimer->start();
    }
    QSettings().setValue(SYNCTIMERKEY,tm);
    return true;
}

void ChipPool::unloadChip(ChipData*c)
{
    if(c==nullptr)return;
    if(chips.contains(c)){
        chips.removeAll(c);
        c->deleteLater(); //give everybody a chance to clean up
        emit chipUnloaded(c->fileName());
    }
}


#include "moc_chippool.cpp"
