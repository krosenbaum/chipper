// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaObject>
#include <QObject>
#include <QPointer>
#include <QVariant>

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Chip {

// ////////////////////////////////////
// Data Details

class ChipData;

///abstract base class of all sub-objects of a chip
class CHIPPER_EXPORT ChipDataPartBase
{
    Q_GADGET
    ///reference to the chip file this is a part of
    QPointer<ChipData>mchip;
    ///reference to its own element
    QDomElement melem;
    //references to reset logic if the file gets reloaded
    QMetaObject::Connection mreset,mpreload;
protected:
    ///child classes should call this function every time data in this part changes
    void altered();

    ///returns a reference to the defining XML element of this part
    QDomElement element()const{return melem;}
    ///changes the reference to the defining XML element of this part (must be part of the defining document)
    void setElement(const QDomElement&);

    ///this method is called AFTER the chip file was reloaded, overrides should attempt to find the same element again
    virtual void resetElement();
    ///this method is called just before the chip file is reloaded, overrides should use it to remember coordinates for this part
    virtual void preloadSync();

    ///returns a reference to the main chip document
    QDomDocument document()const;

    ///used by child-classes to instantiate a valid part
    /// \param parentChip pointer to the chip this is a part of
    /// \param partElement the XML element represented by this object
    explicit ChipDataPartBase(ChipData*parentChip,const QDomElement&partElement);
    ///instantiates a null part
    ChipDataPartBase()=default;
    ///instantiates a part that references the same XML element in the same chip file
    ChipDataPartBase(const ChipDataPartBase&);
    ///(abstract) destructor, cleans up the part and its connection to the chip file object
    virtual ~ChipDataPartBase()=0;

    ///makes this part a copy of the other part, must be overridden by child classes
    ChipDataPartBase& operator=(const ChipDataPartBase&);

public:
    Q_PROPERTY(bool isValid READ isValid);
    ///returns true if this is a valid chip part (i.e. it belongs to a chip and has a valid XML element)
    virtual bool isValid()const;

    ///returns a pointer to the chip this is a part of
    ChipData*chip();
    ///returns a pointer to the chip this is a part of
    const ChipData*chip()const;
};
//can not be instantiated, hence not:
//Q_DECLARE_METATYPE(ChipDataPartBase);

///represents meta data from/about the chip file
class CHIPPER_EXPORT ChipMetaData:public ChipDataPartBase
{
    Q_GADGET
protected:
    //main chip needs to instantiate this
    friend class ChipData;
    ///used by the main chip object to instantiate a meta data object
    ChipMetaData(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){}
    ///used after reload to find meta data again
    void resetElement()override;
public:
    ///instantiates a null object
    ChipMetaData()=default;
    ///instantiates a reference to the same meta data
    ChipMetaData(const ChipMetaData&)=default;

    ///makes this object a reference to the same meta data
    ChipMetaData& operator=(const ChipMetaData&)=default;

    ///returns the name of the chip manufacturer
    QString manufacturer()const;
    ///returns the chip type name
    QString chipType()const;
    ///returns the author of the chip file
    QString author()const;
    ///returns the copyright string of the chip file
    QString copyrightString()const;
    ///returns the license this chip file is under
    QString license()const;
    ///returns the URL to the datasheet for this chip
    QString datasheetUrl()const;
    ///returns the keywords for this chip
    QString keywords()const;
    ///returns the description for this chip
    QString description()const;
    ///returns the reference prefix for this chip
    QString refPrefix()const;

    ///sets the name of the chip manufacturer
    void setManufacturer(QString);
    ///sets the chip type name
    void setChipType(QString);
    ///sets the author of the chip file
    void setAuthor(QString);
    ///sets the copyright string of the chip file
    void setCopyrightString(QString);
    ///sets the license this chip file is under
    void setLicense(QString);
    ///sets the URL to the datasheet for this chip
    void setDatasheetUrl(QString);
    ///returns the keywords for this chip
    void setKeywords(QString);
    ///returns the description for this chip
    void setDescription(QString);
    ///returns the reference prefix for this chip
    void setRefPrefix(QString);

    Q_PROPERTY(QString manufacturer READ manufacturer WRITE setManufacturer);
    Q_PROPERTY(QString chipType READ chipType WRITE setChipType);
    Q_PROPERTY(QString author READ author WRITE setAuthor);
    Q_PROPERTY(QString copyrightString READ copyrightString WRITE setCopyrightString);
    Q_PROPERTY(QString license READ license WRITE setLicense);
    Q_PROPERTY(QString datasheetUrl READ datasheetUrl WRITE setDatasheetUrl);
    Q_PROPERTY(QString keywords READ keywords WRITE setKeywords);
    Q_PROPERTY(QString description READ description WRITE setDescription);
    Q_PROPERTY(QString refPrefix READ refPrefix WRITE setRefPrefix);
};

///represents targeting locations for the file generator - i.e. where Chipper will put the symbol, footprint and 3D files
class CHIPPER_EXPORT ChipTargetFiles:public ChipDataPartBase
{
    Q_GADGET
protected:
    //chip needs to instantiate this class
    friend class ChipData;
    ///used by the main chip object to instantiate this class
    ChipTargetFiles(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){}
    ///tries to find the target tag after reloading the file
    void resetElement()override;
public:
    ChipTargetFiles()=default;
    ///instantiates a reference to the same target data
    ChipTargetFiles(const ChipTargetFiles&)=default;

    ///makes this a reference to the targetting data of the same chip
    ChipTargetFiles& operator=(const ChipTargetFiles&)=default;

    ///returns the target string for symbol files
    QString symbolFile()const;
    ///returns the target string for footprint files
    QString footprintFile()const;
    ///returns the target string for 3D model files
    QString modelFile()const;

    ///sets the target string for symbol files
    void setSymbolFile(QString);
    ///sets the target string for footprint files
    void setFootprintFile(QString);
    ///sets the target string for 3D model files
    void setModelFile(QString);

    Q_PROPERTY(QString symbolFile READ symbolFile WRITE setSymbolFile);
    Q_PROPERTY(QString footprintFile READ footprintFile WRITE setFootprintFile);
    Q_PROPERTY(QString modelFile READ modelFile WRITE setModelFile);
};

///represents a single pin defintion as part of the overall generic pins table of the chip (XPath: /Chip/Pins/Pin)
class CHIPPER_EXPORT ChipPin:public ChipDataPartBase
{
    Q_GADGET
    ///\internal used to remember current name while reloading, not valid during normal operation - use element() instead!
    QString mname,msupername;

    Q_DECLARE_TR_FUNCTIONS(ChipPin)
protected:
    //access for parent hierarchy
    friend class ChipData;
    friend class ChipPins;
    ///used by ChipPins instance to instantiate the pin
    ChipPin(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){preloadSync();}
    ///relocate the pin inside the table
    void resetElement()override;
    ///remember current name for resetElement()
    void preloadSync()override;
public:
    ///instantiate a null pin
    ChipPin()=default;
    ///instantiates a reference to the same pin
    ChipPin(const ChipPin&)=default;

    ///makes this a reference to the same pin
    ChipPin& operator=(const ChipPin&)=default;

    ///This enum describes the function of the pin in KiCAD terms.
    enum class Function{
        ///a signal pin - this is the default value and should be used for almost all functional pins
        Signal,
        ///a signal input pin
        SignalIn,
        ///a signal output pin
        SignalOut,
        ///the power supply opposite ground (can be positive or negative), translates to passive
        Power,
        ///the power supply opposite ground (can be positive or negative), translates to power_in
        PowerIn,
        ///the power supply opposite ground (can be positive or negative), translates to power_out
        PowerOut,
        ///the ground connection of the power supply (usually 0V), translates to passive
        Ground,
        ///the ground connection of the power supply (usually 0V), translates to power_in
        GroundIn,
        ///the ground connection of the power supply (usually 0V), translates to power_out
        GroundOut,

        ///a passive pin, can be pretty much anything
        Passive,
        ///open collector/drain output (needs pull up)
        OpenCollector,
        ///open emitter/source output (needs pull down)
        OpenEmitter,
        ///tri-state output (has a High-Z mode)
        TriState,
        ///not connected pin (should usually be hidden)
        NoConnect,
        ///free style pin, no ERC errors, no known function
        Free,
        ///pin type not specified, lots of ERC errors and angry users
        Unspecified,
    };
    Q_ENUM(Function);

    enum class Line{
        Line,
        Inverted,
        InputLow,
        OutputLow,
        Clock,
        ClockLow,
        EdgeClockHigh,
        InvertedClock,
        NonLogic,
    };
    Q_ENUM(Line);

    ///true if this is a full pin
    bool isPin()const;
    ///true if this is an alternate function
    bool isAlternate()const;

    ///returns the (unique) name of the pin
    QString name()const;
    ///returns alternate function names of the pin
    QStringList alternateNames()const;
    ///true if the alternate function with that name exists
    bool hasAlternate(QString)const;
    ///returns the alternate function
    ChipPin alternate(QString)const;
    ///returns a list of all alternate functions
    QList<ChipPin> allAlternates()const;
    ///\internal returns alternate functions for formula engine
    QVariantList allAlternatesVariant()const;

    ///returns the function of the pin
    Function function()const;
    ///returns the function of the pin as a string
    QString functionString()const{return functionToString(function());}
    ///returns the function of the pin as a KiCad symbol command
    QString functionKiCad(bool isHidden=false)const{return functionToKiCad(function(),isHidden);}
    ///helper for pin filter: converts XML level string to list of functions
    static QList<Function> stringToFunctionList(QString);

    ///returns the line type of the pin
    Line line()const;
    ///returns the line type as a human readable string
    QString lineString()const{return lineToString(line());}
    ///returns the line type as a KiCad symbol setting
    QString lineKiCad()const{return lineToKiCad(line());}

    ///sets the name of the pin, returns true on success - this method fails if the name is invalid or already exists
    bool setName(QString);
    ///sets the function of the pin
    void setFunction(Function);
    ///sets the line type of the pin
    void setLine(Line);
    ///adds an alternative function
    bool addAlternate(QString,Function,Line);
    ///removes an alternative function
    bool removeAlternate(QString);

    Q_PROPERTY(QString name READ name WRITE setName);
    Q_PROPERTY(QStringList alternateNames READ alternateNames);
    Q_PROPERTY(Function function READ function WRITE setFunction);
    Q_PROPERTY(QString functionString READ functionString);
    Q_PROPERTY(QString functionKiCad READ functionKiCad);
    Q_PROPERTY(Line line READ line WRITE setLine);
    Q_PROPERTY(QString lineString READ lineString);
    Q_PROPERTY(QString lineKiCad READ lineKiCad);
    Q_PROPERTY(QVariantList allAlternates READ allAlternatesVariant);

    ///returns a printable, human readable, translated string for a function value
    static QString functionToString(Function);
    ///returns a list of all known function types
    static QList<QPair<Function,QString>> allFunctions();
    ///returns the KiCad symbol type corresponding to a function
    ///\param f the function to translate
    ///\param isHidden if true: the pin is hidden - power and input types are returned as passive instead
    static QString functionToKiCad(Function f,bool isHidden=false);

    ///converts a line type to a human readable string
    static QString lineToString(Line);
    ///returns all known line types
    static QList<QPair<Line,QString>> allLineTypes();
    ///converts a line type to a KiCad setting
    static QString lineToKiCad(Line);

protected:
    class XmlValueError:public QString
    {
    public:
        XmlValueError()=default;
        XmlValueError(QString s):QString(s){}
        XmlValueError(int i):XmlValueError(QString::number(i)){}
        XmlValueError(const XmlValueError&)=default;
        XmlValueError(XmlValueError&&)=default;
    };
    static Function xmlToFunction(QString)noexcept(false);
    static QString functionToXml(Function)noexcept(false);
    static Line xmlToLine(QString)noexcept(false);
    static QString lineToXml(Line)noexcept(false);
};

///represents the generic pin table of the chip file
class CHIPPER_EXPORT ChipPins:public ChipDataPartBase
{
    Q_GADGET
protected:
    //give parent and only child access
    friend class ChipData;
    friend class ChipPin;
    ///used by both main chip element as well as pins to instantiate full table (ChipPin needs this for certain queries)
    ChipPins(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){}
    ///called during reload to find pin table again
    void resetElement()override;
public:
    ChipPins()=default;
    ///instantiates another reference of the same pin table
    ChipPins(const ChipPins&)=default;

    ///makes this a reference to the same pin table
    ChipPins& operator=(const ChipPins&)=default;

    ///returns the number of pin definitions in this table, this need not be the same as the actual number of physical pins on any variant
    int numPins()const;
    ///returns all pins in this table in the order of their definition
    QList<ChipPin>allPins();
    ///returns the unique names of all pins in this table in order of their definition
    QStringList allNames()const;

    Q_PROPERTY(int numPins READ numPins);
    Q_PROPERTY(QList<ChipPin> allPins READ allPins);
    Q_PROPERTY(QStringList allNames READ allNames);

    ///adds a pin to the end of this table and returns a reference to it, returns an invalid pin if the name already exists or is forbidden
    ///\param name the unique name of the new pin
    ChipPin addPin(QString name);
    ///moves a pin to a different position in the table, returns true on success
    ///\param moveThis the pin that will be moved
    ///\param afterThis the pin will be moved behind this or to the top of the table if this parameter is omitted or invalid
    bool movePin(ChipPin moveThis,ChipPin afterThis=ChipPin());
    ///deletes the pin from the table, returns true on success
    bool deletePin(ChipPin);

    ///tries to find a pin by name and returns a reference
    const ChipPin byName(QString)const;
    ///tries to find a pin by name and returns a reference
    ChipPin byName(QString);

    ///returns true if a pin with the given name exists
    bool hasPinNamed(QString)const;
};

//end of namespace
}}
using namespace Chipper::Chip;

Q_DECLARE_METATYPE(ChipMetaData);
Q_DECLARE_METATYPE(ChipTargetFiles);
Q_DECLARE_METATYPE(ChipPin);
Q_DECLARE_METATYPE(ChipPins);
