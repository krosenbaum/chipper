// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include "chipdata.h"
#include "chipdatapriv.h"
#include "chipdetail.h"
#include "chipper.h"
#include "templates.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QPointer>
#include <QTimer>
#include <QSettings>

#include <DomNodeIterator>


// ////////////////////////////////////
// Chip Part Base class

ChipDataPartBase::ChipDataPartBase(ChipData* c, const QDomElement& e)
:mchip(c),melem(e)
{
    if(!mchip.isNull()){
        mreset=QObject::connect(mchip,&ChipData::internalReset,mchip,[this]{resetElement();});
        mpreload=QObject::connect(mchip,&ChipData::preloadSync,mchip,[this]{preloadSync();});
    }
}

ChipDataPartBase::ChipDataPartBase(const ChipDataPartBase&o)
:mchip(o.mchip),melem(o.melem)
{
    if(!mchip.isNull()){
        mreset=QObject::connect(mchip,&ChipData::internalReset,mchip,[this]{resetElement();});
        mpreload=QObject::connect(mchip,&ChipData::preloadSync,mchip,[this]{preloadSync();});
    }
}

ChipDataPartBase & ChipDataPartBase::operator=(const ChipDataPartBase&o)
{
    const bool diff=mchip!=o.mchip;
    if(diff && mreset)
        QObject::disconnect(mreset);
    if(diff && mpreload)
        QObject::disconnect(mpreload);
    mchip=o.mchip;
    melem=o.melem;
    if(diff && !mchip.isNull()){
        mreset=QObject::connect(mchip,&ChipData::internalReset,mchip,[this]{resetElement();});
        mpreload=QObject::connect(mchip,&ChipData::preloadSync,mchip,[this]{preloadSync();});
    }
    return *this;
}

ChipDataPartBase::~ChipDataPartBase()
{
    if(mreset)
        QObject::disconnect(mreset);
    if(mpreload)
        QObject::disconnect(mpreload);
}

QDomDocument ChipDataPartBase::document()const
{
    if(mchip.isNull()){
        return QDomDocument();
    }else return mchip->mxml;
}

bool ChipDataPartBase::isValid() const
{
    return !mchip.isNull() && !melem.isNull();
}

void ChipDataPartBase::altered()
{
    if(!mchip.isNull())mchip->altered();
}

void ChipDataPartBase::resetElement()
{
    melem.clear();
}

void ChipDataPartBase::setElement(const QDomElement&e)
{
    //check that element is part of document
    if(e.ownerDocument()==document())
        melem=e;
    else
        melem.clear();
}

void ChipDataPartBase::preloadSync()
{
}

ChipData * ChipDataPartBase::chip()
{
    return mchip;
}

const ChipData * ChipDataPartBase::chip() const
{
    return mchip;
}

//not allowed, since it is abstract:
//static int metaChipBaseId=qRegisterMetaType<ChipDataPartBase>();






// ////////////////////////////////////
// Chip Meta Data

void ChipMetaData::resetElement()
{
    setElement(findOrCreateElement(document().documentElement(),METAELEMENTNAME));
}

QString ChipMetaData::author() const
{
    return element().attribute("author");
}

QString ChipMetaData::chipType() const
{
    return element().attribute("type");
}

QString ChipMetaData::manufacturer() const
{
    return element().attribute("manufacturer");
}

QString ChipMetaData::copyrightString() const
{
    return element().attribute("copy");
}

QString ChipMetaData::datasheetUrl() const
{
    return element().attribute("datasheet");
}

QString ChipMetaData::license() const
{
    return element().attribute("license");
}

QString ChipMetaData::description() const
{
    return element().attribute("description");
}

QString ChipMetaData::keywords() const
{
    return element().attribute("keywords");
}

QString ChipMetaData::refPrefix() const
{
    return element().attribute("refprefix","U");
}

void ChipMetaData::setAuthor(QString s)
{
    if(s==author())return;
    altered();
    element().setAttribute("author",s);
}

void ChipMetaData::setChipType(QString s)
{
    if(s==chipType())return;
    altered();
    element().setAttribute("type",s);
}

void ChipMetaData::setManufacturer(QString s)
{
    if(s==manufacturer())return;
    altered();
    element().setAttribute("manufacturer",s);
}

void ChipMetaData::setCopyrightString(QString s)
{
    if(s==copyrightString())return;
    altered();
    element().setAttribute("copy",s);
}

void ChipMetaData::setLicense(QString s)
{
    if(s==license())return;
    altered();
    element().setAttribute("license",s);
}

void ChipMetaData::setDatasheetUrl(QString s)
{
    if(s==datasheetUrl())return;
    else altered();
    element().setAttribute("datasheet",s);
}

void ChipMetaData::setDescription(QString s)
{
    if(s==description())return;
    else altered();
    element().setAttribute("description",s);
}

void ChipMetaData::setKeywords(QString s)
{
    if(s==keywords())return;
    else altered();
    element().setAttribute("keywords",s);
}

void ChipMetaData::setRefPrefix(QString s)
{
    if(s==refPrefix())return;
    else altered();
    if(s=="U" || s.isEmpty())
        element().removeAttribute("refprefix");
    else
        element().setAttribute("refprefix",s);
}

static int metaMetaDataId=qRegisterMetaType<ChipMetaData>();





// ////////////////////////////////////
// Chip Generator Target Specs

void ChipTargetFiles::resetElement()
{
    setElement(findOrCreateElement(document().documentElement(),TARGETELEMENTNAME));
}

QString ChipTargetFiles::symbolFile() const
{
    return element().attribute("symbols");
}

void ChipTargetFiles::setSymbolFile(QString s)
{
    if(s!=symbolFile())altered();
    element().setAttribute("symbols",s);
}

QString ChipTargetFiles::footprintFile() const
{
    return element().attribute("footprints");
}

void ChipTargetFiles::setFootprintFile(QString s)
{
    if(s!=footprintFile())altered();
    element().setAttribute("footprints",s);
}

QString ChipTargetFiles::modelFile() const
{
    return element().attribute("models");
}

void ChipTargetFiles::setModelFile(QString s)
{
    if(s!=modelFile())altered();
    element().setAttribute("models",s);
}

static int metaTFilesId=qRegisterMetaType<ChipTargetFiles>();





// ////////////////////////////////////
// Chip Pin Table

void ChipPins::resetElement()
{
    setElement(findOrCreateElement(document().documentElement(),PINSELEMENTNAME));
}

QStringList ChipPins::allNames() const
{
    QStringList r;
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin")
            r.append(n.toElement().attribute("name"));
    return r;
}

QList<ChipPin> ChipPins::allPins()
{
    QList<ChipPin> pins;
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin")
            pins.append(ChipPin(chip(),n.toElement()));
    return pins;
}

int ChipPins::numPins() const
{
    int c=0;
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin")
            c++;
    return c;
}

ChipPin ChipPins::byName(QString nm)
{
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin"){
            QDomElement e=n.toElement();
            if(e.attribute("name")==nm)
                return ChipPin(chip(),e);
        }
    return ChipPin();
}

const ChipPin ChipPins::byName(QString nm) const
{
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin"){
            QDomElement e=n.toElement();
            if(e.attribute("name")==nm)
                return ChipPin(const_cast<ChipData*>(chip()),e);
        }
    return ChipPin();
}

bool ChipPins::hasPinNamed(QString nm) const
{
    for(auto n:element().childNodes())
        if(n.isElement() && n.nodeName()=="Pin" && n.toElement().attribute("name")==nm)
                return true;
    return false;
}

void ChipPin::resetElement()
{
    ChipDataPartBase::resetElement();
    QDomElement par=findElement(document().documentElement(),"Pins");
    auto findPin=[](const QDomElement&par,QString name)->QDomElement{
        for(auto n:par.childNodes()){
            if(!n.isElement())continue;
            QDomElement e=n.toElement();
            if(e.attribute("name")==name){
                return e;
            }
        }
        return QDomElement();
    };
    if(msupername.isEmpty()){
        setElement(findPin(par,mname));
        return;
    }
    auto se=findPin(par,msupername);
    setElement(findPin(se,mname));
}

void ChipPin::preloadSync()
{
    mname=name();
    if(element().tagName()=="Alt")
        msupername=element().parentNode().toElement().attribute("name");
    else
        msupername.clear();
}

bool ChipPin::isPin() const
{
    return element().tagName()=="Pin";
}

bool ChipPin::isAlternate() const
{
    return element().tagName()=="Alt";
}

QString ChipPin::name() const
{
    return element().attribute("name");
}

bool ChipPin::setName(QString n)
{
    if(n==name())return true;
    if(n.isEmpty())return false;
    if(isPin()){
        if(ChipPins(chip(),element().parentNode().toElement()).hasPinNamed(n))return false;
        if(hasAlternate(n))return false;
    }
    if(isAlternate()){
        ChipPin cp(chip(),element().parentNode().toElement());
        if(cp.name()==n)return false;
        if(cp.hasAlternate(n))return false;
    }
    element().setAttribute("name",mname=n);
    altered();
    return true;
}

QStringList ChipPin::alternateNames() const
{
    if(!isPin())return QStringList();
    QStringList r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        r.append(e.attribute("name"));
    }
    return r;
}

bool ChipPin::hasAlternate(QString nm) const
{
    if(!isPin())return false;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.attribute("name")==nm)return true;
    }
    return false;
}

ChipPin ChipPin::alternate(QString nm) const
{
    if(!isPin())return ChipPin();
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.attribute("name")==nm)return ChipPin(const_cast<ChipData*>(chip()),e);
    }
    return ChipPin();
}

bool ChipPin::addAlternate(QString nm, ChipPin::Function fc, ChipPin::Line ln)
{
    if(!isPin())return false;
    //check name
    if(name()==nm || hasAlternate(nm))return false;
    //create element
    auto e=document().createElement("Alt");
    e.setAttribute("name",nm);
    e.setAttribute("func",functionToXml(fc));
    e.setAttribute("line",lineToXml(ln));
    auto n=element().appendChild(e);
    return !n.isNull();
}

bool ChipPin::removeAlternate(QString nm)
{
    if(!isPin())return false;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        if(e.attribute("name")==nm){
            altered();
            return !element().removeChild(e).isNull();
        }
    }
    return false;
}

QList<ChipPin> ChipPin::allAlternates() const
{
    QList<ChipPin>r;
    if(!isPin())return r;
    for(auto n:element().childNodes()){
        if(!n.isElement())continue;
        auto e=n.toElement();
        r<<ChipPin(const_cast<ChipData*>(chip()),e);
    }
    return r;
}

QVariantList Chipper::Chip::ChipPin::allAlternatesVariant() const
{
    QVariantList r;
    for(auto p:allAlternates())r.append(QVariant::fromValue(p));
    return r;
}

ChipPin::Function ChipPin::xmlToFunction(QString f)
{
    if(f=="ground" || f=="gnd") return ChipPin::Function::Ground;
    if(f=="groundin")           return ChipPin::Function::GroundIn;
    if(f=="groundout")          return ChipPin::Function::GroundOut;
    if(f=="power" || f=="pwr")  return ChipPin::Function::Power;
    if(f=="powerin")            return ChipPin::Function::PowerIn;
    if(f=="powerout")           return ChipPin::Function::PowerOut;
    if(f=="signal")             return ChipPin::Function::Signal;
    if(f=="signalin")           return ChipPin::Function::SignalIn;
    if(f=="signalout")          return ChipPin::Function::SignalOut;
    if(f=="passive")            return ChipPin::Function::Passive;
    if(f=="opcol")              return ChipPin::Function::OpenCollector;
    if(f=="opem")               return ChipPin::Function::OpenEmitter;
    if(f=="tri")                return ChipPin::Function::TriState;
    if(f=="nc")                 return ChipPin::Function::NoConnect;
    if(f=="free")               return ChipPin::Function::Free;
    if(f=="unspec")             return ChipPin::Function::Unspecified;
    //not found
    throw XmlValueError(f);
}

QList<ChipPin::Function> ChipPin::stringToFunctionList(QString text)
{
    QList<ChipPin::Function> r;
    for(const auto&s:text.split(' ',Qt::SkipEmptyParts))
        try{r.append(xmlToFunction(s));}
        catch(...){qDebug()<<"Warning: invalid function value"<<s;}
    return r;
}

QString ChipPin::functionToXml(ChipPin::Function f)
{
    switch(f){
        case Function::Power:        return "power";     break;
        case Function::PowerIn:      return "powerin";   break;
        case Function::PowerOut:     return "powerout";  break;
        case Function::Ground:       return "ground";    break;
        case Function::GroundIn:     return "groundin";  break;
        case Function::GroundOut:    return "groundout"; break;
        case Function::Signal:       return "signal";    break;
        case Function::SignalIn:     return "signalin";  break;
        case Function::SignalOut:    return "signalout"; break;
        case Function::Passive:      return "passive";   break;
        case Function::OpenCollector:return "opcol";     break;
        case Function::OpenEmitter:  return "opem";      break;
        case Function::TriState:     return "tri";       break;
        case Function::NoConnect:    return "nc";        break;
        case Function::Free:         return "free";      break;
        case Function::Unspecified:  return "unspec";    break;
    }
    //not found
    throw XmlValueError((int)f);
}

ChipPin::Function ChipPin::function() const
{
    const QString f=element().attribute("func","signal").toLower().trimmed();
    try{return xmlToFunction(f);}
    catch(XmlValueError ve){
        qDebug()<<"oops. Encountered unknown pin type"<<f<<"in"<<chip()->fileName()<<"line"<<element().lineNumber()<<"column"<<element().columnNumber();
        return ChipPin::Function::Signal;
    }
}

void ChipPin::setFunction(ChipPin::Function f)
{
    QString fs;
    try{fs=functionToXml(f);}
    catch(XmlValueError ve){
        qDebug()<<"Ooops! Unhandled pin type"<<(int)f;
        fs="signal";
    }
    altered();
    element().setAttribute("func",fs);
}

QString ChipPin::functionToString(ChipPin::Function f)
{
    switch(f){
        case Function::Ground:          return tr("Ground, Passive");
        case Function::GroundIn:        return tr("Ground, Input");
        case Function::GroundOut:       return tr("Ground, Output");
        case Function::Power:           return tr("Power, Passive");
        case Function::PowerIn:         return tr("Power, Input");
        case Function::PowerOut:        return tr("Power, Output");
        case Function::Signal:          return tr("Signal, Bidirectional");
        case Function::SignalIn:        return tr("Signal, Input");
        case Function::SignalOut:       return tr("Signal, Output");
        case Function::Passive:         return tr("Passive");
        case Function::OpenCollector:   return tr("Open Collector Output");
        case Function::OpenEmitter:     return tr("Open Emitter Output");
        case Function::TriState:        return tr("Tri-State Output");
        case Function::NoConnect:       return tr("Not Connected");
        case Function::Free:            return tr("Free Style");
        case Function::Unspecified:     return tr("Unspecified");
        default:                        return tr("Unknown Function %1").arg((int)f);
    }
}

QString ChipPin::functionToKiCad(ChipPin::Function f, bool isHidden)
{
    static const QString passive    ="passive";
    static const QString pwr_in     ="power_in";
    static const QString pwr_out    ="power_out";
    static const QString bidir      ="bidirectional";
    static const QString free       ="free";
    static const QString input      ="input";
    static const QString noconn     ="no_connect";
    static const QString opencol    ="open_collector";
    static const QString openem     ="open_emitter";
    static const QString output     ="output";
    static const QString tristate   ="tri_state";
    static const QString unspec     ="unspecified";
    switch(f){
        case Function::Ground:
        case Function::Power:
        case Function::Passive:
            return passive;
        case Function::GroundIn:
        case Function::PowerIn:
            if(isHidden)return passive;
            else return pwr_in;
        case Function::GroundOut:
        case Function::PowerOut:
            if(isHidden)return passive;
            else return pwr_out;
        case Function::Signal:
            return bidir;
        case Function::SignalIn:
            return input;
        case Function::SignalOut:
            if(isHidden)return passive;
            else return output;
        case Function::OpenCollector:
            return opencol;
        case Function::OpenEmitter:
            return openem;
        case Function::TriState:
            return tristate;
        case Function::NoConnect:
            return noconn;
        case Function::Free:
            return free;
        case Function::Unspecified:
            return unspec;
        default:
            qDebug()<<"Warning: unknown Function"<<(int)f<<"KiCadding it to 'passive'";
            return passive;
    }
}

QList<QPair<ChipPin::Function, QString>> ChipPin::allFunctions()
{
    static QList<QPair<ChipPin::Function, QString>>r;
    if(r.size()==0)
    for(int i=0;i<=(int)Function::Unspecified;i++)
        r.append(QPair<ChipPin::Function, QString>((Function)i,functionToString((Function)i)));
    return r;
}

//note: we use exactly the KiCad type in XML
ChipPin::Line ChipPin::xmlToLine(QString lt)
{
    if(lt=="line")              return Line::Line;
    if(lt=="inverted")          return Line::Inverted;
    if(lt=="input_low")         return Line::InputLow;
    if(lt=="output_low")        return Line::OutputLow;
    if(lt=="clock")             return Line::Clock;
    if(lt=="clock_low")         return Line::ClockLow;
    if(lt=="edge_clock_high")   return Line::EdgeClockHigh;
    if(lt=="inverted_clock")    return Line::InvertedClock;
    if(lt=="non_logic")         return Line::NonLogic;
    //not known!
    throw XmlValueError(lt);
}

//note: we use exactly the KiCad type in XML
QString ChipPin::lineToXml(ChipPin::Line l)
{
    switch(l){
        case Line::Line:         return "line";
        case Line::Inverted:     return "inverted";
        case Line::InputLow:     return "input_low";
        case Line::OutputLow:    return "output_low";
        case Line::Clock:        return "clock";
        case Line::ClockLow:     return "clock_low";
        case Line::EdgeClockHigh:return "edge_clock_high";
        case Line::InvertedClock:return "inverted_clock";
        case Line::NonLogic:     return "non_logic";
    }
    //not found
    throw XmlValueError((int)l);
}

ChipPin::Line ChipPin::line() const
{
    const QString lt=element().attribute("line","line");
    try{return xmlToLine(lt);}
    catch(XmlValueError ve){
        qDebug()<<"Warning: unknown line type"<<lt<<"in"<<chip()->fileName()<<"line"<<element().lineNumber()<<"column"<<element().columnNumber();
        return Line::Line;
    }
}

void ChipPin::setLine(ChipPin::Line l)
{
    if(l!=line()){
        altered();
        element().setAttribute("line",lineToXml(l));
    }
}

QString ChipPin::lineToString(ChipPin::Line l)
{
    switch(l){
        case Line::Line:         return tr("Simple Line");
        case Line::Inverted:     return tr("Inverted Signal");
        case Line::InputLow:     return tr("Input Active Low");
        case Line::OutputLow:    return tr("Output Active Low");
        case Line::Clock:        return tr("Clock");
        case Line::ClockLow:     return tr("Clock on Low");
        case Line::EdgeClockHigh:return tr("Clock on Edge High");
        case Line::InvertedClock:return tr("Inverted Clock");
        case Line::NonLogic:     return tr("Non-Logic, Not Connected");

        default:                 return tr("Unknown Line Type %1").arg((int)l);
    }
}

QList<QPair<ChipPin::Line, QString>> ChipPin::allLineTypes()
{
    static QList<QPair<ChipPin::Line, QString>> r;
    if(r.size()==0)
    for(int i=0;i<=(int)Line::NonLogic;i++)
        r.append(QPair<Line, QString>((Line)i,lineToString((Line)i)));
    return r;
}

QString ChipPin::lineToKiCad(ChipPin::Line l)
{
    try{return lineToXml(l);}
    catch(XmlValueError ve){
        qDebug()<<"Warning: unhandled line type"<<(int)l<<"...translating to simple line.";
        return "line";
    }
}

ChipPin ChipPins::addPin(QString n)
{
    if(hasPinNamed(n))return ChipPin();
    //create pin and append
    QDomElement npin=document().createElement("Pin");
    npin.setAttribute("name",n);
    npin=element().appendChild(npin).toElement();
    altered();
    //done
    if(npin.isNull())
        return ChipPin();
    else
        return ChipPin(chip(),npin);
}

bool ChipPins::deletePin(ChipPin p)
{
    //validity checks
    if(!p.isValid())return false;
    if(p.document()!=document())return false;
    //delete
    element().removeChild(p.element());
    altered();
    return true;
}

bool ChipPins::movePin(ChipPin moveThis, ChipPin afterThis)
{
    //validity check
    if(!moveThis.isValid())return false;
    if(moveThis.document()!=document())return false;
    //afterThis is null: at beginning
    if(!afterThis.isValid()){
        QDomNode before=element().firstChildElement();
        element().insertBefore(moveThis.element(),before);
        altered();
        return true;
    }
    //sanity check
    if(afterThis.document()!=document())return false;
    //insert behind
    element().insertAfter(moveThis.element(),afterThis.element());
    altered();
    return true;
}

static int metaPinId=qRegisterMetaType<ChipPin>();
static int metaPinsId=qRegisterMetaType<ChipPins>();


#include "moc_chipdetail.cpp"
