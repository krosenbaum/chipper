// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Data Detail Structures
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QMetaObject>
#include <QObject>
#include <QPointer>

#include "chipdetail.h"

#ifndef CHIPPER_EXPORT
#define CHIPPER_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Template {
class TemplateData;
}

namespace Chip {

class ChipPackage;
class ChipFileTarget;
class ChipVariables;

///represents a single physical pin of a variant
class CHIPPER_EXPORT ChipVariantPin
{
    Q_GADGET
public:
    ///physical pin type
    enum class PinType{
        ///pin is internally connected and visible in the symbol
        Connected,
        ///pin is not connected, but visible
        NotConnected,
        ///pin is physically missing
        Missing,
        ///pin is a short stub that does not reach the PCB
        Stub
    };

    ///empty pin
    ChipVariantPin(){}
    ///creates a new pin instance
    ChipVariantPin(QString id,PinType t=PinType::NotConnected,QString n=QString()):mtype(t),mname(n),mid(id){}
    ///copies a pin
    ChipVariantPin(const ChipVariantPin&)=default;
    ///copies a pin
    ChipVariantPin& operator=(const ChipVariantPin&)=default;

    ///true if this is a valid pin
    bool isValid()const{return !mid.isEmpty() && !mname.isEmpty();}

    ///returns the ID/number of the pin
    QString id()const{return mid;}
    ///returns the name of the pin
    QString name()const{return mname;}
    ///returns the type of the pin
    PinType type()const{return mtype;}
    ///returns the type of the pin as a human readable string
    QString typeStr()const;
    ///sets the ID/number of the pin
    void setId(QString i){mid=i;}
    ///sets the ID to a numeric value
    void setNumber(int i){mid=QString::number(i);}
    ///sets the name of the pin
    void setName(QString n){mname=n;}
    ///sets the type of the pin
    void setType(PinType t){mtype=t;}

    ///true if this a standard connected pin
    bool isConnected()const{return mtype==PinType::Connected;}
    ///true if this is a NC pin
    bool isNotConnected()const{return mtype==PinType::NotConnected;}
    ///true if this is a missing pin
    bool isMissing()const{return mtype==PinType::Missing;}
    ///true if this is a stubby pin
    bool isStub()const{return mtype==PinType::Stub;}

    Q_PROPERTY(QString name READ name WRITE setName);
    Q_PROPERTY(PinType type READ type WRITE setType);
    Q_PROPERTY(QString typeStr READ typeStr);
    Q_PROPERTY(QString id READ id WRITE setId);
    Q_PROPERTY(bool isValid READ isValid);
private:
    PinType mtype=PinType::Connected;
    QString mname,mid;
};

///represents a variant of a chip
class CHIPPER_EXPORT ChipVariant:public ChipDataPartBase
{
    Q_GADGET
    ///\internal used during reload to find the correct element again
    QString muid;

    Q_DECLARE_TR_FUNCTIONS(ChipVariant)
protected:
    //access to the chip file for instantiation
    friend class ChipData;
    friend class ChipFileTarget;
    friend class ChipPackage;
    friend class ChipVariables;
    ///used by the chip file object to instantiate the variant
    ChipVariant(ChipData*c,const QDomElement&e):ChipDataPartBase(c,e){}
    ///called after reload to find the variant again and keep the object intact
    void resetElement()override;
    ///called before reload to remember UUID
    void preloadSync()override;
public:
    ///instantiates an invalid chip variant
    ChipVariant()=default;
    ///instantiates another reference to the same variant
    ChipVariant(const ChipVariant&)=default;

    ///makes this object a reference to the same chip variant
    ChipVariant& operator=(const ChipVariant&)=default;

    ///returns the unique ID of the chip variant (used internally only)
    QString uid()const;

    ///returns the human readable name of the variant
    QString name()const;

    ///sets the human readable name of the variant
    void setName(QString);

    ///returns the configured number of pins (does not necessarily correspond to actual number of pins)
    int numPins()const;

    ///changes the (nominal) number of pins (has no influence on actual pins (see pin and setPin)
    void setNumPins(int);

    Q_PROPERTY(QString uid READ uid);
    Q_PROPERTY(QString name READ name);
    Q_PROPERTY(int numPins READ numPins);

    ///returns human readable name of pin type
    static QString pinTypeToString(ChipVariantPin::PinType);
    ///returns pairs of pin types and human readable strings for them
    static QList<QPair<ChipVariantPin::PinType,QString>>allPinTypes();

    ///returns all Pin IDs/numbers
    QStringList pinIds()const;
    ///returns pin by ID
    ChipVariantPin pin(QString)const;
    ///modifies a pin by ID
    bool setPin(const ChipVariantPin&pin);
    ///modifies a pin by ID
    bool setPin(QString id,ChipVariantPin::PinType type,QString name);
    ///adds a pin
    ///\param pin the pin data to be added
    ///\param before if non-empty: adds the new pin before the given old pin; if empty or unknown: appends the pin to the end
    bool addPin(const ChipVariantPin&pin,QString before=QString());
    ///deletes a pin by ID
    void removePin(QString id);
    ///rename a pin (give it a new ID/number), true on success
    bool renumberPin(QString oldid,QString newid);
    ///swaps two pins
    bool swapPins(QString p1,QString p2);

    ///helper for pin filter: convert string to list of pin types
    static QList<ChipVariantPin::PinType> stringToPinTypeList(QString);

    ///convenience for formula: return all pins as list of variant (appears as array in script)
    QList<QVariant> pinVarList()const;
    Q_PROPERTY(QList<QVariant> pins READ pinVarList);
    Q_PROPERTY(QStringList pinIds READ pinIds);

    ///returns UIDs of all ChipPackages in this variant
    QStringList packageIds()const;

    ///returns a ChipPackage by UID
    ChipPackage packageById(QString id)const;

    ///true if a package with that ID exists
    bool hasPackageId(QString id)const;

    ///creates a new package, returns a reference to the package
    ///\param tmp the template for which to create the package
    ///\param id uses this UID, if not a valid UUID generates a fresh one
    ChipPackage newPackage(const Template::TemplateData&tmp,QString id=QString());

    ///deletes a package, returns true on success
    bool deletePackage(QString id);

    QMap<QString,QVariant> packageMap()const;
    Q_PROPERTY(QStringList packageIds READ packageIds);
    Q_PROPERTY(QMap<QString,QVariant> packages READ packageMap);
};

//end of namespace
}}
using namespace Chipper::Chip;

Q_DECLARE_METATYPE(ChipVariantPin);
Q_DECLARE_METATYPE(ChipVariant);
