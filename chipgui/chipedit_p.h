// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget - helpers
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QTabWidget>
#include <QPointer>
#include <QDialog>
#include <QUuid>

#include "chipdetail.h"
#include "chipdata.h"
#include "chipvariant.h"

class QLineEdit;
class QTableView;
class QStandardItemModel;
class QTreeView;
class QTextEdit;
class QComboBox;

namespace Chipper {
namespace GUI {

///used by ChipEditor to edit or create a chip level pin; the dialog saves the pin data and returns the updated pin object
class ChipPinDialog:public QDialog
{
    Q_OBJECT
public:
    ///purpose of this dialog instance
    enum class Mode {
        ///the dialog edits an existing pin or alternate function of a pin
        Edit,
        ///the dialog creates a new pin
        NewPin,
        ///the dialog creates a new alternate function for an existing pin
        NewAlt
    };
    ///creates a new dialog for editing a pin
    ///\param parent the editor for which it works, must not be null
    ///\param pin when editing: the pin or alternate function to be edited; when creating a new alternate: the parent pin; when creating a new pin: unused
    ///\param md the editor mode, see Mode enum type
    ChipPinDialog(ChipEditor*parent,ChipPin pin,Mode md=Mode::Edit);

    ///returns the name of the pin before it was edited (used for the update signal)
    QString oldName()const{return moldname;}
    ///returns the pin after editing/creating it
    ChipPin pin()const{return mpin;}

private slots:
    ///saves the pin data
    void save();

private:
    ChipPin mpin,mppin;
    ChipData *mchip;
    QString moldname;
    QLineEdit*mname;
    QComboBox*mtype,*mline;
    Mode mmode;
};

///offers to create a new variant, asks for some basic data
class ChipAddVariantDialog:public QDialog
{
    Q_OBJECT

    ChipData*mchip;
    ChipVariant mvar;
    QUuid muid;
    QLineEdit *mname;
    QSpinBox *mpins;
public:
    ///creates the dialog
    ///\param parent the calling editor, must not be null
    ///\param chip the chip for which to create a variant, must not be null
    ChipAddVariantDialog(ChipEditor*parent,ChipData*chip);

    ///returns the newly created (and added) variant
    ChipVariant variant(){return mvar;}
private slots:
    ///creates and adds the variant to the chip
    void create();
};

//end of namespace
}}
