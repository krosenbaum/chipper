// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Variant Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTimer>
#include <QTreeView>

#include "chipdata.h"
#include "chipdetail.h"
#include "chipedit.h"
#include "chipper.h"
#include "chipvariantedit.h"
#include "chippackageedit.h"
#include "chipfileedit.h"
#include "filepinedit.h"
#include "variableedit.h"
#include "vlabel.h"
#include "templates.h"




// ////////////////////////////////////
// Generic File Editor Base

FileEditPage::FileEditPage(const ChipFileTarget&target, QWidget* parent)
:PageWidget(parent)
{
    setLayout(mlayout=new QFormLayout);
    mlayout->addRow(mlabel=new QLabel("<html>?"));
    QTimer::singleShot(0,this,&FileEditPage::updateTitle);
    //show UID
    mlayout->addRow(tr("UID:"),new QLabel(target.uid()));
    //editable target name
    mlayout->addRow(tr("Name:"),mname=new QLineEdit(target.name()));
    connect(mname,&QLineEdit::textChanged,this,[this]{this->target().setName(mname->text());updateTitle();});
    //populate template link
    auto temp=target.package().templateRef();
    QString tname;
    switch(target.fileType()){
        case ChipFileTarget::FileType::Symbol:
            tname=temp.symbol(target.templateId()).name();
            break;
        case ChipFileTarget::FileType::Footprint:
            tname=temp.footprint(target.templateId()).name();
            break;
        case ChipFileTarget::FileType::Model3D:
            tname=temp.model(target.templateId()).name();
            break;
        case ChipFileTarget::FileType::Invalid:
            qDebug()<<"Arrg. Cannot display invalid target file type.";
            tname="INVALID!";
            break;
    }
    QString temptext=QString("%1 (%2)")
        .arg(target.templateId())
        .arg(tname);
    mlayout->addRow(tr("Template:"),new QLabel(temptext));
}

void FileEditPage::updateTitle()
{
    //update own label
    auto&tgt=target();
    mlabel->setText(tr("<html><b>%1: %2</b>","Symbol/Footprint/Model: Name").arg(tgt.fileTypeName()).arg(tgt.name()));
    //update parent tree
    emit titleChanged(tgt.name());
}

QString FileEditPage::pageTitle() const
{
    return target().name();
}

PageWidget * FileEditPage::createPinPage()
{
    return new FilePinEditPage(this);
}

PageWidget * FileEditPage::createVariablePage()
{
    if(target().variables().templateVariables().hasVisibleVariables())
        return new VariableEditPage(target().variables(),this);
    else
        return nullptr;
}

QList<PageWidget *> FileEditPage::createSubPages()
{
    QList<PageWidget*>ret;
    PageWidget*w;
    w=createPinPage();
    if(w)ret.append(w);
    w=createVariablePage();
    if(w)ret.append(w);
    return ret;
}



// ////////////////////////////////////
// Symbol Editor

SymbolEditPage::SymbolEditPage(const ChipSymbol&sym, QWidget* parent)
:FileEditPage(sym,parent),msymbol(sym)
{
    QFormLayout*fl=mainLayout();
    fl->addRow(tr("Default Footprint:"),mdfprint=new QLineEdit(msymbol.defaultFootprint()));//TODO: add select button
    connect(mdfprint,&QLineEdit::textChanged,this,[this]{msymbol.setDefaultFootprint(mdfprint->text());});
    fl->addRow(tr("Default Value:"),mdvalue=new QLineEdit(msymbol.defaultValue()));
    mdvalue->setPlaceholderText(msymbol.name());
    connect(mdvalue,&QLineEdit::textChanged,this,[this]{msymbol.setDefaultValue(mdvalue->text());});
    connect(this,&SymbolEditPage::titleChanged,this,[this]{mdvalue->setPlaceholderText(msymbol.name());});
}

QString SymbolEditPage::pageName() const
{
    return symbolPageString+" "+msymbol.uid();
}




// ////////////////////////////////////
// Footprint Editor

FootprintEditPage::FootprintEditPage(const ChipFootprint&fp, QWidget* parent)
:FileEditPage(fp,parent),mfprint(fp)
{
    QFormLayout*fl=mainLayout();
    fl->addRow(tr("Default 3D Model:"),mdmodel=new QLineEdit(mfprint.defaultModel()));//TODO: add select button
    connect(mdmodel,&QLineEdit::textChanged,this,[this]{mfprint.setDefaultModel(mdmodel->text());});
}

QString FootprintEditPage::pageName() const
{
    return footprintPageString+" "+mfprint.uid();
}



// ////////////////////////////////////
// Model Editor

ModelEditPage::ModelEditPage(const ChipModel&mdl, QWidget* parent)
:FileEditPage(mdl,parent),m3d(mdl)
{
    //bottom of page intentionally left blank ;-)
}

QString ModelEditPage::pageName() const
{
    return modelPageString+" "+m3d.uid();
}





#include "moc_chipfileedit.cpp"
