// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>
#include <QUuid>
#include <QStyledItemDelegate>

#include "chipdetail.h"
#include "chipvariant.h"
#include "chippackage.h"

class QComboBox;
class QLabel;
class QLineEdit;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {
class ChipVariantEditor;

///simple dialog to add a package to a variant
class AddPackageDialog:public QDialog
{
    Q_OBJECT
    ChipVariant mvar;
    ChipPackage mpack;
    QUuid muid;
    QComboBox *mtemp;
    QLineEdit *mname;
public:
    ///creates the dialog window
    AddPackageDialog(ChipVariantEditor*parent,ChipVariant v);

    ///returns the resulting package after accepting the dialog
    ChipPackage package(){return mpack;}

private slots:
    ///creates the package and makes sure package() returns a result
    void create();
};

///handles editing the pin table
class VariantPinDelegate:public QStyledItemDelegate
{
    Q_OBJECT
    ChipVariantEditor*mparent;
    QAbstractItemModel*mmodel;
    mutable ChipVariant mvar;
public:
    VariantPinDelegate(ChipVariantEditor*parent,QAbstractItemModel*model,const ChipVariant&v):QStyledItemDelegate(parent),mparent(parent),mmodel(model),mvar(v){}
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
signals:
    void pinStored(QString oname,QString nname)const;
};

//end of namespace
}}
