// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Variant Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTreeView>

#include "chipdata.h"
#include "chipedit.h"
#include "chipper.h"
#include "chipvariantedit.h"
#include "chipvariantedit_p.h"
#include "chippackageedit.h"
#include "vlabel.h"
#include "templates.h"
#include "templatepool.h"

#include <algorithm>



// ////////////////////////////////////
// Chip Variant Editor

#define PINCOL_NUM  0
#define PINCOL_TYPE 1
#define PINCOL_NAME 2
#define PINCOL_STAT 3

ChipVariantEditor::ChipVariantEditor(ChipEditor* parent, QString variant)
:QWidget(parent),mparent(parent),mchip(parent->chip()),mvariant(mchip->variantById(variant))
{
    QHBoxLayout*hl;
    setLayout(hl=new QHBoxLayout);
    hl->setContentsMargins(0,0,0,0);
    VLabel*l;
    hl->addWidget(l=new VLabel(windowTitle()),0);
    connect(this,&QWidget::windowTitleChanged,l,&VLabel::setText);
    l->scaleFontSize(2.5);
    l->setFrameShape(QFrame::Box);
    l->setFrameShadow(QFrame::Sunken);
    hl->addWidget(mtab=new QTabWidget,1);
    mtab->setTabPosition(QTabWidget::West);
    mtab->addTab(createMainTab(),tr("Variant Basics"));
    for(const auto&id:mvariant.packageIds())
        openPackageTab(id);

    connect(this,&ChipVariantEditor::showTemplate,parent,&ChipEditor::showTemplate);
    connect(mtab,&QTabWidget::currentChanged,parent,&ChipEditor::tabChanged);
    connect(parent,&ChipEditor::pinChanged,this,&ChipVariantEditor::chipPinChanged);
    connect(parent,&ChipEditor::propertyChanged,this,&ChipVariantEditor::chipPropertyChanged);

    updateTitle();
}

void ChipVariantEditor::updateTitle()
{
    setWindowTitle(tr("Variant: %1").arg(mvariant.name()));
}

QWidget * ChipVariantEditor::createMainTab()
{
    QWidget *w=new QWidget;
    QFormLayout*fl;
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("<html><h1>Main Variant Data</h1>")),0);
    vl->addSpacing(10);
    vl->addLayout(fl=new QFormLayout,0);

    fl->addRow(tr("Variant ID:"),new QLabel(mvariant.uid()));
    fl->addRow(tr("Variant Name:"),mname=new QLineEdit(mvariant.name()));
    connect(mname,&QLineEdit::textChanged,this,[this]{mvariant.setName(mname->text());updateTitle();emit propertyChanged();});
    fl->addRow(tr("Number of Pins:"),mnumpin=new QSpinBox);
    mnumpin->setRange(1,9999);
    const int nump=mvariant.numPins();
    mnumpin->setValue(nump);
    connect(mnumpin,qOverload<int>(&QSpinBox::valueChanged),this,[this]{
        mvariant.setNumPins(mnumpin->value());
        updateStatus();
        emit propertyChanged();
    });

    vl->addSpacing(10);
    vl->addWidget(new QLabel(tr("Pins:")),0);
    vl->addLayout(hl=new QHBoxLayout,1);
    hl->addWidget(mpintab=new QTableView,1);
    mpintab->setModel(mpinmodel=new QStandardItemModel(this));
    refreshPins();
    auto*delg=new VariantPinDelegate(this,mpinmodel,mvariant);
    mpintab->setItemDelegate(delg);
    connect(delg,&VariantPinDelegate::pinStored,this,&ChipVariantEditor::updateStatus);
    connect(delg,&VariantPinDelegate::pinStored,this,&ChipVariantEditor::pinNumberChanged);

    hl->addLayout(vl=new QVBoxLayout,0);
    QPushButton*pb;
    vl->addWidget(pb=new QPushButton(tr("&Remove Pin")),0);
    connect(pb,&QPushButton::clicked,this,&ChipVariantEditor::removePin);
    vl->addSpacing(10);
    vl->addWidget(pb=new QPushButton(tr("Move &Up")),0);
    connect(pb,&QPushButton::clicked,this,[this]{movePin(true);});
    vl->addWidget(pb=new QPushButton(tr("Move &Down")),0);
    connect(pb,&QPushButton::clicked,this,[this]{movePin(false);});
    vl->addSpacing(10);
    vl->addWidget(pb=new QPushButton(tr("Re&fresh")),0);
    connect(pb,&QPushButton::clicked,this,&ChipVariantEditor::refreshPins);
    vl->addStretch(1);

    return w;
}

ChipVariantEditor::~ChipVariantEditor()
{
    //nothing to do, we sync live
}

ChipData * ChipVariantEditor::chip() const
{
    return mchip;
}

void ChipVariantEditor::chipPinChanged(QString oldn, QString newn)
{
    //new pin or name unchanged: nothing to be done
    if(oldn.isEmpty() || oldn==newn)return;
    //new name or deletion of pin:
    QStringList nums;
    //correct all references to pin
    for(int r=0;r<mpinmodel->rowCount();r++){
        //only act on connected pins
        if(mpinmodel->data(mpinmodel->index(r,PINCOL_TYPE),Qt::UserRole).toInt()!=(int)ChipVariantPin::PinType::Connected)
            continue;
        //compare name
        const auto idx=mpinmodel->index(r,PINCOL_NAME);
        if(mpinmodel->data(idx).toString()!=oldn)continue;
        //correct model
        mpinmodel->setData(idx,newn);
        //correct actual data
        const QString num=mpinmodel->data(mpinmodel->index(r,PINCOL_NUM)).toString();
        nums<<num;
        ChipVariantPin pin=mvariant.pin(num);
        pin.setName(newn);
        mvariant.setPin(pin);
    }
    updateStatus();
    //relay the update
    for(const auto &num:nums)emit pinNumberChanged(num,num);
    //pin was deleted - alert user
    if(newn.isEmpty() && nums.size()>0){
        QMessageBox::warning(this,tr("Warning"),
                                tr("The variant %1 had references to pin name %2 on pin number: %3","may need plural for 'number'",nums.size())
                                .arg(mvariant.name())
                                .arg(oldn)
                                .arg(nums.join(tr(", ","list of numbers")))
                        );
    }
    //pin was just renamed: remain silent
}

void ChipVariantEditor::refreshPins()
{
    mpinmodel->clear();
    if(mpinmodel->columnCount()<4){
        mpinmodel->insertColumns(0,4);
        mpinmodel->setHorizontalHeaderLabels(QStringList()
            <<tr("Number","pin number header")
            <<tr("Type","pin type header")
            <<tr("Name","pin name header")
            <<tr("Status","pin status header")
        );
    }
    QStringList pids=mvariant.pinIds();
    mpinmodel->insertRows(0,pids.size());
    mpintab->verticalHeader()->hide();
    for(int i=0;i<pids.size();i++){
        auto p=mvariant.pin(pids[i]);
        mpinmodel->setData(mpinmodel->index(i,PINCOL_NUM),pids[i]);
        mpinmodel->setData(mpinmodel->index(i,PINCOL_TYPE),ChipVariant::pinTypeToString(p.type()));
        mpinmodel->setData(mpinmodel->index(i,PINCOL_TYPE),int(p.type()),Qt::UserRole);
        mpinmodel->setData(mpinmodel->index(i,PINCOL_NAME),p.name());
        mpinmodel->setData(mpinmodel->index(i,PINCOL_STAT),"????");
    }
    updateStatus();
    mpintab->resizeColumnsToContents();

}

void ChipVariantEditor::updateStatus()
{
    //check number of lines
    const int maxlines=mnumpin->value();
    int rc=mpinmodel->rowCount();
    if(maxlines>rc){
        mpinmodel->insertRows(rc,maxlines-rc);
    }else if(maxlines<rc){
        //delete empty lines from back
        for(int r=rc-1;r>=0;r--){
            if(mpinmodel->data(mpinmodel->index(r,PINCOL_NUM)).toString().isEmpty()){
                mpinmodel->removeRow(r);
                if(mpinmodel->rowCount()<=maxlines)break;
            }
        }
    }
    //check rows
    QStringList nums;
    for(int r=0;r<mpinmodel->rowCount();r++){
        //get data
        const QString num=mpinmodel->data(mpinmodel->index(r,PINCOL_NUM)).toString();
        const QString name=mpinmodel->data(mpinmodel->index(r,PINCOL_NAME)).toString();
        const auto type=ChipVariantPin::PinType(mpinmodel->data(mpinmodel->index(r,PINCOL_TYPE),Qt::UserRole).toInt());
        //check rules
        auto idx=mpinmodel->index(r,PINCOL_STAT);
        if(r>=maxlines){
            mpinmodel->setData(idx,tr("too many pins!","pin state"));
            mpinmodel->setData(idx,QColor(Qt::red),Qt::BackgroundRole);
            continue;
        }
        if(num.isEmpty()){
            mpinmodel->setData(idx,tr("no number!","pin state"));
            mpinmodel->setData(idx,QColor(Qt::red),Qt::BackgroundRole);
            continue;
        }
        if(nums.contains(num)){
            mpinmodel->setData(idx,tr("duplicate number!","pin state"));
            mpinmodel->setData(idx,QColor(Qt::red),Qt::BackgroundRole);
            continue;
        }
        nums<<num;
        if(type==ChipVariantPin::PinType::Connected){
            if(name.isEmpty()){
                mpinmodel->setData(idx,tr("select pin!","pin state"));
                mpinmodel->setData(idx,QColor(Qt::red),Qt::BackgroundRole);
            }else{
                mpinmodel->setData(idx,tr("okay","pin state"));
                mpinmodel->setData(idx,QVariant(),Qt::BackgroundRole);
            }
            continue;
        }
        //no more rules
        mpinmodel->setData(idx,tr("okay","pin state"));
        mpinmodel->setData(idx,QVariant(),Qt::BackgroundRole);
    }
}


QWidget *VariantPinDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    if(index.column()==PINCOL_NUM)return new QLineEdit(parent);
    if(index.column()==PINCOL_NAME){
        if(mmodel->data(mmodel->index(index.row(),PINCOL_TYPE),Qt::UserRole).toInt()==(int)ChipVariantPin::PinType::Connected){
            auto*w=new QComboBox(parent);
            for(auto p:mvar.chip()->pins().allPins())
                w->addItem(p.name());
            return w;
        }else
            return nullptr;
    }
    if(index.column()==PINCOL_TYPE){
        auto*w=new QComboBox(parent);
        for(auto pt:ChipVariant::allPinTypes())
            w->addItem(pt.second,(int)pt.first);
        return w;
    }
    //anything else is readonly
    return nullptr;
}
void VariantPinDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(index.column()==PINCOL_NUM){
        qobject_cast<QLineEdit*>(editor)->setText(mmodel->data(index).toString());
        return;
    }
    if(index.column()==PINCOL_NAME){
        auto*w=qobject_cast<QComboBox*>(editor);
        if(w==nullptr)return;
        const auto name=mmodel->data(index).toString();
        if(!name.isEmpty())
            for(int i=0;i<w->count();i++)
                if(w->itemText(i)==name){
                    w->setCurrentIndex(i);
                    return;
                }
        return;
    }
    if(index.column()==PINCOL_TYPE){
        auto*w=qobject_cast<QComboBox*>(editor);
        if(w==nullptr)return;
        const auto tp=mmodel->data(index,Qt::UserRole).toInt();
        for(int i=0;i<w->count();i++)
            if(w->itemData(i).toInt()==tp){
                w->setCurrentIndex(i);
                return;
            }
        return;
    }
}
void VariantPinDelegate::setModelData(QWidget *editor, QAbstractItemModel *, const QModelIndex &index) const
{
    if(editor==nullptr)return;
    if(index.column()==PINCOL_NUM){
        const QString nnum=qobject_cast<QLineEdit*>(editor)->text();
        const QString onum=mmodel->data(index).toString();
        //any change?
        if(nnum==onum)return;
        //empty not allowed
        if(nnum.isEmpty()){
            QMessageBox::warning(mparent,tr("Warning"),tr("Pin number must not be empty!"));
            return;
        }
        //check syntax
        if(nnum.startsWith('-') || std::find_if(nnum.cbegin(),nnum.cend(),[](QChar c)->auto{return c.isSpace();})!=nnum.cend()){
            QMessageBox::warning(mparent,tr("Invalid Pin Number"),tr("Pin number must not start with a dash or contain spaces."));
            return;
        }
        static QRegularExpression pinnumre("^[a-zA-Z0-9]+$");
        if(!pinnumre.match(nnum).hasMatch()){
            if(QMessageBox::question(mparent,tr("Invalid Pin Number"),tr("Pin numbers should only consist of letters and digits. Store anyway?"))!=QMessageBox::Yes)
                return;
        }
        //check duplicates
        if(mvar.pinIds().contains(nnum)){
            QMessageBox::warning(mparent,tr("Duplicate Warning"),tr("A pin with the number '%1' already exists. Cannot store this one.").arg(nnum));
            return;
        }
        //store
        if(onum.isEmpty()){
            //find next
            QString before;
            for(int row=index.row()+1;row<mmodel->rowCount();row++){
                before=mmodel->data(mmodel->index(row,PINCOL_NUM)).toString();
                if(!before.isEmpty())break;
            }
            //store
            if(!mvar.addPin(ChipVariantPin(nnum),before)){
                QMessageBox::warning(mparent,tr("Warning"),tr("Unable to add pin."));
                return;
            }
            //display fresh data
            auto pin=mvar.pin(nnum);
            auto tidx=mmodel->index(index.row(),PINCOL_TYPE);
            mmodel->setData(tidx,ChipVariant::pinTypeToString(pin.type()));
            mmodel->setData(tidx,(int)pin.type(),Qt::UserRole);
            mmodel->setData(mmodel->index(index.row(),PINCOL_NAME),pin.name());
        }else{
            if(!mvar.renumberPin(onum,nnum)){
                QMessageBox::warning(mparent,tr("Warning"),tr("Oops. Unable to renumber pin."));
                return;
            }
        }
        //change display
        mmodel->setData(index,nnum);
        emit pinStored(onum,nnum);
        return;
    }
    if(index.column()==PINCOL_TYPE){
        const auto otype=ChipVariantPin::PinType(mmodel->data(index,Qt::UserRole).toInt());
        const auto ntype=ChipVariantPin::PinType(qobject_cast<QComboBox*>(editor)->currentData().toInt());
        if(otype==ntype)return;
        //overwrite pin
        const QString num=mmodel->data(mmodel->index(index.row(),PINCOL_NUM)).toString();
        mvar.setPin(num,ntype,QString());
        //reset display
        auto pin=mvar.pin(num);
        mmodel->setData(index,ChipVariant::pinTypeToString(ntype));
        mmodel->setData(index,int(ntype),Qt::UserRole);
        mmodel->setData(mmodel->index(index.row(),PINCOL_NAME),pin.name());
        emit pinStored(num,num);
        return;
    }
    if(index.column()==PINCOL_NAME){
        const QString oname=mmodel->data(index).toString();
        const QString nname=qobject_cast<QComboBox*>(editor)->currentText();
        if(oname==nname)return;
        const QString num=mmodel->data(mmodel->index(index.row(),PINCOL_NUM)).toString();
        //store
        auto pin=mvar.pin(num);
        pin.setName(nname);
        mvar.setPin(pin);
        //update display
        mmodel->setData(index,nname);
        emit pinStored(num,num);
    }
}

void ChipVariantEditor::removePin()
{
    //get line
    auto idx=mpintab->currentIndex();
    if(!idx.isValid())return;
    const int row=idx.row();
    //ask
    const QString num=mpinmodel->data(mpinmodel->index(row,PINCOL_NUM)).toString();
    if(!num.isEmpty())
    if(QMessageBox::question(this,tr("Delete Pin"),tr("Really delete pin %1?").arg(num))!=QMessageBox::Yes)return;
    //delete from variant
    if(!num.isEmpty())
        mvariant.removePin(num);
    //delete from table
    mpinmodel->removeRow(row);
    updateStatus();
    if(!num.isEmpty())emit pinNumberChanged(num,QString());
}

void ChipVariantEditor::movePin(bool isUp)
{
    //get line
    auto idx=mpintab->currentIndex();
    if(!idx.isValid())return;
    const int row=idx.row();
    //sanity check
    if(!isUp && row==mpinmodel->rowCount()-1)return;
    if(isUp && row==0)return;
    //get other line
    const int orow=row + (isUp ? -1 : 1);
    //get both nums
    const QString num=mpinmodel->data(mpinmodel->index(row,PINCOL_NUM)).toString();
    const QString onum=mpinmodel->data(mpinmodel->index(orow,PINCOL_NUM)).toString();
    //swap in variant
    if(!num.isEmpty() && !onum.isEmpty())
        mvariant.swapPins(num,onum);
    //swap in model
    if(isUp){
        auto data=mpinmodel->takeRow(row);
        mpinmodel->insertRow(orow,data);
    }else{
        auto data=mpinmodel->takeRow(orow);
        mpinmodel->insertRow(row,data);
    }
    //adjust current
    mpintab->setCurrentIndex(mpinmodel->index(orow,idx.column()));
}



bool ChipVariantEditor::isInPackage() const
{
    auto pe=qobject_cast<ChipPackageEditor*>(mtab->currentWidget());
    return pe!=nullptr;
}

QString ChipVariantEditor::selectedFileType()
{
    auto pe=qobject_cast<ChipPackageEditor*>(mtab->currentWidget());
    if(pe==nullptr)return QString();
    return pe->selectedFileType();
}

void ChipVariantEditor::deleteChipFile()
{
    auto pe=qobject_cast<ChipPackageEditor*>(mtab->currentWidget());
    if(pe==nullptr)return;
    pe->deleteChipFile();
}

void ChipVariantEditor::addChipFile(int type)
{
    auto pe=qobject_cast<ChipPackageEditor*>(mtab->currentWidget());
    if(pe==nullptr)return;
    pe->addChipFile(type);
}

int ChipVariantEditor::openPackageTab(QString id)
{
        auto*w=new ChipPackageEditor(this,mvariant.packageById(id));
        auto idx=mtab->addTab(w,w->windowTitle());
        connect(w,&QWidget::windowTitleChanged,mtab,[this,w](QString t){mtab->setTabText(mtab->indexOf(w),t);});
        connect(w,&ChipPackageEditor::pageSelectionChanged,mparent,&ChipEditor::tabChanged);
        return idx;
}

void ChipVariantEditor::addPackage()
{
    //ask for data
    AddPackageDialog apd(this,mvariant);
    if(apd.exec()!=QDialog::Accepted)
        return;
    //create tab
    auto idx=openPackageTab(apd.package().uid());
    mtab->setCurrentIndex(idx);
}

void ChipVariantEditor::deletePackage()
{
    //get tab
    auto pe=qobject_cast<ChipPackageEditor*>(mtab->currentWidget());
    if(pe==nullptr)return;
    //ask
    ChipPackage cp=pe->package();
    if(QMessageBox::question(this,tr("Really delete?"),tr("Do you really want to delete the package '%1'?").arg(cp.name()))!=QMessageBox::Yes)
        return;
    //delete
    mtab->removeTab(mtab->indexOf(pe));
    pe->deleteLater();
    mvariant.deletePackage(cp.uid());
}





// //////////////////////////////////////////////
// Add Package Dialog

AddPackageDialog::AddPackageDialog(ChipVariantEditor*parent,ChipVariant v)
:QDialog(parent),mvar(v),muid(QUuid::createUuid())
{
    setWindowTitle(tr("Add Package to '%1'").arg(v.name()));
    QHBoxLayout*hl;
    QVBoxLayout*vl;
    QFormLayout*fl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(fl=new QFormLayout);
    fl->addRow(tr("UID:"),new QLabel(muid.toString(QUuid::WithoutBraces)));
    fl->addRow(tr("Template:"),mtemp=new QComboBox);
    for(auto t:TemplatePool::instance().titleList())
        mtemp->addItem(t);
    fl->addRow(tr("Name:"),mname=new QLineEdit);
    mname->setPlaceholderText(mtemp->currentText());
    connect(mtemp,&QComboBox::currentTextChanged,mname,&QLineEdit::setPlaceholderText);

    vl->addSpacing(15);
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("Create Package")),0);
    connect(pb,&QPushButton::clicked,this,&AddPackageDialog::create);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);
}

void AddPackageDialog::create()
{
    const auto name=mname->text();
    const auto temp=mtemp->currentText();
    if(temp.isEmpty())return;
    mpack=mvar.newPackage(TemplatePool::instance().byTitle(temp),muid.toString(QUuid::WithoutBraces));
    mpack.setName(name);
    accept();
}


#include "moc_chipvariantedit.cpp"
#include "moc_chipvariantedit_p.cpp"
