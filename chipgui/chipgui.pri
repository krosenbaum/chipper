#sources
SOURCES += \
    $$PWD/chipedit.cpp \
    $$PWD/chipvariantedit.cpp \
    $$PWD/chippackageedit.cpp \
    $$PWD/chipfileedit.cpp \
    $$PWD/newchipwiz.cpp \
    $$PWD/variableedit.cpp \
    $$PWD/filepinedit.cpp

HEADERS += \
    $$PWD/chipedit.h \
    $$PWD/chipedit_p.h \
    $$PWD/chipvariantedit.h \
    $$PWD/chipvariantedit_p.h \
    $$PWD/chippackageedit.h \
    $$PWD/chipfileedit.h \
    $$PWD/newchipwiz.h \
    $$PWD/variableedit.h \
    $$PWD/filepinedit.h

INCLUDEPATH += $$PWD

#global resources
RESOURCES += $$PWD/guifiles.qrc
