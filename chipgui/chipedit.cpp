// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QStyledItemDelegate>
#include <QComboBox>
#include <QDebug>
#include <QFile>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QKeyEvent>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QRegularExpression>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTimer>
#include <QTreeView>

#include "chipdata.h"
#include "chipdetail.h"
#include "chipedit.h"
#include "chipedit_p.h"
#include "chippackage.h"
#include "chipper.h"
#include "chipvariant.h"
#include "chipvariantedit.h"
#include "griddelegate.h"

#include <functional>
#include <algorithm>

// ////////////////////////////////////
// Chip Editor

ChipEditor::ChipEditor(ChipData* chip, QWidget* parent)
:QTabWidget(parent),mchip(chip)
{
    updateTitle();
    setTabPosition(West);
    setElideMode(Qt::ElideNone);
    setUsesScrollButtons(true);

    addTab(createMainTab(),tr("Basics"));
    addTab(createTargetTab(),tr("Target"));
    addTab(createNotesTab(),tr("Notes"));
    addTab(createPinTab(),tr("Pins"));

    for(auto vid:mchip->variantIds()){
        openVariantTab(vid);
    }

    connect(this,&QTabWidget::currentChanged,this,&ChipEditor::tabChanged);
}

QWidget * ChipEditor::createMainTab()
{
    auto*w=new QWidget;
    QFormLayout*fl;
    w->setLayout(fl=new QFormLayout);
    fl->addRow(new QLabel(tr("<html><h1>Basic Chip Meta Data</h1>")));
    fl->addRow(tr("Chip File:"),new QLabel(mchip->fileName()));

    fl->addRow(new QLabel);
    fl->addRow(new QLabel(tr("<html><b>Hardware Data:</b>")));
    fl->addRow(tr("Manufacturer:"),mmanu=new QLineEdit(mchip->metaData().manufacturer()));
    connect(mmanu,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setManufacturer(t);emit propertyChanged();});
    fl->addRow(tr("Chip Type:"),mtype=new QLineEdit(mchip->metaData().chipType()));
    connect(mtype,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setChipType(t);updateTitle();emit propertyChanged();});
    fl->addRow(tr("Datasheet URL:"),mdsheet=new QLineEdit(mchip->metaData().datasheetUrl()));
    connect(mdsheet,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setDatasheetUrl(t);emit propertyChanged();});
    fl->addRow(tr("Keywords:"),mkeyw=new QLineEdit(mchip->metaData().keywords()));
    connect(mkeyw,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setKeywords(t);emit propertyChanged();});
    fl->addRow(tr("Description:"),mdescr=new QLineEdit(mchip->metaData().description()));
    connect(mdescr,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setDescription(t);emit propertyChanged();});
    fl->addRow(tr("Reference Prefix:"),mrefpfx=new QLineEdit(mchip->metaData().refPrefix()));
    mrefpfx->setPlaceholderText("U");
    connect(mrefpfx,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setRefPrefix(t);emit propertyChanged();});

    fl->addRow(new QLabel);
    fl->addRow(new QLabel(tr("<html><b>Soft-Model Data:</b>")));
    fl->addRow(tr("File Author:"),mauth=new QLineEdit(mchip->metaData().author()));
    connect(mauth,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setAuthor(t);emit propertyChanged();});
    fl->addRow(tr("File Copyright:"),mcopy=new QLineEdit(mchip->metaData().copyrightString()));
    connect(mcopy,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setCopyrightString(t);emit propertyChanged();});
    fl->addRow(tr("File License:"),mlic=new QLineEdit(mchip->metaData().license()));
    connect(mlic,&QLineEdit::textEdited,this,[this](QString t){mchip->metaData().setLicense(t);emit propertyChanged();});

    return w;
}

QWidget * ChipEditor::createTargetTab()
{
    auto*w=new QWidget;
    QFormLayout*fl;
    w->setLayout(fl=new QFormLayout);

    fl->addRow(new QLabel(tr("<html><h1>Target Files</h1>")));
    fl->addRow(tr("Symbol File:"),msym=new QLineEdit(mchip->targets().symbolFile()));
    connect(msym,&QLineEdit::textEdited,this,[this](QString t){mchip->targets().setSymbolFile(t);emit propertyChanged();});
    fl->addRow(tr("Symbol Root Dir:"),new QLabel(absoluteDefaultPath(ChipperPath::Symbol)));
    fl->addRow(tr("Footprint Dir:"),mfoot=new QLineEdit(mchip->targets().footprintFile()));
    connect(mfoot,&QLineEdit::textEdited,this,[this](QString t){mchip->targets().setFootprintFile(t);emit propertyChanged();});
    fl->addRow(tr("Footprint Root Dir:"),new QLabel(absoluteDefaultPath(ChipperPath::Footprint)));
    fl->addRow(tr("3D Model Dir:"),m3dmod=new QLineEdit(mchip->targets().modelFile()));
    connect(m3dmod,&QLineEdit::textEdited,this,[this](QString t){mchip->targets().setModelFile(t);emit propertyChanged();});
    fl->addRow(tr("3D Model Root Dir:"),new QLabel(absoluteDefaultPath(ChipperPath::Model3D)));
    auto notes=new QTextBrowser;
    fl->addRow(tr("Notes:"),notes);
    auto pal=notes->palette();
    pal.setColor(QPalette::Active,QPalette::Base,pal.color(QPalette::Active,QPalette::Window));
    pal.setColor(QPalette::Inactive,QPalette::Base,pal.color(QPalette::Inactive,QPalette::Window));
    pal.setColor(QPalette::Normal,QPalette::Base,pal.color(QPalette::Normal,QPalette::Window));
    notes->setPalette(pal);
    notes->setHtml(targetHelpText());

    return w;
}

QString ChipEditor::targetHelpText()
{
    QFile fd(":/target_en.html");
    fd.open(QIODevice::ReadOnly);
    QString txt=QString::fromUtf8(fd.readAll());
    fd.close();
    txt.replace("@SYMVAR@",envNameForPath(ChipperPath::Symbol));
    txt.replace("@FOOTVAR@",envNameForPath(ChipperPath::Footprint));
    txt.replace("@3DVAR@",envNameForPath(ChipperPath::Model3D));
    txt.replace("@ROOTVAR@",envNameForPath(ChipperPath::Root));
    txt.replace("@SYMVAL@",absoluteDefaultPath(ChipperPath::Symbol));
    txt.replace("@FOOTVAL@",absoluteDefaultPath(ChipperPath::Footprint));
    txt.replace("@3DVAL@",absoluteDefaultPath(ChipperPath::Model3D));
    txt.replace("@ROOTVAL@",absoluteDefaultPath(ChipperPath::Root));
    return txt;
}

QWidget * ChipEditor::createNotesTab()
{
    QWidget*r=new QWidget(this);
    QHBoxLayout*hl;
    QVBoxLayout*vl;
    r->setLayout(vl=new QVBoxLayout);
    //button bar (see below)
    vl->addLayout(hl=new QHBoxLayout,0);
    //editor/viewer
    vl->addWidget(mnotes=new QTextEdit,1);
    mnotes->setMarkdown(mchip->notes());
    mnotes->setReadOnly(true);
    connect(mnotes,&QTextEdit::textChanged,this,[this]{mnotechanged=true;});
    //view button and logic
    QPushButton*p;
    hl->addWidget(p=new QPushButton(tr("View")),0);
    p->setCheckable(true);
    p->setChecked(true);
    p->setAutoExclusive(true);
    connect(p,&QPushButton::toggled,this,[this](bool c){
        saveNotes();
        if(c){
            mnoteedit=false;
            mnotechanged=false;
            mnotes->setMarkdown(mchip->notes());
            mnotes->setReadOnly(true);
        }
    });
    //edit button and logic
    hl->addWidget(p=new QPushButton(tr("Edit")),0);
    p->setCheckable(true);
    p->setChecked(false);
    p->setAutoExclusive(true);
    connect(p,&QPushButton::toggled,this,[this](bool c){
        saveNotes();
        if(c){
            mnoteedit=true;
            mnotechanged=false;
            mnotes->setPlainText(mchip->notes());
            mnotes->setReadOnly(false);
        }
    });
    //end of button bar
    hl->addStretch(1);
    //auto-save timer
    auto t=new QTimer(this);
    t->setSingleShot(false);
    t->setInterval(20000);
    connect(t,&QTimer::timeout,this,&ChipEditor::saveNotes);
    t->start();
    //done
    return r;
}

void ChipEditor::saveNotes()
{
    if(!mnoteedit || !mnotechanged)return;
    mchip->setNotes(mnotes->toPlainText());
}

static QIcon functionIcon(ChipPin::Function f)
{
    static QMap<ChipPin::Function,QIcon> icons;
    if(icons.isEmpty()){
        icons.insert(ChipPin::Function::Free,           QIcon(":/free.png"));
        icons.insert(ChipPin::Function::Signal,         QIcon(":/bidi.png"));
        icons.insert(ChipPin::Function::SignalIn,       QIcon(":/sigin.png"));
        icons.insert(ChipPin::Function::SignalOut,      QIcon(":/sigout.png"));
        icons.insert(ChipPin::Function::Power,          QIcon(":/power.png"));
        icons.insert(ChipPin::Function::PowerIn,        QIcon(":/powerin.png"));
        icons.insert(ChipPin::Function::PowerOut,       QIcon(":/powerout.png"));
        icons.insert(ChipPin::Function::Ground,         QIcon(":/ground.png"));
        icons.insert(ChipPin::Function::GroundIn,       QIcon(":/groundin.png"));
        icons.insert(ChipPin::Function::GroundOut,      QIcon(":/groundout.png"));
        icons.insert(ChipPin::Function::Passive,        QIcon(":/passive.png"));
        icons.insert(ChipPin::Function::NoConnect,      QIcon(":/noconnect.png"));
        icons.insert(ChipPin::Function::OpenCollector,  QIcon(":/opencol.png"));
        icons.insert(ChipPin::Function::OpenEmitter,    QIcon(":/openem.png"));
        icons.insert(ChipPin::Function::TriState,       QIcon(":/tristate.png"));
        icons.insert(ChipPin::Function::Unspecified,    QIcon(":/unspec.png"));
    }
    if(icons.contains(f))return icons[f];
    else return QIcon();
}

static QIcon lineIcon(ChipPin::Line l)
{
    static QMap<ChipPin::Line,QIcon> icons;
    if(icons.isEmpty()){
        icons.insert(ChipPin::Line::Clock,          QIcon(":/lineclock.png"));
        icons.insert(ChipPin::Line::ClockLow,       QIcon(":/lineclockfall.png"));
        icons.insert(ChipPin::Line::EdgeClockHigh,  QIcon(":/lineclockfall.png"));
        icons.insert(ChipPin::Line::InvertedClock,  QIcon(":/lineclockinv.png"));
        icons.insert(ChipPin::Line::OutputLow,      QIcon(":/linelow.png"));
        icons.insert(ChipPin::Line::Line,           QIcon(":/line.png"));
        icons.insert(ChipPin::Line::Inverted,       QIcon(":/lineinv.png"));
        icons.insert(ChipPin::Line::InputLow,       QIcon(":/linelow.png"));
        icons.insert(ChipPin::Line::NonLogic,       QIcon(":/linenon.png"));
    }
    if(icons.contains(l))return icons[l];
    else return QIcon();
}

class PinEventFilter:public QObject
{
    std::function<void()>mcall;
public:
    PinEventFilter(QObject*parent,std::function<void()>c):QObject(parent),mcall(c){}
    virtual bool eventFilter(QObject * , QEvent * event) override
    {
        if(event->type() == QEvent::KeyPress){
            auto kev=static_cast<QKeyEvent*>(event);
            auto key=kev->key();
            if(key==Qt::Key_Return ||key==Qt::Key_Space){
                mcall();
                return true;
            }else
                return false;
        }
        return false;
    }
};

class ChipPinDelegate:public GridDelegate
{
    std::function<void()>mcall;
public:
    ChipPinDelegate(QObject*parent,std::function<void()>c):GridDelegate(parent),mcall(c){}
    virtual QWidget*createEditor(QWidget *, const QStyleOptionViewItem &, const QModelIndex &) const override
    {
        mcall();
        return nullptr;
    }
};

QWidget * ChipEditor::createPinTab()
{
    auto*w=new QWidget;
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr(
        "<html><h1>Pin Definitions</h1>\n"
        "<p>This page defines all logical pins that the chip can offer.</p>\n"
        "<p>Each distinct pin should appear exactly once (e.g. only one \"GND\", but e.g. \"Vcc\" vs. \"AVcc\" are "
        "different) and every pin that exists in any variant of the chip must appear.<br/>\n"
        "Variants can only reference pins that exist here, but they can reference each existing pin "
        "never, once or even multiple times.<br/>\n"
        "Pins that have no function (NC) should not appear here.</p>"
        )));
    vl->addSpacing(10);
    vl->addWidget(new QLabel(tr("Pins:")));
    vl->addLayout(hl=new QHBoxLayout,1);
    hl->addWidget(mpins=new QTreeView,1);
    mpins->setModel(mpinmodel=new QStandardItemModel(this));
    mpins->installEventFilter(new PinEventFilter(this,[this]{editPinOrAlt();}));//Space + Enter
    mpins->setItemDelegate(new ChipPinDelegate(this,[this]{editPinOrAlt();}));//F2 and double click
    mpins->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(mpins,&QWidget::customContextMenuRequested,this,&ChipEditor::pinContextMenu);
    //mpins->verticalHeader()->hide();
    reloadPins();
    hl->addLayout(vl=new QVBoxLayout,0);
    QPushButton*pb;
    vl->addWidget(pb=new QPushButton(tr("&Add Pin...")));
    connect(pb,&QPushButton::clicked,this,&ChipEditor::addPin);
    vl->addWidget(pb=new QPushButton(tr("&Add Pin Group...")));pb->setDisabled(true);
    vl->addWidget(pb=new QPushButton(tr("Add Alt &Function...")));
    connect(pb,&QPushButton::clicked,this,&ChipEditor::addPinAlt);
    vl->addWidget(pb=new QPushButton(tr("&Edit...")));
    connect(pb,&QPushButton::clicked,this,&ChipEditor::editPinOrAlt);
    vl->addSpacing(10);
    vl->addWidget(pb=new QPushButton(tr("&Delete Pin or Alt")));
    connect(pb,&QPushButton::clicked,this,&ChipEditor::deletePinOrAlt);
    vl->addSpacing(20);
    vl->addWidget(pb=new QPushButton(tr("Move &Up")));pb->setDisabled(true);
    vl->addWidget(pb=new QPushButton(tr("&Move Down")));pb->setDisabled(true);
    vl->addSpacing(10);
    vl->addWidget(pb=new QPushButton(tr("&Sort Pins")));pb->setDisabled(true);
    vl->addStretch(1);

    return w;
}

void ChipEditor::reloadPins()
{
    auto pins=mchip->pins();
    mpinmodel->insertColumns(0,3);
    mpinmodel->setHorizontalHeaderLabels(QStringList()<<tr("Name")<<tr("Electrical Type")<<tr("Line Type"));
    auto apins=pins.allPins();
    mpinmodel->insertRows(0,apins.size());
    for(int i=0;i<apins.size();i++){
        mpinmodel->setData(mpinmodel->index(i,0),apins[i].name());
        mpinmodel->setData(mpinmodel->index(i,1),apins[i].functionString());
        mpinmodel->setData(mpinmodel->index(i,1),int(apins[i].function()),Qt::UserRole);
        mpinmodel->setData(mpinmodel->index(i,1),functionIcon(apins[i].function()),Qt::DecorationRole);
        mpinmodel->setData(mpinmodel->index(i,2),apins[i].lineString());
        mpinmodel->setData(mpinmodel->index(i,2),lineIcon(apins[i].line()),Qt::DecorationRole);
        auto alt=apins[i].allAlternates();
        if(alt.size()==0)continue;
        auto pix=mpinmodel->index(i,0);
        mpinmodel->insertColumns(0,3,pix);
        mpinmodel->insertRows(0,alt.size(),pix);
        for(int j=0;j<alt.size();j++){
            mpinmodel->setData(mpinmodel->index(j,0,pix),alt[j].name());
            mpinmodel->setData(mpinmodel->index(j,1,pix),alt[j].functionString());
            mpinmodel->setData(mpinmodel->index(j,1,pix),int(alt[j].function()),Qt::UserRole);
            mpinmodel->setData(mpinmodel->index(j,1,pix),functionIcon(alt[j].function()),Qt::DecorationRole);
            mpinmodel->setData(mpinmodel->index(j,2,pix),alt[j].lineString());
            mpinmodel->setData(mpinmodel->index(j,2,pix),lineIcon(alt[j].line()),Qt::DecorationRole);
        }
    }
    //mpins->resizeColumnsToContents();
    mpins->resizeColumnToContents(0);
    mpins->resizeColumnToContents(1);
    mpins->resizeColumnToContents(2);
}

void ChipEditor::addPin()
{
    //editor
    ChipPinDialog d(this,ChipPin(),ChipPinDialog::Mode::NewPin);
    if(d.exec()!=QDialog::Accepted)return;
    //update display
    auto pin=d.pin();
    const int i=mpinmodel->rowCount();
    mpinmodel->insertRows(i,1);
    mpinmodel->setData(mpinmodel->index(i,0),pin.name());
    mpinmodel->setData(mpinmodel->index(i,1),pin.functionString());
    mpinmodel->setData(mpinmodel->index(i,1),int(pin.function()),Qt::UserRole);
    mpinmodel->setData(mpinmodel->index(i,1),functionIcon(pin.function()),Qt::DecorationRole);
    mpinmodel->setData(mpinmodel->index(i,2),pin.lineString());
    mpinmodel->setData(mpinmodel->index(i,2),lineIcon(pin.line()),Qt::DecorationRole);
    //move there
    mpins->setCurrentIndex(mpinmodel->index(i,0));
    //tell a friend
    emit pinChanged(QString(),pin.name());
}

void ChipEditor::addPinAlt()
{
    //get pin object
    auto idx=mpins->currentIndex();
    if(!idx.isValid())return;
    auto pidx=idx.parent();
    if(pidx.isValid())idx=pidx;
    if(idx.column()!=0)idx=mpinmodel->index(idx.row(),0);
    QString pn=mpinmodel->data(idx).toString();
    if(pn.isEmpty())return;
    ChipPin pin=mchip->pins().byName(pn);
    //create
    ChipPinDialog d(this,pin,ChipPinDialog::Mode::NewAlt);
    if(d.exec()!=QDialog::Accepted)return;
    //update display
    pin=d.pin();
    const int i=mpinmodel->rowCount(idx);
    mpinmodel->insertRows(i,1,idx);
    if(i==0)mpinmodel->insertColumns(0,3,idx);
    mpinmodel->setData(mpinmodel->index(i,0,idx),pin.name());
    mpinmodel->setData(mpinmodel->index(i,1,idx),pin.functionString());
    mpinmodel->setData(mpinmodel->index(i,1,idx),int(pin.function()),Qt::UserRole);
    mpinmodel->setData(mpinmodel->index(i,1,idx),functionIcon(pin.function()),Qt::DecorationRole);
    mpinmodel->setData(mpinmodel->index(i,2,idx),pin.lineString());
    mpinmodel->setData(mpinmodel->index(i,2,idx),lineIcon(pin.line()),Qt::DecorationRole);
    //move there
    mpins->setCurrentIndex(mpinmodel->index(i,0,idx));
    //tell a friend
    emit pinChanged(pn,pn);
}

void ChipEditor::editPinOrAlt()
{
    //get pin object
    auto idx=mpins->currentIndex();
    if(!idx.isValid())return;
    auto pidx=idx.parent();
    ChipPin pin;
    QString pn;
    if(pidx.isValid()){
        //get parent pin
        pn=mpinmodel->data(pidx).toString();
        if(pn.isEmpty())return;
        ChipPin ppin=mchip->pins().byName(pn);
        if(!ppin.isValid())return;
        //get alternate function
        if(idx.column()!=0)idx=mpinmodel->index(idx.row(),0,pidx);
        pn=mpinmodel->data(idx).toString();
        if(pn.isEmpty())return;
        pin=ppin.alternate(pn);
    }else{
        //get pin directly
        if(idx.column()!=0)idx=mpinmodel->index(idx.row(),0);
        QString pn=mpinmodel->data(idx).toString();
        if(pn.isEmpty())return;
        pin=mchip->pins().byName(pn);
    }
    if(!pin.isValid())return;
    //call editor
    ChipPinDialog d(this,pin);
    if(d.exec()!=QDialog::Accepted)return;
    //refresh data
    pin=d.pin();
    const int i=idx.row();
    mpinmodel->setData(mpinmodel->index(i,0,pidx),pin.name());
    mpinmodel->setData(mpinmodel->index(i,1,pidx),pin.functionString());
    mpinmodel->setData(mpinmodel->index(i,1,pidx),int(pin.function()),Qt::UserRole);
    mpinmodel->setData(mpinmodel->index(i,1,pidx),functionIcon(pin.function()),Qt::DecorationRole);
    mpinmodel->setData(mpinmodel->index(i,2,pidx),pin.lineString());
    mpinmodel->setData(mpinmodel->index(i,2,pidx),lineIcon(pin.line()),Qt::DecorationRole);
    //tell a friend; on alt, emit parent!
    if(pin.isPin())
        emit pinChanged(d.oldName(),pin.name());
    else
        emit pinChanged(pn,pn);
}

void ChipEditor::deletePinOrAlt()
{
    //get pin object
    auto idx=mpins->currentIndex();
    qDebug()<<"request delete"<<idx;
    if(!idx.isValid())return;
    auto pidx=idx.parent();
    if(pidx.isValid()){
        //get parent pin & basic checks
        auto pn=mpinmodel->data(pidx).toString();
        qDebug()<<"parent"<<pn;
        if(pn.isEmpty())return;
        if(idx.column()!=0)idx=mpinmodel->index(idx.row(),0,pidx);
        auto an=mpinmodel->data(idx).toString();
        qDebug()<<"alt"<<an;
        if(an.isEmpty())return;
        //ask really nicely
        if(QMessageBox::question(this,tr("Really Delete?"),tr("Really delete alternate function '%1' of pin '%2'?").arg(an).arg(pn))!=QMessageBox::Yes)return;
        //delete the sucker!
        ChipPin ppin=mchip->pins().byName(pn);
        if(!ppin.isValid()){
            qDebug()<<"Oooops! Parent Pin"<<pn<<"not found while attempting to delete Alt Func"<<an;
            return;
        }
        //get alternate function
        if(!ppin.removeAlternate(an)){
            QMessageBox::warning(this,tr("Warning"),tr("Unable to delete this alternate function!"));
            return;
        }
        //adjust display
        mpinmodel->removeRow(idx.row(),pidx);
        //tell everyone
        emit pinChanged(pn,pn);
    }else{
        //get pin directly
        if(idx.column()!=0)idx=mpinmodel->index(idx.row(),0);
        QString pn=mpinmodel->data(idx).toString();
        if(pn.isEmpty())return;
        //ask nicely
        if(QMessageBox::question(this,tr("Really Delete?"),tr("Really delete pin '%1'?").arg(pn))!=QMessageBox::Yes)return;
        //delete pin
        auto pin=mchip->pins().byName(pn);
        if(!mchip->pins().deletePin(pin)){
            QMessageBox::warning(this,tr("Warning"),tr("Unable to delete this pin!"));
            return;
        }
        //adjust display
        mpinmodel->removeRow(idx.row());
        //tell a friend
        emit pinChanged(pn,QString());
    }
}

void ChipEditor::pinContextMenu(const QPoint&pos)
{
    auto idx=mpins->indexAt(pos);
    qDebug()<<"Context Menu requested at"<<idx.row()<<idx.column();
}

ChipEditor::~ChipEditor()
{
    if(mchip)mchip->saveFile();
}

ChipData * ChipEditor::chip() const
{
    return mchip;
}

void ChipEditor::updateTitle()
{
    if(mchip.isNull())
        setWindowTitle(tr("Unknown Chip"));
    else
        setWindowTitle(tr("Chip: %1").arg(mchip->metaData().chipType()));
}

int ChipEditor::openVariantTab(QString vid)
{
    QWidget*w=new ChipVariantEditor(this,vid);
    auto idx=addTab(w,w->windowTitle());
    connect(w,&QWidget::windowTitleChanged,this,[this,w](QString t){setTabText(indexOf(w),t);});
    return idx;
}

void ChipEditor::addVariant()
{
    //dialog, ask for data
    ChipAddVariantDialog avd(this,mchip);
    if(avd.exec()!=QDialog::Accepted)return;
    //create tab
    auto idx=openVariantTab(avd.variant().uid());
    //change to new tab
    setCurrentIndex(idx);
}

void ChipEditor::addPackage()
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve!=nullptr)
        ve->addPackage();
}

void ChipEditor::deleteVariant()
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve==nullptr)return;
    //ask
    auto var=ve->variant();
    if(QMessageBox::question(this,tr("Really delete?"),tr("Do you really want to delete the variant '%1'?").arg(var.name()))!=QMessageBox::Yes)
        return;
    //remove it
    removeTab(indexOf(ve));
    ve->deleteLater();
    mchip->deleteVariant(var.uid());
}

void ChipEditor::deletePackage()
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve!=nullptr)
        ve->deletePackage();
}

bool ChipEditor::isInPackage() const
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve==nullptr)return false;
    return ve->isInPackage();
}

bool ChipEditor::isInVariant() const
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    return ve!=nullptr;
}

QString ChipEditor::selectedFileType()
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve==nullptr)return QString();
    return ve->selectedFileType();
}

void ChipEditor::deleteChipFile()
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve==nullptr)return;
    return ve->deleteChipFile();
}

void ChipEditor::addChipFile(int type)
{
    auto ve=qobject_cast<ChipVariantEditor*>(currentWidget());
    if(ve==nullptr)return;
    return ve->addChipFile(type);
}





// //////////////////////////////////////////////
// Pin Dialog

ChipPinDialog::ChipPinDialog(ChipEditor*parent,ChipPin pin,Mode md)
    :QDialog(parent)
    ,mpin(md==Mode::Edit?pin:ChipPin())
    ,mppin(md==Mode::NewAlt?pin:ChipPin())
    ,mchip(parent->chip())
    ,moldname(pin.name())
    ,mmode(md)
{
    //handle new pin / new alt
    bool isPin=pin.isPin();
    if(md!=Mode::Edit)isPin=md==Mode::NewPin;
    setWindowTitle(
        tr("%1 %2 %3","Verb(Edit/New) ObjType(Pin/Alt) Name")
        .arg(md==Mode::Edit?tr("Edit"):tr("Create New"))
        .arg(isPin?tr("Pin"):tr("Alternate Function"))
        .arg(pin.name())
    );
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QFormLayout*fl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(fl=new QFormLayout);
    //input controls
    fl->addRow(isPin?tr("Pin Name:"):tr("Alternate Name:"),mname=new QLineEdit(pin.name()));
    fl->addRow(tr("Electrical Pin Type:"),mtype=new QComboBox);
    const auto fnc=pin.function();
    int pos=0;
    for(auto fc:ChipPin::allFunctions()){
        if(fc.first==fnc)pos=mtype->count();
        mtype->addItem(functionIcon(fc.first),fc.second,(int)fc.first);
    }
    mtype->setCurrentIndex(pos);
    fl->addRow(tr("Line Style:"),mline=new QComboBox);
    const auto line=pin.line();
    pos=0;
    for(auto ln:ChipPin::allLineTypes()){
        if(ln.first==line)pos=mline->count();
        mline->addItem(lineIcon(ln.first),ln.second,(int)ln.first);
    }
    mline->setCurrentIndex(pos);
    //buttons...
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("&Save")),0);
    connect(pb,&QPushButton::clicked,this,&ChipPinDialog::save);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);
}

void ChipPinDialog::save()
{
    //attempt to save name first
    const QString nname=mname->text();
    const auto nfunc=ChipPin::Function(mtype->currentData().toInt());
    const auto nline=ChipPin::Line(mline->currentData().toInt());
    if(nname.isEmpty()){
        QMessageBox::warning(this,tr("Warning"),tr("Pin names must not be empty, use ~ if you want it to appear empty in KiCad."));
        return;
    }
    auto isSpc=[](QChar c)->auto{return c.isSpace();};
    if(std::find_if(nname.cbegin(),nname.cend(),isSpc)!=nname.cend()){
        if(QMessageBox::question(this,tr("Pin with Spaces"),tr("Pin names must not contain spaces. Replace with '_'?"))!=QMessageBox::Yes)
            return;
        std::replace_if((QString::iterator)nname.begin(),(QString::iterator)nname.end(),isSpc,QChar('_'));
    }
    const QRegularExpression pnameRE("^[a-zA-Z0-9_~{}]+$");
    if(nname!=moldname || mmode!=Mode::Edit){
        //validate soft requirements
        if(!pnameRE.match(nname).hasMatch()){
            if(QMessageBox::question(this,tr("Invalid Pin Name"),tr("The pin name '%1' is not valid - only letters, digits, and the special characters '~', '_', '{' and '}' should be used.\n\nStore it anyway?").arg(nname))!=QMessageBox::Yes)
                return;
        }
        //store and hard duplication checks
        if(mmode==Mode::Edit){
            if(!mpin.setName(nname)){
                QMessageBox::warning(this,tr("Warning"),tr("Unable to change pin name. Please make sure there is no other pin/alternate function with the same name!"));
                return;
            }
        }else if(mmode==Mode::NewPin){
            mpin=mchip->pins().addPin(nname);
            if(!mpin.isValid()){
                QMessageBox::warning(this,tr("Warning"),tr("Unable to create new pin named '%1' - make sure there is no pin with this name yet!").arg(nname));
                return;
            }
        }else{
            if(!mppin.isValid()){
                QMessageBox::warning(this,tr("Ooops!"),tr("Hmm, parent pin for this alternate function is invalid - cannot create one.\nRepent! The end is nigh! Or something."));
                qDebug()<<"Ooops! Attempting to create alternate function for non-existing pin. Is this still real?";
                return;
            }
            if(mppin.addAlternate(nname,nfunc,nline)){
                mpin=mppin.alternate(nname);
            }else{
                QMessageBox::warning(this,tr("Warning"),tr("Unable to create alternate function. Make sure there is no pin or alternate function with the same name!"));
                return;
            }
        }
    }
    //save type & line
    mpin.setFunction(nfunc);
    mpin.setLine(nline);
    //done!
    accept();
}





// //////////////////////////////////////////////
// add variant dialog

ChipAddVariantDialog::ChipAddVariantDialog(ChipEditor*parent,ChipData*chip)
:QDialog(parent),mchip(chip),muid(QUuid::createUuid())
{
    setWindowTitle(tr("Create New Variant for Chip %1").arg(chip->metaData().chipType()));
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QFormLayout*fl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(fl=new QFormLayout);
    fl->addRow(tr("UID:"),new QLabel(muid.toString(QUuid::WithoutBraces)));
    fl->addRow(tr("Name:"),mname=new QLineEdit(chip->metaData().chipType()));
    fl->addRow(tr("Number of Pins:"),mpins=new QSpinBox);
    mpins->setRange(1,10000);
    int p=chip->pins().numPins();
    if(p<1)p=8;
    mpins->setValue(p);

    vl->addSpacing(15);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("Create &Variant")),0);
    connect(pb,&QPushButton::clicked,this,&ChipAddVariantDialog::create);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);
}

void ChipAddVariantDialog::create()
{
    //get data
    const QString name=mname->text();
    const int pins=mpins->value();
    //create
    mvar=mchip->newVariant(muid.toString(QUuid::WithoutBraces));
    mvar.setName(name);
    mvar.setNumPins(pins);
    //done
    accept();
}






// //////////////////////////////////////////////
// MOC


#include "moc_chipedit.cpp"
#include "moc_chipedit_p.cpp"
