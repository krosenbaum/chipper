// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QPointer>

#include "chippackageedit.h"
#include "chippackage.h"
#include "chipfile.h"

class QFormLayout;
class QLabel;
class QLineEdit;
class QTableView;
class QTreeView;
class QStackedWidget;
class QStandardItemModel;
class QSpinBox;

namespace Chipper {

namespace Chip{
class ChipData;
class ChipSymbol;
}

namespace GUI {

///Symbol Editor pages on the package editor.
class CHIPPERGUI_EXPORT FileEditPage:public PageWidget
{
    Q_OBJECT
    QFormLayout*mlayout;
    QLabel*mlabel;
    QLineEdit*mname;

protected:
    QFormLayout* mainLayout(){return mlayout;}

    FileEditPage()=delete;
    FileEditPage(const ChipFileTarget&,QWidget*parent=nullptr);

    virtual PageWidget*createPinPage();
    virtual PageWidget*createVariablePage();

protected slots:
    void updateTitle();

public:
    virtual ChipFileTarget&target()=0;
    virtual const ChipFileTarget&target()const=0;
    ///return the human readable title of the page
    virtual QString pageTitle()const override;
    ///return all sub-pages
    virtual QList<PageWidget*> createSubPages()override;
};

///Symbol Editor pages on the package editor.
class CHIPPERGUI_EXPORT SymbolEditPage:public FileEditPage
{
    Q_OBJECT
    ChipSymbol msymbol;
    QLineEdit*mdfprint,*mdvalue;

public:
    explicit SymbolEditPage(const ChipSymbol&,QWidget*parent=nullptr);
    ///override this to return the unique name of the page
    virtual QString pageName()const override;

    ChipSymbol symbol()const{return msymbol;}

    ///override this to return the correct page type
    PageType pageType()const override{return PageType::SymbolPage;}

    virtual ChipFileTarget&target()override{return msymbol;}
    virtual const ChipFileTarget&target()const override{return msymbol;}
};

///Symbol Editor pages on the package editor.
class CHIPPERGUI_EXPORT FootprintEditPage:public FileEditPage
{
    Q_OBJECT
    ChipFootprint mfprint;
    QLineEdit*mdmodel;

public:
    explicit FootprintEditPage(const ChipFootprint&,QWidget*parent=nullptr);
    ///override this to return the unique name of the page
    virtual QString pageName()const override;

    ChipFootprint footprint()const{return mfprint;}

    ///override this to return the correct page type
    PageType pageType()const override{return PageType::FootprintPage;}

    virtual ChipFileTarget&target()override{return mfprint;}
    virtual const ChipFileTarget&target()const override{return mfprint;}
};

///Symbol Editor pages on the package editor.
class CHIPPERGUI_EXPORT ModelEditPage:public FileEditPage
{
    Q_OBJECT
    ChipModel m3d;

public:
    explicit ModelEditPage(const ChipModel&,QWidget*parent=nullptr);
    ///override this to return the unique name of the page
    virtual QString pageName()const override;

    ChipModel model()const{return m3d;}

    ///override this to return the correct page type
    PageType pageType()const override{return PageType::ModelPage;}

    virtual ChipFileTarget&target()override{return m3d;}
    virtual const ChipFileTarget&target()const override{return m3d;}
};

//end of namespace
}}
using namespace Chipper::GUI;
