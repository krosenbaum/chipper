// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QPointer>

#include "chippackageedit.h"
#include "chippackage.h"
#include "chipfile.h"

class QFormLayout;
class QLabel;
class QLineEdit;
class QMenu;
class QTableView;
class QTreeView;
class QStackedWidget;
class QStandardItemModel;
class QSpinBox;

namespace Chipper {

namespace Chip{
class ChipData;
class ChipSymbol;
}

namespace GUI {
class FileEditPage;

///Pin Editor page as sub-page of files
class CHIPPERGUI_EXPORT FilePinEditPage:public PageWidget
{
    Q_OBJECT
    QPointer<FileEditPage>mparent;
    QTableView*mtable;
    QStandardItemModel*mmodel;
    QMenu*msymmenu,*mfprmenu,*m3dmmenu;
public:
    ///instantiate pin sub-editor for specific file editor
    FilePinEditPage(FileEditPage*parent);
    ///this is a sub-editor
    virtual PageType pageType()const override{return PageType::SubFilePage;}
    ///return the unique (static) name of the page
    virtual QString pageName()const override;
    ///return the human readable title of the page
    virtual QString pageTitle()const override;
    ///make sure menus are updated on any change
    virtual void connectMainEditor(ChipPackageEditor*)override;

private slots:
    ///redraw table
    void redisplay();
    ///edit selected row
    void editRow();
    ///execute auto-fill algorithm
    void autoFill(QString);
    ///repopulate copy button-menu
    void genCopyMenu();
    ///copy from other target
    void copyFrom(ChipFileTarget::FileType,QString);
    ///copy from other target
    void copyFrom(const ChipFileTarget&source);
    ///reacts to pin number changes
    void pinNumberChanged(QString oldNum,QString newNum);
};

class CHIPPERGUI_EXPORT PinRowEditor:public QDialog
{
    Q_OBJECT
    ChipFileTarget mtarget;
    QString mrowid;
    QTableView*mallpins,*mrowpins;
    QStandardItemModel*mallmodel,*mrowmodel;
public:
    PinRowEditor(QWidget*parent,const ChipFileTarget&target,QString rowid);
    ~PinRowEditor();

private slots:
    ///stores the current state into the target pin row
    void savePins();
    ///move selected pins up
    void moveUp();
    ///move selected pins down
    void moveDown();
    ///move selected pins to top
    void moveTop();
    ///move selected pins to bottom
    void moveBottom();
    ///add a gap
    void addGap();
    ///add selected variant pins to row
    void addSelected();
    ///remove selected pins from row
    void removeSelected();
    ///hide selected pins
    void hidePins();
    ///unhide selected pins
    void unhidePins();
private:
    ///helper for constructor: fill pin tables
    void fillTables();
};

//end of namespace
}}
using namespace Chipper::GUI;
