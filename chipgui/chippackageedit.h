// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QTabWidget>
#include <QPointer>
#include <QSplitter>
#include <QDialog>

#include "chippackage.h"

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLabel;
class QLineEdit;
class QTableView;
class QTreeView;
class QStackedWidget;
class QStandardItemModel;
class QSpinBox;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace Template{
class TemplateData;
}

namespace GUI {
class ChipEditor;
class ChipVariantEditor;
class PageWidget;

///Chip Package Editor Tab class. Sub-Tab of ChipVariantEditor.
class CHIPPERGUI_EXPORT ChipPackageEditor:public QWidget
{
    Q_OBJECT
    QPointer<ChipVariantEditor>mparent;
    QPointer<Chip::ChipData>mchip;
    ChipPackage mpackage;
    QTreeView *mtree;
    QStandardItemModel *mmodel;
    QStackedWidget *mstack;
    QSplitter *msplit;
    QLineEdit*mname,*mkeyw,*mdescr;
    QLabel*mchipkeyw;
public:
    ///instantiate editor tab, parameters are mandatory
    ChipPackageEditor(ChipVariantEditor*parent,const ChipPackage &package);
    ~ChipPackageEditor();

    ///returns the package represented by this editor
    ChipPackage package()const{return mpackage;}
    ///returns the parent variant editor
    ChipVariantEditor* variantEditor()const{return mparent;}

    QString selectedFileType();
    ///delete target file from package
    void deleteChipFile();
    ///add a target file to package
    void addChipFile(int type);

private slots:
    ///internal: add page
    void addPage(PageWidget*,QString parentIndex=QString());
    ///internal: delete page by name
    void deletePage(QString);
    ///internal: find parent page
    PageWidget* getParentPage(PageWidget*);
    ///internal: switch to page
    void gotoPage(QString);
    ///change page title
    void pageTitleChanged(QString);

signals:
    ///emitted whenever the current page changes
    void pageSelectionChanged();
    ///relay signal to tell file level variables to change
    void packageVariablesChanged();
    ///tells sub-pages that pages have been added, removed, or renamed
    void pagesChanged();

private:
    static const QString symbolsIndexName;
    static const QString footprintsIndexName;
    static const QString modelsIndexName;
};

///Abstract base class of editor pages on the package editor.
class CHIPPERGUI_EXPORT PageWidget:public QWidget
{
    Q_OBJECT
protected:
    explicit PageWidget(QWidget*parent=nullptr);
public:
    ///override this to return the unique (static) name of the page
    virtual QString pageName()const=0;
    ///override this to return the human readable title of the page
    virtual QString pageTitle()const=0;

    ///the kind of page represented by an instance of a sub-class
    enum class PageType {
        ///dummy fallback page that is displayed if the actual page is missing
        NullPage,
        ///something generic, nothing to worry about, no actions connected
        GenericPage,
        ///the package level variables
        VariablePage,
        ///README text for the template being used in this package
        ReadmePage,
        ///a symbol file, can be added/deleted
        SymbolPage,
        ///a footprint file, can be added/deleted
        FootprintPage,
        ///a 3D model file, can be added/deleted
        ModelPage,
        ///sub-page of a symbol/footprint/model, parent can be added/deleted
        SubFilePage
    };
    ///override this to return the correct page type
    virtual PageType pageType()const{return PageType::GenericPage;}

    ///true if the widget is an editor for a target file (symbol/footprint/model)
    virtual bool isTargetFilePage()const;

    ///return all sub-pages
    virtual QList<PageWidget*> createSubPages(){return QList<PageWidget*>();}

    ///override to connect page to package editor
    virtual void connectMainEditor(ChipPackageEditor*){}

    static const QString nullPageString;
    static const QString variablePageString;
    static const QString symbolPageString;
    static const QString footprintPageString;
    static const QString modelPageString;
    static const QString readmePageString;
signals:
    void titleChanged(QString);
};

//Null Pseudo-Page as a fall-back
class CHIPPERGUI_EXPORT NullPage:public PageWidget
{
    Q_OBJECT
public:
    NullPage(QWidget*parent=nullptr):PageWidget(parent){}
    virtual QString pageName()const override{return nullPageString;}
    virtual QString pageTitle()const override{return "(null)";}
    virtual PageType pageType()const override{return PageType::NullPage;}
};

///simple label page
class CHIPPERGUI_EXPORT LabelPage:public PageWidget
{
    Q_OBJECT
    QString mname,mtitle;
    QLabel*mlabel;
public:
    LabelPage(QString name,QString title,QString label,QWidget*parent=nullptr);
    virtual QString pageName()const override{return mname;}
    virtual QString pageTitle()const override{return mtitle;}

    void setPageTitle(QString t){mtitle=t;emit titleChanged(t);}
    void setLabelText(QString);
};

///Template ReadMe page
class CHIPPERGUI_EXPORT ReadmePage:public PageWidget
{
    Q_OBJECT
    QString mtitle;
public:
    ReadmePage(const Template::TemplateData&,QWidget*parent=nullptr);
    virtual QString pageName()const override{return readmePageString;}
    virtual QString pageTitle()const override{return mtitle;}
    virtual PageType pageType()const override{return PageType::ReadmePage;}
};

///Dialog for adding target files
class CHIPPERGUI_EXPORT ChipAddTargetFileDialog:public QDialog
{
    Q_OBJECT
    QLabel*mlabel;
    QComboBox*mtemplate;
    QLineEdit*mname;
public:
    explicit ChipAddTargetFileDialog(QWidget*parent=nullptr);
    void setLabel(QString);
    void addItem(QString id,QString text);
    void setName(QString);

    QString name()const;
    QString selectedId()const;
};

//end of namespace
}}
using namespace Chipper::GUI;
