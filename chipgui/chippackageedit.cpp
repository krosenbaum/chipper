// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Variant Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QHeaderView>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTreeView>

#include "chipdata.h"
#include "chipdetail.h"
#include "chipedit.h"
#include "chipper.h"
#include "chipvariantedit.h"
#include "chippackageedit.h"
#include "vlabel.h"
#include "templates.h"
#include "chipfileedit.h"
#include "variableedit.h"



// ////////////////////////////////////
// Chip Package Editor


ChipPackageEditor::ChipPackageEditor(ChipVariantEditor* parent, const ChipPackage &package)
:QWidget(parent),mparent(parent),mchip(parent->chip()),mpackage(package)
{
    auto changeTitle=[this]{
        setWindowTitle(tr("Package: %1").arg(mpackage.displayName()));
    };
    changeTitle();

    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QPushButton*pb;
    setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("<html><h1>Package</h1>")),0);
    vl->addSpacing(10);
    QFormLayout*fl;
    QWidget*w;
    vl->addLayout(fl=new QFormLayout,0);
    fl->addRow(tr("UID:"),new QLabel(mpackage.uid()));
    fl->addRow(tr("Template:"),w=new QWidget);
    w->setLayout(hl=new QHBoxLayout);
    hl->setContentsMargins(0,0,0,0);
    QLabel*tmpl;
    hl->addWidget(tmpl=new QLabel(mpackage.templateName()),1);
    auto tref=mpackage.templateRef();
    if(tref.isValid()){
        tmpl->setText(tr("%1 (%2)","template ID (description)").arg(tref.title()).arg(tref.description()));
        hl->addWidget(pb=new QPushButton(tr("Show...","show template")),0);
        connect(pb,&QPushButton::clicked,mparent,std::bind(&ChipVariantEditor::showTemplate,parent,tref.title()));
    }else
        hl->addWidget(new QLabel(tr("<html><font color='red'>Warning: Template not found!")),0);
    fl->addRow(tr("Name:"),mname=new QLineEdit(mpackage.name()));
    connect(mname,&QLineEdit::textChanged,this,[this]{mpackage.setName(mname->text());});
    connect(mname,&QLineEdit::textChanged,this,changeTitle,Qt::QueuedConnection);//queued: make sure it is called AFTER the update
    mname->setPlaceholderText(mpackage.templateName());
    fl->addRow(tr("Description:"),mdescr=new QLineEdit(mpackage.packageDescription()));
    connect(mdescr,&QLineEdit::textChanged,this,[this]{mpackage.setPackageDescription(mdescr->text());});
    mdescr->setPlaceholderText(mpackage.chipDescription());
    connect(mparent,&ChipVariantEditor::chipPropertyChanged,this,[this]{mdescr->setPlaceholderText(mpackage.chipDescription());});
    fl->addRow(tr("Keywords:"),hl=new QHBoxLayout);
    hl->addWidget(mchipkeyw=new QLabel(mpackage.chipKeywords()),0);
    connect(mparent,&ChipVariantEditor::chipPropertyChanged,this,[this]{mchipkeyw->setText(mpackage.chipKeywords());});
    hl->addWidget(mkeyw=new QLineEdit(mpackage.packageKeywords()),1);
    connect(mkeyw,&QLineEdit::textChanged,this,[this]{mpackage.setPackageKeywords(mkeyw->text());});
    vl->addSpacing(10);

    vl->addWidget(msplit=new QSplitter,1);
    msplit->setOrientation(Qt::Horizontal);

    msplit->addWidget(mtree=new QTreeView);
    mtree->setModel(mmodel=new QStandardItemModel(this));
    mtree->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mtree->setSelectionMode(QAbstractItemView::SingleSelection);
    mtree->setSelectionBehavior(QAbstractItemView::SelectRows);

    msplit->addWidget(mstack=new QStackedWidget);

    mmodel->insertColumn(0);
    addPage(new NullPage);
    addPage(new VariableEditPage(mpackage.variables()));
    addPage(new ReadmePage(mpackage.templateRef()));
    addPage(new LabelPage(symbolsIndexName,tr("Symbols"),tr("Please select a symbol!")));
    addPage(new LabelPage(footprintsIndexName,tr("Footprints"),tr("Please select a footprint!")));
    addPage(new LabelPage(modelsIndexName,tr("3D Models"),tr("Please select a 3D model!")));
    mtree->setHeaderHidden(true);

    for(auto uid:mpackage.symbolIds())
        addPage(new SymbolEditPage(mpackage.symbol(uid)),symbolsIndexName);

    for(auto uid:mpackage.footprintIds())
        addPage(new FootprintEditPage(mpackage.footprint(uid)),footprintsIndexName);

    for(auto uid:mpackage.modelIds())
        addPage(new ModelEditPage(mpackage.model(uid)),modelsIndexName);

    gotoPage(PageWidget::readmePageString);

    mtree->expandAll();

    connect(mtree->selectionModel(),&QItemSelectionModel::currentChanged,this,[this](const QModelIndex&idx){
        if(idx.isValid() && !mmodel->data(idx,Qt::UserRole).isNull()){
            QString id=mmodel->data(idx,Qt::UserRole).toString();
            gotoPage(id);
            emit pageSelectionChanged();
        }
    });

    QSettings set;
    set.beginGroup("packageEditor");
    if(set.contains("geo"))
        msplit->restoreGeometry(set.value("geo").toByteArray());
    if(set.contains("state"))
        msplit->restoreState(set.value("state").toByteArray());

}

ChipPackageEditor::~ChipPackageEditor()
{
    //save Geometry
    QSettings set;
    set.beginGroup("packageEditor");
    set.setValue("geo",msplit->saveGeometry());
    set.setValue("state",msplit->saveState());
}

QString ChipPackageEditor::selectedFileType()
{
    //get current page
    auto *page=qobject_cast<PageWidget*>(mstack->currentWidget());
    while(page!=nullptr && page->pageType()==PageWidget::PageType::SubFilePage)page=getParentPage(page);
    if(page==nullptr)return QString();
    //evaluate
    switch(page->pageType()){
        case PageWidget::PageType::SymbolPage:return tr("Symbol");
        case PageWidget::PageType::FootprintPage:return tr("Footprint");
        case PageWidget::PageType::ModelPage:return tr("3D Model");
        default:return QString();
    }
}

void ChipPackageEditor::addChipFile(int itype)
{
    auto type=(ChipFileTarget::FileType)itype;
    ChipAddTargetFileDialog d(this);
    d.setName(mpackage.displayName()+"-?");
    auto temp=mpackage.templateRef();
    switch(type){
        case ChipFileTarget::FileType::Symbol:{
            d.setWindowTitle(tr("Add Symbol"));
            d.setLabel(tr("Please select a symbol template and a name!"));
            for(const auto&id:temp.symbolIds())d.addItem(id,temp.symbol(id).name());
            if(d.exec()!=QDialog::Accepted)return;
            auto uid=mpackage.addSymbol(d.selectedId(),d.name());
            if(uid.isEmpty())return;
            auto *page=new SymbolEditPage(mpackage.symbol(uid));
            addPage(page,symbolsIndexName);
            gotoPage(page->pageName());
            return;
        }
        case ChipFileTarget::FileType::Footprint:{
            d.setWindowTitle(tr("Add Footprint"));
            d.setLabel(tr("Please select a footprint template and a name!"));
            for(const auto&id:temp.footprintIds())d.addItem(id,temp.footprint(id).name());
            if(d.exec()!=QDialog::Accepted)return;
            auto uid=mpackage.addFootprint(d.selectedId(),d.name());
            if(uid.isEmpty())return;
            auto *page=new FootprintEditPage(mpackage.footprint(uid));
            addPage(page,footprintsIndexName);
            gotoPage(page->pageName());
            return;
        }
        case ChipFileTarget::FileType::Model3D:{
            d.setWindowTitle(tr("Add 3D Model"));
            d.setLabel(tr("Please select a 3D model template and a name!"));
            for(const auto&id:temp.modelIds())d.addItem(id,temp.model(id).name());
            if(d.exec()!=QDialog::Accepted)return;
            auto uid=mpackage.addModel(d.selectedId(),d.name());
            if(uid.isEmpty())return;
            auto *page=new ModelEditPage(mpackage.model(uid));
            addPage(page,modelsIndexName);
            gotoPage(page->pageName());
            return;
        }
        default:
            qDebug()<<"Ooops. ChipPackageEditor::addChipFile called with unknown file type"<<itype;
            return;
    }
}

static inline QModelIndex findIndex(QStandardItemModel*model,QString n,const QModelIndex&pidx=QModelIndex())
{
    for(int r=0;r<model->rowCount(pidx);r++){
        auto idx=model->index(r,0,pidx);
        if(model->data(idx,Qt::UserRole).toString()==n)return idx;
        auto idx2=findIndex(model,n,idx);
        if(idx2.isValid())return idx2;
    }
    return QModelIndex();
}

void ChipPackageEditor::deleteChipFile()
{
    //get current page
    auto *page=qobject_cast<PageWidget*>(mstack->currentWidget());
    while(page!=nullptr && page->pageType()==PageWidget::PageType::SubFilePage)
        page=getParentPage(page);
    if(page==nullptr)return;
    if(!page->isTargetFilePage())return;
    //ask
    if(QMessageBox::question(this,tr("Really delete?"),tr("Really delete '%1'?").arg(page->pageTitle()))!=QMessageBox::Yes)
        return;
    //evaluate
    QString onam=page->pageName();
    if(onam.isEmpty())return;
    QStringList onl=onam.split(' ');
    if(onl.size()<2)return;
    QString pnam;
    switch(page->pageType()){
        case PageWidget::PageType::SymbolPage:
            mpackage.removeSymbol(onl[1]);
            pnam=symbolsIndexName;
            break;
        case PageWidget::PageType::FootprintPage:
            mpackage.removeFootprint(onl[1]);
            pnam=footprintsIndexName;
            break;
        case PageWidget::PageType::ModelPage:
            mpackage.removeModel(onl[1]);
            pnam=modelsIndexName;
            break;
        default:
            qDebug()<<"Ooops! Unhandled Page Type:"<<(int)page->pageType();
            return;
    }
    //delete from tree
    if(onl.size()>2)onam=onl[0]+" "+onl[1];
    deletePage(onam);
    //go to parent
    gotoPage(pnam);
}

void ChipPackageEditor::deletePage(QString pgname)
{
    //get base index
    auto idx=findIndex(mmodel,pgname);
    if(!idx.isValid())return;
    //depth first: delete children
    QStringList cnam;
    for(int r=0;r<mmodel->rowCount(idx);r++)
        cnam.append(mmodel->data(mmodel->index(r,0,idx),Qt::UserRole).toString());
    for(QString cn:cnam)
        deletePage(cn);
    //delete from tree
    mmodel->removeRow(idx.row(),idx.parent());
    //delete from stack
    for(int i=0;i<mstack->count();i++){
        auto *w=qobject_cast<PageWidget*>(mstack->widget(i));
        if(w==nullptr)continue;
        if(w->pageName()==pgname){
            mstack->removeWidget(w);
            w->deleteLater();
        }
    }
    //tell other pages
    emit pagesChanged();
}

PageWidget * ChipPackageEditor::getParentPage(PageWidget*pw)
{
    if(pw==nullptr)return nullptr;
    //find name in tree
    auto idx=findIndex(mmodel,pw->pageName());
    if(!idx.isValid())return nullptr;
    //get parent index -> page name
    QString pnam=mmodel->data(idx.parent(),Qt::UserRole).toString();
    //find corresponding widget
    for(int i=0;i<mstack->count();i++){
        auto *w=qobject_cast<PageWidget*>(mstack->widget(i));
        if(w==nullptr)continue;
        if(w->pageName()==pnam)
            return w;
    }
    //hmm...
    return nullptr;
}

void ChipPackageEditor::addPage(PageWidget*w, QString parentStr)
{
    if(w==nullptr)return;
    //add widget to stack
    const auto sidx=mstack->addWidget(w);
    mstack->setCurrentIndex(sidx);
    //add line to tree
    if(w->pageType()!=PageWidget::PageType::NullPage){
        //find parent
        QModelIndex pidx;
        if(!parentStr.isEmpty()){
            pidx=findIndex(mmodel,parentStr);
        }
        //add row
        const int r=mmodel->rowCount(pidx);
        mmodel->insertRows(r,1,pidx);
        if(mmodel->columnCount(pidx)==0)mmodel->insertColumn(0,pidx);
        auto idx=mmodel->index(r,0,pidx);
        mmodel->setData(idx,w->pageTitle());
        mmodel->setData(idx,w->pageName(),Qt::UserRole);
        //switch to item
        mtree->setCurrentIndex(idx);
    }
    //react to titleChanged
    connect(w,&PageWidget::titleChanged,this,&ChipPackageEditor::pageTitleChanged,Qt::DirectConnection);
    connect(w,&PageWidget::titleChanged,this,&ChipPackageEditor::pagesChanged,Qt::QueuedConnection);
    //setup other reactions
    w->connectMainEditor(this);
    emit pagesChanged();
    //recurse into sub-pages
    auto sp=w->createSubPages();
    for(auto*sw:sp)
        addPage(sw,w->pageName());
}

void ChipPackageEditor::pageTitleChanged(QString nt)
{
    //find page
    auto*w=qobject_cast<PageWidget*>(sender());
    if(w==nullptr)return;
    //find index
    auto idx=findIndex(mmodel,w->pageName());
    if(!idx.isValid())return;
    //set new title
    mmodel->setData(idx,nt);
}

void ChipPackageEditor::gotoPage(QString pgname)
{
    if(pgname.isEmpty())pgname=LabelPage::nullPageString;
    //set tree index
    auto idx=findIndex(mmodel,pgname);
    if(idx.isValid()){
        if(mtree->currentIndex()!=idx){
            mtree->scrollTo(idx);
            mtree->setCurrentIndex(idx);
        }
    }
    //show widget
    for(int i=0;i<mstack->count();i++){
        auto *w=qobject_cast<PageWidget*>(mstack->widget(i));
        if(w==nullptr)continue;
        if(w->pageName()==pgname){
            mstack->setCurrentIndex(i);
            return;
        }
    }
    //fall back to null page
    for(int i=0;i<mstack->count();i++){
        auto *w=qobject_cast<PageWidget*>(mstack->widget(i));
        if(w==nullptr)continue;
        if(w->pageName()==LabelPage::nullPageString){
            mstack->setCurrentIndex(i);
            return;
        }
    }
    qDebug()<<"Ooops. No Null Page found. Disco is dead.";
}


PageWidget::PageWidget(QWidget* parent)
:QWidget(parent)
{
}

bool PageWidget::isTargetFilePage() const
{
    const auto t=pageType();
    return t==PageType::SymbolPage || t==PageType::FootprintPage || t==PageType::ModelPage;
}

const QString PageWidget::nullPageString="null";
const QString PageWidget::variablePageString="variables";
const QString PageWidget::symbolPageString="symbol";
const QString PageWidget::footprintPageString="footprint";
const QString PageWidget::modelPageString="3d";
const QString PageWidget::readmePageString="readme";

const QString ChipPackageEditor::symbolsIndexName="symbols";
const QString ChipPackageEditor::footprintsIndexName="fprints";
const QString ChipPackageEditor::modelsIndexName="3ds";


LabelPage::LabelPage(QString name, QString title, QString label, QWidget* parent)
:PageWidget(parent),mname(name),mtitle(title)
{
    QHBoxLayout*hl;
    setLayout(hl=new QHBoxLayout);
    hl->addWidget(mlabel=new QLabel(label),1);
}

void LabelPage::setLabelText(QString l)
{
    mlabel->setText(l);
}

ReadmePage::ReadmePage(const Template::TemplateData&temp, QWidget* parent)
:PageWidget(parent)
{
    mtitle=tr("%1 ReadMe").arg(temp.title());
    QBoxLayout*la;
    setLayout(la=new QVBoxLayout);
    auto*text=new QTextBrowser;
    text->setSource(QUrl::fromLocalFile(temp.absoluteReadmePath()));
    la->addWidget(text,1);
}


ChipAddTargetFileDialog::ChipAddTargetFileDialog(QWidget* parent)
:QDialog(parent)
{
    QFormLayout*fl;
    setLayout(fl=new QFormLayout);
    fl->addRow(mlabel=new QLabel("..."));
    fl->addRow(tr("Template:"),mtemplate=new QComboBox);
    fl->addRow(tr("Name:"),mname=new QLineEdit);
    fl->addRow(new QLabel);
    QHBoxLayout*hl;
    fl->addRow(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("&OK")));
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")));
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);
}

void ChipAddTargetFileDialog::addItem(QString id, QString text)
{
    mtemplate->addItem(text,id);
}

void ChipAddTargetFileDialog::setLabel(QString l)
{
    mlabel->setText(l);
}

void ChipAddTargetFileDialog::setName(QString n)
{
    mname->setText(n);
}

QString ChipAddTargetFileDialog::name() const
{
    return mname->text();
}

QString ChipAddTargetFileDialog::selectedId() const
{
    return mtemplate->currentData().toString();
}


#include "moc_chippackageedit.cpp"
