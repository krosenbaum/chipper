// Chipper KiCAD symbol/footprint/3Dmodel generator
// new chip wizard
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "newchipwiz.h"

#include <QBoxLayout>
#include <QCommandLinkButton>
#include <QFile>
#include <QFileDialog>
#include <QFormLayout>
#include <QInputDialog>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QStackedLayout>

#include "chipdata.h"
#include "chipdetail.h"
#include "chippool.h"

NewChipWizard::NewChipWizard(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("Create New Chip File"));

    QStackedLayout *sl;
    setLayout(sl=new QStackedLayout);

    QWidget*w;
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QPushButton*pb;
    QCommandLinkButton*cb;
    QLineEdit*le;
    QFormLayout*fl;
    const int start=sl->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("<html><h1>Create Chip File from:</h1>")),0);
    vl->addWidget(cb=new QCommandLinkButton(tr("Empty"),tr("Create a new empty chip file.")),0);
    connect(cb,&QCommandLinkButton::clicked,this,&NewChipWizard::fromEmpty);
    vl->addWidget(cb=new QCommandLinkButton(tr("Minimal"),tr("Create a new chip file with minimal example data.")),0);
    connect(cb,&QCommandLinkButton::clicked,this,&NewChipWizard::fromMinimal);
    vl->addWidget(cb=new QCommandLinkButton(tr("Clone Loaded"),tr("Create a new chip file as a clone of a currently loaded chip.")),0);
    connect(cb,&QCommandLinkButton::clicked,this,&NewChipWizard::cloneFromLoaded);
    vl->addWidget(cb=new QCommandLinkButton(tr("Clone External"),tr("Create a new chip file as a clone of an unloaded chip file.")),0);
    connect(cb,&QCommandLinkButton::clicked,this,&NewChipWizard::cloneFromExternal);
    vl->addSpacing(10);vl->addStretch(1);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);

    const int nfile=sl->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("<html><h1>Create Chip File into:</h1>")),0);
    vl->addSpacing(10);
    vl->addWidget(new QLabel("New File Name:"),0);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(le=new QLineEdit,1);
    le->setPlaceholderText(tr("Enter File Name: *.chip"));
    le->setMinimumWidth(le->fontMetrics().averageCharWidth()*60);
    hl->addWidget(pb=new QPushButton("..."),0);
    connect(pb,&QPushButton::clicked,this,&NewChipWizard::selectNewFile);

    vl->addSpacing(10);vl->addStretch(1);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("Continue")),0);
    connect(pb,&QPushButton::clicked,this,[=]{enterNewFile(le->text());});
    connect(le,&QLineEdit::textChanged,this,[=]{pb->setEnabled(!le->text().isEmpty());});
    pb->setEnabled(false);
    hl->addWidget(pb=new QPushButton(tr("Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);

    const int cfile=sl->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("<html><h1>Create Chip File:</h1>")),0);
    vl->addSpacing(10);
    vl->addLayout(fl=new QFormLayout,0);
    QLabel*src,*tgt;
    fl->addRow(tr("Source:"),src=new QLabel);
    fl->addRow(tr("New File:"),tgt=new QLabel);
    connect(this,&NewChipWizard::gotoCreateFile,this,[=]{src->setText(mfromfile);tgt->setText(mnewfile);});
    vl->addSpacing(10);
    vl->addWidget(cb=new QCommandLinkButton(tr("Create"),tr("Create the new chip file.")),0);
    connect(cb,&QCommandLinkButton::clicked,this,&NewChipWizard::createNewFile);

    vl->addSpacing(10);vl->addStretch(1);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("Create Chip File")),0);
    connect(pb,&QPushButton::clicked,this,&NewChipWizard::createNewFile);
    hl->addWidget(pb=new QPushButton(tr("Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);

    connect(this,&NewChipWizard::gotoNewFile,sl,[=]{sl->setCurrentIndex(nfile);});
    connect(this,&NewChipWizard::gotoCreateFile,sl,[=]{sl->setCurrentIndex(cfile);});
    sl->setCurrentIndex(start);
}

void NewChipWizard::cloneFromLoaded()
{
    //construct list
    QStringList chips;
    auto &pool=ChipPool::instance();
    for(int i=0;i<pool.numChips();i++){
        auto*chip=pool.chip(i);
        chips<<tr("%1) Chip: %2; File: %3","translated format must start with %1 followed by a non-numeric char")
            .arg(i+1)
            .arg(chip->metaData().chipType())
            .arg(chip->fileName());
    }
    //ask nicely
    bool ok;
    const QString itm=QInputDialog::getItem(this,tr("Clone Chip"),tr("Select the chip that should be cloned:"),chips,0,false,&ok);
    if(!ok)return;
    //get number
    QString n;
    for(auto c:itm)if(c>='0'&&c<='9')n+=c;else break;
    if(n.isEmpty())return;
    const int num=n.toInt();
    if(num<1 || num>pool.numChips())return;
    //get chip and remember
    auto*chip=pool.chip(num-1);
    if(!chip)return;
    chip->syncFile();
    mfromfile=chip->fileNameOfState();
    mstat=OriginSelected;
    emit gotoNewFile();
}

void NewChipWizard::cloneFromExternal()
{
    QFileDialog fd(this,tr("Select Source Chip"));
    fd.setDefaultSuffix(".chip");
    fd.setFileMode(QFileDialog::ExistingFile);
    fd.setAcceptMode(QFileDialog::AcceptOpen);
    fd.setNameFilters(QStringList()<<tr("Chip Files (*.chip)")<<tr("Any Files (*)"));
    if(fd.exec()!=QDialog::Accepted)return;
    mfromfile=fd.selectedFiles().value(0);
    if(mfromfile.isEmpty())return;
    mstat=OriginSelected;
    emit gotoNewFile();
}

void NewChipWizard::setCloneFrom(ChipData*chip)
{
    if(!chip)return;
    chip->syncFile();
    mfromfile=chip->fileNameOfState();
    mstat=OriginSelected;
    emit gotoNewFile();
}

void NewChipWizard::fromEmpty()
{
    mfromfile=":/empty.chip";
    mstat=OriginSelected;
    emit gotoNewFile();
}

void NewChipWizard::fromMinimal()
{
    mfromfile=":/minimal.chip";
    mstat=OriginSelected;
    emit gotoNewFile();
}

void NewChipWizard::selectNewFile()
{
    QFileDialog fd(this,tr("Create new Chip File"));
    fd.setDefaultSuffix(".chip");
    fd.setNameFilters(QStringList()<<tr("Chip Files (*.chip)")<<tr("Any Files (*)"));
    fd.setFileMode(QFileDialog::AnyFile);
    fd.setAcceptMode(QFileDialog::AcceptSave);
    if(fd.exec()!=QDialog::Accepted)return;
    mnewfile=fd.selectedFiles().value(0);
    if(mnewfile.isEmpty())return;
    mstat=NewSelected;
    emit gotoCreateFile();
}

void NewChipWizard::enterNewFile(QString nf)
{
    if(nf.isEmpty())return;
    mnewfile=nf;
    if(!mnewfile.endsWith(".chip"))mnewfile+=".chip";
    mnewfile=QFileInfo(mnewfile).absoluteFilePath();
    mstat=NewSelected;
    emit gotoCreateFile();
}

void NewChipWizard::createNewFile()
{
    QFile::remove(mnewfile);
    if(QFile::copy(mfromfile,mnewfile)){
        mstat=Created;
        accept();
    }else{
        mstat=Failed;
        QMessageBox::warning(this,tr("Error"),tr("Unable to create new file."));
        reject();
    }
}


#include "moc_newchipwiz.cpp"
