// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include "chippackageedit.h"
#include "chipvariables.h"
#include "formula.h"
#include "templatevariables.h"

#include <QPointer>
#include <QStyledItemDelegate>

class QLabel;
class QLineEdit;
class QTableView;
class QTreeView;
class QStackedWidget;
class QStandardItemModel;
class QSpinBox;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {

class FileEditPage;

///Base class of editor pages on the package editor.
class CHIPPERGUI_EXPORT VariableEditPage:public PageWidget
{
    Q_OBJECT
    QStandardItemModel *mvarmodel;
    ChipVariables mvars;
    QPointer<FileEditPage> mfileparent;
    Formula mformula;
    QTableView*mtable;
public:
    explicit VariableEditPage(const ChipVariables&, QWidget*parent=nullptr);
    ///override this to return the unique name of the page
    virtual QString pageName()const override;
    ///override this to return the human readable title of the page
    virtual QString pageTitle()const override;

    ///override this to return the correct page type
    PageType pageType()const override;

    ///sets up reactions to higher level variable changes
    virtual void connectMainEditor(ChipPackageEditor*)override;
public slots:
    void redisplay();
private slots:
    ///helper for table delegate: retrieves variable definition
    TemplateVariables::Variable getVariableType(QString vname);
    ///helper for table delegate: retrieves enum definition
    QList<TemplateVariables::Enum> getEnumType(QString ename);
    ///helper for table delegate: retrieves variable values
    void getVariableValue(QString vname,QString&directValue,QString&hintValue);
    ///helper for table delegate: reacts to updates
    void triggerUpdate(QString vname,QString vvalue);
signals:
    void valuesUpdated();
};


///helper delegate for variable table
class ChipVariableDelegate:public QStyledItemDelegate
{
    Q_OBJECT
public:
    ///creates the delegate
    ChipVariableDelegate(QObject*parent=nullptr);
    ///create correct editor widget for the variable, deny editing non-value columns
    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    ///puts the correct value into the editor
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    ///pushes the editor data back to the model/chip
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
signals:
    ///retrieves variable definition from main widget
    TemplateVariables::Variable getVariableType(QString vname)const;
    ///retrieves enum definition from main widget
    QList<TemplateVariables::Enum> getEnumType(QString ename)const;
    ///retrieves variable values from main widget
    void getVariableValue(QString vname,QString&directValue,QString&hintValue)const;
    ///triggers an update in main widget/chip
    void triggerUpdate(QString vname,QString vvalue)const;
};


//end of namespace
}}
using namespace Chipper::GUI;
