// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Variant Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QAction>
#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTimer>
#include <QTreeView>

#include "chipdata.h"
#include "chipdetail.h"
#include "chipedit.h"
#include "chipper.h"
#include "chipvariantedit.h"
#include "chippackageedit.h"
#include "chipfileedit.h"
#include "filepinedit.h"
#include "formula.h"
#include "variableedit.h"
#include "vlabel.h"
#include "templates.h"



// ////////////////////////////////////
// Pin Table Editor

FilePinEditPage::FilePinEditPage(FileEditPage* parent)
:PageWidget(parent),mparent(parent)
{
    QHBoxLayout*hl;
    setLayout(hl=new QHBoxLayout);
    //Pins
    hl->addWidget(mtable=new QTableView,1);
    mtable->setModel(mmodel=new QStandardItemModel(this));
    mmodel->insertColumns(0,3);
    mmodel->setHorizontalHeaderLabels(QStringList()<<tr("Row","file pin table")<<tr("Pins","file pin table")<<tr("Description","file pin table"));
    mtable->verticalHeader()->hide();
    mtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mtable->setSelectionMode(QAbstractItemView::SingleSelection);
    mtable->setSelectionBehavior(QAbstractItemView::SelectRows);
    redisplay();
    mtable->resizeColumnsToContents();
    //Buttons
    hl->addSpacing(10);
    QVBoxLayout*vl;
    QPushButton*pb;
    QMenu*m;
    hl->addLayout(vl=new QVBoxLayout);
    vl->addWidget(pb=new QPushButton(tr("Edit Row")));
    connect(pb,&QPushButton::clicked,this,&FilePinEditPage::editRow);
    vl->addSpacing(10);
    vl->addWidget(pb=new QPushButton(tr("Auto Set")));
    m=new QMenu(this);
    auto tdef=mparent->target().templatePinDefinition();
    for(auto aid:tdef.fillAlgoIds()){
        auto algo=tdef.fillAlgo(aid);
        m->addAction(algo.name(),this,std::bind(&FilePinEditPage::autoFill,this,aid));
    }
    pb->setMenu(m);
    vl->addWidget(pb=new QPushButton(tr("Copy From")));
    m=new QMenu(this);
    msymmenu=m->addMenu(tr("Copy from &Symbol"));
    mfprmenu=m->addMenu(tr("Copy from &Footprint"));
    m3dmmenu=m->addMenu(tr("Copy from 3D &Model"));
    genCopyMenu();
    pb->setMenu(m);

    vl->addStretch(1);
}

void FilePinEditPage::redisplay()
{
    auto&target=mparent->target();
    auto prl=target.pinRowNames();
    mmodel->removeRows(0,mmodel->rowCount());
    mmodel->insertRows(0,prl.size());
    auto i2s=[](const QList<TargetPin>il)->QString{
        QString r;
        for(auto i:il){
            if(!i.isValid())continue;
            if(!r.isEmpty())r+=" ";
            if(i.isHidden())r+="("+i.id()+")";
            else if(i.isGap())r+="()";
            else r+=i.id();
        }
        return r;
    };
    for(int r=0;r<prl.size();r++){
        mmodel->setData(mmodel->index(r,0),prl[r]);
        mmodel->setData(mmodel->index(r,1),i2s(target.pinRow(prl[r])));
        mmodel->setData(mmodel->index(r,2),target.templatePinRow(prl[r]).text());
    }
}

QString FilePinEditPage::pageName() const
{
    return mparent->pageName()+" pins";
}

QString FilePinEditPage::pageTitle() const
{
    return tr("Pins");
}

void FilePinEditPage::editRow()
{
    //get row ID
    auto idx=mtable->currentIndex();
    if(!idx.isValid())return;
    const QString rowid=mmodel->data(idx.siblingAtColumn(0)).toString();
    if(rowid.isEmpty())return;
    //edit
    PinRowEditor pre(this,mparent->target(),rowid);
    if(pre.exec()!=QDialog::Accepted)return;
    //display
    redisplay();
}

///helper: returns a comparison algo for a "sort-by" type fill algorithm step
std::function<bool(const TargetPinExt&,const TargetPinExt&)> sortByAlgo(TemplatePins::FillStep::SortBy by,const ChipFileTarget&tgt)
{
    switch(by){
        case TemplatePins::FillStep::SortBy::Id:
            return [](const TargetPinExt&p1,const TargetPinExt&p2)->bool{return p1.id().compare(p2.id(),Qt::CaseInsensitive)<0;};
        case TemplatePins::FillStep::SortBy::Name:
            return [](const TargetPinExt&p1,const TargetPinExt&p2)->bool{return p1.name().compare(p2.name(),Qt::CaseInsensitive)<0;};
        case TemplatePins::FillStep::SortBy::Table:{
            auto pl=tgt.variant().pinIds();
            return [=](const TargetPinExt&p1,const TargetPinExt&p2)->bool{
                for(const auto&p:pl){
                    if(p==p1.id())return true;//p1 first: p1 is "less than" in table order
                    if(p==p2.id())return false;//p2 first: p1 is "greater than" in table order
                }
                //should not happen, but anyway...
                return false;
            };
        }
    }
    //should be unreachable
    qDebug()<<"Ooops! Unknown sort-by algorithm"<<(int)by;
    return [=](const TargetPinExt&,const TargetPinExt&)->bool{return false;};
}

static inline std::function<bool(const TargetPinExt&)> filterAlgo(TemplatePins::FillStep::FilterBy by,QString text)
{
    switch(by){
        case TemplatePins::FillStep::FilterBy::Type:{
            auto tl=ChipVariant::stringToPinTypeList(text);
            return [=](const TargetPinExt&p)->bool{return tl.contains(p.type());};
        }
        case TemplatePins::FillStep::FilterBy::Function:{
            auto fl=ChipPin::stringToFunctionList(text);
            return [=](const TargetPinExt&p)->bool{return fl.contains(p.function());};
        }
        case TemplatePins::FillStep::FilterBy::Name:
        case TemplatePins::FillStep::FilterBy::NameRegex:{
            QRegularExpression regex(by==TemplatePins::FillStep::FilterBy::NameRegex ? text : QRegularExpression::wildcardToRegularExpression(text));
            return [=](const TargetPinExt&p)->bool{return regex.match(p.name()).hasMatch();};
        }
    }
    //should be unreachable
    qDebug()<<"Ooops! Unknown filter algorithm! Not filtering for"<<(int)by<<text;
    return [](const TargetPinExt&)->bool{return false;};
}

static inline void autoHidePins(QList<TargetPinExt>&pl,TemplatePins::FillStep::AutoHide ah)
{
    //check type of auto-hide algo
    std::function<bool(const TargetPinExt&,const TargetPinExt&)> compare;
    switch(ah){
        case TemplatePins::FillStep::AutoHide::All:
            compare=[](const TargetPinExt&,const TargetPinExt&)->bool{return true;};
            break;
        case TemplatePins::FillStep::AutoHide::Name:
            compare=[](const TargetPinExt&p1,const TargetPinExt&p2)->bool{return p1.name()==p2.name();};
            break;
        case TemplatePins::FillStep::AutoHide::Type:
            compare=[](const TargetPinExt&p1,const TargetPinExt&p2)->bool{return p1.type()==p2.type();};
            break;
        case TemplatePins::FillStep::AutoHide::None:
            //nothing to do
            return;
    }
    if(!compare){
        qDebug()<<"Ooops. Unhandled auto-hide type"<<(int)ah;
        return;
    }
    //run through list
    for(int i=1;i<pl.size();i++)
        if(compare(pl[i-1],pl[i]))
            pl[i].hide();
}

void FilePinEditPage::autoFill(QString aid)
{
    //ask if already filled
    auto target=mparent->target();
    int cnt=0;
    for(const auto&rn:target.pinRowNames())
        cnt += target.pinRow(rn).size();
    if(cnt>0){
        if(QMessageBox::question(this,tr("Warning"),tr("This will overwrite current pin assignments.\nContinue anyway?"))!=QMessageBox::Yes)
            return;
    }
    //prepare target
    for(const auto&rn:target.pinRowNames())
        target.setPinRow(rn,QList<TargetPin>());
    //load autofill algo
    auto algo=target.templatePinDefinition().fillAlgo(aid);
    //create formula env
    Formula form;
    form.setTarget(target);
    form.reCalculate();
    //prepare pool
    auto pool=target.makePinPool();
    //run through steps
    for(const auto &step:algo){
        switch(step.stepType()){
            case TemplatePins::FillStep::StepType::Sort:
                std::sort(pool.begin(),pool.end(),sortByAlgo(step.sortBy(),target));
                break;
            case TemplatePins::FillStep::StepType::Filter:{
                //filter...
                auto filter=filterAlgo(step.filterBy(),step.nameFilter());
                QList<TargetPinExt> extract,npool;
                for(const auto&pin:pool)
                    if(filter(pin))extract.append(pin);
                    else npool.append(pin);
                pool=npool;
                //auto-hide
                autoHidePins(extract,step.autoHide());
                //assign
                target.setPinRow(step.intoRow(),ChipFileTarget::pinList(extract));
                break;
            }
            case TemplatePins::FillStep::StepType::Take:{
                //set formula pool
                form.setVariable("pool",ChipFileTarget::pinListToVariant(pool));
                //execute & check type
                auto result=form.evaluate(step.takeFormula());
                if(result.canConvert<ELAM::Exception>()){
                    qDebug()<<"Warning: expression on pin pool yielded an error.";
                    qDebug()<<"    Expression:"<<step.takeFormula();
                    qDebug()<<"    Error:"<<result.value<ELAM::Exception>();
                    break;
                }
                if(!result.canConvert<QVariantList>()){
                    qDebug()<<"Warning: expression on pin pool did not yield a list of pins.";
                    qDebug()<<"    Expression:"<<step.takeFormula();
                    qDebug()<<"    Result type:"<<result.typeName();
                    break;
                }
                //filter pool
                QList<TargetPinExt> extract;
                for(auto var:result.value<QVariantList>()){
                    if(!var.canConvert<TargetPinExt>()){
                        qDebug()<<"Warning: expression on pin pool yielded non-pin elements.";
                        qDebug()<<"    Expression:"<<step.takeFormula();
                        qDebug()<<"    Element type:"<<result.typeName();
                        continue;
                    }
                    auto pin=var.value<TargetPinExt>();
                    extract.append(pin);
                    pool.removeAll(pin);
                }
                //hide & assign
                autoHidePins(extract,step.autoHide());
                target.setPinRow(step.intoRow(),ChipFileTarget::pinList(extract));
                break;
            }
        }
    }
    //update display
    redisplay();
}

///helper: returns true if there is at least one overlap in pin row names or map hints, i.e. if at least some pins can be copied
static inline bool canRowMap(const ChipFileTarget&target, const ChipFileTarget&source)
{
    //collect source names
    QStringList snames,tnames;
    auto st=source.templatePinDefinition();
    for(auto rn:st.rowNames()){
        snames<<rn;
        snames<<st.rowDefinition(rn).mapHints();
    }
    //collect target names
    auto tt=target.templatePinDefinition();
    for(auto rn:tt.rowNames()){
        tnames<<rn;
        tnames<<tt.rowDefinition(rn).mapHints();
    }
    //check that there is an intersection
    for(auto rn:snames)
        if(tnames.contains(rn))
            return true;
    return false;
}

void FilePinEditPage::copyFrom(ChipFileTarget::FileType ft, QString uid)
{
    //find source
    auto target=mparent->target();
    auto pack=target.package();
    switch(ft){
        case ChipFileTarget::FileType::Symbol:
            copyFrom(pack.symbol(uid));
            break;
        case ChipFileTarget::FileType::Footprint:
            copyFrom(pack.footprint(uid));
            break;
        case ChipFileTarget::FileType::Model3D:
            copyFrom(pack.model(uid));
            break;
        default:
            qDebug()<<"Ooops. FilePinEditPage::copyFrom called with invalid target type"<<(int)ft;
            break;
    }
}

///helper: tries very desperately to map a source row name to a target row name
/// \param target the target symbol/footprint/model for which we seek the appropriate row name
/// \param source the source symbol/footprint/model from which we want to copy
/// \param srow the source row name that we want to copy
/// \return returns the name of a target row that maps to srow (tries to find best match first) or empty string if none is found
static inline QString mapSourceRow(const ChipFileTarget&target, const ChipFileTarget&source, QString srow)
{
    //get source names
    QStringList snames;
    snames<<srow<<source.templatePinRow(srow).mapHints();
    //go through target: direct hit
    if(target.pinRowNames().contains(srow))
        return srow;
    //go through target: source contains a hint for target name
    for(auto rn:target.pinRowNames()){
        if(snames.contains(rn))return rn;
    }
    //go through target: target contains a hint for source name
    for(auto rn:target.pinRowNames()){
        if(target.templatePinRow(rn).mapHints().contains(srow))
            return rn;
    }
    //go through target: desperately compare hints
    for(auto rn:target.pinRowNames()){
        for(auto an:target.templatePinRow(rn).mapHints())
            if(snames.contains(an))return rn;
    }
    //dang! not found
    return QString();
}

void FilePinEditPage::copyFrom(const ChipFileTarget& source)
{
    auto target=mparent->target();
    //check source has any copyable pins and whether target will be overwritten
    int cnt=0;
    bool over=false;
    for(auto rn:source.pinRowNames()){
        const QString trn=mapSourceRow(target,source,rn);
        if(!trn.isEmpty()){
            cnt+=source.pinRow(rn).size();
            over|=target.pinRow(trn).size()!=0;
        }
    }
    if(cnt==0){
        QMessageBox::warning(this,tr("Warning"),tr("There are no copy-able pins in %1, not trying.").arg(source.name()));
        return;
    }
    if(over){
        if(QMessageBox::question(this,tr("Warning"),tr("This will overwrite at least some rows. Do it anyway?"))!=QMessageBox::Yes)
            return;
    }
    //now actually copy
    for(auto rn:source.pinRowNames()){
        const QString trow=mapSourceRow(target,source,rn);
        if(trow.isEmpty())continue;
        target.setPinRow(trow,source.pinRow(rn));
    }
    //update display
    redisplay();
}

void FilePinEditPage::connectMainEditor(ChipPackageEditor*cpe)
{
    connect(cpe,&ChipPackageEditor::pagesChanged,this,&FilePinEditPage::genCopyMenu,Qt::QueuedConnection);
    connect(cpe->variantEditor(),&ChipVariantEditor::pinNumberChanged,this,&FilePinEditPage::pinNumberChanged,Qt::QueuedConnection);
}

void FilePinEditPage::genCopyMenu()
{
    //clear
    msymmenu->clear();
    mfprmenu->clear();
    m3dmmenu->clear();
    //rework
    auto target=mparent->target();
    auto pack=target.package();
    const auto mytype=target.fileType();
    const QString myid=target.uid();
    //symbols
    bool en=false;
    for(auto sid:pack.symbolIds()){
        if(mytype==ChipFileTarget::FileType::Symbol && myid==sid)
            continue;
        en=true;
        auto sym=pack.symbol(sid);
        if(!canRowMap(target,sym))continue;
        msymmenu->addAction(sym.name(),this,std::bind(qOverload<ChipFileTarget::FileType,QString>(&FilePinEditPage::copyFrom),this,ChipFileTarget::FileType::Symbol,sym.uid()));
    }
    msymmenu->setEnabled(en);
    //footprints
    en=false;
    for(auto fid:pack.footprintIds()){
        if(mytype==ChipFileTarget::FileType::Footprint && myid==fid)
            continue;
        en=true;
        auto fpr=pack.footprint(fid);
        if(!canRowMap(target,fpr))continue;
        mfprmenu->addAction(fpr.name(),this,std::bind(qOverload<ChipFileTarget::FileType,QString>(&FilePinEditPage::copyFrom),this,ChipFileTarget::FileType::Footprint,fpr.uid()));
    }
    mfprmenu->setEnabled(en);
    //3D models
    en=false;
    for(auto mid:pack.modelIds()){
        if(mytype==ChipFileTarget::FileType::Model3D && myid==mid)
            continue;
        en=true;
        auto m3d=pack.model(mid);
        if(!canRowMap(target,m3d))continue;
        m3dmmenu->addAction(m3d.name(),this,std::bind(qOverload<ChipFileTarget::FileType,QString>(&FilePinEditPage::copyFrom),this,ChipFileTarget::FileType::Model3D,m3d.uid()));
    }
    m3dmmenu->setEnabled(en);
}

void FilePinEditPage::pinNumberChanged(QString oldNum, QString newNum)
{
    if(oldNum.isEmpty() || oldNum==newNum)return;
    //go through rows
    auto target=mparent->target();
    for(const auto&rn:target.pinRowNames()){
        QList<TargetPin>npins;
        bool alt=false;
        for(auto pin:target.pinRow(rn)){
            if(pin.id()==oldNum){
                if(!newNum.isEmpty())
                    npins.append(target.makePin(newNum,pin.isHidden()));
                alt=true;
            }else
                npins.append(pin);
        }
        if(alt)target.setPinRow(rn,npins);
    }
    //update display
    redisplay();
}






// ////////////////////////////////////
// Pin Row Editor Dialog


PinRowEditor::PinRowEditor(QWidget* parent, const ChipFileTarget& target, QString rowid)
:QDialog(parent),mtarget(target),mrowid(rowid)
{
    setWindowTitle(tr("Edit Pin Row: %1 (%2)").arg(target.name()).arg(rowid));
    QVBoxLayout*vl,*vl2;
    QHBoxLayout*hl;
    QGridLayout*gl;
    QPushButton*pb;
    setLayout(vl=new QVBoxLayout);
    //top explanations
    vl->addWidget(new QLabel(tr("Pins for row '%1' of Package '%2':").arg(rowid).arg(target.name())));
    vl->addSpacing(10);
    //main pin table
    vl->addLayout(gl=new QGridLayout,1);
    gl->addWidget(new QLabel(tr("Source Pins")),0,0,Qt::AlignHCenter);
    gl->addWidget(new QLabel(tr("Assign")),0,1,Qt::AlignHCenter);
    gl->addWidget(new QLabel(tr("Row Pins")),0,2,Qt::AlignHCenter);
    gl->addWidget(new QLabel(tr("Organize")),0,3,Qt::AlignHCenter);
    gl->addWidget(mallpins=new QTableView,1,0);
    mallpins->setModel(mallmodel=new QStandardItemModel(this));
    mallpins->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mallpins->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mallpins->setSelectionBehavior(QAbstractItemView::SelectRows);
    mallpins->verticalHeader()->hide();
    gl->addLayout(vl2=new QVBoxLayout,1,1);
    vl2->addStretch(1);
    vl2->addWidget(pb=new QPushButton(tr("Add to Row")));
    pb->setIcon(QIcon(":/arrowlongrightgreen.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::addSelected);
    vl2->addWidget(pb=new QPushButton(tr("Remove")));
    pb->setIcon(QIcon(":/arrowlongleftred.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::removeSelected);
    vl2->addStretch(1);
    gl->addWidget(mrowpins=new QTableView,1,2);
    mrowpins->setModel(mrowmodel=new QStandardItemModel(this));
    mrowpins->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mrowpins->setSelectionMode(QAbstractItemView::ContiguousSelection);
    mrowpins->setSelectionBehavior(QAbstractItemView::SelectRows);
    mrowpins->verticalHeader()->hide();
    fillTables();
    gl->addLayout(vl2=new QVBoxLayout,1,3);
    vl2->addWidget(pb=new QPushButton(tr("Move to Start")));
    pb->setIcon(QIcon(":/arrowtop.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::moveTop);
    vl2->addWidget(pb=new QPushButton(tr("Move Up")));
    pb->setIcon(QIcon(":/arrowup.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::moveUp);
    vl2->addWidget(pb=new QPushButton(tr("Move Down")));
    pb->setIcon(QIcon(":/arrowdown.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::moveDown);
    vl2->addWidget(pb=new QPushButton(tr("Move to End")));
    pb->setIcon(QIcon(":/arrowbottom.png"));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::moveBottom);
    vl2->addSpacing(15);
    vl2->addWidget(pb=new QPushButton(tr("Hide")));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::hidePins);
    vl2->addWidget(pb=new QPushButton(tr("Unhide")));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::unhidePins);
    vl2->addSpacing(15);
    vl2->addWidget(pb=new QPushButton(tr("Insert Gap")));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::addGap);
    vl2->addStretch(1);
    //buttons
    vl->addSpacing(15);
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("&Save")));
    connect(pb,&QPushButton::clicked,this,&PinRowEditor::savePins);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")));
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);
    setSizeGripEnabled(true);
    QSettings set;
    set.beginGroup("PinRowEditor");
    if(set.contains("winSize"))
        resize(set.value("winSize").toSize());
}

PinRowEditor::~PinRowEditor()
{
    QSettings set;
    set.beginGroup("PinRowEditor");
    set.setValue("winSize",size());
}

void PinRowEditor::savePins()
{
    //retrieve table
    QList<TargetPin>pins;
    for(int r=0;r<mrowmodel->rowCount();r++){
        const auto idx=mrowmodel->index(r,2);
        const QString id=mrowmodel->data(idx,Qt::UserRole+1).toString();
        const bool ishidden=mrowmodel->data(idx,Qt::UserRole).toBool();
        if(id.isEmpty())pins.append(mtarget.makeGap());
        else pins.append(mtarget.makePin(id,ishidden));
    }
    //store
    mtarget.setPinRow(mrowid,pins);
    //done
    accept();
}

void PinRowEditor::fillTables()
{
    //left side: variant pins
    auto var=mtarget.variant();
    const auto vpins=var.pinIds();
    mallmodel->insertColumns(0,3);
    mallmodel->insertRows(0,vpins.size());
    mallmodel->setHorizontalHeaderLabels(QStringList()<<tr("Number","variant pin")<<tr("Type","variant pin")<<tr("Name","variant pin"));
    for(int i=0;i<vpins.size();i++){
        auto pin=var.pin(vpins[i]);
        mallmodel->setData(mallmodel->index(i,0),vpins[i]);
        mallmodel->setData(mallmodel->index(i,1),pin.typeStr());
        mallmodel->setData(mallmodel->index(i,2),pin.name());
    }
    //right side: row pins
    const auto rpins=mtarget.pinRow(mrowid);
    mrowmodel->insertColumns(0,3);
    mrowmodel->setHorizontalHeaderLabels(QStringList()<<tr("Number","row pin")<<tr("Name","row pin")<<tr("Status","row pin"));
    for(const auto&pin:rpins){
        //validate
        auto vpin=var.pin(pin.id());
        if(!pin.isGap()&&!vpin.isValid())continue;
        //insert
        const int r=mrowmodel->rowCount();
        mrowmodel->insertRows(r,1);
        mrowmodel->setData(mrowmodel->index(r,0),pin.id());
        mrowmodel->setData(mrowmodel->index(r,1),pin.isGap()?tr("(Gap)"):vpin.name());
        if(!pin.isGap())
            mrowmodel->setData(mrowmodel->index(r,2),pin.isHidden()?tr("Hidden"):tr("Visible"));
        mrowmodel->setData(mrowmodel->index(r,2),pin.isHidden(),Qt::UserRole);
        mrowmodel->setData(mrowmodel->index(r,2),pin.id(),Qt::UserRole+1);
    }
}

void PinRowEditor::addSelected()
{
    //get selection
    QStringList pins;
    for(auto idx:mallpins->selectionModel()->selectedRows(0))
        pins<<mallmodel->data(idx).toString();
    if(pins.size()==0)return;
    //insert position
    int first=mrowmodel->rowCount();
    for(auto idx:mrowpins->selectionModel()->selectedRows())
        if(idx.row()<first)
            first=idx.row();
    //insert new pins
    mrowmodel->insertRows(first,pins.size());
    auto var=mtarget.variant();
    for(auto pid:pins){
        auto vpin=var.pin(pid);
        mrowmodel->setData(mrowmodel->index(first,0),pid);
        mrowmodel->setData(mrowmodel->index(first,1),vpin.name());
        mrowmodel->setData(mrowmodel->index(first,2),tr("Visible"));
        mrowmodel->setData(mrowmodel->index(first,2),false,Qt::UserRole);
        mrowmodel->setData(mrowmodel->index(first,2),pid,Qt::UserRole+1);
        first++;
    }
}

void PinRowEditor::addGap()
{
    //insert position
    int first=mrowmodel->rowCount();
    for(auto idx:mrowpins->selectionModel()->selectedRows())
        if(idx.row()<first)
            first=idx.row();
    //insert new pins
    mrowmodel->insertRows(first,1);
    mrowmodel->setData(mrowmodel->index(first,0),QString());
    mrowmodel->setData(mrowmodel->index(first,1),tr("(Gap)"));
    mrowmodel->setData(mrowmodel->index(first,2),false,Qt::UserRole);
    mrowmodel->setData(mrowmodel->index(first,2),QString(),Qt::UserRole+1);
}

void PinRowEditor::removeSelected()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows();
    if(sel.size()==0)return;
    int first=sel[0].row();
    for(auto idx:sel)if(idx.row()<first)first=idx.row();
    //delete from model
    mrowmodel->removeRows(first,sel.size());
}

void PinRowEditor::hidePins()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows(2);
    //hide
    for(auto idx:sel){
        //check it is a pin
        if(mrowmodel->data(idx,Qt::UserRole+1).toString().isEmpty())continue;
        //hide it
        mrowmodel->setData(idx,true,Qt::UserRole);
        mrowmodel->setData(idx,tr("Hidden"));
    }
}

void PinRowEditor::unhidePins()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows(2);
    //hide
    for(auto idx:sel){
        //check it is a pin
        if(mrowmodel->data(idx,Qt::UserRole+1).toString().isEmpty())continue;
        //hide it
        mrowmodel->setData(idx,false,Qt::UserRole);
        mrowmodel->setData(idx,tr("Visible"));
    }
}

void PinRowEditor::moveTop()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows();
    if(sel.size()==0)return;
    int first=sel[0].row();
    int last=first;
    for(auto idx:sel){
        if(idx.row()<first)first=idx.row();
        if(idx.row()>last)last=idx.row();
    }
    if(first==0)return;
    //remove rows
    QList<QList<QStandardItem*>>taken;
    for(;last>=first;last--)
        taken.append(mrowmodel->takeRow(first));
    const int count=taken.size();
    //move to top
    for(int r=0;taken.size()>0;r++)
        mrowmodel->insertRow(r,taken.takeFirst());
    //re-select
    mrowpins->selectionModel()->select(
        QItemSelection(mrowmodel->index(0,0), mrowmodel->index(count-1,0)),
        QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect
    );
}

void PinRowEditor::moveBottom()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows();
    if(sel.size()==0)return;
    int first=sel[0].row();
    int last=first;
    for(auto idx:sel){
        if(idx.row()<first)first=idx.row();
        if(idx.row()>last)last=idx.row();
    }
    if(last==(mrowmodel->rowCount()-1))return;
    //remove rows
    QList<QList<QStandardItem*>>taken;
    for(;last>=first;last--)
        taken.append(mrowmodel->takeRow(first));
    const int count=taken.size();
    //move to bottom
    while(taken.size()>0)
        mrowmodel->insertRow(mrowmodel->rowCount(),taken.takeFirst());
    //re-select
    const int rc=mrowmodel->rowCount();
    mrowpins->selectionModel()->select(
        QItemSelection(mrowmodel->index(rc-count,0), mrowmodel->index(rc-1,0)),
        QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect
    );
}

void PinRowEditor::moveUp()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows();
    if(sel.size()==0)return;
    int first=sel[0].row();
    int last=first;
    for(auto idx:sel){
        if(idx.row()<first)first=idx.row();
        if(idx.row()>last)last=idx.row();
    }
    if(first==0)return;
    //remove previous row
    auto taken=mrowmodel->takeRow(first-1);
    //insert behind
    mrowmodel->insertRow(last,taken);
    //re-select
    mrowpins->selectionModel()->select(
        QItemSelection(mrowmodel->index(first-1,0), mrowmodel->index(last-1,0)),
        QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect
    );
}

void PinRowEditor::moveDown()
{
    //get selection
    auto sel=mrowpins->selectionModel()->selectedRows();
    if(sel.size()==0)return;
    int first=sel[0].row();
    int last=first;
    for(auto idx:sel){
        if(idx.row()<first)first=idx.row();
        if(idx.row()>last)last=idx.row();
    }
    if(last==(mrowmodel->rowCount()-1))return;
    //remove previous row
    auto taken=mrowmodel->takeRow(last+1);
    //insert behind
    mrowmodel->insertRow(first,taken);
    //re-select
    mrowpins->selectionModel()->select(
        QItemSelection(mrowmodel->index(first+1,0), mrowmodel->index(last+1,0)),
        QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect
    );
}






// ////////////////////////////////////
// MOC

#include "moc_filepinedit.cpp"
