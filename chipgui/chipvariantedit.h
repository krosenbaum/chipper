// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QTabWidget>
#include <QPointer>
#include <QSplitter>

#include "chipdetail.h"
#include "chipvariant.h"

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLabel;
class QLineEdit;
class QTableView;
class QTreeView;
class QStackedWidget;
class QStandardItemModel;
class QSpinBox;
class QTabWidget;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {
class ChipEditor;

///Chip Variant Editor Tab class. Sub-Tab of ChipEditor.
class CHIPPERGUI_EXPORT ChipVariantEditor:public QWidget
{
    Q_OBJECT
    QPointer<ChipEditor>mparent;
    QPointer<Chip::ChipData>mchip;
    ChipVariant mvariant;

    QLineEdit*mname;
    QSpinBox*mnumpin;
    QTableView*mpintab;
    QStandardItemModel*mpinmodel;
    QTabWidget*mtab;

    ///helper: creates the main sub-tab of the variant
    QWidget* createMainTab();
public:
    ///instantiate editor tab, parameters are mandatory
    ChipVariantEditor(ChipEditor*parent,QString variant);
    ~ChipVariantEditor();

    ///returns the chip this refers to.
    Chip::ChipData*chip()const;

    ///true if it is currently in a package tab -> appropriate menu items should be available
    bool isInPackage()const;

    ///returns the variant it is editing
    ChipVariant variant()const{return mvariant;}

    ///returns the currently selected target file type or empty string
    QString selectedFileType();

public slots:
    ///adds a package to this variant
    void addPackage();
    ///deletes a package from this variant
    void deletePackage();
    ///delete target file from package
    void deleteChipFile();
    ///add a target file to package
    void addChipFile(int type);

private slots:
    ///opens a package tab
    int openPackageTab(QString id);
    ///reacts to changes in uplevel pins
    void chipPinChanged(QString,QString);
    ///updates the status column
    void updateStatus();
    ///refresh pin list
    void refreshPins();
    ///delete a pin
    void removePin();
    ///move pin one line up/down
    void movePin(bool isUp);
    ///update window title, so the tab title changes
    void updateTitle();

signals:
    ///emitted if the user wants to see the template
    void showTemplate(QString);
    ///emitted when any value changes that may need a recalculation of variables
    void propertyChanged();
    ///emitted when a property on chip level changes
    void chipPropertyChanged();
    ///emitted when any pin number changes, which may need recalculation of target files
    ///\param oldNum - the old pin number (or empty if it is a new pin)
    ///\param newNum - the new pin number (or empty if the pin was deleted)
    void pinNumberChanged(QString oldNum,QString newNum);
};

//end of namespace
}}
using namespace Chipper::GUI;
