// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Editor Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QTabWidget>
#include <QPointer>
#include <QDialog>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLineEdit;
class QTableView;
class QStandardItemModel;
class QTreeView;
class QTextEdit;

namespace Chipper {

namespace Chip {
class ChipData;
class ChipPin;
}

namespace GUI {

///Chip Editor Tab class.
class CHIPPERGUI_EXPORT ChipEditor:public QTabWidget
{
    Q_OBJECT
    ///pointer to the chip that is being edited
    QPointer<Chip::ChipData>mchip;
    //editors
    QLineEdit *mmanu,*mtype,*mdsheet,*mauth,*mcopy,*mlic,*msym,*mfoot,*m3dmod,*mkeyw,*mdescr,*mrefpfx;
    QTreeView *mpins;
    QStandardItemModel*mpinmodel;
    QTextEdit*mnotes;
    bool mnoteedit=false,mnotechanged=false;
public:
    ///instantiates a new editor tab
    explicit ChipEditor(Chip::ChipData*chip,QWidget*parent=nullptr);
    ///deletes the tab and saves data to disk
    ~ChipEditor();

    ///returns a pointer to the actual chip data
    Chip::ChipData*chip()const;

    ///true if a variant tab is currently open -> variant related menu items should be available
    bool isInVariant()const;
    ///true if a package tab is currently open -> package related menu items should be available
    bool isInPackage()const;
    ///returns the type of target file that is currently selected
    QString selectedFileType();
private slots:
    ///used internally whenever the chip name changes to update the tab title
    void updateTitle();
    ///used internally to save notes
    void saveNotes();
    ///reloads all pins
    void reloadPins();
    ///add a pin
    void addPin();
    ///add a function
    void addPinAlt();
    ///edit pin or function
    void editPinOrAlt();
    ///delete pin or function
    void deletePinOrAlt();
    ///show pin context menu
    void pinContextMenu(const QPoint&);
    ///open variant tab, returns the index of the tab
    int openVariantTab(QString);
public slots:
    ///add a variant to the chip
    void addVariant();
    ///add a package to the chip (to the currently open variant)
    void addPackage();
    ///delete a variant from the chip
    void deleteVariant();
    ///delete a package from the chip (the currently open variant)
    void deletePackage();
    ///delete target file from package
    void deleteChipFile();
    ///add a target file to package
    void addChipFile(int type);
signals:
    ///emitted when the user wants to see details about the template
    void showTemplate(QString);
    ///emitted whenever a property has changed
    void propertyChanged();
    ///emitted whenever a pin has changed
    ///
    ///if oldName and newName are identical then some other property of the pin has changed
    ///\param oldName old pin name (or empty if it was added)
    ///\param newName new pin name (or empty if it was deleted)
    void pinChanged(QString oldName,QString newName);

    ///signal to parent that the current tab has changed and a menu change may be necessary
    void tabChanged();
private:
    ///\internal helper to create the main tab, used from constructor
    QWidget* createMainTab();
    ///\internal helper to create the target files tab, used from constructor
    QWidget* createTargetTab();
    ///\internal helper to create the pin table tab, used from constructor
    QWidget* createPinTab();
    ///\internal helper to create notes tab, used from constructor
    QWidget* createNotesTab();
    ///\internal returns help text for target config
    QString targetHelpText();
};

//end of namespace
}}
using namespace Chipper::GUI;
