// Chipper KiCAD symbol/footprint/3Dmodel generator
// new chip wizard
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {

///wizard dialog when user wants to create a new chip file
class CHIPPERGUI_EXPORT NewChipWizard:public QDialog
{
    Q_OBJECT

public:
    ///instantiates the wizard
    explicit NewChipWizard(QWidget*parent);

    ///returns the file name of the new chip file
    QString fileName()const{return mnewfile;}

    ///wizard status
    enum Status {
        ///initial wizard state
        Initialized,
        ///origin of the new file has been selected
        OriginSelected,
        ///new file name has been selected
        NewSelected,
        ///new file has been created
        Created,
        ///dialog has failed or was aborted
        Failed
    };
    ///returns the current status of the wizard
    Status status()const{return mstat;}
    ///returns true if/when the new file has been created
    bool fileCreated()const{return mstat==Created;}

public slots:
    ///initializes the wizard to clone the new file from an existing chip
    void setCloneFrom(Chip::ChipData*);

private slots:
    ///internal: file dialog to enter new file name
    void selectNewFile();
    ///internal: enter new file name manually without file dialog
    void enterNewFile(QString);
    ///internal: select an already loaded chip to clone
    void cloneFromLoaded();
    ///internal: file dialog to select a chip file to clone
    void cloneFromExternal();
    ///internal: make the new file a copy of an "empty" chip file (valid XML, no real content)
    void fromEmpty();
    ///internal: make the new file a copy of a minimal example
    void fromMinimal();
    ///internal: actually create the new file
    void createNewFile();

signals:
    ///internal: switch to the new file page
    void gotoNewFile();
    ///internal:  switch to the create file page
    void gotoCreateFile();

private:
    QString mnewfile,mfromfile;
    Status mstat=Initialized;
};


//end of namespace
}}
using namespace Chipper::GUI;
