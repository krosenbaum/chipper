// Chipper KiCAD symbol/footprint/3Dmodel generator
// Chip Variant Editor Widget
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QComboBox>
#include <QDebug>
#include <QFormLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QStackedWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTreeView>
#include <QDoubleValidator>
#include <QIntValidator>
#include <QLocale>

#include "chipdata.h"
#include "chipdetail.h"
#include "chipedit.h"
#include "chipfileedit.h"
#include "chipper.h"
#include "chipvariantedit.h"
#include "chippackageedit.h"
#include "vlabel.h"
#include "templates.h"
#include "templatevariables.h"
#include "variableedit.h"


// ////////////////////////////////////
// Extend Validator to accept empty values

class EmptyIntValidator:public QIntValidator
{
public:
    EmptyIntValidator(QObject*p):QIntValidator(p){}

    virtual QValidator::State validate(QString &input, int &pos) const override
    {
        if(input.isEmpty())return QValidator::Acceptable;
        return QIntValidator::validate(input,pos);
    }
};

class EmptyDoubleValidator:public QDoubleValidator
{
public:
    EmptyDoubleValidator(QObject*p):QDoubleValidator(p){}

    virtual QValidator::State validate(QString &input, int &pos) const override
    {
        if(input.isEmpty())return QValidator::Acceptable;
        return QDoubleValidator::validate(input,pos);
    }
};

// ////////////////////////////////////
// Handle Variable Table

ChipVariableDelegate::ChipVariableDelegate(QObject* parent)
:QStyledItemDelegate(parent)
{
}

QWidget * ChipVariableDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    if(index.column()!=1)return nullptr;
    //get vname
    const QString vname=index.model()->data(index.sibling(index.row(),0)).toString();
    //get type
    auto vtype=getVariableType(vname);
    auto etype=getEnumType(vtype.enumReference());
    switch(vtype.variableType()){
        case TemplateVariables::VarType::Enum:{
            auto*cb=new QComboBox(parent);
            cb->setEditable(false);
            for(auto e:etype)
                cb->addItem(e.displayString(),e.value());
            return cb;
        }
        case TemplateVariables::VarType::Float:{
            if(etype.size()<1){
                //float without enum defaults
                auto*le=new QLineEdit(parent);
                auto*val=new EmptyDoubleValidator(le);
                auto s2d=[](QString in,double def=0.0)->double{
                    bool b=false;
                    double r=in.toDouble(&b);
                    if(!b)r=def;
                    return r;
                };
                val->setNotation(QDoubleValidator::StandardNotation);
                val->setRange(s2d(vtype.minimumValue()),s2d(vtype.maximumValue(),10000.),2);
                le->setValidator(val);
                return le;
            }
            //float with enum defaults
            auto*cb=new QComboBox(parent);
            cb->setEditable(true);
            cb->setInsertPolicy(QComboBox::NoInsert);
            for(auto e:etype)
                cb->addItem(e.displayString(),e.value());
            connect(cb,qOverload<int>(&QComboBox::currentIndexChanged),this,[cb](int i){
                if(i==0)cb->setEditText(QString());
                else cb->setEditText(QLocale().toString(cb->itemData(i).toDouble()));
            });
            return cb;
        }
        case TemplateVariables::VarType::Int:{
            auto*le=new QLineEdit(parent);
            auto*val=new EmptyIntValidator(le);
            auto s2i=[](QString in,long long def=0)->double{
                bool b=false;
                auto r=in.toLongLong(&b);
                if(!b)r=def;
                return r;
            };
            val->setRange(s2i(vtype.minimumValue()),s2i(vtype.maximumValue(),10000.));
            le->setValidator(val);
            return le;
        }
        default:
            return new QLineEdit(parent);
    }
}

void ChipVariableDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    if(index.column()!=1)return;
    //get vname
    const QString vname=index.model()->data(index.sibling(index.row(),0)).toString();
    //get values
    QString dval,hval;
    getVariableValue(vname,dval,hval);
    //check editor type
    auto*le=qobject_cast<QLineEdit*>(editor);
    if(le!=nullptr){
        le->setText(dval);
        le->setPlaceholderText(hval);
        return;
    }
    auto*cb=qobject_cast<QComboBox*>(editor);
    if(cb!=nullptr){
        cb->insertItem(0,tr("Default"),QString());
        if(cb->isEditable()){
            cb->setPlaceholderText(hval);
            cb->setCurrentText(dval);
        }else{
            cb->setCurrentIndex(cb->findData(dval));
        }
        return;
    }
    //fall back
    qDebug()<<"Warning: unknown editor type in ChipVariableDelegate::setEditorData:"<<(editor ? editor->metaObject()->className() : "((QWidget*)nullptr)");
    QStyledItemDelegate::setEditorData(editor,index);
}

void ChipVariableDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    if(index.column()!=1)return;
    //get vname
    const QString vname=model->data(index.sibling(index.row(),0)).toString();
    //check editor type
    auto*le=qobject_cast<QLineEdit*>(editor);
    if(le!=nullptr){
        QString val=le->text();
        emit triggerUpdate(vname,val);
        return;
    }
    auto*cb=qobject_cast<QComboBox*>(editor);
    if(cb!=nullptr){
        if(cb->isEditable()){//float w/ enum
            const auto val=cb->currentText().trimmed();
            emit triggerUpdate(vname,val);
        }else{//pure enum
            const auto val=cb->currentData().toString().trimmed();
            emit triggerUpdate(vname,val);
        }
        return;
    }
    //fall back
    qDebug()<<"Warning: unknown editor type in ChipVariableDelegate::setModelData:"<<(editor ? editor->metaObject()->className() : "((QWidget*)nullptr)");
}


// ////////////////////////////////////
// Variable Editor

VariableEditPage::VariableEditPage(const ChipVariables&cv,QWidget* parent)
:PageWidget(parent),mvars(cv),mfileparent(qobject_cast<FileEditPage*>(parent))
{
    //initialize formula engine
    if(cv.isOnPackage())
        mformula.setPackage(cv.package());
    else
        mformula.setTarget(cv.fileTarget());
    //draw widget
    QHBoxLayout*hl;
    setLayout(hl=new QHBoxLayout);
    mtable=new QTableView;
    hl->addWidget(mtable,1);
    mtable->setModel(mvarmodel=new QStandardItemModel(mtable));
    auto*vdel=new ChipVariableDelegate(mtable);
    mtable->setItemDelegate(vdel);
    connect(vdel,&ChipVariableDelegate::getVariableType,this,&VariableEditPage::getVariableType,Qt::DirectConnection);
    connect(vdel,&ChipVariableDelegate::getEnumType,this,&VariableEditPage::getEnumType,Qt::DirectConnection);
    connect(vdel,&ChipVariableDelegate::getVariableValue,this,&VariableEditPage::getVariableValue,Qt::DirectConnection);
    connect(vdel,&ChipVariableDelegate::triggerUpdate,this,&VariableEditPage::triggerUpdate,Qt::DirectConnection);
    mvarmodel->insertColumns(0,3);
    mvarmodel->setHorizontalHeaderLabels(QStringList()<<tr("Name","variable")<<tr("Value","variable")<<tr("Description","variable"));
    mtable->verticalHeader()->hide();
    auto tv=cv.templateVariables();
    const int numvar=tv.numVariables();
    int r=-1;
    for(int nv=0;nv<numvar;nv++){
        auto v=tv.variable(nv);
        if(v.isHidden())continue;
        mvarmodel->insertRows(++r,1);
        mvarmodel->setData(mvarmodel->index(r,0),v.id());
        auto idx=mvarmodel->index(r,1);
        mvarmodel->setData(idx,v.defaultValue(),Qt::ToolTipRole);//formula hint
        mvarmodel->setData(idx,QString());//placeholder for current value
        mvarmodel->setData(mvarmodel->index(r,2),v.description());
    }
    redisplay();
    mtable->resizeColumnsToContents();
}

void VariableEditPage::redisplay()
{
    //reset formula data and recalculate values
    mformula.reset();
    //reset display
    auto tvars=mvars.templateVariables();
    for(int r=0;r<mvarmodel->rowCount();r++){
        QString vn=mvarmodel->data(mvarmodel->index(r,0)).toString();
        auto idx=mvarmodel->index(r,1);
        QVariant var=mformula.getVariable(vn);
        QString text;
        auto tv=tvars.variable(vn);
        switch(tv.variableType()){
            case TemplateVariables::VarType::Float:
                text=QLocale().toString(var.toDouble(),'f',2);
                break;
            case TemplateVariables::VarType::Enum:{
                auto en=tvars.enumByValue(tv.enumReference(),var.toDouble());
                if(en.displayString().isEmpty() && mvars.isOnTargetFile())
                    en=mvars.packageVariables().templateVariables().enumByValue(tv.enumReference(),var.toDouble());
                if(en.displayString().isEmpty())
                    text=QLocale().toString(var.toDouble(),'f',2);
                else text=en.displayString();
                break;
            }
            case TemplateVariables::VarType::Int:
                text=QLocale().toString(var.toLongLong());
                break;
            default:
                text=var.toString();
                break;
        }
//         qDebug()<<"variable"<<vn<<"="<<mformula.getVariable(vn)<<text;
        mvarmodel->setData(idx,text);//current value
        const bool hasVal=mvars.hasVariable(vn) && !mvars.variableValue(vn).trimmed().isEmpty();
        mvarmodel->setData(idx,QBrush(hasVal ? Qt::black : Qt::darkGray),Qt::ForegroundRole);
    }
}

void VariableEditPage::connectMainEditor(ChipPackageEditor*ed)
{
    if(mvars.isOnTargetFile())
        connect(ed,&ChipPackageEditor::packageVariablesChanged,this,&VariableEditPage::redisplay);
    else
        connect(this,&VariableEditPage::valuesUpdated,ed,&ChipPackageEditor::packageVariablesChanged);
}

QString VariableEditPage::pageTitle() const
{
    return tr("Variables","title");
}

PageWidget::PageType VariableEditPage::pageType() const
{
    if(mvars.isOnPackage())
        return PageType::VariablePage;
    else
        return PageType::SubFilePage;
}

QString VariableEditPage::pageName() const
{
    if(mvars.isOnPackage()||mfileparent.isNull())
        return variablePageString;
    else
        return mfileparent->pageName()+" vars";
}

TemplateVariables::Variable VariableEditPage::getVariableType(QString vname)
{
    return mvars.templateVariables().variable(vname);
}

QList<TemplateVariables::Enum> VariableEditPage::getEnumType(QString ename)
{
    if(ename.isEmpty())return QList<TemplateVariables::Enum>();
    auto enums=mvars.templateVariables().enumsByName(ename);
    if(enums.size()<1 && mvars.isOnTargetFile())
        enums=mvars.packageVariables().templateVariables().enumsByName(ename);
    if(enums.size()<1)
        qDebug()<<"Hmm. Enum"<<ename<<"was not found or is empty.";
    return enums;
}

void VariableEditPage::getVariableValue(QString vname, QString& directValue, QString& hintValue)
{
    //fix formatting
    const auto tp=mvars.templateVariables().variable(vname).variableType();
    QLocale loc;
    QString vval=mvars.variableValue(vname);
    switch(tp){
        case Template::TemplateVariables::VarType::Float:
            directValue=vval.isEmpty()?QString():loc.toString(vval.toDouble(),'f',2);
            hintValue=loc.toString(mformula.getVariable(vname).toDouble(),'f',2);
            break;
        case Template::TemplateVariables::VarType::Int:
            directValue=vval.isEmpty()?QString():loc.toString(vval.toLongLong());
            hintValue=loc.toString(mformula.getVariable(vname).toLongLong());
            break;
        default:
            directValue=vval;
            hintValue=mformula.getVariable(vname).toString();
            break;
    }
}

void VariableEditPage::triggerUpdate(QString vname, QString vvalue)
{
    //check type and convert formatting if necessary
    if(!vvalue.isEmpty())
    switch(mvars.templateVariables().variable(vname).variableType()){
        case Template::TemplateVariables::VarType::Float:
            vvalue=QString::number(QLocale().toDouble(vvalue));
            break;
        case Template::TemplateVariables::VarType::Int:
            vvalue=QString::number(QLocale().toLongLong(vvalue));
            break;
        case Template::TemplateVariables::VarType::Enum:
        case Template::TemplateVariables::VarType::String:
            //nothing to do here, move along!
            break;
    }
    //set value in chip file and tell downstream pages
    mvars.setVariable(vname,vvalue);
    emit valuesUpdated();
    //send to display
    redisplay();
}



#include "moc_variableedit.cpp"
