// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QWidget>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLineEdit;
class QStandardItemModel;
class QSplitter;
class QModelIndex;
class QSpinBox;

namespace Chipper { namespace GUI {

///Helper class: line edit that holds a file or directory name with a button to open a file dialog
class CHIPPERGUI_EXPORT FileLineEdit:public QWidget
{
    Q_OBJECT
public:
    ///specifies whether the line is in file or directory mode
    enum Mode{File,Dir};
    ///instantiates the line edit with an initial content and a mode
    FileLineEdit(QString,Mode);

    ///returns the current content of the line
    QString value()const;
private slots:
    void updateFile();
private:
    QLineEdit*mline;
    Mode mmode;
};


//end of namespace
}}
using namespace Chipper::GUI;
