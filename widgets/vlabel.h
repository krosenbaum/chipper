// Vertical Label Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QFrame>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QPen;
class QBrush;

namespace Chipper { namespace GUI {

///\private
class VLabelPrivate;

///A very simple vertical label, can only display simple text.
class CHIPPERGUI_EXPORT VLabel : public QFrame
{
    Q_OBJECT
public:
    ///instantiates the label
    explicit VLabel(const QString &text, QWidget *parent=nullptr);
    ~VLabel();

    ///exact orientation of the text
    enum TopSide {
        ///Top of the text is left, bottom is right
        LeftIsTop,
        ///Top of the text is right, bottom is left
        RightIsTop
    };

    ///returns the exact text orientation
    TopSide topSide()const;
    ///returns the pen used for painting the text
    QPen pen()const;
    ///returns the brush used for painting the text
    QBrush brush()const;
    ///returns the current text
    QString text();
    ///returns text alignment (from the view of the text itself, not the widget)
    Qt::Alignment alignment()const;

public slots:
    ///sets the orientation of the text
    void setTopSide(TopSide t);
    ///sets the displayed text
    void setText(QString);
    ///sets the pen used for painting
    void setPen(const QPen&);
    ///sets the brush used for painting
    void setBrush(const QBrush&);
    ///sets the text alignment
    void setAlignment(const Qt::Alignment&);

    ///changes the size of the font by factor
    void scaleFontSize(qreal factor);

protected:
    void paintEvent(QPaintEvent*)override;
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

private:
    VLabelPrivate*d;
    ///rotated content rect for painting text
    QRect contentsRectRot()const;
    ///rotated content rect adjusted for text alignment
    QRect contentsRectRotA()const;
};

//end of namespace
}}
using namespace Chipper::GUI;
