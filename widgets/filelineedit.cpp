// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QFileDialog>
#include <QLineEdit>
#include <QToolButton>

#include "filelineedit.h"


// ////////////////////////////////////
// Widgets

FileLineEdit::FileLineEdit(QString fn,Mode m)
:mmode(m)
{
    QHBoxLayout*hl;
    setLayout(hl=new QHBoxLayout);
    hl->setContentsMargins(0,0,0,0);
    hl->addWidget(mline=new QLineEdit(fn),1);
    auto*p=new QToolButton;p->setText("...");
    hl->addWidget(p,0);
    connect(p,&QToolButton::clicked,this,&FileLineEdit::updateFile);
}

QString FileLineEdit::value() const
{
    return mline->text();
}

void FileLineEdit::updateFile()
{
    QString r;
    if(mmode==File)r=QFileDialog::getOpenFileName(this);
    else r=QFileDialog::getExistingDirectory(nullptr,QString(),mline->text());
    if(!r.isEmpty())mline->setText(r);
}



#include "moc_filelineedit.cpp"
