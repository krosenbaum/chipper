// Chipper KiCAD symbol/footprint/3Dmodel generator
// Helper: Grid Delegate for TreeView
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer


#include <QPainter>
#include <QDebug>
#include <QSize>

#include "griddelegate.h"


void GridDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    if(index.parent().isValid() || mtopframe){
        painter->save();
        painter->setPen(QColor(Qt::gray));
        auto rect=option.rect;
        if(mgoleft && index.column()==0)rect.setX(0);
        painter->drawRect(rect);
        painter->restore();
    }

    QStyledItemDelegate::paint(painter, option, index);
}

QSize GridDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QSize sz=QStyledItemDelegate::sizeHint(option,index);
    sz.setHeight(sz.height()+6);
    return sz;
}
