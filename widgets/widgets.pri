#sources
SOURCES += \
    $$PWD/filelineedit.cpp \
    $$PWD/vlabel.cpp \
    $$PWD/griddelegate.cpp

HEADERS += \
    $$PWD/filelineedit.h \
    $$PWD/vlabel.h \
    $$PWD/griddelegate.h

INCLUDEPATH += $$PWD
