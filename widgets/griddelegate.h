// Chipper KiCAD symbol/footprint/3Dmodel generator
// Helper: Grid Delegate for TreeView
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QStyledItemDelegate>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper { namespace Gui {

///Helper Delegate to paint frames around cells in a tree view
class CHIPPERGUI_EXPORT GridDelegate : public QStyledItemDelegate
{
    bool mtopframe=true,mgoleft=false;
public:
    ///instantiate
    explicit GridDelegate(QObject * parent = 0) : QStyledItemDelegate(parent) { }

    ///draws a grid and then delegates to QStyledItemDelegate
    virtual void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const override;

    ///increases vertical height of each row by a few pixels to make it look nicer
    virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setTopLevelFrame(bool enabled){mtopframe=enabled;}

    void setStartFrameLeft(bool enabled){mgoleft=enabled;}
};

//end of namespace
}}

using namespace Chipper::Gui;
