// Vertical Label Widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "vlabel.h"

#include <QBrush>
#include <QDebug>
#include <QFont>
#include <QFontMetrics>
#include <QPainter>
#include <QPen>

class Chipper::GUI::VLabelPrivate
{
public:
    VLabel::TopSide mtop=VLabel::LeftIsTop;
    QString mtext;
    QBrush mbrush=Qt::SolidPattern;
    QPen mpen=QColor(Qt::black);
    Qt::Alignment malign=Qt::AlignCenter;
};

VLabel::VLabel(const QString& text, QWidget* parent)
:QFrame(parent)
{
    d=new VLabelPrivate;
    d->mtext=text;
}

VLabel::~VLabel()
{
    if(d)delete d;
    d=nullptr;
}

void VLabel::paintEvent(QPaintEvent*e)
{
    QFrame::paintEvent(e);

    QPainter painter(this);
    painter.setPen(pen());
    painter.setBrush(brush());

    painter.translate(width()/2,height()/2);
    if(d->mtop==RightIsTop)
        painter.rotate(90);
    else
        painter.rotate(-90);
    painter.translate(-height()/2,-width()/2);

    painter.drawText(contentsRectRotA(), 0, d->mtext);
}

QSize VLabel::minimumSizeHint() const
{
    auto fm=fontMetrics();
    auto mrg=contentsMargins();
    auto rect=fm.boundingRect(d->mtext);
    return QSize(rect.height()+mrg.left()+mrg.right(),rect.width()+mrg.top()+mrg.bottom());
}

QSize VLabel::sizeHint() const
{
    return minimumSizeHint();
}

void VLabel::setTopSide(VLabel::TopSide t)
{
    d->mtop=t;
    update();
}

void VLabel::setText(QString t)
{
    d->mtext=t;
    update();
}

QBrush VLabel::brush() const
{
    return d->mbrush;
}

QPen VLabel::pen() const
{
    return d->mpen;
}

void VLabel::setBrush(const QBrush&b)
{
    d->mbrush=b;
    update();
}

void VLabel::setPen(const QPen&p)
{
    d->mpen=p;
    update();
}

VLabel::TopSide VLabel::topSide() const
{
    return d->mtop;
}

QString VLabel::text()
{
    return d->mtext;
}

Qt::Alignment VLabel::alignment() const
{
    return d->malign;
}

void VLabel::setAlignment(const Qt::Alignment&a)
{
    d->malign=a;
}

QRect VLabel::contentsRectRot() const
{
    auto r=contentsRect();
    QRect rr(r.top(),r.left(),r.height(),r.width());
    return rr;
}

QRect VLabel::contentsRectRotA() const
{
    auto r=contentsRectRot();
    auto s=fontMetrics().boundingRect(d->mtext).size();
    if(r.width()>s.width() && (d->malign&Qt::AlignLeft)==0){
        int df=(r.width()-s.width())/((d->malign&Qt::AlignRight)!=0?1:2);
        r.setLeft(r.left()+df);
        r.setWidth(s.width());
    }
    if(r.height()>s.height() && (d->malign&Qt::AlignTop)==0){
        int df=(r.height()-s.height())/((d->malign&Qt::AlignBottom)!=0?1:2);
        r.setTop(r.top()+df);
        r.setHeight(s.height());
    }
    return r;
}

void VLabel::scaleFontSize(qreal factor)
{
    auto f=font();
//     qDebug()<<f;
    f.setPointSizeF(f.pointSizeF()*factor);
//     qDebug()<<f;
    setFont(f);
}

#include "moc_vlabel.cpp"
