// Chipper KiCAD Variable Tool
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QRegularExpression>

#include "chipper.h"
#include "kicadenv.h"

#include <iostream>

using namespace std;

namespace VTool {

///helper to output QString on stdout
inline std::ostream& operator<<(std::ostream&out,const QString&str)
{
    out<<str.toStdString();
    return out;
}

///output help to user
void help(const char*appName)
{
    cerr<<"Chipper Variable Tool"<<endl;
    cerr<<"  version "<<VERSIONSTR<<endl;
    cerr<<endl;
    cerr<<"Usage: "<<appName<<" command [options...] [NAME[=/value]]"<<endl;
    cerr<<"  Commands:"<<endl;
    cerr<<"    -help -> show this help"<<endl;
    cerr<<"    -list -> list all KiCad versions"<<endl;
    cerr<<"    -get --> get/list all variables/pathes, you can use a prefix or use the NAME syntax"<<endl;
    cerr<<"    -set --> set variables, use the NAME=value syntax"<<endl;
    cerr<<endl;
    cerr<<"  Options:"<<endl;
    cerr<<"    -5  --> use KiCad version 5.x only"<<endl;
    cerr<<"    -6  --> use KiCad version 6.x only"<<endl;
    cerr<<"    -prefix=... -> filter variables by name prefix"<<endl;
    cerr<<"    -version=... -> use specific KiCad version only (use -list to find versions)"<<endl;
}

///if set: use specific version only
static QString versionFilter;
///if set: filter variables by name prefix
static QString varPrefix;

///filter by KiCad version
enum class FilterVersion {
    ///allow both KiCad 5 and 6
    Any,
    ///look for KiCad 5 only
    V5only,
    ///look for KiCad 6 only
    V6only
};
///what KiCad version to query/manipulate
static FilterVersion versionClass=FilterVersion::Any;

///list of variable name-value pairs from command line
static QMap<QString,QString> variables;
///helper regex for variable name syntax
static const QRegularExpression varnameregex("[A-Za-z_]+");

///check whether the environment reference fits the command line filters
inline bool projectFilter(KiCadEnvironment&env,QString path)
{
        if(path.startsWith(":"))return false;
        if(!versionFilter.isEmpty() && versionFilter!=env.envVersion(path))return false;
        if(versionClass!=FilterVersion::Any){
            if(env.isKiCad6(path) != (versionClass==FilterVersion::V6only))return false;
        }
        return true;
}

///check whether a variable name fits the command line filters
inline bool varNameFilter(QString name)
{
    if(!varPrefix.isEmpty() && !name.startsWith(varPrefix))return false;
    if(variables.size()>0)
        return variables.keys().contains(name);

    return true;
}

///parse a variable assignment from the command line
inline bool varParse(QString arg)
{
    //find =
    QString name,val;
    int pos=arg.indexOf('=');
    if(pos<0)name=arg;
    else{
        name=arg.left(pos);
        val=arg.mid(pos+1);
    }
    //syntax checks
    if(!varnameregex.match(name).hasMatch()){
        cerr<<"Error: '"<<name<<"' is not a valid variable name";
        return false;
    }
    //store
    variables.insert(name,val);
    return true;
}

///output: list all KiCad versions that were found & filtered
void listVersion()
{
    auto&env=KiCadEnvironment::instance();
    const auto ep=env.envPathes();
    for(const auto &epn:ep){
        //filter
        if(!projectFilter(env,epn))continue;
        //output
        cout<<epn<<" --> "<<env.envName(epn)<<" --> Version="<<env.envVersion(epn)<<" Type="<<(env.isKiCad6(epn)?"6.x":"5.x")<<endl;
    }
}

///output: list all variables that were found and filtered
void listVars()
{
    auto&env=KiCadEnvironment::instance();
    const auto ep=env.envPathes();
    for(const auto &epn:ep){
        //filter versions
        if(!projectFilter(env,epn))continue;
        //scan this path
        cout<<"Config: "<<epn<<endl;
        auto map=env.environment(epn);
        auto keys=map.keys();
        sort(keys.begin(),keys.end());
        for(auto k:keys){
            //filter var names
            if(!varNameFilter(k))continue;
            //print
            cout<<"  "<<k<<"="<<map[k]<<endl;
        }
        cout<<endl;
    }
}

///command: set variables into a KiCad configuration
void setVars()
{
    //sanity check
    if(variables.size()==0){
        cerr<<"Error: no variables assigned, cannot set any. Bailing out."<<endl;
        return;
    }
    //list variables
    cout<<"Setting Variables:"<<endl;
    for(auto k:variables.keys())
        cout<<"  "<<k<<"="<<variables[k]<<endl;
    cout<<endl;
    //go through configs
    auto&env=KiCadEnvironment::instance();
    const auto ep=env.envPathes();
    for(const auto &epn:ep){
        //filter versions
        if(!projectFilter(env,epn))continue;
        //submit
        cout<<"Config: "<<epn<<endl;
        if(env.setVariables(epn,variables))
            cout<<"  Success."<<endl;
        else
            cout<<"  Failed."<<endl;
    }
}

///command: delete variables from a KiCad configuration
void delVars()
{
    //sanity check
    if(variables.size()==0 && varPrefix.isEmpty()){
        cerr<<"Error: no variables or prefix selected, cannot delete any. Bailing out."<<endl;
        return;
    }
    //list variables
    cout<<"Deleting Variables:"<<endl;
    auto vars=variables.keys();
    for(auto k:vars)
        cout<<"  "<<k<<endl;
    cout<<endl;
    //go through configs
    auto&env=KiCadEnvironment::instance();
    const auto ep=env.envPathes();
    for(const auto &epn:ep){
        //filter versions
        if(!projectFilter(env,epn))continue;
        //submit
        cout<<"Config: "<<epn<<endl;
        if(env.deleteVariables(epn,vars))
            cout<<"  Success."<<endl;
        else
            cout<<"  Failed."<<endl;
    }
}

///command requested by the user
enum class Command{
    ///show help text
    Help,
    ///list all found KiCad versions
    ListVersion,
    ///list all found variables (and values)
    ListVars,
    ///set variables
    SetVars,
    ///delete variables
    DeleteVars
};

///parse command line and execute requested command
int main(int ac,char**av)
{
    initChipperApp(ac,av);

    const auto&args=qApp->arguments();

    Command cmd=Command::Help;
    for(const auto&arg:args.mid(1)){
        //commands
        if(arg=="-list")cmd=Command::ListVersion;
        else if(arg=="-get")cmd=Command::ListVars;
        else if(arg=="-set")cmd=Command::SetVars;
        else if(arg=="-delete")cmd=Command::DeleteVars;
        else if(arg=="help" || arg=="-help" || arg=="--help" || arg=="-h" || arg=="-?")cmd=Command::Help;
        //options
        else if(arg=="-5")versionClass=FilterVersion::V5only;
        else if(arg=="-6")versionClass=FilterVersion::V6only;
        else if(arg.startsWith("-prefix="))varPrefix=arg.mid(8);
        else if(arg.startsWith("-version="))versionFilter=arg.mid(9);
        else if(!arg.startsWith('-')){
            if(!varParse(arg))return 2;
        }
        //oops:
        else{
            cerr<<"Unknown argument '"<<arg<<"' - please try -help !"<<endl;
            return 1;
        }
    }

    switch(cmd){
        case Command::ListVersion:
            listVersion();
            break;
        case Command::ListVars:
            listVars();
            break;
        case Command::SetVars:
            setVars();
            break;
        case Command::DeleteVars:
            delVars();
            break;
        case Command::Help:
            help(av[0]);
            break;
        default:
            cerr<<"Ooops! Unhandled Command Mode. Help! Fix me please!"<<endl;
            break;
    }

    return 0;
}

//end of namespace
}
///\private real main
int main(int ac, char**av){return VTool::main(ac,av);}
