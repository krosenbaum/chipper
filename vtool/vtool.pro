#main defs
TEMPLATE = app
TARGET = chipper-vtool

include(../basics.pri)
#include(../src/src.pri)
LIBS += -lchipper
INCLUDEPATH += $$PWD/../src

#Qt basics
QT -= gui
QT += xml
CONFIG += console


SOURCES += vtool.cpp
