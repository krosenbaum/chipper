// SMD Chip Script Library
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This is the main include for SMD chip scripts.

//basics
$fn=20;

//various constants
include <colors.scad>
include <constants.scad>
include <box.scad>

//calculated values
H = A - (A2/2); //pin hight

//create pins (normal Z-shape and stubby kind)
module pin(isStub)
{
    if(isStub){
        translate([L-L1,-b/2,H])color($silver,1)cube([L1-L,b,c]); //stub connects to chip
    }else{
        translate([-L1,-b/2,0]) //make sure it is centered on Z axis
        union(){
            translate([L+PinRound,0,H])color($silver,1)cube([L1-L-PinRound,b,c]); //top connects to chip
            translate([L+PinRound,b/2,H-PinRound+c])rotate([-90,0,0])difference(){//top rounding
                color($silver,1)cylinder(h=b,r=PinRound,center=true);
                color($silver,1)cylinder(h=b+1,r=PinRound-c,center=true);
                translate([-PinRound+c,0,-b/2-0.1])color($silver,1)cube([2*PinRound-c,2*PinRound,b+0.2]);
                translate([-0*PinRound,-PinRound+c,-b/2-0.1])color($silver,1)cube([PinRound,2*PinRound,b+0.2]);
            }
            translate([L,0,PinRound])color($silver,1)cube([c,b,H-2*PinRound+c]); //vertical part
            translate([L-PinRound+c,b/2,PinRound])rotate([-90,0,0])difference(){//bottom rounding
                color($silver,1)cylinder(h=b,r=PinRound,center=true);
                color($silver,1)cylinder(h=b+1,r=PinRound-c,center=true);
                translate([-PinRound-c,-2*PinRound,-b/2-0.1])color($silver,1)cube([2*PinRound,2*PinRound,b+0.2]);
                translate([-2*PinRound,-PinRound-c,-b/2-0.1])color($silver,1)cube([2*PinRound,2*PinRound,b+0.2]);
            }
            color($silver,1)cube([L+c-PinRound,b,c]); //bottom on PCB
        };
    }
}

//create body including text
module body()
{
    //rotate pin 1 to top left; align chip centered on top of Z=0 plane
    rotate([0,0,-90])translate([-E1/2,-D1/2,A-A2])
    difference(){
        union(){
            //chip package (simple cube)
            box(E1,D1,A2,Chamfer,$black);
            //chip label: position so it appears on top, centered
            translate([E1/2,D1/2,A2-0.03])
            intersection(){
                rotate([0,0,LRot])color($white,1)linear_extrude(height=0.05){
                    color($white,1)text(text=Label1,size=LSize,halign="center",valign="center");
                    translate([0,1.5*LSize,0])color($white,1)text(text=Label0,size=LSize,halign="center",valign="center");
                    translate([0,-1.5*LSize,0])color($white,1)text(text=Label2,size=LSize,halign="center",valign="center");
                }
                //truncate label at about chip size
                color($white,1)cube([E1-0.2,D1-0.2,0.15],center=true);
            }
            // Printed Pin marks (see smdlib.xml: enum pinmark)
            if(PinMark==pinmark_dot) //dot
                translate([e,e,A2])color($white,1)cylinder(h=0.05,d=0.2,center=true);
            if(PinMark==pinmark_triangle){ //tri-angle
                translate([0.5,0.5,A2-0.025])color($white,1)cube([1,0.05,0.05]);
                translate([0.5,0.5,A2-0.025])color($white,1)cube([0.05,1,0.05]);
            }
            if(PinMark==pinmark_diagonal) //diagonal line
                translate([1.2,0.5,A2-0.025])rotate([0,0,45])color($white,1)cube([0.05,1,0.05]);
            if(PinMark==pinmark_leftline)
                translate([E1*0.05,0.5,A2-0.025])color($white,1)cube([E1*0.9,0.05,0.05]);
            if(PinMark==pinmark_topline)
                translate([0.5,D1*0.05,A2-0.025])color($white,1)cube([0.05,D1*0.9,0.05]);
        }

        //drilled Pin marks: drill hole into chip at pin 1 (see smdlib.xml: enum pinmark)
        if(PinMark==pinmark_round)translate([1.5*e,1.5*e,A2])color($black2,1)cylinder(h=0.2,d=e,center=true);
        if(PinMark==pinmark_indent){
            translate([1,D1/2,A2])color($black2,1)cylinder(h=0.2,d=1,center=true);
            translate([0,D1/2,A2])color($black2,1)cube([2,1,0.2],center=true);
        }
        //Chamfer as pin mark
        if(PinMark==pinmark_chamferleft)
            translate([-.5,-0.1,A2/2+c])rotate([30,0,0])color($black2,1)cube([E1+1,20,1]);
        if(PinMark==pinmark_chamfertop)
            translate([-.1,-0.5,A2/2+c])rotate([0,-30,0])color($black2,1)cube([20,D1+1,1]);
    };
}
