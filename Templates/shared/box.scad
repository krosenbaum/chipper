// Chamfered Box Library
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This script is part of the OpenSCAD library for Chipper.

//NOTE: all faces need to be defined with their points in CW order (seen from outside) and the poly needs to be fully closed with no overlaps.
// see https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Primitive_Solids#polyhedron
// use F12 (thrown together view) to see offending faces, use F10 to return to normal mode

///Draw a chamfered box.
/// x,y,z - size of the box (origin is always 0-0-0)
/// ch - size of the chamfer
/// col - color of the box
module box(x,y,z,ch,col)
{
    if(ch<=0)
        color(col,1)cube([x,y,z]);
    else
        color(col,1)polyhedron(
            [ //points
                //0-0-0 origin
                [ch,ch,0],//0
                [0,ch,ch],
                [ch,0,ch],
                //x-0-0 x
                [x-ch,ch,0],//3
                [x,ch,ch],
                [x-ch,0,ch],
                //x-y-0 xy
                [x-ch,y-ch,0],//6
                [x,y-ch,ch],
                [x-ch,y,ch],
                //0-y-0 y
                [ch,y-ch,0],//9
                [0,y-ch,ch],
                [ch,y,ch],
                //0-0-9 z:origin
                [ch,ch,z],//12
                [0,ch,z-ch],
                [ch,0,z-ch],
                //x-0-z z:x
                [x-ch,ch,z],//15
                [x,ch,z-ch],
                [x-ch,0,z-ch],
                //x-y-z z:xy
                [x-ch,y-ch,z],//18
                [x,y-ch,z-ch],
                [x-ch,y,z-ch],
                //0-y-z z:y
                [ch,y-ch,z],//21
                [0,y-ch,z-ch],
                [ch,y,z-ch],
            ],
            [ //faces
                //corners
                [0,1,2],
                [4,3,5],
                [6,7,8],
                [10,9,11],
                [13,12,14],
                [15,16,17],
                [18,20,19],
                [21,22,23],
                //main faces
                [14,17,5,2],//y=0
                [11,8,20,23],//y=y
                [10,22,13,1],//x=0
                [4,16,19,7],//x=x
                [0,3,6,9],//z=0
                [21,18,15,12],//z=z
                //lines z=0
                [0,2,5,3],//y=0 o-x
                [9,10,1,0],//x=0 o-y
                [6,8,11,9],//y=y y-xy
                [3,4,7,6],//x=x x-xy
                //lines z=z
                [15,17,14,12],//y=0 o-x
                [12,13,22,21],//x=0 o-y
                [21,23,20,18],//y=y y-xy
                [18,19,16,15],//x=x x-xy
                //lines mid
                [2,1,13,14],//o
                [17,16,4,5],//x
                [8,7,19,20],//xy
                [23,22,10,11],//y
            ],
            10 //convexity (10 is a generally safe value)
        );
}

//box(1,4,9,0.2,"#456");
