// KiCad & Favourite Colors
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This script is part of the OpenSCAD library for Chipper.

//black enough to look the part, not black enough to be invisible
$black="#222";
//slightly less black for highlights
$black2="#333";

//pin colors
$silver="#c0c0c8";
$gold="#dc8";

//font color
$white="#fff";
