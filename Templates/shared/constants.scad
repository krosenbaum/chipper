// Library: constants
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This is the main include for SMD chip scripts.

//see also smdlib.xml enum pinmark
pinmark_none=0;
pinmark_round=1;
pinmark_indent=2;
pinmark_dot=4;
pinmark_triangle=8;
pinmark_diagonal=16;
pinmark_leftline=32;
pinmark_topline=64;
pinmark_chamferleft=128;
pinmark_chamfertop=256;
