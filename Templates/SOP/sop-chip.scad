// SOP Chip Script
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This script is always called from a generated parameter script.
// Use sop-test.scad for test runs.

//calculated values
Poff=(N/2-1)*e/2; //pin offset (position of 1st pin)
numPin2=max(len(pinleft),len(pinright)); //pins per side

//load library
include <../shared/smdlib.scad>


//final assembly
union(){
    body();
    for(pn=[0:numPin2-1]){
        if(pinleft[pn])
            translate([-D1/2,Poff-pn*e,0])rotate([0,0,0])pin(pinleft[pn]==2);
        if(pinright[pn])
            translate([D1/2,Poff-pn*e,0])rotate([0,0,180])pin(pinright[pn]==2);
    }
}
