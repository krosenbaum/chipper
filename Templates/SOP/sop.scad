SOP 3D Model - input template
=============================

SOP/SOIC 3D Model (2 rows, regular spacing, SMD)
by: Konrad Rosenbaum <konrad@silmor.de>, 2022
protected under the KiCAD (footprint) license

This is the template for the generated script. It will include sop-chip.scad, which does the real heavy lifting.

///AREA:PRE
    ///OUTPUT on noempty trim

    Generating SOP 3D Model...

    ///OUTPUT off

    pre-calc...

///AREA:TEMPLATE

//copy all variables to OpenSCAD
N={{N}};
e={{e}};
b={{b}};
c={{c}};
A={{A}};
A1={{A1}};
A2={{A2}};
L={{L}};
L1={{L1}};
E1={{E1}};
D1={{D1}};
D={{D}};
Label0={{strEscape(Label0)}};
Label1={{strEscape(Label1)}};
Label2={{strEscape(Label2)}};
LSize={{LSize}};
LRot=0;
PinMark={{PinMark}};
Chamfer={{Chamfer}};
PinRound={{PinRound}};

//left side (top-down)
// 0=missing, 1=fill, 2=stubby
pinleft=[
///IF {{hasKey(target.pinRowMap,'left')}}
  ///FOREACH pin {{target.pinRowMap['left']}}
    ///IF {{pin.isGap | pin.isMissing}}
        0, //gap
    ///ELSE
      ///IF {{pin.isStub}}
        2, //stub
      ///ELSE
        1, //pin
      ///END
    ///END
  ///END
///END
];

//right side (top-down)
// 0=missing, 1=fill, 2=stubby
pinright=[
///IF {{hasKey(target.pinRowMap,'right')}}
  ///FOREACH pin {{target.pinRowMap['right']}}
    ///IF {{pin.isGap | pin.isMissing}}
        0, //gap
    ///ELSE
      ///IF {{pin.isStub}}
        2, //stub
      ///ELSE
        1, //pin
      ///END
    ///END
  ///END
///END
];

include <{{resolve('%D/sop-chip.scad')}}>;

///AREA:POST
    ///OUTPUT on noempty trim

    Done generating SOP 3D Model SCAD for {{manufacturer}} - {{targetName}}.
    Into File {{targetFilePath}}

    ...continuing with conversion to final format...
///AREA:END
