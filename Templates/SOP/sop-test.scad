// Test version of SOP Chip

N=28;
e=0.65;
b=0.3;
c=0.1;
A=2.5;
A1=0.75;
A2=1.75;
L=0.75;
L1=1.25;
E1=10.2;
D1=5.3;
D=7.8;
Label0="Murks Corp.";
Label1="...X     XYZ-3209    A...";
Label2="Lot# 1234";
LSize=0.8;
LRot=0;
PinMark=2;
Chamfer=c;
PinRound=2.2*c;

//left and right pins, both from top to bottom
// 0 = missing
// 1 = full pin
// 2 = stubby pin
pinleft=[
 1,
 0,
 1,
 1,

 1,
 1,
 1,
 1,

 1,
 1,
 2,
 2,

 1,
 1,
];
pinright=[
 1,
 2,
 1,
 1,

 1,
 1,
 1,
 1,

 1,
 1,
 0,
 0,

 1,
 1,
];

include <sop-chip.scad>;
