// SOP Chip Script
// (c) Konrad Rosenbaum, 2021-22
// License: KiCAD (footprint) license
//
// This script is always called from a generated parameter script.
// Use sop-test.scad for test runs.

//calculated values
Poff=(Nd-1)*e/2; //pin offset (position of 1st pin)
Poff2=-(Ne-1)*e/2; //pin offset (position of last pin)
numPin2=max(len(pinleft),len(pinright)); //pins per side

//load library
include <../shared/smdlib.scad>

//final assembly
union(){
    body();
    for(pn=[0:Nd-1]){
        if(pinleft[pn])
            translate([-D1/2,Poff-pn*e,0])rotate([0,0,0])pin(pinleft[pn]==2);
        if(pinright[pn])
            translate([D1/2,Poff-pn*e,0])rotate([0,0,180])pin(pinright[pn]==2);
    }
    for(pn=[0:Ne-1]){
        if(pintop[pn])
            translate([Poff2+pn*e,E1/2,0])rotate([0,0,-90])pin(pintop[pn]==2);
        if(pinbtm[pn])
            translate([Poff2+pn*e,-E1/2,0])rotate([0,0,90])pin(pinbtm[pn]==2);
    }
}
