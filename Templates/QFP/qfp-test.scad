// Test version of SOP Chip

N=28;
Ne=8;
Nd=8;
e=0.65;
b=0.3;
c=0.1;
A=2.5;
A1=0.75;
A2=1.75;
L=0.75;
L1=1.25;
E1=7;
D1=7;
D=9;
E=9;
Label0="AdAstra";
Label1="...X     XYZ-3209    A...";
Label2="SUPR";
LSize=1;
LRot=45;
PinMark=4;
Chamfer=c;
PinRound=2.2*c;

//4x pins, both from top to bottom
// 0 = missing
// 1 = full pin
// 2 = stubby pin
pinleft=[
 1,
 0,
 1,
 1,

 1,
 1,
 1,
 1,
];
pinbtm=[
 1,
 1,
 1,
 2,

 1,
 1,
 1,
 1,
];
pinright=[
 2,
 1,
 1,
 1,

 1,
 1,
 1,
 1,
];
pintop=[
 2,
 1,
 0,
 1,

 1,
 1,
 1,
 1,
];

include <qfp-chip.scad>;
