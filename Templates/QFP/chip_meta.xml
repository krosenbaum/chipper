<ChipMeta v="1" symbol="default" footprint="minimal" m3d="default">
    <Description title="QFP" author="Konrad Rosenbaum"
                 license="GPLv3" copy="(c) Konrad Rosenbaum, 2021-22"
                 readme="qfp.html">
        Quad Flat Pack (SMD)
    </Description>

    <Include path="../shared/smdpitch.xml"/>
    <Variables>
        <v id="N" type="int" default="variant.numPins" min="4" max="10000" step="2">Number of Leads</v>
        <v id="N_d" type="int" default="N/4" min="1" max="N/2">Number of pins on side with pin 1</v>
        <v id="N_e" type="int" default="N/2 - N_d" min="1" max="N/2">Number of pins on side with last pin</v>
        <v id="e" default="0.8" min="0.1" enum="pitch">Lead Pitch</v>
        <v id="b" default="0.3" min="0.01">Terminal Width (width of pin)</v>
        <v id="c" default="0.1" min="0.01">Terminal Thickness (thickness of pin)</v>
        <v id="A" default="1.2" min="0.1">Overall Height (PCB to top of chip package)</v>
        <v id="A2" default="A-0.2" min="0.1">Molded Package Thickness</v>
        <v id="A1" default="A-A2" min="0.0">Standoff (PCB to bottom of chip package)</v>
        <v id="L" default="0.6" min="0.0">Foot Length (Footprint on single pad)</v>
        <v id="L1" default="1.0" min="0.0">Footprint (total pin length)</v>
        <v id="E1" default="ceil((N_e - 1)*e+b+1)" min="0.1">Molded Package Width</v>
        <v id="D1" default="ceil((N_d - 1)*e+b+1)" min="0.1">Molded Package Length</v>
        <v id="E" default="E1+2*L1" min="0.1">Total Package Width, side with last pin</v>
        <v id="D" default="D1+2*L1" min="0.1">Total Package Length, side with pin 1</v>
    </Variables>
    <Pins id="qfp" name="QFP physical layout">
        <Row id="w" maphint="left">West: 1st row, starts at pin 1 top-left, goes down</Row>
        <Row id="s" maphint="bottom">South: 2nd row, bottom left-to-right</Row>
        <Row id="e" maphint="right">East: 3rd row, right, bottom-to-top</Row>
        <Row id="n" maphint="top">North: last row, top, right-to-left, ends with last pin</Row>
        <Fill name="QFP" id="qfp">
            <Take into="w">lrange(pool,0,N_d)</Take>
            <Take into="s">lrange(pool,0,N_e)</Take>
            <Take into="e">lreverse(lrange(pool,0,N_d))</Take>
            <Take into="n">lreverse(pool)</Take>
        </Fill>
    </Pins>

    <Include path="../BasicSymbol"/>

    <Footprint id="minimal" name="QFP Minimal Footprint" file="qfp.kicad_mod" rows="qfp" prefill="qfp">
        <Variables>
            <h id="X1" default="b+0.1"/>
            <h id="Y1" default="L1+0.5"/>
            <h id="Ltx" default="0"/>
        </Variables>
    </Footprint>
    <Footprint id="hand" name="QFP Footprint for Hand Soldering" file="qfp.kicad_mod" rows="qfp" prefill="qfp">
        <Variables>
            <v id="X1" default="b+0.1">Pad Width</v>
            <v id="Y1" default="L1+0.5">Pad Length</v>
            <v id="Ltx" default="3" min="0">Extra Trace Length</v>
        </Variables>
    </Footprint>

    <Include path="../shared/smdlib.xml"/>
    <Model id="default" name="QFP 3D Model" file="qfp.scad" generator="3d-openscad" rows="qfp" prefill="qfp">
        <Variables>
            <v id="Label1" type="string" default="package.displayName">Chip Label</v>
            <v id="Label0" type="string" default="manufacturer">Extra Chip Label, Top</v>
            <v id="Label2" type="string" default="''">Extra Chip Label, Bottom</v>
            <v id="LSize" default="1">Font Size (mm)</v>
            <v id="LRot" default="90" type="float" enum="labelrot">Label Rotation</v>
            <v id="PinMark" default="1" type="enum" enum="pinmark">Marking Pin at 1</v>
            <v id="Chamfer" default="c" type="float">Chamfering Depth</v>
            <v id="PinRound" default="2*c" type="float">Rounding of pin bends</v>
        </Variables>
    </Model>
</ChipMeta>
