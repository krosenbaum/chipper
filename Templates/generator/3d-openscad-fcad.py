#OpenSCAD/FreeCAD 3D converter script
#=====================================


#This is a very simple Python script to convert a CSG (OpenSCAD) file to STEP and WRL.

#Note: This needs to be fixed to use the KiCad StepUp plugin and headless FreeCAD.

#Author: Konrad Rosenbaum

#History:
# jan 2022 - working version using FC GUI
# oct 2021 - initial version using FC GUI

##AREA:TEMPLATE
#!/usr/bin/freecad
import FreeCAD
from FreeCAD import ImportGui
import Draft
import FreeCADGui

FreeCAD.loadFile('{{tempDir}}/model.csg')

#refine shape before exporting to minimize artefacts
for obj in FreeCAD.ActiveDocument.Objects:
    try:
        obj.Refine = True
    except:
        {}


obs=[]

# iterate through all objects
for o in App.ActiveDocument.RootObjects:
  if len(o.InList)==0:
    obs.append(o)

#export STEP
ImportGui.export(obs,"{{tempDir}}/model.step")
#scale by 1/2.54 for KiCad
sf=1/2.54
Draft.scale(obj,FreeCAD.Vector(sf, sf, sf))
#export WRL
FreeCADGui.export(obs,"{{tempDir}}/model.wrl")

#done
sys.exit(0)
##AREA:END
