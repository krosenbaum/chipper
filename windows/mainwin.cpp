// Chipper KiCAD symbol/footprint/3Dmodel generator
// main window
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QSettings>
#include <QStatusBar>
#include <QTabWidget>
#include <QTimer>

#include <functional>

#include "chipdata.h"
#include "chipedit.h"
#include "chipfile.h"
#include "chippackage.h"
#include "chipper.h"
#include "chippool.h"
#include "envinspector.h"
#include "generator.h"
#include "helpwin.h"
#include "logtab.h"
#include "mainwin.h"
#include "metricconv.h"
#include "newchipwiz.h"
#include "pathconfigdlg.h"
#include "preferencedlg.h"
#include "templatepool.h"
#include "templateviewer.h"

// ////////////////////////////////////
// Main Window

//how long to show status bar messages in ms
#define MSGTIMEOUT 10000

MainWindow::MainWindow()
{
    setWindowTitle(tr("Chipper"));
    setAttribute(Qt::WA_DeleteOnClose);

    auto*mb=menuBar();
    auto *m=mb->addMenu(tr("&File"));
    m->addAction(tr("&New Chip..."),this,&MainWindow::newChip);
    m->addAction(tr("&Open Chip..."),this,qOverload<>(&MainWindow::openChip));
    m->addSeparator();
    mcpoolmenu=m->addMenu(tr("&Chip Pool"));
    updateChipPoolMenu();
    mcrcntmenu=m->addMenu(tr("&Recent Chips"));
    updateRecentChipMenu();
    connect(&ChipPool::instance(),&ChipPool::chipLoaded,this,&MainWindow::updateChipPoolMenu);
    connect(&ChipPool::instance(),&ChipPool::chipUnloaded,this,&MainWindow::updateChipPoolMenu);
    connect(&ChipPool::instance(),&ChipPool::chipUnloaded,this,&MainWindow::addRecentChip);
    m->addSeparator();
    m->addAction(tr("&Quit"),this,&MainWindow::close);

    mchipmenu=m=mb->addMenu(tr("&Chip"));
    auto save=m->addAction(tr("&Save Chip"),this,&MainWindow::saveChip);
    save->setShortcut(QKeySequence("Ctrl+S"));
    m->addAction(tr("C&lone Current Chip..."),this,&MainWindow::cloneChip);
    m->addSeparator();
    m->addAction(tr("Add &Variant..."),this,&MainWindow::addChipVariant);
    maddpack=m->addAction(tr("Add &Package..."),this,&MainWindow::addChipPackage);
    maddsym=m->addAction(tr("Add Symbol..."),this,&MainWindow::addChipSymbol);
    maddfprn=m->addAction(tr("Add Footprint..."),this,&MainWindow::addChipFPrint);
    madd3d=m->addAction(tr("Add 3D Model..."),this,&MainWindow::addChipModel);
    m->addSeparator();
    mdelvar=m->addAction(tr("Delete Current Variant"),this,&MainWindow::deleteChipVariant);
    mdelpack=m->addAction(tr("Delete Current Package"),this,&MainWindow::deleteChipPackage);
    mdelfile=m->addAction(tr("Delete Current File"),this,&MainWindow::deleteChipFile);
    m->addSeparator();
    m->addAction(tr("&Unload Current Chip"),this,&MainWindow::unloadChip);

    mgenmenu=m=mb->addMenu(tr("&Generate"));
    m->addAction(tr("Generate &Current"),this,&MainWindow::generateOne);
    m->addAction(tr("Generate &All in Pool"),this,&MainWindow::generateAll);

    mtmpmenu=mb->addMenu(tr("&Templates"));
    reloadTemplateMenu();

    m=mb->addMenu(tr("&Settings"));
    m->addAction(tr("P&ath Configuration..."),this,&MainWindow::pathConfig);
    m->addAction(tr("&Preferences..."),this,&MainWindow::preferences);

    m=mb->addMenu(tr("&View"));
    m->addAction(tr("Inspect &Environment..."),this,&MainWindow::environInspect);
    m->addAction(tr("&Log Viewer..."),this,&MainWindow::logViewer);
    m->addAction(tr("&Metric Converter..."),this,&MainWindow::metricConv);

    m=mb->addMenu(tr("&Help"));
    HelpWindow::buildMenu(m,this,std::bind(&MainWindow::help,this,std::placeholders::_1));
    m->addSeparator();
    m->addAction(tr("Chipper &Version..."),this,[this]{QMessageBox::information(this,tr("Chipper Version"),Util::versionInfo());});
    m->addAction(tr("About &Qt..."),qApp,&QApplication::aboutQt);

    setCentralWidget(mtab=new QTabWidget);
    mtab->setTabsClosable(true);
    mtab->setMovable(true);
    connect(mtab,&QTabWidget::tabCloseRequested,this,&MainWindow::closeTab);
    connect(mtab,&QTabWidget::currentChanged,this,&MainWindow::tabChanged);//make sure menu is up to date
    QTimer::singleShot(1,this,&MainWindow::tabChanged);//initial update

    auto&cp=ChipPool::instance();
    for(int i=0;i<cp.numChips();i++)
        openChip(cp.chip(i));

    auto*sb=statusBar();
    sb->setSizeGripEnabled(true);
    QLabel*sl;
    sb->addPermanentWidget(sl=new QLabel);
    auto upchip=[=]{sl->setText(tr("Chips: %1").arg(ChipPool::instance().numChips()));};
    sl->setFrameShadow(QFrame::Sunken);
    sl->setFrameShape(QFrame::Box);
    connect(&ChipPool::instance(),&ChipPool::chipLoaded,sl,upchip);
    connect(&ChipPool::instance(),&ChipPool::chipUnloaded,sl,upchip);
    upchip();
    sb->addPermanentWidget(sl=new QLabel);
    auto uptmpl=[=]{sl->setText(tr("Templates: %1").arg(TemplatePool::instance().pathList().size()));};
    sl->setFrameShadow(QFrame::Sunken);
    sl->setFrameShape(QFrame::Box);
    connect(&TemplatePool::instance(),&TemplatePool::reloaded,sl,uptmpl);
    connect(&TemplatePool::instance(),&TemplatePool::reloaded,sl,uptmpl);
    uptmpl();

    QSettings set;
    restoreGeometry(set.value("mainGeo").toByteArray());
    restoreState(set.value("mainState").toByteArray());

    connect(&ChipPool::instance(),&ChipPool::chipLoadFailed,this,[this](QString fn,QString err){
        QMessageBox::warning(this,tr("Warning"),tr("Error while loading chip file %1:\n%2").arg(fn).arg(err));
    });
    auto&g=Generator::instance();
    connect(&g,&Generator::generating,sb,[=](QString fn){sb->showMessage(tr("Generating %1 ...").arg(fn),MSGTIMEOUT);});
    connect(&g,&Generator::chipFinished,sb,[=](QString fn){sb->showMessage(tr("Successfully generated: %1").arg(fn),MSGTIMEOUT);});
    connect(&g,&Generator::error,sb,[=](QString fn,QString er){sb->showMessage(tr("Error in %1: %2").arg(fn).arg(er),MSGTIMEOUT);});
}

MainWindow::~MainWindow()
{
    //save geometry
    QSettings set;
    set.setValue("mainGeo",saveGeometry());
    set.setValue("mainState",saveState());
    //save chips for next session
    mcrcntmenu=nullptr;
    for(int i=0;i<mtab->count();i++){
        auto *e=qobject_cast<ChipEditor*>(mtab->widget(i));
        if(e==nullptr)continue;
        auto*c=e->chip();
        if(c==nullptr)continue;
        c->saveFile();
        addRecentChip(c->fileName());
    }
}

void MainWindow::reloadTemplateMenu()
{
    auto&tp=TemplatePool::instance();
    mtmpmenu->clear();
    for(auto t:tp.pathList()){
        auto*a=mtmpmenu->addAction(tr("View %1...").arg(tp.titleForPath(t)),this,&MainWindow::openTemplate);
        a->setData(t);
    }
    mtmpmenu->addSeparator();
    mtmpmenu->addAction(tr("&Reload"),this,[this](){TemplatePool::instance().reload();reloadTemplateMenu();});
}

void MainWindow::help(QString topic)
{
    if(mhelpwin.isNull()){
        mhelpwin=new HelpWindow(topic,this);
        mhelpwin->show();
    }else
        mhelpwin->setTopic(topic);
}

void MainWindow::pathConfig()
{
    PathConfigDialog d(this);
    d.exec();
}

void MainWindow::preferences()
{
    PreferenceDialog d(this);
    if(d.exec()==QDialog::Accepted){
        /*TODO: update chip editor*/;
    }
}

void MainWindow::environInspect()
{
    EnvironmentInspector(this).exec();
}

void MainWindow::logViewer()
{
    if(!LogTab::hasInstance())
        mtab->addTab(LogTab::instance(),QString("Log Viewer"));
    mtab->setCurrentWidget(LogTab::instance());
}

void MainWindow::metricConv()
{
    auto *mcd=new MetricConvertDialog(this);
    mcd->setAttribute(Qt::WA_DeleteOnClose);
    mcd->show();
}

void MainWindow::openTemplate()
{
    auto *act=qobject_cast<QAction*>(sender());
    if(act==nullptr)return;
    const QString path=act->data().toString();
    openTemplatePath(path);
}

void MainWindow::openTemplatePath(QString path)
{
    //try to find it
    for(int i=0;i<mtab->count();i++){
        auto*tw=qobject_cast<TemplateViewer*>(mtab->widget(i));
        if(tw==nullptr)continue;
        if(tw->path()==path){
            mtab->setCurrentWidget(tw);
            return;
        }
    }
    //create new one
    auto*tw=new TemplateViewer(path);
    mtab->addTab(tw,tw->windowTitle());
    mtab->setCurrentWidget(tw);
    connect(tw,&QWidget::windowTitleChanged,mtab,[this,tw](const QString&t){mtab->setTabText(mtab->indexOf(tw),t);});
}

void MainWindow::openTemplateTitle(QString title)
{
    openTemplatePath(TemplatePool::instance().pathForTitle(title));
}

void MainWindow::openChip(ChipData*cd)
{
    if(!cd)return;
    //try to find it
    for(int i=0;i<mtab->count();i++){
        auto*cw=qobject_cast<ChipEditor*>(mtab->widget(i));
        if(cw==nullptr)continue;
        if(cw->chip()==cd){
            mtab->setCurrentWidget(cw);
            return;
        }
    }
    //create new one
    auto *cw=new ChipEditor(cd);
    mtab->addTab(cw,cw->windowTitle());
    mtab->setCurrentWidget(cw);
    connect(cw,&QWidget::windowTitleChanged,mtab,[this,cw](const QString&t){mtab->setTabText(mtab->indexOf(cw),t);});
    connect(cw,&ChipEditor::showTemplate,this,qOverload<QString>(&MainWindow::openTemplateTitle));
    connect(cw,&ChipEditor::tabChanged,this,&MainWindow::tabChanged);
}

void MainWindow::openChip(QString s)
{
    openChip(ChipPool::instance().findOrLoadChip(s));
}

void MainWindow::openChip()
{
    QFileDialog d(this,tr("Open Chip File"),QString(),"Chip Files (*.chip);;All Files (*)");
    d.setDefaultSuffix(".chip");
    d.setFileMode(QFileDialog::ExistingFile);
    if(d.exec()!=QDialog::Accepted)return;
    auto fn=d.selectedFiles().value(0);
    auto *chip=ChipPool::instance().findOrLoadChip(fn);
    if(!chip){
        QMessageBox::warning(this,tr("Warning"),tr("Unable to open chip file %1").arg(fn));
        return;
    }
    openChip(chip);
}

void MainWindow::updateChipPoolMenu()
{
    mcpoolmenu->clear();
    auto &cp=ChipPool::instance();
    for(int i=0;i<cp.numChips();i++){
        const auto*chip=cp.chip(i);
        mcpoolmenu->addAction(
            tr("%1: %2","manufacturer: chip").arg(chip->metaData().manufacturer()).arg(chip->metaData().chipType()),
            this,std::bind(qOverload<QString>(&MainWindow::openChip),this,chip->fileName())
        );
    }
}

void MainWindow::addRecentChip(QString fn)
{
    if(fn.isEmpty())return;
    QSettings set;
    auto recent=set.value("recentChips").toStringList();
    if(recent.contains(fn))
        recent.move(recent.indexOf(fn),0);
    else{
        recent.prepend(fn);
        while(recent.size()>10)
            recent.removeLast();
    }
    set.setValue("recentChips",recent);
    QTimer::singleShot(0,this,&MainWindow::updateRecentChipMenu);
}

void MainWindow::updateRecentChipMenu()
{
    if(!mcrcntmenu)return;
    mcrcntmenu->clear();
    for(auto fn:QSettings().value("recentChips").toStringList())
        mcrcntmenu->addAction(fn,this,std::bind(qOverload<QString>(&MainWindow::openChip),this,fn));
}

void MainWindow::newChip()
{
    NewChipWizard wiz(this);
    wiz.exec();
    if(wiz.fileCreated())
        openChip(ChipPool::instance().findOrLoadChip(wiz.fileName()));
}

void MainWindow::cloneChip()
{
    ChipEditor*e=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(e==nullptr){
        qDebug()<<"Cannot clone a non-chip into a chip.";
        return;
    }
    NewChipWizard wiz(this);
    auto*chip=e->chip();
    if(!chip){
        qDebug()<<"OOps: have an editor, but no chip. How to find an invisible planet master Obi-Wan?";
        return;
    }
    wiz.setCloneFrom(chip);
    wiz.exec();
    if(wiz.fileCreated())
        openChip(ChipPool::instance().findOrLoadChip(wiz.fileName()));
}

void MainWindow::saveChip()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->chip()->saveFile();
}

void MainWindow::addChipVariant()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->addVariant();
}

void MainWindow::addChipPackage()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->addPackage();
}

void MainWindow::deleteChipVariant()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->deleteVariant();
}

void MainWindow::deleteChipPackage()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->deletePackage();
}

void MainWindow::deleteChipFile()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->deleteChipFile();
}
void MainWindow::addChipSymbol()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->addChipFile((int)ChipFileTarget::FileType::Symbol);
}
void MainWindow::addChipFPrint()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->addChipFile((int)ChipFileTarget::FileType::Footprint);
}
void MainWindow::addChipModel()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce)return;
    ce->addChipFile((int)ChipFileTarget::FileType::Model3D);
}

void MainWindow::tabChanged()
{
    auto*ce=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(!ce){
        mchipmenu->setEnabled(false);
        mgenmenu->setEnabled(false);
        return;
    }//else
    mchipmenu->setEnabled(true);
    mgenmenu->setEnabled(true);
    bool inV=ce->isInVariant();
    mdelvar->setEnabled(inV);
    maddpack->setEnabled(inV);
    bool inP=ce->isInPackage();
    mdelpack->setEnabled(inP);
    maddsym->setEnabled(inP);
    maddfprn->setEnabled(inP);
    madd3d->setEnabled(inP);
    QString sel=ce->selectedFileType();
    mdelfile->setEnabled(!sel.isEmpty());
    mdelfile->setText(tr("Delete Current %1...").arg(sel));
}

void MainWindow::closeTab(int idx)
{
    auto*w=mtab->widget(idx);
    if(w==nullptr)return;
    mtab->removeTab(idx);
    w->deleteLater();
}

void MainWindow::unloadChip()
{
    ChipEditor*e=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(e==nullptr){
        qDebug()<<"Cannot unload a non-chip from chip pool.";
        return;
    }
    auto*chip=e->chip();
    delete e;
    chip->saveFile();
    ChipPool::instance().unloadChip(chip);
}

void MainWindow::generateOne()
{
    ChipEditor*e=qobject_cast<ChipEditor*>(mtab->currentWidget());
    if(e==nullptr){
        qDebug()<<"Cannot generate a non-chip.";
        return;
    }
    auto*chip=e->chip();
    Generator::instance().createChip(chip);
}

void MainWindow::generateAll()
{
    Generator::instance().createPool();
}



#include "moc_mainwin.cpp"
