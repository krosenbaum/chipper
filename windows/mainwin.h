// Chipper KiCAD symbol/footprint/3Dmodel generator
// main window
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QMainWindow>
#include <QDialog>
#include <QPointer>

#include <functional>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QListView;
class QMenu;
class QModelIndex;
class QSplitter;
class QStandardItemModel;
class QTabWidget;
class QTextBrowser;
class QTreeView;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {
class HelpWindow;

///Main Chipper window.
class CHIPPERGUI_EXPORT MainWindow:public QMainWindow
{
    Q_OBJECT
public:
    ///instantiates the window
    MainWindow();
    ///deletes the window
    ~MainWindow();

private slots:
    //basic functions
    ///called from help menu - displays specific help topic
    void help(QString);
    ///displays preferences dialog
    void preferences();
    ///displays path configuration dialog
    void pathConfig();
    ///opens environment variable inspector
    void environInspect();
    ///show log viewer
    void logViewer();
    ///metric/imperial converter
    void metricConv();
    ///closes the current tab
    void closeTab(int);

    //chip related
    ///opens a tab for a specific chip file object, changes to the tab if it already exists
    void openChip(Chip::ChipData*);
    ///convenience: open chip by name (calls openChip(Chipdata*) )
    void openChip(QString);
    ///shows file dialog to open another chip file
    void openChip();
    ///updates the chip pool menu
    void updateChipPoolMenu();
    ///updates the recent chips menu
    void updateRecentChipMenu();
    ///add a recent chip to the menu (called when unloading and in destructor)
    void addRecentChip(QString);
    ///shows wizard to create new chip file
    void newChip();
    ///clones the current chip to a new file
    void cloneChip();
    ///saves the current chip to disk
    void saveChip();
    ///closes and unloads the current chip
    void unloadChip();
    ///asks the chip tab to add another variant
    void addChipVariant();
    ///asks the chip tab to delete a variant
    void deleteChipVariant();
    ///asks the chip tab to add another package
    void addChipPackage();
    ///ask the chip tab to delete a package
    void deleteChipPackage();
    ///delete current target file
    void deleteChipFile();
    ///add a symbol to current package
    void addChipSymbol();
    ///add a footprint to current package
    void addChipFPrint();
    ///add a 3D model to current package
    void addChipModel();

    //generator related
    ///create KiCAD files for current tab
    void generateOne();
    ///create KiCAD files for entire pool
    void generateAll();

    //template related
    ///displays meta data of a template (expects to be called from Template menu with details in the calling action)
    void openTemplate();
    ///displays meta data of a template (expects template title as parameter)
    void openTemplateTitle(QString title);
    ///displays meta data of a template (expects template path as parameter)
    void openTemplatePath(QString path);
    ///\internal: repopulates template menu
    void reloadTemplateMenu();

    ///make sure the correct menu items are available
    void tabChanged();
private:
    QPointer<HelpWindow>mhelpwin;
    QTabWidget*mtab;
    QMenu*mtmpmenu,*mcpoolmenu,*mcrcntmenu,*mchipmenu,*mgenmenu;
    QAction*maddpack,*mdelvar,*mdelpack,*mdelfile,*maddsym,*maddfprn,*madd3d;
};

//end of namespace
}}
using namespace Chipper::GUI;
