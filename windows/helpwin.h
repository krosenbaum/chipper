// Chipper KiCAD symbol/footprint/3Dmodel generator
// main window
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>
#include <QPointer>

#include <functional>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QListView;
class QMenu;
class QModelIndex;
class QSplitter;
class QStandardItemModel;
class QTabWidget;
class QTextBrowser;
class QTreeView;

namespace Chipper {

namespace Chip{
class ChipData;
}

namespace GUI {

///Display help texts, uses ":/doc/*" as possible topics
class CHIPPERGUI_EXPORT HelpWindow:public QDialog
{
    Q_OBJECT
public:
    ///instantiates a new help window
    ///\param start the first topic to be displayed, should be one of the strings returns by topics()
    explicit HelpWindow(QString start=defaultTopic(),QWidget*parent=nullptr);
    ///deletes the help window
    ~HelpWindow();

    ///returns all possible topics as unique file names
    static QStringList topics();
    ///formats a topic (file name) into a human readable string (e.g. for the help menu)
    static QString formatTopic(QString);
    ///returns the default topic
    static QString defaultTopic();

    ///creates a complete help menu with recursive topics
    ///\param menu the main menu widget, help items are appended to it
    ///\param target target object for action selection signal-slot connection
    ///\param slotFunc function "pointer" it should take the topic as argument, use std::bind(&class::method,this,std::placeholders::_1) for methods
    static void buildMenu(QMenu*menu,QObject*target,std::function<void(QString)>slotFunc);
    ///fills the model for a tree view (appends to the top level)
    ///\param model the model to be filled
    ///\param topic the initial topic for which the index is returned
    ///\return returns the index for the initial topic, so the tree view can be set
    static QModelIndex buildModel(QStandardItemModel*model,QString topic=QString());
public slots:
    ///changes the topic in the help window to the given file name
    void setTopic(QString);
    ///\internal called when the user clicks on a new topic
    void changeTopic();
private:
    QTreeView*mlist;
    QTextBrowser*mtext;
    QStandardItemModel*mmodel;
    QSplitter *msp;
};

//end of namespace
}}
using namespace Chipper::GUI;
