// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QComboBox>
#include <QFormLayout>
#include <QPushButton>
#include <QSpinBox>

#include "preferencedlg.h"
#include "chippool.h"



// ////////////////////////////////////
// Preferences


PreferenceDialog::PreferenceDialog(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("Chipper Preferences"));
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    setLayout(vl=new QVBoxLayout);
    QFormLayout*fl;
    QPushButton*p;
    vl->addLayout(fl=new QFormLayout,1);
    QComboBox*mm;
    fl->addRow(tr("Unit of Measure:"),mm=new QComboBox);
    mm->addItems(QStringList()<<tr("mm")<<tr("Millimeters")<<tr("Millimetres")<<tr("Metric Millimeters")<<tr("1/25.4 inch"));
    fl->addRow(tr("Autosync Interval (s):"),msync=new QSpinBox);
    msync->setRange(0,3600);
    msync->setValue(ChipPool::syncTimerInterval());
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(p=new QPushButton(tr("&OK")));
    connect(p,&QPushButton::clicked,this,&PreferenceDialog::accept);
    connect(p,&QPushButton::clicked,this,&PreferenceDialog::saveData);
    hl->addWidget(p=new QPushButton(tr("&Cancel")));
    connect(p,&QPushButton::clicked,this,&PreferenceDialog::reject);
}

void PreferenceDialog::saveData()
{
    ChipPool::instance().setSyncTimer(msync->value());
}



#include "moc_preferencedlg.cpp"
