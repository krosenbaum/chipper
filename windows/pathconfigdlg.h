// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLineEdit;
class QStandardItemModel;
class QSplitter;
class QModelIndex;
class QSpinBox;

namespace Chipper { namespace GUI {

class FileLineEdit;

///Configuration Dialog for generator target pathes.
class CHIPPERGUI_EXPORT PathConfigDialog:public QDialog
{
    Q_OBJECT
public:
    explicit PathConfigDialog(QWidget*parent=nullptr);

private slots:
    void saveData();
private:
    QLineEdit*menv;
    FileLineEdit*mroot,*msym,*mfoot,*m3dmod,*mtemp;
};

//end of namespace
}}
using namespace Chipper::GUI;
