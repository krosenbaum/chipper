// Chipper KiCAD symbol/footprint/3Dmodel generator
// log file tab
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QWidget>
#include <QMessageLogger>

class QTextBrowser;
class QCheckBox;
class QLineEdit;
class QSpinBox;

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

namespace Chipper {

namespace GUI {

class CHIPPERGUI_EXPORT LogTab:public QWidget
{
    Q_OBJECT
    LogTab(QWidget*parent=nullptr);

    QTextBrowser *mbrws;
    QCheckBox *mdolog,*mscrlend,*mdebug;
    QLineEdit *mlogname;
    QSpinBox *mmaxlines;

public:
    static bool hasInstance();
    static LogTab* instance(QWidget*parent=nullptr);
    static void init();

public slots:
    void updateLog();
};


//end of namespace
}}
