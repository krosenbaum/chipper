// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>
#include <QMap>
#include <QStringList>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QLineEdit;
class QStandardItemModel;
class QSplitter;
class QModelIndex;
class QSpinBox;

namespace Chipper { namespace GUI {

///Helper dialog that allows inspection of the system environment variables as well as KiCAD environment variables.
class CHIPPERGUI_EXPORT EnvironmentInspector:public QDialog
{
    Q_OBJECT
public:
    explicit EnvironmentInspector(QWidget*parent=nullptr);
    ~EnvironmentInspector();

private:
    QStandardItemModel*mmodel;
    QSplitter*msp;
    void addMap(const QModelIndex&,QMap<QString,QString>);
};

//end of namespace
}}
using namespace Chipper::GUI;
