// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif

class QSpinBox;

namespace Chipper { namespace GUI {

///Configuration dialog for general program preferences.
class CHIPPERGUI_EXPORT PreferenceDialog:public QDialog
{
    Q_OBJECT
public:
    explicit PreferenceDialog(QWidget*parent=nullptr);
private slots:
    void saveData();
private:
    QSpinBox*msync;
};

//end of namespace
}}
using namespace Chipper::GUI;
