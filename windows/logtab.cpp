// Chipper KiCAD symbol/footprint/3Dmodel generator
// log file tab
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "logtab.h"

#include <QBoxLayout>
#include <QCheckBox>
#include <QDateTime>
#include <QDebug>
#include <QLabel>
#include <QLineEdit>
#include <QPointer>
#include <QScrollBar>
#include <QSettings>
#include <QSpinBox>
#include <QTextBrowser>
#include <QTimer>

#include <QtGlobal>

#include "chipper.h"


namespace Chipper {namespace GUI{


struct MessageLine{
    MessageLine()=default;
    MessageLine(const MessageLine&)=default;
    MessageLine(MessageLine&&)=default;
    MessageLine& operator=(const MessageLine&)=default;
    MessageLine& operator=(MessageLine&&)=default;

    MessageLine(QtMsgType mt,const QMessageLogContext&ct,QString tx):msgtype(mt),text(tx),file(ct.file),function(ct.function),category(ct.category),line(ct.line){}

    QtMsgType msgtype=QtMsgType();
    QString text,file,function,category;
    int line=0;
    QDateTime time=QDateTime::currentDateTime();

    QString toHtml(bool debug)const;
};

static QPointer<LogTab> logtabinst;
static QList<MessageLine>msglines;
static QtMessageHandler prevhandler=nullptr;
static int maxlines=1000;

LogTab::LogTab(QWidget* parent)
:QWidget(parent)
{
    QVBoxLayout*vl;
    setLayout(vl=new QVBoxLayout);
    //main browser
    vl->addWidget(mbrws=new QTextBrowser,1);
    //additional controls: browser settings
    QHBoxLayout*hl;
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(new QLabel(tr("Max Lines:")),0);
    hl->addWidget(mmaxlines=new QSpinBox,0);
    mmaxlines->setRange(1,9999);
    mmaxlines->setValue(QSettings().value("gui/log/maxlines",1000).toInt());
    connect(mmaxlines,qOverload<int>(&QSpinBox::valueChanged),this,[this](){maxlines=mmaxlines->value();QSettings().setValue("gui/log/maxlines",maxlines);});
    hl->addSpacing(20);
    hl->addWidget(mscrlend=new QCheckBox("Always scroll to End"),0);
    mscrlend->setChecked(true);
    hl->addSpacing(20);
    hl->addWidget(mdebug=new QCheckBox("Debug Mode"),0);
    mdebug->setChecked(isDebugMode());
    connect(mdebug,&QCheckBox::stateChanged,this,[this]{setDebugMode(mdebug->isChecked());updateLog();});
    hl->addStretch(1);
    //additional controls: log file settings
    vl->addLayout(hl=new QHBoxLayout);
    hl->addWidget(mdolog=new QCheckBox(tr("Log to File:")),0);
    hl->addWidget(mlogname=new QLineEdit,1);
    //initial update
    updateLog();
}

QString MessageLine::toHtml(bool debug)const
{
    QString color="black";
    const QString errorcolor="darkred";
    const QString warncolor="brown";
    const QString succcesscolor="darkgreen";
    if(msgtype==QtWarningMsg || msgtype==QtCriticalMsg || msgtype==QtFatalMsg)color=errorcolor;
    else if(msgtype==QtInfoMsg)color=succcesscolor;
    else if(msgtype==QtDebugMsg){
        if(text.contains("error",Qt::CaseInsensitive))color=errorcolor;
        else if(text.contains("warning",Qt::CaseInsensitive))color=warncolor;
    }
    QString special;
    if(debug){
        QStringList spl;
        if(!file.isEmpty())spl.append("file "+file);
        if(line>0)spl.append(QString("line %1").arg(line));
        if(!function.isEmpty())spl.append("function "+function);
        if(!category.isEmpty())spl.append("category "+category);
        special="["+spl.join("; ")+"] ";
    }
    return QString("<font color='%1'>%4: %2%3</font><br/>\n").arg(color).arg(special).arg(text).arg(time.toString("yyyy-MM-dd hh:mm:ss"));
}

void LogTab::updateLog()
{
    QString html="<html>\n";
    const bool debug=isDebugMode();
    for(const auto&line:msglines)
        html+=line.toHtml(debug);
    html+="<a id='end' name='end'/>";
    mbrws->setHtml(html);
    if(mscrlend->isChecked())
        QTimer::singleShot(1,[this]{
            auto*sb=mbrws->verticalScrollBar();
            if(sb)sb->setValue(sb->maximum());
        });
}

static void guiLogTabMessageHandler(QtMsgType mtype, const QMessageLogContext &mctx, const QString &text)
{
    if(prevhandler)prevhandler(mtype,mctx,text);
    msglines.append(MessageLine(mtype,mctx,text));
    if(msglines.size()>maxlines)msglines.mid(msglines.size()-maxlines);
    if(logtabinst)logtabinst->updateLog();
}

bool LogTab::hasInstance()
{
    return logtabinst;
}

void LogTab::init()
{
    static bool loginited=false;
    if(!loginited){
        qDebug()<<"Initializing GUI Log Handler.";
        prevhandler=qInstallMessageHandler(&guiLogTabMessageHandler);
        loginited=true;
    }
}

LogTab * LogTab::instance(QWidget* parent)
{
    init();
    if(logtabinst.isNull())logtabinst=new LogTab(parent);
    return logtabinst;
}

//end of namespace
}}


#include "moc_logtab.cpp"
