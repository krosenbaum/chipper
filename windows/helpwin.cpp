// Chipper KiCAD symbol/footprint/3Dmodel generator
// help window
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QDebug>
#include <QDir>
#include <QMenu>
#include <QPushButton>
#include <QSettings>
#include <QSplitter>
#include <QStandardItemModel>
#include <QTextBrowser>
#include <QTreeView>

#include <functional>

#include "chipper.h"
#include "helpwin.h"





// ////////////////////////////////////
// Help Window

HelpWindow::HelpWindow(QString start, QWidget* parent)
    :QDialog(parent)
{
    setWindowTitle(tr("Chipper Help"));
    setAttribute(Qt::WA_DeleteOnClose,true);

    QVBoxLayout*vl;
    QHBoxLayout*hl;
    setLayout(vl=new QVBoxLayout);
    vl->addWidget(msp=new QSplitter);
    msp->setSizes(QList<int>()<<1<<10);
    msp->setStretchFactor(0,1);
    msp->setStretchFactor(1,10);
    msp->addWidget(mlist=new QTreeView);
    msp->addWidget(mtext=new QTextBrowser);
    mtext->setOpenExternalLinks(true);
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*p;
    hl->addWidget(p=new QPushButton(tr("&Close")));
    connect(p,&QPushButton::clicked,this,&HelpWindow::accept);

    mmodel=new QStandardItemModel(this);
    mlist->setModel(mmodel);
    mlist->setHeaderHidden(true);
    mlist->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(mlist,&QTreeView::clicked,this,&HelpWindow::changeTopic);
    auto cur=buildModel(mmodel);
    for(QModelIndex idx=cur;idx.isValid();idx=idx.parent())
        mlist->expand(idx);
    mlist->setCurrentIndex(cur);

    QSettings set;
    restoreGeometry(set.value("helpWinGeo").toByteArray());
    msp->restoreState(set.value("helpWinGeo2").toByteArray());

    setTopic(start);
}

HelpWindow::~HelpWindow()
{
    QSettings s;
    s.setValue("helpWinGeo",saveGeometry());
    s.setValue("helpWinGeo2",msp->saveState());
}


template<class State>
static inline void runTopicDir(
    QString dir,QString prefix,State state,
    std::function<void(QString,QString,QString,State)>addFile,
    std::function<State(QString,QString,QString,State)>addDir)
{
    QDir d(dir);
    QStringList ret;
    for(auto f:d.entryList(QDir::Files,QDir::Name)){
        if(f.endsWith(".md")||f.endsWith(".html"))
            addFile(dir,prefix,f,state);
    }
    for(auto f:d.entryList(QDir::Dirs|QDir::NoDotAndDotDot,QDir::Name)){
        runTopicDir(dir+"/"+f,prefix+f+"/",addDir(dir,prefix,f,state),addFile,addDir);
    }
}

void HelpWindow::buildMenu(QMenu*m,QObject*target,std::function<void(QString)>slotFunc)
{
    runTopicDir<QMenu*>(":/doc","",m,
        [&](QString d,QString p,QString f,QMenu*m){
            Q_UNUSED(d);
            m->addAction(formatTopic(f),target,std::bind(slotFunc,p+f));
        },
        [&](QString d,QString p,QString f,QMenu*m)->QMenu*{
            Q_UNUSED(d);Q_UNUSED(p);
            return m->addMenu(formatTopic(f));
        }
    );
}

QModelIndex HelpWindow::buildModel(QStandardItemModel*model,QString topic)
{
    if(model->columnCount()<1)model->insertColumn(0);
    QModelIndex ret;
    runTopicDir<QModelIndex>(":/doc","",QModelIndex(),
        [&](QString d,QString p,QString f,QModelIndex m){
            Q_UNUSED(d);
            const int r=model->rowCount(m);
            model->insertRows(r,1,m);
            auto idx=model->index(r,0,m);
            model->setData(idx,formatTopic(f));
            model->setData(idx,p+f,Qt::UserRole);
            if((p+f)==topic)ret=idx;
        },
        [&](QString d,QString p,QString f,QModelIndex m)->QModelIndex{
            Q_UNUSED(d);Q_UNUSED(p);
            const int r=model->rowCount(m);
            model->insertRows(r,1,m);
            auto idx=model->index(r,0,m);
            model->setData(idx,formatTopic(f));
            model->insertColumns(0,1,idx);
            return idx;
        }
    );
    return ret;
}

QStringList HelpWindow::topics()
{
    QStringList ret;
    runTopicDir<std::nullptr_t>(":/doc","",nullptr,
        [&](QString,QString p,QString f,std::nullptr_t){ret.append(p+f);},
        [&](QString,QString,QString,std::nullptr_t)->std::nullptr_t{return nullptr;}
    );
    return ret;
}

QString HelpWindow::defaultTopic()
{
    //try to find runtime then about, then simply first
    QStringList t=topics();
    for(const auto&s:t)if(s.toLower().startsWith("Runtime."))return s;
    for(const auto&s:t)if(s.toLower().startsWith("About."))return s;
    return t.value(0);
}

static inline QModelIndex findTopic(QStandardItemModel*model,QString topic,QModelIndex parent=QModelIndex())
{
    //go through sub-items of parent index
    for(int r=0;r<model->rowCount(parent);r++){
        //check current item
        auto idx=model->index(r,0,parent);
        if(model->data(idx,Qt::UserRole).toString()==topic){
            return idx;
        }
        //recursion into sub-items of current
        idx=findTopic(model,topic,idx);
        if(idx.isValid())return idx;
    }
    return QModelIndex();
}

void HelpWindow::setTopic(QString topic)
{
    QString fn=":/doc/"+topic;
    mtext->setSource(QUrl::fromLocalFile(fn));
    //correct list
    auto idx=mlist->currentIndex();
    if(idx.isValid() && mmodel->data(idx,Qt::UserRole).toString()==topic)return;
    idx=findTopic(mmodel,topic);
    mlist->setCurrentIndex(idx);
}

void HelpWindow::changeTopic()
{
    auto idx=mlist->currentIndex();
    if(!idx.isValid())return;
    setTopic(mmodel->data(idx,Qt::UserRole).toString());
}

QString HelpWindow::formatTopic(QString t)
{
    //strip extension
    int i=t.lastIndexOf('.');
    if(i>1)t=t.left(i);
    //insert spaces
    QString r;
    bool upper=false;
    for(QChar c:t){
        if(c.isUpper()&&!upper){
            upper=true;
            if(!r.isEmpty())r+=' ';
        }else upper=false;
        r+=c;
    }
    return r;
}

#include "moc_helpwin.cpp"
