// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QHeaderView>
#include <QListView>
#include <QPushButton>
#include <QSettings>
#include <QSplitter>
#include <QStandardItemModel>
#include <QTableView>

#include "envinspector.h"
#include "kicadenv.h"

// ////////////////////////////////////
// Environment

EnvironmentInspector::EnvironmentInspector(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("Environment Inspector"));
    //gui
    QVBoxLayout*vl;
    setLayout(vl=new QVBoxLayout);
    vl->addWidget(msp=new QSplitter);
    mmodel=new QStandardItemModel(this);
    auto*lst=new QListView;
    auto*tab=new QTableView;
    msp->addWidget(lst);
    msp->addWidget(tab);
    msp->restoreState(QSettings().value("envInspState2").toByteArray());
    lst->setModel(mmodel);
    lst->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tab->setModel(mmodel);
    tab->setEditTriggers(QAbstractItemView::NoEditTriggers);

    vl->addSpacing(10);
    QHBoxLayout*hl;
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*p;
    hl->addWidget(p=new QPushButton(tr("&Close")),0);
    connect(p,&QPushButton::clicked,this,&EnvironmentInspector::accept);

    restoreGeometry(QSettings().value("envInspState").toByteArray());

    //transform env to model
    const auto&env=KiCadEnvironment::instance();
    QStringList pl=env.envPathes();

    mmodel->insertColumn(0);
    mmodel->insertRows(0,pl.size());
    for(int r=0;r<pl.size();r++){
        QModelIndex idx=mmodel->index(r,0);
        mmodel->setData(idx,env.envName(pl[r]));
        mmodel->setData(idx,pl[r],Qt::ToolTipRole);
        addMap(idx,env.environment(pl[r]));
    }

    mmodel->setHorizontalHeaderLabels(QStringList()<<tr("Variable")<<tr("Value"));
    tab->verticalHeader()->hide();

    //init views
    QModelIndex presel=mmodel->index(0,0);
    lst->setCurrentIndex(presel);
    tab->setRootIndex(presel);
    tab->resizeColumnsToContents();
    connect(lst,&QListView::activated,tab,&QTableView::setRootIndex);
}

EnvironmentInspector::~EnvironmentInspector()
{
    QSettings s;
    s.setValue("envInspState2",msp->saveState());
    s.setValue("envInspState",saveGeometry());
}

void EnvironmentInspector::addMap(const QModelIndex&idx, QMap<QString, QString>map)
{
    QStringList keys=map.keys();
    std::sort(keys.begin(),keys.end());
    mmodel->insertColumns(0,2,idx);
    mmodel->insertRows(0,keys.size(),idx);
    int r=0;
    for(auto k:keys){
        mmodel->setData(mmodel->index(r,0,idx),k);
        mmodel->setData(mmodel->index(r,1,idx),map[k]);
        r++;
    }
}


#include "moc_envinspector.cpp"
