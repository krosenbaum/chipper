#sources
SOURCES += \
    $$PWD/mainwin.cpp \
    $$PWD/pathconfigdlg.cpp \
    $$PWD/preferencedlg.cpp \
    $$PWD/envinspector.cpp \
    $$PWD/helpwin.cpp \
    $$PWD/logtab.cpp \
    $$PWD/metricconv.cpp

HEADERS += \
    $$PWD/mainwin.h \
    $$PWD/pathconfigdlg.h \
    $$PWD/preferencedlg.h \
    $$PWD/envinspector.h \
    $$PWD/helpwin.h \
    $$PWD/logtab.h \
    $$PWD/metricconv.h

INCLUDEPATH += $$PWD
