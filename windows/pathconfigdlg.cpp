// Chipper KiCAD symbol/footprint/3Dmodel generator
// configuration dialogs
//
// (c) Konrad Rosenbaum, 2021-22
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QStandardPaths>

#include "chipper.h"
#include "pathconfigdlg.h"
#include "filelineedit.h"


// ////////////////////////////////////
// Path Config

PathConfigDialog::PathConfigDialog(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("Chipper Path Configuration"));
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    setLayout(vl=new QVBoxLayout);
    QFormLayout*fl;
    QPushButton*p;
    vl->addLayout(fl=new QFormLayout,1);
    fl->addRow(tr("Environment Prefix:"),menv=new QLineEdit(pathConfig(ChipperPath::EnvPrefix)));
    fl->addRow(tr("Session Env. Prefix:"),new QLabel(defaultPath(ChipperPath::EnvPrefix)));
    fl->addRow(tr("Root Path:"),mroot=new FileLineEdit(pathConfig(ChipperPath::Root),FileLineEdit::Dir));
    fl->addRow(tr("Session Root Path:"),new QLabel(defaultPath(ChipperPath::Root)));
    fl->addRow(tr("Symbol Path:"),msym=new FileLineEdit(pathConfig(ChipperPath::Symbol),FileLineEdit::Dir));
    fl->addRow(tr("Session Symbol Path:"),new QLabel(defaultPath(ChipperPath::Symbol)));
    fl->addRow(tr("Footprint Path:"),mfoot=new FileLineEdit(pathConfig(ChipperPath::Footprint),FileLineEdit::Dir));
    fl->addRow(tr("Session Footprint Path:"),new QLabel(defaultPath(ChipperPath::Footprint)));
    fl->addRow(tr("3D Model Path:"),m3dmod=new FileLineEdit(pathConfig(ChipperPath::Model3D),FileLineEdit::Dir));
    fl->addRow(tr("Session 3D Model Path:"),new QLabel(defaultPath(ChipperPath::Model3D)));
    fl->addRow(tr("Template Path:"),mtemp=new FileLineEdit(pathConfig(ChipperPath::Template),FileLineEdit::Dir));
    fl->addRow(tr("Session Template Path:"),new QLabel(defaultPath(ChipperPath::Template)));
    fl->addRow(new QLabel("<html><font color='red'><b>Note:</b></font> Settings will only become current after a restart."));
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(p=new QPushButton(tr("&OK")));
    connect(p,&QPushButton::clicked,this,&PathConfigDialog::accept);
    connect(p,&QPushButton::clicked,this,&PathConfigDialog::saveData);
    hl->addWidget(p=new QPushButton(tr("&Cancel")));
    connect(p,&QPushButton::clicked,this,&PathConfigDialog::reject);
}

void PathConfigDialog::saveData()
{
    setPathConfig(ChipperPath::EnvPrefix,menv->text());
    setPathConfig(ChipperPath::Root,mroot->value());
    setPathConfig(ChipperPath::Symbol,msym->value());
    setPathConfig(ChipperPath::Footprint,mfoot->value());
    setPathConfig(ChipperPath::Model3D,m3dmod->value());
    setPathConfig(ChipperPath::Template,mtemp->value());
}


#include "moc_pathconfigdlg.cpp"
