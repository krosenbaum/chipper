// Chipper KiCAD symbol/footprint/3Dmodel generator
// converting metric-imperial dialogs
//
// (c) Konrad Rosenbaum, 2022
// protected under the GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLabel>

#include "metricconv.h"

MetricConvertDialog::MetricConvertDialog(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("Metric Conversion"));
    setSizeGripEnabled(true);
    QHBoxLayout*hl;
    QVBoxLayout*vl;
    QGridLayout*gl;
    setLayout(vl=new QVBoxLayout);
    vl->addLayout(gl=new QGridLayout,1);
    QSpinBox*mil;
    QDoubleSpinBox*mm,*inch;
    gl->addWidget(new QLabel(tr("inch")),0,0);
    gl->addWidget(new QLabel(tr("mils")),0,1);
    gl->addWidget(new QLabel(tr("mm")),0,2);
    gl->addWidget(inch=new QDoubleSpinBox,1,0);
    inch->setRange(0,99.99);
    gl->addWidget(mil=new QSpinBox,1,1);
    mil->setRange(0,99999);
    gl->addWidget(mm=new QDoubleSpinBox,1,2);
    mm->setRange(0,9999.99);
    static bool updating=false;//synchronize updates to avoid endless loops
    connect(inch,qOverload<double>(&QDoubleSpinBox::valueChanged),this,[=](double val){
        if(updating)return;
        updating=true;
        mil->setValue(val*1000);
        mm->setValue(val*25.4);
        updating=false;
    });
    connect(mm,qOverload<double>(&QDoubleSpinBox::valueChanged),this,[=](double val){
        if(updating)return;
        updating=true;
        mil->setValue(val/25.4*1000);
        inch->setValue(val/25.4);
        updating=false;
    });
    connect(mil,qOverload<int>(&QSpinBox::valueChanged),this,[=](int val){
        if(updating)return;
        updating=true;
        mm->setValue(val*25.4/1000.);
        inch->setValue(val/1000.);
        updating=false;
    });
    static QList<int>prevals={10,50,100,125,200,250,500,750,800,1000};
    for(int i=0;i<prevals.size();i++){
        gl->addWidget(new QLabel(QString::number(prevals[i]/1000.,'f',3)),2+i,0);
        gl->addWidget(new QLabel(QString::number(prevals[i])),2+i,1);
        gl->addWidget(new QLabel(QString::number(prevals[i]*0.0254,'f',2)),2+i,2);
    }
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("&Close")));
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);
}
