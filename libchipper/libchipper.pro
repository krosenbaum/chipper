#main defs
TEMPLATE = lib
TARGET = chipper

include(../basics.pri)
DEFINES += CHIPPER_EXPORT=Q_DECL_EXPORT

#Qt basics
QT -= gui
QT += xml

#Taurus Libs: ELAM, DOMext, ...
include(../Taurus/chester.pri)
include(../Taurus/elam.pri)
include(../Taurus/domext.pri)

#internal directories
include(../chip/chip.pri)
include(../generate/generate.pri)
include(../src/src.pri)
include(../calc/calc.pri)
include(../template/template.pri)

#make sure we re-compile dependend sources
DEPENDPATH += $$INCLUDEPATH

#translations
CONFIG += lrelease
LRELEASE_DIR = $$DESTDIR
TRANSLATIONS += \
    chipper_lib_de.ts
