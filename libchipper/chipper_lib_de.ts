<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ChipFileTarget</name>
    <message>
        <location filename="../chip/chipfile.cpp" line="156"/>
        <source>Symbol File</source>
        <translation>Symbol-Datei</translation>
    </message>
    <message>
        <location filename="../chip/chipfile.cpp" line="157"/>
        <source>Footprint File</source>
        <translation>Fußabdruck-Datei</translation>
    </message>
    <message>
        <location filename="../chip/chipfile.cpp" line="158"/>
        <source>3D Model File</source>
        <translation>3D-Modell-Datei</translation>
    </message>
</context>
<context>
    <name>ChipPin</name>
    <message>
        <location filename="../chip/chipdetail.cpp" line="574"/>
        <source>Ground, Passive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="575"/>
        <source>Ground, Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="576"/>
        <source>Ground, Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="577"/>
        <source>Power, Passive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="578"/>
        <source>Power, Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="579"/>
        <source>Power, Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="580"/>
        <source>Signal, Bidirectional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="581"/>
        <source>Signal, Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="582"/>
        <source>Signal, Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="583"/>
        <source>Passive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="584"/>
        <source>Open Collector Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="585"/>
        <source>Open Emitter Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="586"/>
        <source>Tri-State Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="587"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="588"/>
        <source>Free Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="589"/>
        <source>Unspecified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="590"/>
        <source>Unknown Function %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="710"/>
        <source>Simple Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="711"/>
        <source>Inverted Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="712"/>
        <source>Input Active Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="713"/>
        <source>Output Active Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="714"/>
        <source>Clock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="715"/>
        <source>Clock on Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="716"/>
        <source>Clock on Edge High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="717"/>
        <source>Inverted Clock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="718"/>
        <source>Non-Logic, Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdetail.cpp" line="720"/>
        <source>Unknown Line Type %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChipVariant</name>
    <message>
        <location filename="../chip/chipvariant.cpp" line="179"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipvariant.cpp" line="180"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipvariant.cpp" line="181"/>
        <source>Missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipvariant.cpp" line="182"/>
        <source>Stub</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper</name>
    <message>
        <location filename="../src/chipper.cpp" line="31"/>
        <source>Chipper version: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chipper.cpp" line="32"/>
        <source>
Compiled for Qt %1 (%6)
Running with Qt %2
Running on: %3; kernel %4; CPU %5
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chipper.cpp" line="40"/>
        <source>
ELAM version: %1
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::Chip::ChipData</name>
    <message>
        <location filename="../chip/chipdata.cpp" line="59"/>
        <source>Internal error, invalid copy ID %1 while loading %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdata.cpp" line="66"/>
        <source>Unable to open %1 for reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdata.cpp" line="79"/>
        <source>XML parser error in %1: %2, line %3, column %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chip/chipdata.cpp" line="85"/>
        <source>Not a Chip file: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::Chip::ChipPool</name>
    <message>
        <location filename="../chip/chippool.cpp" line="89"/>
        <source>Chip file %1 does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::Generate::FileGen</name>
    <message>
        <location filename="../generate/filegen.cpp" line="119"/>
        <source>Invalid template reference &apos;%1&apos; generator will fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="126"/>
        <source>File generator chip &apos;%1&apos;, file &apos;%2&apos; unable to load stages, using default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="200"/>
        <source>File generator chip &apos;%1&apos;, file &apos;%2&apos; unable to open script &apos;%3&apos;: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="214"/>
        <source>File generator chip &apos;%1&apos;, file &apos;%2&apos; XML error in script &apos;%3&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="226"/>
        <source>File generator chip &apos;%1&apos;, file &apos;%2&apos; - file &apos;%3&apos; is not a generator script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="247"/>
        <source>Warning: unknown tag %1 in generator script %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="298"/>
        <source>Error while loading template file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="308"/>
        <source>Unable to open output file &apos;%1&apos;: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="368"/>
        <source>Process exited with non-ideal state: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="372"/>
        <source>Process still running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="387"/>
        <source>Error copying file %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="396"/>
        <source>Error moving file %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="405"/>
        <source>Error removing file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generate/filegen.cpp" line="411"/>
        <source>Unknown file operation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chipper::Generate::Generator</name>
    <message>
        <location filename="../generate/generator.cpp" line="93"/>
        <source>Unable to generate NULL chip.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KiCadEnvironment</name>
    <message>
        <location filename="../src/kicadenv.cpp" line="99"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/kicadenv.cpp" line="111"/>
        <source>Chipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/kicadenv.cpp" line="121"/>
        <source>KiCAD 5: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/kicadenv.cpp" line="163"/>
        <source>KiCAD 6: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplateFile::FileType</name>
    <message>
        <location filename="../template/templatefile.cpp" line="43"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatefile.cpp" line="44"/>
        <source>Footprint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatefile.cpp" line="45"/>
        <source>3D Model</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatePins</name>
    <message>
        <location filename="../template/templatepins.cpp" line="63"/>
        <source>Row Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="64"/>
        <source>Grid Mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatePins::FillStep::AutoHide</name>
    <message>
        <location filename="../template/templatepins.cpp" line="132"/>
        <source>All Pins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="133"/>
        <source>Same Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="134"/>
        <source>Same Electrical Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="135"/>
        <source>No Hiding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatePins::FillStep::FilterBy</name>
    <message>
        <location filename="../template/templatepins.cpp" line="144"/>
        <source>Electrical Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="145"/>
        <source>Name, Wildcards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="146"/>
        <source>Name, RegExp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="147"/>
        <source>Pin Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatePins::FillStep::SortBy</name>
    <message>
        <location filename="../template/templatepins.cpp" line="156"/>
        <source>Variant Table Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="157"/>
        <source>Pin Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="158"/>
        <source>Pin &apos;Number&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatePins::FillStep::StepType</name>
    <message>
        <location filename="../template/templatepins.cpp" line="121"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="122"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../template/templatepins.cpp" line="123"/>
        <source>Take</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
