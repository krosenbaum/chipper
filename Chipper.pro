TEMPLATE = subdirs

SUBDIRS += libchipper libgui guimain cmdmain symview fpview vtool

libgui.depends += libchipper
symview.depends += libchipper libgui
fpview.depends += libchipper libgui
vtool.depends += libchipper

guimain.file = main/gui.pro
guimain.depends += libchipper libgui
cmdmain.file = main/cmd.pro
cmdmain.depends += libchipper libgui

CONFIG(tests, tests|notests){
    SUBDIRS += tests
    tests.depends += libchipper libgui
}
