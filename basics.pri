#in-tree build: hide object files
APWD=$$absolute_path($$clean_path($$_PRO_FILE_PWD_))
AOPWD=$$absolute_path($$clean_path($$OUT_PWD))
equals(APWD, $$AOPWD) {
    error("In Tree Build not supported!")
}

#make sure binaries are in "bin" relative to top build dir
REL = $$relative_path( $$PWD , $$_PRO_FILE_PWD_ )
DESTDIR = $$OUT_PWD/$$REL/bin
LIBS += -L$$OUT_PWD/$$REL/bin

#Version number (also used inside the app)
VERSION = 0.1

#Qt basics
CONFIG += hide_symbols separate_debug_info c++17 link_prl create_prl

#global preproc definitions
DEFINES += VERSION=$$VERSION

#some linker magic, so libs are found and ASLR is active
linux {
  QMAKE_CFLAGS += -fPIE
  QMAKE_CXXFLAGS += -fPIE
  QMAKE_LFLAGS += -pie
  #make sure we find our libs
  QMAKE_LFLAGS += -Wl,-rpath,\'\$$ORIGIN\'
}
QMAKE_CFLAGS += -fstack-protector-all -Wstack-protector
QMAKE_CXXFLAGS += -fstack-protector-all -Wstack-protector

#Windows ASLR
win32 {
  QMAKE_LFLAGS +=  -Wl,--nxcompat -Wl,--dynamicbase
  LIBS += -lssp
}
