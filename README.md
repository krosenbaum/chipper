README for Chipper
===================

This is the Chipper symbol and footprint generator application for KiCad.
see doc/About.md for details

License: GPL-v3+, see doc/License.html

Building it: doc/Advanced/Build.md
Development: doc/Advanced/Developer.md

See the doc directory for general documentation.
