// Chipper KiCAD symbol/footprint/3Dmodel generator
// template viewer widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "templateviewer.h"
#include "templatepool.h"
#include "griddelegate.h"

#include <QDebug>
#include <QFormLayout>
#include <QLabel>
#include <QSplitter>
#include <QStandardItemModel>
#include <QStringList>
#include <QTableView>
#include <QTextBrowser>
#include <QTreeView>
#include <QUrl>

inline void TemplateViewer::setGeneratorModel(QStandardItemModel*mod)
{
    mod->insertRows(0,3);
    mod->insertColumns(0,1);
    QModelIndex idx;
    mod->setData(idx=mod->index(0,0),tr("Symbols"));
    QStringList keys=mdata.symbolIds();
    mod->insertRows(0,keys.size(),idx);
    mod->insertColumns(0,6,idx);
    auto defval=[](const QString&s)->QString{return s.isEmpty()?"default":s;};
    for(int i=0;i<keys.size();i++){
        auto sym=mdata.symbol(keys[i]);
        mod->setData(mod->index(i,0,idx),sym.id());
        mod->setData(mod->index(i,1,idx),sym.name());
        mod->setData(mod->index(i,2,idx),sym.fileName());
        mod->setData(mod->index(i,2,idx),sym.absoluteFileName(),Qt::ToolTipRole);
        mod->setData(mod->index(i,3,idx),defval(sym.generator()));
        mod->setData(mod->index(i,4,idx),sym.pinSpecId());
        mod->setData(mod->index(i,5,idx),sym.pinPrefillId());
    }
    mod->setData(idx=mod->index(1,0),tr("Footprints"));
    keys=mdata.footprintIds();
    mod->insertRows(0,keys.size(),idx);
    mod->insertColumns(0,6,idx);
    for(int i=0;i<keys.size();i++){
        auto sym=mdata.footprint(keys[i]);
        mod->setData(mod->index(i,0,idx),sym.id());
        mod->setData(mod->index(i,1,idx),sym.name());
        mod->setData(mod->index(i,2,idx),sym.fileName());
        mod->setData(mod->index(i,2,idx),sym.absoluteFileName(),Qt::ToolTipRole);
        mod->setData(mod->index(i,3,idx),defval(sym.generator()));
        mod->setData(mod->index(i,4,idx),sym.pinSpecId());
        mod->setData(mod->index(i,5,idx),sym.pinPrefillId());
    }
    mod->setData(idx=mod->index(2,0),tr("3D Models"));
    keys=mdata.modelIds();
    mod->insertRows(0,keys.size(),idx);
    mod->insertColumns(0,6,idx);
    for(int i=0;i<keys.size();i++){
        auto sym=mdata.model(keys[i]);
        mod->setData(mod->index(i,0,idx),sym.id());
        mod->setData(mod->index(i,1,idx),sym.name());
        mod->setData(mod->index(i,2,idx),sym.fileName());
        mod->setData(mod->index(i,2,idx),sym.absoluteFileName(),Qt::ToolTipRole);
        mod->setData(mod->index(i,3,idx),defval(sym.generator()));
        mod->setData(mod->index(i,4,idx),sym.pinSpecId());
        mod->setData(mod->index(i,5,idx),sym.pinPrefillId());
    }
    mod->setHorizontalHeaderLabels(QStringList()<<tr("ID")<<tr("Name")<<tr("Input Filename")<<tr("Script Name")<<tr("Pin Definition")<<tr("Pin Prefill"));
}

inline void TemplateViewer::setEnumsModel(QStandardItemModel*mod)
{
    mod->insertColumns(0,2);
    mod->setHorizontalHeaderLabels(QStringList()<<tr("Value")<<tr("Display"));
    const auto vars=mdata.variables();
    mod->insertRows(0,vars.numEnums());
    const QStringList k=vars.enumNames();
    for(int i=0;i<k.size();i++){
        QModelIndex idx=mod->index(i,0);
        mod->setData(idx,k[i]);
        auto vals=vars.enumsByName(k[i]);
        mod->insertColumns(0,2,idx);
        mod->insertRows(0,vals.size(),idx);
        for(int j=0;j<vals.size();j++){
            mod->setData(mod->index(j,0,idx),QString::number(vals[j].value(),'f',2));
            mod->setData(mod->index(j,1,idx),vals[j].displayString());
        }
    }
}

inline void TemplateViewer::setVariablesModel(QStandardItemModel*mod)
{
    mod->insertColumns(0,9);
    mod->setHorizontalHeaderLabels(QStringList()
        <<tr("Visibility")<<tr("ID")<<tr("Type")<<tr("Default")
        <<tr("Minimum")<<tr("Maximum")<<tr("Step")<<tr("Enum Ref.")
        <<tr("Description"));
    const auto vars=mdata.variables();
    mod->insertRows(0,vars.numVariables());
    for(int i=0;i<vars.numVariables();i++){
        const auto var=vars.variable(i);
        mod->setData(mod->index(i,0),var.isHidden()?tr("Hidden"):tr("Visible"));
        mod->setData(mod->index(i,1),var.id());
        mod->setData(mod->index(i,2),TemplateVariables::varTypeToString(var.variableType()));
        mod->setData(mod->index(i,3),var.defaultValue());
        mod->setData(mod->index(i,4),var.minimumValue());
        mod->setData(mod->index(i,5),var.maximumValue());
        mod->setData(mod->index(i,6),var.stepValue());
        mod->setData(mod->index(i,7),var.enumReference());
        mod->setData(mod->index(i,8),var.description());
    }
}

inline void TemplateViewer::setPinRowsModel(QStandardItemModel*mod)
{
    mod->insertColumns(0,5);
    mod->setHorizontalHeaderLabels(QStringList()
        <<tr("Def. ID\n ⇒ Row ID\n ⇒ Fill Algo ID\n ⇒⇒ Step Type")
        <<tr("Mode\n \n \nBy")
        <<tr("Formula\nMap Hint\n \nInto")
        <<tr("\n \n \nAuto-Hide")
        <<tr("Definition Name\nRow Hint\nAlgo. Name\nParameters")
    );
    for(auto defid:mdata.pinDefinitionIds()){
        //main data
        const auto pins=mdata.pins(defid);
        const int pr=mod->rowCount();
        mod->insertRows(pr,1);
        const auto pidx=mod->index(pr,0);
        mod->setData(pidx,pins.definitionId());
        mod->setData(mod->index(pr,1),pins.modeString());
        mod->setData(mod->index(pr,2),pins.repeatFormula());
        mod->setData(mod->index(pr,4),pins.definitionName());
        //rows
        const auto rnames=pins.rowNames();
        mod->insertRows(0,rnames.size(),pidx);
        mod->insertColumns(0,mod->columnCount(),pidx);
        for(int i=0;i<rnames.size();i++){
            const auto row=pins.rowDefinition(rnames[i]);
            mod->setData(mod->index(i,0,pidx),tr("Row: %1").arg(row.id()));
            mod->setData(mod->index(i,2,pidx),row.mapHints().join(", "));
            mod->setData(mod->index(i,4,pidx),row.text());
        }
        //fill algos
        const auto fids=pins.fillAlgoIds();
        for(const auto &fid:fids){
            const int r=mod->rowCount(pidx);
            mod->insertRows(r,1,pidx);
            const auto aidx=mod->index(r,0,pidx);
            mod->setData(aidx,tr("Fill Algo: %1").arg(fid));
            const auto fa=pins.fillAlgo(fid);
            mod->setData(mod->index(r,4,pidx),fa.name());
            //algo steps
            mod->insertRows(0,fa.size(),aidx);
            mod->insertColumns(0,mod->columnCount(),aidx);
            for(int i=0;i<fa.size();i++){
                mod->setData(mod->index(i,0,aidx),fa[i].stepTypeString());
                if(fa[i].stepType()==TemplatePins::FillStep::StepType::Filter)
                    mod->setData(mod->index(i,1,aidx),fa[i].filterByString());
                else if(fa[i].stepType()==TemplatePins::FillStep::StepType::Filter)
                    mod->setData(mod->index(i,1,aidx),fa[i].sortByString());
                mod->setData(mod->index(i,2,aidx),fa[i].intoRow());
                mod->setData(mod->index(i,3,aidx),fa[i].autoHideString());
                mod->setData(mod->index(i,4,aidx),fa[i].nameFilter());
            }
        }
    }
}

TemplateViewer::TemplateViewer(QString path, QWidget* parent)
:QTabWidget(parent),mdata(TemplatePool::instance().byPath(path))
{
    setWindowTitle(tr("Template: %1").arg(mdata.title()));
    setTabPosition(West);

    QWidget *w=new QWidget;
    addTab(w,tr("Main Data"));
    QFormLayout*fl;
    w->setLayout(fl=new QFormLayout);
    fl->addRow(tr("Location:"),new QLabel(mdata.pathName()));
    fl->addRow(tr("Title:"),new QLabel(mdata.title()));
    fl->addRow(tr("Author:"),new QLabel(mdata.author()));
    fl->addRow(tr("Copyright:"),new QLabel(mdata.copyright()));
    fl->addRow(tr("License:"),new QLabel(mdata.license()));
    fl->addRow(tr("Description:"),new QLabel(mdata.description()));

    QTreeView*tv;
    QTableView*tv2;
    QStandardItemModel*mod;
    QSplitter*splt;
    QVBoxLayout*vl;
    GridDelegate*gdel;
    addTab(splt=new QSplitter,tr("Variables"));
    splt->setOrientation(Qt::Vertical);
    splt->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("Enums:")),0);
    vl->addWidget(tv=new QTreeView,1);
    tv->setItemDelegate(gdel=new GridDelegate(tv));
    gdel->setTopLevelFrame(false);
    tv->setSelectionMode(QAbstractItemView::NoSelection);
    tv->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tv->setModel(mod=new QStandardItemModel(this));
    setEnumsModel(mod);
    tv->expandAll();
    for(int c=0;c<mod->columnCount();c++)tv->resizeColumnToContents(c);
    splt->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("Variables:")),0);
    vl->addWidget(tv2=new QTableView,1);
    tv2->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tv2->setModel(mod=new QStandardItemModel(this));
    setVariablesModel(mod);
    for(int c=0;c<mod->columnCount();c++)tv2->resizeColumnToContents(c);


    addTab(w=new QWidget,tr("Pins"));
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(new QLabel(tr("Pin Row Definitions:")));
    vl->addWidget(tv=new QTreeView,1);
    tv->setItemDelegate(gdel=new GridDelegate(tv));
    gdel->setStartFrameLeft(true);
    tv->setSelectionMode(QAbstractItemView::NoSelection);
    tv->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tv->setModel(mod=new QStandardItemModel(this));
    setPinRowsModel(mod);
    tv->expandAll();
    for(int c=0;c<mod->columnCount();c++)tv->resizeColumnToContents(c);


    addTab(tv=new QTreeView,tr("Generators"));
    tv->setItemDelegate(gdel=new GridDelegate(tv));
    gdel->setTopLevelFrame(false);
    tv->setSelectionMode(QAbstractItemView::NoSelection);
    tv->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tv->setModel(mod=new QStandardItemModel(this));
    setGeneratorModel(mod);
    tv->expandAll();
    for(int c=0;c<mod->columnCount();c++)tv->resizeColumnToContents(c);

    QTextBrowser*tb;
    addTab(tb=new QTextBrowser,tr("ReadMe"));
    tb->setOpenExternalLinks(true);
    tb->setOpenLinks(true);
    tb->setSource(QUrl::fromLocalFile(mdata.absoluteReadmePath()));
}


#include "moc_templateviewer.cpp"
