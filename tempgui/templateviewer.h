// Chipper KiCAD symbol/footprint/3Dmodel generator
// template viewer widget
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QTabWidget>

#include "templates.h"

#ifndef CHIPPERGUI_EXPORT
#define CHIPPERGUI_EXPORT Q_DECL_IMPORT
#endif


class QStandardItemModel;

namespace Chipper { namespace GUI {

///Viewer tab for templates.
class CHIPPERGUI_EXPORT TemplateViewer:public QTabWidget
{
    Q_OBJECT
    TemplateData mdata;
    //helpers for constructor
    void setGeneratorModel(QStandardItemModel*);
    void setEnumsModel(QStandardItemModel*);
    void setVariablesModel(QStandardItemModel*);
    void setPinRowsModel(QStandardItemModel*);
public:
    ///create viewer from template path (as requested from pool)
    explicit TemplateViewer(QString path,QWidget*parent=nullptr);

    ///returns the title of this template (may be used by chip tab to find corresponding template tab)
    QString title()const{return mdata.title();}
    ///returns the path name of this template (used to not open two viewers of the same template)
    QString path()const{return mdata.pathName();}
};


//end of namespace
}}
using namespace Chipper::GUI;
